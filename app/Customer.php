<?php



namespace App;


use App\User;
use App\Payment;
use Illuminate\Database\Eloquent\Model;



class Customer extends Model

{

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
}

