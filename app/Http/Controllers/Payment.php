<?php

namespace App\Http\Controllers;

use App\User;
use App\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Payment extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $wallets = Wallet::all()->sortByDesc('created_at');
        return view('customers.wallet_histories', compact('wallets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail(decrypt($id));
        return view('user.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->balance = $request->amount;
        if(strlen($request->password) > 0){
            $user->password = Hash::make($request->password);
        }
        if($user->save()){
            flash(__('User has been updated successfully'))->success();
            return back();
        }

        flash(__('Something went wrong'))->error();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(Wallet::destroy($id)){
            flash(__('Payment history been deleted successfully'))->success();
            return redirect()->route('payments.index');
        }

        flash(__('Something went wrong'))->error();
        return back();
    }
    public function approved(Request $request)
    {

        $wallet = Wallet::findOrFail($request->id);
        $wallet->status = $request->status;
        $user = User::findOrFail($wallet->user_id);
        if($wallet->status == 1){
            $user->balance = $user->balance + $wallet->amount;
            $user->save();
        }else{
            $user->balance = $user->balance - $wallet->amount;
            $user->save();
        }

        if($wallet->save()){
            return 1;
        }
        return 0;
    }
    public function profile_modal(Request $request)
    {
        $user = User::findOrFail($request->id);
        return view('user.profile_modal', compact('user'));
    }
}
