<?php

namespace App\Http\Controllers;
use Session;
use Auth;
use App\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\PaypalController;
use App\Http\Controllers\StripePaymentController;
use smasif\ShurjopayLaravelPackage\ShurjopayService;
use App\Http\Controllers\PublicSslCommerzPaymentController;
use App\Http\Controllers\InstamojoController;


class WalletController extends Controller
{
    public function index()
    {
        $wallets = Wallet::where('user_id', Auth::user()->id)->orderBy('updated_at', 'desc')->paginate(9);
        return view('frontend.wallet', compact('wallets'));
    }

    public function recharge(Request $request)
    {
        $data['amount'] = $request->amount;
        $data['payment_method'] = $request->payment_option;
        $data['number'] = $request->number;
        $data['TnxId'] = $request->TnxId;

        // dd($data);

        $request->session()->put('payment_type', 'wallet_payment');
        $request->session()->put('payment_data', $data);

        if($request->payment_option == 'paypal'){
            $paypal = new PaypalController;
            return $paypal->getCheckout();
        }
        elseif ($request->payment_option == 'stripe') {
            $stripe = new StripePaymentController;
            return $stripe->stripe();
        }
        elseif ($request->payment_option == 'sslcommerz') {
            $sslcommerz = new PublicSslCommerzPaymentController;
            return $sslcommerz->index($request);
        }
        elseif ($request->payment_option == 'instamojo') {
            $instamojo = new InstamojoController;
            return $instamojo->pay($request);
        }
        elseif ($request->payment_option == 'razorpay') {
            $razorpay = new RazorpayController;
            return $razorpay->payWithRazorpay($request);
        }
        elseif ($request->payment_option == 'paystack') {
            $paystack = new PaystackController;
            return $paystack->redirectToGateway($request);
        }
        elseif ($request->payment_option == 'voguepay') {
            $voguepay = new VoguePayController;
            return $voguepay->customer_showForm();
        }
    }
    public function wallet_recharge(Request $request)
    {
        $shurjopay_service = new ShurjopayService();
        $tx_id = $shurjopay_service->generateTxId();

        $success_route = route('shurjopay.wallet');
        $data['amount'] = $request->amount;
        $data['payment_method'] = $request->payment_option;
        $data['TnxId'] = $request->$tx_id;

        // $user->balance = $user->balance + $data['amount'];
        // $user->save();
        $shurjopay_service->sendPayment($data['amount'],$success_route);
        if($request->sp_code=='000'){
            $user = Auth::user();
            $wallet = new Wallet;
            $wallet->user_id = $user->id;
            $wallet->amount = $data['amount'];
            $wallet->payment_method = $data['payment_method'];
            $wallet->number = null;
            $wallet->TnxId = $data['TnxId'];
            Session::forget('payment_data');
            Session::forget('payment_type');
            $wallet->save();
            flash(__('Payment completed'))->success();
        }elseif($request->sp_code=='001'){
            flash(__('Payment faield'))->warning();
        }

    }
    public function wallet_payment_done($payment_data, $payment_details){
        $user = Auth::user();
        $user->balance = $user->balance + $payment_data['amount'];
        $user->save();

        $wallet = new Wallet;
        $wallet->user_id = $user->id;
        $wallet->amount = $payment_data['amount'];
        $wallet->payment_method = $payment_data['payment_method'];
        $wallet->payment_details = $payment_details;
        $wallet->save();

        Session::forget('payment_data');
        Session::forget('payment_type');

        flash(__('Payment completed'))->success();
        return redirect()->route('wallet.index');
    }
    public function confirm(Request $request){
        $user = Auth::user();
        $data['amount'] = $request->amount;
        $data['payment_method'] = $request->payment_option;
        $data['number'] = $request->number;
        $data['TnxId'] = $request->TnxId;

        // $user->balance = $user->balance + $data['amount'];
        // $user->save();

        $wallet = new Wallet;
        $wallet->user_id = $user->id;
        $wallet->amount = $data['amount'];
        $wallet->payment_method = $data['payment_method'];
        // $wallet->payment_details = $payment_details;
        $wallet->number = $data['number'];
        $wallet->TnxId = $data['TnxId'];
        $wallet->save();

        Session::forget('payment_data');
        Session::forget('payment_type');

        flash(__('Payment Pending completed'))->success();
        return redirect()->route('wallet.index');
    }
}
