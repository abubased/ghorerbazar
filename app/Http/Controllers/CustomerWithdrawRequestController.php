<?php

namespace App\Http\Controllers;

use App\User;
use App\Seller;
use Illuminate\Http\Request;
use App\CustomerWithdrawRequest;
use Illuminate\Support\Facades\Auth;

class CustomerWithdrawRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer_withdraw_requests = CustomerWithdrawRequest::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(9);
        return view('frontend.customer.customer_withdraw_requests.index', compact('customer_withdraw_requests'));
    }

    public function request_index()
    {
        $customer_withdraw_requests = CustomerWithdrawRequest::orderBy('created_at', 'desc')->paginate(9);
        return view('customer_withdraw_requests.index', compact('customer_withdraw_requests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer_withdraw_request = new CustomerWithdrawRequest;
        $customer_withdraw_request->user_id = Auth::user()->id;
        $customer_withdraw_request->amount = $request->amount;
        $customer_withdraw_request->message = $request->message;
        $customer_withdraw_request->status = '0';
        $customer_withdraw_request->viewed = '0';
        if ($customer_withdraw_request->save()) {
            flash(__('Request has been sent successfully'))->success();
            return redirect()->route('customer_withdraw_requests.index');
        }
        else{
            flash(__('Something went wrong'))->error();
            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function message_modal(Request $request)
    {
        $customer_withdraw_request = CustomerWithdrawRequest::findOrFail($request->id);
        if (Auth::user()->user_type == 'customer') {
            return view('frontend.partials.withdraw_message_modal', compact('seller_withdraw_request'));
        }
        elseif (Auth::user()->user_type == 'admin' || Auth::user()->user_type == 'staff') {
            return view('seller_withdraw_requests.withdraw_message_modal', compact('seller_withdraw_request'));
        }
    }

    public function approved(Request $request)
    {

        $customer_withdraw_request = CustomerWithdrawRequest::findOrFail($request->id);
        $customer_withdraw_request->status = $request->status;
        $user = User::findOrFail($customer_withdraw_request->user_id);
        if($customer_withdraw_request->status == 1){
            $user->balance = $user->balance - $customer_withdraw_request->amount;
            $user->save();
        }else{
            $user->balance = $user->balance + $customer_withdraw_request->amount;
            $user->save();
        }

        if($customer_withdraw_request->save()){
            return 1;
        }
        return 0;
    }
}
