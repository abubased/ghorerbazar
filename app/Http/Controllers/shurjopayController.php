<?php

namespace App\Http\Controllers;
use Session;
use App\Order;
use App\Wallet;
use App\BusinessSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShurjopayController extends Controller
{
    public function wallet(Request $request)
    {
        if($request->sp_code=='000'){
            $user = Auth::user();
            $wallet = new Wallet;
            $wallet->user_id = $user->id;
            $wallet->amount = $request['amount'];
            $wallet->payment_method = $request['sp_payment_option'];
            $wallet->number = null;
            $wallet->TnxId = $request['tx_id'];
            Session::forget('payment_data');
            Session::forget('payment_type');
            $wallet->save();
            flash(__('Payment completed'))->success();
        }elseif($request->sp_code=='001'){
            flash(__('Payment Failed'))->warning();
        }
        return redirect()->route('wallet.index');
    }
    public function checkOut(Request $request)
    {
        if($request->sp_code=='000'){
            $order_id=$request->session()->get('order_id');
            $order = Order::findOrFail($order_id);
            $order->payment_status = 'paid';
            $order->payment_details = $request['sp_payment_option'];;
            $order->save();
            if (BusinessSetting::where('type', 'category_wise_commission')->first()->value != 1) {
                $commission_percentage = BusinessSetting::where('type', 'vendor_commission')->first()->value;
                foreach ($order->orderDetails as $key => $orderDetail) {
                    $orderDetail->payment_status = 'paid';
                    $orderDetail->save();
                    if($orderDetail->product->user->user_type == 'seller'){
                        $seller = $orderDetail->product->user->seller;
                        $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                        $seller->save();
                    }
                }
            }
            else{
                foreach ($order->orderDetails as $key => $orderDetail) {
                    $orderDetail->payment_status = 'paid';
                    $orderDetail->save();
                    if($orderDetail->product->user->user_type == 'seller'){
                        $commission_percentage = $orderDetail->product->category->commision_rate;
                        $seller = $orderDetail->product->user->seller;
                        $seller->admin_to_pay = $seller->admin_to_pay + ($orderDetail->price*(100-$commission_percentage))/100;
                        $seller->save();
                    }
                }
            }
            Session::put('cart', collect([]));
            Session::forget('order_id');
            Session::forget('payment_type');
            Session::forget('delivery_info');
            Session::forget('coupon_id');
            Session::forget('coupon_discount');
            flash(__('Payment completed'))->success();
            // return redirect()->route('home');
        }
        elseif($request->sp_code=='001'){
            flash(__('Payment Failed'))->warning();
        }
        return redirect()->route('home');
    }
}
