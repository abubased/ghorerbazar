<?php

namespace App\Http\Controllers;
use App\User;
use App\Topup;
use App\Wallet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class topupController extends Controller
{
    public function index()
    {
        $wallets = Wallet::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc');
        $topups = Topup::where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(9);
        return view('frontend.topup', compact('wallets','topups'));
    }

    public function confirm(Request $request){
        $user = Auth::user();
        $data['amount'] = $request->amount;
        // $data['payment_method'] = $request->payment_option;
        $data['number'] = $request->number;
        // $data['TnxId'] = $request->TnxId;

        // $user->balance = $user->balance + $data['amount'];
        // $user->save();

        $topup = new Topup;
        $topup->user_id = $user->id;
        $topup->amount = $data['amount'];
        // $topup->payment_method = $data['payment_method'];
        // $topup->payment_details = $payment_details;
        $topup->number = $data['number'];
        // $topup->TnxId = $data['TnxId'];
        $topup->save();

        Session::forget('payment_data');
        Session::forget('payment_type');

        flash(__('Topup request send successfully'))->success();
        return redirect()->route('topup.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topupHistory()
    {
        $topups = Topup::all()->sortByDesc('created_at');
        return view('customers.topuphistories', compact('topups'));
    }
    public function approved(Request $request)
    {
        // dd($request->status);
        $topup = Topup::findOrFail($request->id);
        $topup->status = $request->status;
        $user = User::findOrFail($topup->user_id);
        if($topup->status == 1){
            $user->balance = $user->balance - $topup->amount;
            $user->save();
        }else{
            $user->balance = $user->balance + $topup->amount;
            $user->save();
        }

        if($topup->save()){
            return 1;
        }
        return 0;
    }
}
