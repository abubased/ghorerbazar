<div class="panel">
    <div class="panel-body">
        <div class="">

            <!-- Simple profile -->
            <div class="text-center">
                <div class="pad-ver">
                    <img src="{{ asset($user->avatar_original) }}" class="img-lg img-circle" alt="Profile Picture">
                </div>
                <h4 class="text-lg text-overflow mar-no">{{ $user->name }}</h4>
            </div>
            <hr>

            <!-- Profile Details -->
            <p class="pad-ver text-main text-sm text-uppercase text-bold">About {{ $user->name }}</p>
            <p><i class="demo-pli-map-marker-2 icon-lg icon-fw"></i>{{ $user->address }}</p>
            <p><i class="demo-pli-old-telephone icon-lg icon-fw"></i>{{ $user->phone }}</p>
            <br>

            <div class="table-responsive">
                <table class="table table-striped mar-no">
                    <tbody>
                    <tr>
                        <td>Total Products</td>
                        <td>{{ App\Product::where('user_id', $user->id)->get()->count() }}</td>
                    </tr>
                    <tr>
                        <td>Total Orders</td>
                        <td>{{ App\OrderDetail::where('order_id', $user->id)->get()->count() }}</td>
                    </tr>
                    <tr>
                        <td>Wallet Balance</td>
                        <td>{{ single_price($user->balance) }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
