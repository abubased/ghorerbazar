@extends('frontend.layouts.app')

@section('content')

<div id="page-content">
    <section class="slice-xs sct-color-2 border-bottom">
        <div class="container container-sm">
            <div class="row cols-delimited justify-content-center">
                <div class="col-3">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-shopping-cart"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">1. {{__('My Cart')}}
                            </h3>
                        </div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon c-gray-light mb-0">
                            <i class="la la-truck"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">2.
                                {{__('Shipping info')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="icon-block icon-block--style-1-v5 text-center">
                        <div class="block-icon mb-0 c-gray-light">
                            <i class="la la-truck"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">3.
                                {{__('Delivery info')}}</h3>
                        </div>
                    </div>
                </div>

                <div class="col-3">
                    <div class="icon-block icon-block--style-1-v5 text-center active">
                        <div class="block-icon mb-0">
                            <i class="la la-credit-card"></i>
                        </div>
                        <div class="block-content d-none d-md-block">
                            <h3 class="heading heading-sm strong-300 c-gray-light text-capitalize">3. {{__('Payment')}}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <section class="py-3 gry-bg">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-8">
                    <form action="{{ route('payment.checkout') }}" class="form-default" data-toggle="validator"
                        role="form" method="POST" id="checkout-form">
                        @csrf
                        <div class="card">
                            <div class="card-title px-4 py-3">
                                <h3 class="heading heading-5 strong-500">
                                    {{__('Select a payment option')}}
                                </h3>
                            </div>
                            <div class="card-body text-center">
                                {{-- <iframe class="payment_frame" src="https://www.smanager.xyz/@GhorerbazarCom" frameborder="0"></iframe> --}}
                                <div class="row">
                                    <div class="col-md-6 mx-auto">
                                        <div class="row">
                                            @if(\App\BusinessSetting::where('type', 'paypal_payment')->first()->value ==
                                            1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="Paypal">
                                                    <input type="radio" id="" name="payment_option" value="paypal"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/paypal.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'stripe_payment')->first()->value ==
                                            1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="Stripe">
                                                    <input type="radio" id="" name="payment_option" value="stripe"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/stripe.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type',
                                            'sslcommerz_payment')->first()->value == 1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="sslcommerz">
                                                    <input type="radio" id="" name="payment_option" value="sslcommerz"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/sslcommerz.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value
                                            == 1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="Instamojo">
                                                    <input type="radio" id="" name="payment_option" value="instamojo"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/instamojo.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'razorpay')->first()->value == 1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="Razorpay">
                                                    <input type="radio" id="" name="payment_option" value="razorpay"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/rozarpay.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'paystack')->first()->value == 1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="Paystack">
                                                    <input type="radio" id="" name="payment_option" value="paystack"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/paystack.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'voguepay')->first()->value == 1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="VoguePay">
                                                    <input type="radio" id="" name="payment_option" value="voguepay"
                                                        checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/vogue.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'cash_payment')->first()->value ==
                                            1)
                                            <div class="col-6">
                                                <label class="payment_option mb-4" data-toggle="tooltip"
                                                    data-title="Cash on Delivery">
                                                    <input type="radio" id="" name="payment_option"
                                                        value="cash_on_delivery" checked>
                                                    <span>
                                                        <img loading="lazy"
                                                            src="{{ asset('frontend/images/icons/cards/cod.png')}}"
                                                            class="img-fluid">
                                                    </span>
                                                </label>
                                            </div>
                                            @endif
                                            @if(\App\BusinessSetting::where('type', 'shurjopay')->first()->value == 1)
                                                <div class="col-6">
                                                    <label class="payment_option mb-4" data-toggle="tooltip" data-title="shurjopay">
                                                        <input type="hidden" id="" name="amount" value={{ $total }}>
                                                        <input type="radio" id="" name="payment_option" value="shurjopay" checked>
                                                        <span>
                                                            <img loading="lazy"
                                                                src="{{ asset('frontend/images/icons/cards/sslcommerz.png')}}"
                                                                class="img-fluid">
                                                        </span>
                                                    </label>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="or or--1 mt-2">
                                    <span>or</span>
                                </div>
                                    <div class="row">
                                        <div class="col-xxl-3 col-lg-4 col-md-5 mx-auto">
                                        <img class="img-fluid" src="{{ asset('frontend/images/icons/bkash.png')}}"><br>
                                        {{-- <button  type="button" onclick="show_wallet_modal()" class="btn btn-base-1">{{ __('Pay with Bkash/Rocket/Cash') }}</button>
                                        --}}
                                        <a href={{ url('users/spay') }} type="button"
                                            class="btn btn-base-1">{{ __('Pay with Bkash/Rocket/Cash') }}</a>
                                    </div>
                                </div>
                                @if (Auth::check() && \App\BusinessSetting::where('type',
                                'wallet_system')->first()->value == 1)
                                <div class="or or--1 mt-2">
                                    <span>or</span>
                                </div>
                                <div class="row">
                                    <div class="col-xxl-6 col-lg-8 col-md-10 mx-auto">
                                        <div class="text-center bg-gray py-4">
                                            <i class="fa"></i>
                                            <div class="h5 mb-4">
                                                {{ __('Your wallet balance :') }}<strong>{{ single_price(Auth::user()->balance) }}</strong>
                                            </div>
                                            @if(Auth::user()->balance < $total) <button type="button"
                                                class="btn btn-base-2" disabled>
                                                {{ __('Insufficient Balance') }}</button>
                                                @else
                                                <button type="button" onclick="use_wallet()"
                                                    class="btn btn-base-1">{{ __('Pay with wallet') }}</button>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="row align-items-center pt-4">
                            <div class="col-6">
                                <a href="{{ route('home') }}" class="link link--style-3">
                                    <i class="ion-android-arrow-back"></i>
                                    {{__('Return to shop')}}
                                </a>
                            </div>
                            <div class="col-6 text-right">
                                <button type="submit"
                                    class="btn btn-styled btn-base-1">{{__('Complete Order')}}</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-lg-4 ml-lg-auto">
                    @include('frontend.partials.cart_summary')
                </div>
            </div>
        </div>
    </section>
</div>
<div class="modal fade" id="spay" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
        <div class="modal-content position-relative">
            <div class="modal-header">
                {{-- <h5 class="modal-title strong-600 heading-5">{{__('Pay With Bkash Or Rocket')}}</h5> --}}
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <iframe class="payment_frame" src="{{ url('users/spay') }}" frameborder="0"></iframe>
        </div>
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
        function show_wallet_modal(){
            $('#wallet_modal').modal('show');
            $(".final").hide();
            $("#btnTest,.initial").show();
            $("#btnSubmit").attr('disabled',true);
        };

        function use_wallet(){
        $('input[name=payment_option]').val('wallet');
        $('#checkout-form').submit();
        }
</script>
@endsection
