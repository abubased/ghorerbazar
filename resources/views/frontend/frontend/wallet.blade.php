@extends('frontend.layouts.app')

@section('content')
    <section class="gry-bg py-4 profile">
        <div class="container">
            <div class="row cols-xs-space cols-sm-space cols-md-space">
                <div class="col-lg-3 d-none d-lg-block">
                    @if(Auth::user()->user_type == 'seller')
                        @include('frontend.inc.seller_side_nav')
                    @elseif(Auth::user()->user_type == 'customer')
                        @include('frontend.inc.customer_side_nav')
                    @endif
                </div>

                <div class="col-lg-9">
                    <div class="main-content">
                        <!-- Page title -->
                        <div class="page-title">
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12 d-flex align-items-center">
                                    <h2 class="heading heading-6 text-capitalize strong-600 mb-0">
                                        {{__('My Wallet')}}
                                    </h2>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="float-md-right">
                                        <ul class="breadcrumb">
                                            <li><a href="{{ route('home') }}">{{__('Home')}}</a></li>
                                            <li><a href="{{ route('dashboard') }}">{{__('Dashboard')}}</a></li>
                                            <li class="active"><a href="{{ route('wallet.index') }}">{{__('My Wallet')}}</a></li>
                                        </ul>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 offset-md-2">
                                <div class="dashboard-widget text-center green-widget text-white mt-4 c-pointer">
                                    <i class="fa fa-money" aria-hidden="true"></i>
                                    <span class="d-block title heading-3 strong-400">{{ single_price(Auth::user()->balance) }}</span>
                                    <span class="d-block sub-title">{{ __('Wallet Balance') }}</span>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="dashboard-widget text-center plus-widget mt-4 c-pointer" onclick="show_wallet_modal()">
                                    <i class="la la-plus"></i>
                                    <span class="d-block title heading-6 strong-400 c-base-1">{{ __('Recharge Wallet') }}</span>
                                </div>
                            </div>
                        </div>

                        <div class="card no-border mt-5">
                            <div class="card-header py-3">
                                <h4 class="mb-0 h6">{{__('Wallet recharge history')}}</h4>
                            </div>
                            <div class="card-body">
                                <table class="table table-sm table-responsive-md mb-0">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>{{ __('Date') }}</th>
                                            <th>{{__('Amount')}}</th>
                                            <th>{{__('Payment Method')}}</th>
                                            <th>{{__('Number')}}</th>
                                            <th>{{__('Transaction ID')}}</th>
                                            <th>{{__('Status')}}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if(count($wallets) > 0)
                                            @foreach ($wallets as $key => $wallet)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ date('d-m-y - h:i A', strtotime($wallet->created_at)) }}</td>
                                                    <td>{{ single_price($wallet->amount) }}</td>
                                                    <td>{{ ucfirst(str_replace('_', ' ', $wallet ->payment_method)) }}</td>
                                                    <td>{{ $wallet ->number }}</td>
                                                    <td>{{ $wallet ->TnxId }}</td>
                                                    <td>@if ($wallet ->status == 1)
                                                        {{ "Aprroved" }}
                                                    @else
                                                    {{ "Pending" }}
                                                    @endif</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td class="text-center pt-5 h4" colspan="100%">
                                                    <i class="la la-meh-o d-block heading-1 alpha-5"></i>
                                                <span class="d-block">{{ __('No history found.') }}</span>
                                                </td>
                                            </tr>
                                        @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pagination-wrapper py-4">
                            <ul class="pagination justify-content-end">
                                {{ $wallets->links() }}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade" id="wallet_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content position-relative">
                <div class="modal-header">
                    <h5 class="modal-title strong-600 heading-5">{{__('Recharge Wallet')}}</h5>
                    <button type="button" class="btn btn-base-1" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">{{ __('Cancel') }}</span>
                    </button>
                </div>
                <iframe class="wallet_frame" src="https://www.smanager.xyz/@GhorerbazarCom" frameborder="0"></iframe>
                <div class="col-12 guide">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-base-1" id="btnTest">{{__('Next')}}</button>
                    <button type="submit" id="btnSubmit" class="btn btn-base-1">{{ __('Confirm') }}</button>
                </div>
                {{-- <form class="" action="{{ route('wallet.confirm') }}" method="post">
                    @csrf
                    <div class="modal-body gry-bg px-3 pt-3">
                        <div class="row initial">
                            <div class="col-md-2">
                                <label>{{__('Amount')}} <span class="required-star">*</span></label>
                            </div>
                            <div class="col-md-10">
                                <input type="number" class="form-control mb-3" name="amount" placeholder="{{ __('Amount') }}" required>
                            </div>
                        </div>
                        <div class="row initial">
                            <div class="col-md-2">
                                <label>{{__('Payment Method')}}</label>
                            </div>
                            <div class="col-md-10">
                                <div class="mb-3">
                                    <select id="payment_option" class="form-control selectpicker" data-minimum-results-for-search="Infinity" name="payment_option">
                                        @if (\App\BusinessSetting::where('type', 'paypal_payment')->first()->value == 1)
                                            <option value="paypal">{{__('Paypal')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'stripe_payment')->first()->value == 1)
                                            <option value="stripe">{{__('Stripe')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'sslcommerz_payment')->first()->value == 1)
                                            <option value="sslcommerz">{{__('SSLCommerz')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'instamojo_payment')->first()->value == 1)
                                            <option value="instamojo">{{__('Instamojo')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'paystack')->first()->value == 1)
                                            <option value="paystack">{{__('Paystack')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'voguepay')->first()->value == 1)
                                            <option value="voguepay">{{__('VoguePay')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'bkash')->first()->value == 1)
                                            <option value="bkash">{{__('Bkash')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'rocket')->first()->value == 1)
                                            <option value="rocket">{{__('Rocket')}}</option>
                                        @endif
                                        @if (\App\BusinessSetting::where('type', 'cash')->first()->value == 1)
                                            <option value="cash">{{__('Cash Payment')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row final">
                            <div class="col-12 guide">
                            </div>
                            <div class="col-md-2">
                                <label>{{__('Account Number')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" name="number" placeholder="{{ __('Account Number') }}" required id="number">
                            </div>
                        </div>
                        <div class="row final">
                            <div class="col-md-2">
                                <label>{{__('Transaction ID Number')}}</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" class="form-control mb-3" name="TnxId" placeholder="TnxId" required>
                            </div>
                        </div>
                        <div class="cash"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-base-1" id="btnTest">{{__('Next')}}</button>
                        <button type="submit" id="btnSubmit" class="btn btn-base-1">{{ __('Confirm') }}</button>
                    </div>
                </form> --}}
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        function show_wallet_modal(){
            $('#wallet_modal').modal('show').modal('handleUpdate');
            // $(".final").hide();
            $(".guide").hide();
            $("#btnTest,.initial").show();
            $("#btnSubmit").attr('disabled',true);
        };
        $("#btnTest").click(function(){
            $(".wallet_frame").hide();
            $(".guide").show().html("<ol><li>{{ __('Go to your Rocket Mobile Menu by dialing') }} <span>*322#</span></li><li>{{ __('Choose') }} <span>Merchant Payment</span></li><li>{{ __('Enter the Merchant Rocket Account Number') }} <span>018422109991</span></li><li>{{ __('Enter a reference') }}<span>"+"z"+"</span></li><li>{{ __('Enter the amount with service charge') }} <span>"+"z"+"</span></li><li>{{ __('Now enter your Rocket Mobile Menu PIN') }}<span> to Confirm</span></li><li>{{ __('Done! You will receive a confirmation message from Rocket') }}</li><li>{{ __('Put your account number & the Transaction ID in the following box and press') }}<span>Confirm</span></li></ol>");
            $(".guide span").css({'color':'red','font-weight':'bold'});
        });
/*        $("#btnTest").click(function () {
            let x = $("#payment_option").val();
            let y = parseFloat($( "input[name='amount']").val());
            let z = Math.round(y+y*0.018);
            if(y>9){
                $("#btnTest,.initial").hide();
                $("#btnSubmit").attr('disabled',false);
                if($("#payment_option").val()!="cash"){
                    $(".final").show();
                    if(x=="bkash"){
                        $(".guide").html("<ol><li>{{ __('Go to your bKash Mobile Menu by dialing') }} <span>*247#</span></li><li>{{ __('Choose') }}<span>Payment</span></li><li>{{ __('Enter the Merchant bKash Account Number') }} <span>01842210999</span></li><li>{{ __('Enter the amount with service charge') }} <span>"+z+"</span></li><li>{{ __('Enter a reference') }} <span>"+z+"</span></li><li>{{ __('Enter the Counter Number') }} <span>1</span></li><li>{{ __('Now enter your bKash Mobile Menu PIN') }} <span>to Confirm</span></li><li>{{ __('Done! You will receive a confirmation message from bKash') }}</li><li>{{ __('Put your account number & the Transaction ID in the following box and press') }} <span>Confirm</span></li></ol>");
                    }else if(x=="rocket"){
                        $(".guide").html("<ol><li>{{ __('Go to your Rocket Mobile Menu by dialing') }} <span>*322#</span></li><li>{{ __('Choose') }} <span>Merchant Payment</span></li><li>{{ __('Enter the Merchant Rocket Account Number') }} <span>018422109991</span></li><li>{{ __('Enter a reference') }}<span>"+z+"</span></li><li>{{ __('Enter the amount with service charge') }} <span>"+z+"</span></li><li>{{ __('Now enter your Rocket Mobile Menu PIN') }}<span> to Confirm</span></li><li>{{ __('Done! You will receive a confirmation message from Rocket') }}</li><li>{{ __('Put your account number & the Transaction ID in the following box and press') }}<span>Confirm</span></li></ol>");
                    };
                    $(".final span").css({'color':'red','font-weight':'bold'});
                }else{
                    $("input[name='number']").attr("required",false);
                    $("input[name='TnxId']").attr("required",false);
                };
            };
        }); */




    </script>
@endsection
