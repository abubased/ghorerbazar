@extends('layouts.app')

@section('content')

    <div class="panel">
        <div class="panel-heading">
            <h3 class="panel-title">{{__('Customer Withdraw Request')}}</h3>
        </div>
        <div class="panel-body">
            <table class="table table-striped res-table mar-no" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{__('Date')}}</th>
                        <th>{{__('Customer')}}</th>
                        <th>{{__('Balance')}}</th>
                        <th>{{__('Requested Amount')}}</th>
                        <th>{{ __('Message') }}</th>
                        <th>{{ __('Status') }}</th>
                        <th>{{ __('Approval') }}</th>
                        <th width="10%">{{__('Options')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customer_withdraw_requests as $key => $customer_withdraw_request)
                        @if (\App\User::find($customer_withdraw_request->user_id) != null)
                            <tr>
                                <td>{{ ($key+1) + ($customer_withdraw_requests->currentPage() - 1)*$customer_withdraw_requests->perPage() }}</td>
                                <td>{{ $customer_withdraw_request->created_at }}</td>
                                <td>
                                    {{  \App\User::find($customer_withdraw_request->user_id)->name ?? 'Anonumus' }}
                                </td>
                                <td>{{ single_price(\App\User::find($customer_withdraw_request->user_id)->balance) }}</td>
                                <td>{{ single_price($customer_withdraw_request->amount) }}</td>
                                <td>
                                    {{ $customer_withdraw_request->message }}
                                </td>
                                <td>
                                    @if ($customer_withdraw_request->status == 1)
                                        <span class="ml-2" style="color:green"><strong>{{__('Paid')}}</strong></span>
                                    @else
                                        <span class="ml-2" style="color:red"><strong>{{__('Pending')}}</strong></span>
                                    @endif
                                </td>
                                <td>
                                    <label class="switch">
                                        <input onchange="update_approved(this)" value="{{ $customer_withdraw_request->id }}" type="checkbox" <?php if($customer_withdraw_request->status == 1) echo "checked";?> >
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="btn-group dropdown">
                                        <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                            {{__('Actions')}} <i class="dropdown-caret"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a onclick="show_user_profile('{{$customer_withdraw_request->user->id}}');">{{__('Profile')}}</a></li>
                                            <li>
                                            <li><a href="{{route('payments.edit', encrypt($customer_withdraw_request->user->id))}}">{{__('Edit')}}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            <div class="clearfix">
                <div class="pull-right">
                    {{ $customer_withdraw_requests->links() }}
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="payment_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content">

            </div>
        </div>
    </div>
    <div class="modal fade" id="message_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-zoom product-modal" id="modal-size" role="document">
            <div class="modal-content" id="modal-content">

            </div>
        </div>
    </div>

    <div class="modal fade" id="profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modal-content">

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">

        function update_approved(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('customer_withdraw_request.approved') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Topup approved successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                    console.log(data);
                }
            });
        }
        function show_user_profile(id){
            $.post(
                '{{ route('user.profile_modal') }}',
                {_token:'{{ @csrf_token() }}', id:id},
                function(data){
                    $('#profile_modal #modal-content').html(data);
                    $('#profile_modal').modal('show', {backdrop: 'static'});
                }
            );
        }
    </script>
@endsection
