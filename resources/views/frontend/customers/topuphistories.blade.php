@extends('layouts.app')

@section('content')

<!-- Basic Data Tables -->
<!--===================================================-->
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">{{__('Topup History')}}</h3>
    </div>
    <div class="panel-body">
        <div class="table-responsive" style="overflow:visible">
            <table class="table table-striped table-responsive mar-no" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ __('Date') }}</th>
                        <th>{{ __('Name') }}</th>
                        <th>{{__('Amount')}}</th>
                        <th>{{__('Number')}}</th>
                        <th>{{__('Status')}}</th>
                        <th>{{__('Approval')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($topups) > 0)
                        @foreach ($topups as $key => $topup)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ date('d-m-y - h:i A', strtotime($topup->created_at)) }}</td>
                                <td>{{ $topup->user->name ?? 'Anonymous'}}</td>
                                <td>{{ single_price($topup->amount) }}</td>
                                <td>{{ $topup ->number }}</td>
                                <td>@if ($topup ->status == 1)
                                    {{ "Aprroved" }}
                                @else
                                {{ "Pending" }}
                                @endif</td>
                                <td>
                                    <label class="switch">
                                        <input onchange="update_approved(this)" value="{{ $topup->id }}" type="checkbox" <?php if($topup->status == 1) echo "checked";?> >
                                        <span class="slider round"></span>
                                    </label>
                                </td>
                                <td>
                                    <div class="btn-group dropdown">
                                        <button class="btn btn-primary dropdown-toggle dropdown-toggle-icon" data-toggle="dropdown" type="button">
                                            {{__('Actions')}} <i class="dropdown-caret"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a onclick="show_user_profile('{{$topup->user->id}}');">{{__('Profile')}}</a></li>
                                            <li>
                                            <li><a href="{{route('payments.edit', encrypt($topup->user->id))}}">{{__('Edit')}}</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                    <tr>
                        <td class="text-center pt-5 h4" colspan="100%">
                            <i class="la la-meh-o d-block heading-1 alpha-5"></i>
                        <span class="d-block">{{ __('No history found.') }}</span>
                        </td>
                    </tr>
                @endif
            </tbody>
            </table>
            <div class="clearfix">
                <div class="pull-right">
                    {{-- {{ $topup->links() }} --}}
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="profile_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="modal-content">

        </div>
    </div>
</div>
@endsection
@section('script')
    <script type="text/javascript">
        function update_approved(el){
            if(el.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('topup.approved') }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
                if(data == 1){
                    showAlert('success', 'Topup approved successfully');
                }
                else{
                    showAlert('danger', 'Something went wrong');
                    console.log(data);
                }
            });
        };
        function show_user_profile(id){
            $.post(
                '{{ route('user.profile_modal') }}',
                {_token:'{{ @csrf_token() }}', id:id},
                function(data){
                    $('#profile_modal #modal-content').html(data);
                    $('#profile_modal').modal('show', {backdrop: 'static'});
                }
            );
        }
    </script>
@endsection
