-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jun 14, 2021 at 09:44 AM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ghorerba_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `published` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `photo`, `url`, `position`, `published`, `created_at`, `updated_at`) VALUES
(6, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-03-12 05:58:52', '2019-03-12 05:58:57'),
(7, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-05-26 05:16:38', '2019-05-26 05:17:34'),
(8, 'uploads/banners/banner.jpg', '#', 2, 1, '2019-06-11 05:00:06', '2019-06-11 05:00:27'),
(9, 'uploads/banners/abCz59E4RNyGCFb4UjIolT2qgwCVMFY8XzrHXezJ.gif', '#', 1, 0, '2020-06-20 01:50:25', '2020-06-20 01:57:26'),
(10, 'uploads/banners/h1LjrSdS0SvVo9cT0yNj4orHq9Gq7rRUSrD7zn90.gif', '#', 1, 0, '2020-06-20 01:53:42', '2020-06-20 01:57:25'),
(11, 'uploads/banners/VFadByL7raz7gxaY2fhb1MpaUvBppHh9zbP0XNjY.gif', '#', 1, 0, '2020-06-20 01:54:30', '2020-06-20 02:33:14'),
(12, 'uploads/banners/LLcD8dtK6dRMyvF3klYxjMzO05wHEqxPHIss0fUC.jpeg', 'http://ghorerbazar.com/shops/create', 1, 0, '2020-06-22 08:49:26', '2020-06-30 08:02:19'),
(13, 'uploads/banners/0vlMhBGGaCqtAcgLr9MRy0NCMIbt9q3eumW6vWq9.jpeg', '#', 1, 0, '2020-06-22 19:38:03', '2020-06-30 08:02:19');

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `top` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `logo`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(48, 'ঘরেরবাজার ghorerbazar', 'uploads/brands/RaBKZBz4Zsc7MkGq9fv9CPNJGzLz6AmRvYOgzzFp.png', 1, 'ঘরেরবাজার-ghorerbazar', NULL, NULL, '2020-05-12 07:43:29', '2020-05-13 23:31:25'),
(49, 'ফ্রেশ Fresh', 'uploads/brands/tOw3TPF2rVPqfROsJmycNrSKCGp7WtypZTXMOGgK.jpeg', 1, '-Fresh-dJHsa', 'ফ্রেশ-Fresh', NULL, '2020-05-12 16:30:40', '2020-05-13 23:31:25'),
(50, 'বসুন্ধরা Bashundhaa', 'uploads/brands/kAYWFrqhs58mcq16syPtDpyTedIIlvwqjSaAJ6J1.jpeg', 1, '-Bashundhaa-HxB8g', 'বসুন্ধরা-Bashundhaa', NULL, '2020-05-12 16:32:44', '2020-05-13 23:31:25'),
(51, 'এসিআই Aci', 'uploads/brands/xR0aJZMvRiYszXV3LDLaD98YKvO86VxGwv6v54CJ.png', 1, '-Aci-NYF1o', 'এসিআই-Aci', NULL, '2020-05-12 16:33:33', '2020-05-13 23:31:25'),
(52, 'একমি Acme', 'uploads/brands/lDTdMwWQm30RyBVyNacPWQqXwwPkqtGRc4V78vJw.jpeg', 0, '-Acme-wtNxE', 'একমি-Acme', NULL, '2020-05-12 16:34:22', '2020-05-12 16:34:22'),
(53, 'স্কয়ার Sqare', 'uploads/brands/kzKVdx775WK0lvHvWCIz3DRKBczKPd5XXcX0seRK.jpeg', 1, '-Sqare-1OirU', 'স্কয়ার-Sqare', NULL, '2020-05-12 16:35:18', '2020-05-13 23:31:25'),
(55, 'আকিজ Akij', 'uploads/brands/wFXckSCDUn208bsDZBR02QkNXltSvuRLVAIQp82U.png', 0, '-Akij-n2vLy', 'আকিজ-Akij', NULL, '2020-05-12 16:36:22', '2020-05-12 16:36:22'),
(56, 'ইউনিলিভার Unilever', 'uploads/brands/JSBV0r9MGiCCQsGQuEjIAuJm1S7BD2qq1flnmASj.jpeg', 1, '-Unilever-1XYJO', 'ইউনিলিভার-Unilever', NULL, '2020-05-12 16:37:30', '2020-05-13 23:31:25'),
(57, 'প্রান Pran', 'uploads/brands/2iNfsSoNCQ3T3xwZTzPLHrQUACfU1wv4cZjtYfzY.jpeg', 1, '-Pran-KbT4o', 'প্রান-Pran', NULL, '2020-05-12 16:38:43', '2020-05-13 23:31:25'),
(58, 'ডেনিশ Danish', 'uploads/brands/AwathchPbgrPXQs3LAvsfWmsozQb1qUWJ2pSrF3G.png', 0, '-Danish-pycns', 'ডেনিশ-Danish', NULL, '2020-05-12 16:39:49', '2020-05-12 16:39:49'),
(59, 'বিডি ফুড Bd Food', 'uploads/brands/35dOYzM9wo6aIPd34tcLnvgf3WWYfcL1Gkm6YNXi.jpeg', 1, '--Bd-Food-TjqeV', 'বিডি-ফুড -Bd-Food', NULL, '2020-05-12 16:41:08', '2020-05-13 23:31:25'),
(60, 'ইফাদ Ifad', 'uploads/brands/Rdn4YMvZ0SlicbGvxYTEekGpQBNOU5JIsV4QEB1w.jpeg', 0, '-Ifad-16xRE', 'ইফাদ-Ifad', NULL, '2020-05-14 23:45:19', '2020-05-14 23:45:19'),
(61, 'ইস্পাহানি Ispahani', 'uploads/brands/uH9WmCG1CxuWpQpvWBiatxCGXuFIhKOl1FYcqvVf.png', 0, '-Ispahani-vOqTY', 'ইস্পাহানি-Ispahani', NULL, '2020-05-14 23:47:12', '2020-05-14 23:47:12'),
(62, 'খুশবো Khusboo', 'uploads/brands/9UwwzIIyU6B8qlsI5fM6OLlRYW2SVhscII2CUV6b.png', 0, '-Khusboo-ZKTQ8', 'খুশবো-Khusboo', NULL, '2020-05-14 23:48:11', '2020-05-14 23:48:11'),
(63, 'চাষি Farmer', 'uploads/brands/vTs5FecrzRiomTdzhz11LqKFelk44fqeHOTjtDos.jpeg', 0, '-Farmer-lj1Bb', 'চাষি-Farmer', NULL, '2020-05-14 23:49:04', '2020-05-14 23:49:04'),
(64, 'স্বাদ Shad', 'uploads/brands/oKO1K7yIiRKQzfwDFISwAbz3qvUMZFR2IqDj7j0A.jpeg', 0, '-Shad-oifch', 'স্বাদ-Shad', NULL, '2020-05-14 23:49:37', '2020-05-14 23:49:37'),
(65, 'তীর Teer', 'uploads/brands/LfQNqNhfMP2JvbMI7btA9gNL9GjZgFazIiChJ4ph.png', 0, '-Teer-8G5Kz', 'তীর-Teer', NULL, '2020-05-14 23:51:32', '2020-05-14 23:51:32'),
(66, 'আড়ং Arang', 'uploads/brands/wkJA5vT20JVw9nF5gqWf0L5cD9DBpkD5rsB1tOgz.png', 0, '-Arang-t4FpH', 'আড়ং-Arang', NULL, '2020-05-14 23:52:54', '2020-05-14 23:52:54'),
(67, 'রুপচাঁদা Rupchanda', 'uploads/brands/DiHroMTi1b6hWOec6nNN9iUjyXOEybaQ27jEb9TU.jpeg', 0, '-Rupchanda-yMdAc', 'রুপচাঁদা-Rupchanda', NULL, '2020-05-14 23:54:30', '2020-05-14 23:54:30'),
(69, 'পুষ্টি Pusti', 'uploads/brands/hAFmjpfWNiILc5Bxi7OrXlZgWEtuKyYydvL2LDpV.jpeg', 0, '-Pusti-b4UOa', 'পুষ্টি-Pusti', NULL, '2020-05-15 04:14:12', '2020-05-15 04:14:12'),
(70, 'বনফুল Banoful', 'uploads/brands/rpVlg4w5fR8RRROj0Ef4TL43KriIrbPP5Dnz8ApR.png', 0, '-Banoful-uiLdP', 'বনফুল-Banoful', NULL, '2020-05-16 00:13:32', '2020-05-16 00:13:32'),
(71, 'রাধুনী Radhuni', 'uploads/brands/HsbOO2knCbdKc6k9KdXttZzSODwQGkuwQK9U1Qxq.jpeg', 0, '-Radhuni-5y0ji', 'রাধুনী-Radhuni', NULL, '2020-05-16 00:23:05', '2020-05-16 00:23:05'),
(72, 'মার্কস Marks', 'uploads/brands/wb089FJ4hrXjqrCsvR4cNtWtv4Ifmg8PfdX2maId.jpeg', 0, '-Marks-gHAWA', 'মার্কস-Marks', NULL, '2020-05-16 01:58:39', '2020-05-16 01:58:39'),
(73, 'Diploma', 'uploads/brands/MqXadGOpD2fkkUYnEX9zxoSWmSwLP3xzNJrDm8py.jpeg', 0, 'Diploma-JieyL', 'Diploma', NULL, '2020-05-16 02:00:03', '2020-05-16 02:00:03'),
(75, 'Nestle', 'uploads/brands/BqU1Wly6ygrjwdvb6sitih6UpzxE5YEgXWpXeWiV.png', 1, 'Nestle-gfFIY', 'Nestle', NULL, '2020-05-16 02:04:06', '2020-06-02 01:40:46'),
(76, 'ভিম  Vim', 'uploads/brands/A8UlKABbtmRx1ZWjD9urtde131GtgXXxq3v61DLb.jpeg', 1, '--Vim-Hhqoi', 'Vim', NULL, '2020-06-02 00:28:15', '2020-06-02 01:40:46'),
(77, 'জেনারেল ফার্মাসিউটিক্যাল  Generel Pharmaceuticals', 'uploads/brands/jBgbMtJKXpaAodFiacxUuYND9hLCHCzWywvbAST1.jpeg', 0, '---Generel-Pharmaceuticals-TCRdL', 'জেনারেল ফার্মাসিউটিক্যাল  Generel Pharmaceuticals', NULL, '2020-06-08 01:34:28', '2020-06-08 01:34:28'),
(78, 'এসকে+এফ ফার্মাসিউটিক্যাল  SK+F Pharmaceuticals', 'uploads/brands/iaIVSJG1wJErP0Qj8cZsQpQRwv9cG5r7w9oozHNY.jpeg', 0, '---SKF-Pharmaceuticals-zzGNX', 'এসকে+এফ ফার্মাসিউটিক্যাল  SK+F Pharmaceuticals', NULL, '2020-06-08 01:35:51', '2020-06-08 01:35:51'),
(79, 'ইনসেপ্টা ফার্মাসিউটিক্যাল Incepta  Pharmaceuticals', 'uploads/brands/u8nc33XOMZiryvaVFB1PBbzZwkAKzlQWfqTPUCPS.png', 0, '--Incepta--Pharmaceuticals-yQuoC', 'ইনসেপ্টা ফার্মাসিউটিক্যাল Incepta  Pharmaceuticals', NULL, '2020-06-08 01:37:33', '2020-06-08 01:37:33'),
(80, 'এরিস্টোফার্মা ফার্মাসিউটিক্যাল  AristoPharma Pharm', 'uploads/brands/vIt73GFLXpfqIiQQ7pAi6jETVZlk6AzZnymQPqaY.jpeg', 0, '---AristoPharma-Pharmaceuticals-OyGAR', 'এরিস্টোফার্মা ফার্মাসিউটিক্যাল  AristoPharma Pharmaceuticals', NULL, '2020-06-08 01:39:52', '2020-06-08 01:39:52'),
(81, 'বিকন ফার্মাসিউটিক্যাল  Beacon Pharmaceuticals', 'uploads/brands/EDjpiiRnNqk9TTful8utrTRbuYaw3a0eyVUvUOOV.jpeg', 0, '---Becon-Pharmaceuticals-99S5Y', 'বিকন ফার্মাসিউটিক্যাল  Becon Pharmaceuticals', NULL, '2020-06-08 01:40:52', '2020-06-08 01:41:19'),
(82, 'বেক্সিমকো ফার্মাসিউটিক্যাল  Beximco Pharmaceutical', 'uploads/brands/F268IGMZRYxd2UMayGZyq6ZGJ3RDZ9C5X7lL93tR.jpeg', 0, '---Beximco-Pharmaceuticals-y9Jzm', 'বেক্সিমকো ফার্মাসিউটিক্যাল  Beximco Pharmaceuticals', NULL, '2020-06-08 01:43:27', '2020-06-08 01:43:27'),
(83, 'হার্পিক Harpic', 'uploads/brands/ZEQMkjkr3fkFJQnSlrdZ97ayddTM0VgTx3xAmyje.jpeg', 0, '-Harpic-y62eP', 'হার্পিক-Harpic', NULL, '2020-06-16 23:04:07', '2020-06-16 23:04:07'),
(84, 'এলপিজি গ্যাস LPG GAS', 'uploads/brands/CwnXrjk3VFb6FEiK5Swj6zS3723DdDG14S1Gkln1.jpeg', 0, '--LPG-GAS-eOXyM', 'এলপিজি গ্যাস LPG GAS', NULL, '2020-06-17 22:59:13', '2020-06-17 22:59:13'),
(85, 'বসুন্ধরা Bashundhara Lpg Gas', 'uploads/brands/Qn3C3Mx7mYotPJqELpj7HLNSe3wA3g4yhmWasWXe.png', 0, '-Bashundhara-Lpg-Gas-8ouic', 'বসুন্ধরা Bashundhara Lpg Gas', NULL, '2020-06-17 23:27:28', '2020-06-17 23:27:28'),
(86, 'ফ্রেশ Fresh Lpg Gas', 'uploads/brands/ifkI6TkHi68GF7WDWFmDi5QXXQPk2pjpnrfmc5RT.jpeg', 0, '-Fresh-Lpg-Gas-ur67s', 'ফ্রেশ Fresh Lpg Gas', NULL, '2020-06-17 23:29:08', '2020-06-17 23:29:08'),
(87, 'যমুনা Jamuna Lpg Gas', 'uploads/brands/crcWOkCGkpbAQdab5EOCxmb2bbMKehxlXREvvXzH.png', 0, '-Jamuna-Lpg-Gas-syjtg', 'যমুনা Jamuna Lpg Gas', NULL, '2020-06-17 23:30:08', '2020-06-17 23:30:08'),
(88, 'বিএম Bm Lpg Gas', 'uploads/brands/miEsfUpWPK8GNoHjzhAsQfgiZXNAGD0mK2g8w7o9.png', 0, '-Bm-Lpg-Gas-t97uf', 'বিএম Bm Lpg Gas', NULL, '2020-06-17 23:31:11', '2020-06-17 23:31:11'),
(89, 'বেক্সিমকো Beximco Lpg Gas', 'uploads/brands/v9xZla3mcRIkzW6VzKbfGlP0ANOFhLs6N3fjpi33.png', 0, '-Beximco-Lpg-Gas-E9aLJ', 'বেক্সিমকো Beximco Lpg Gas', NULL, '2020-06-17 23:34:05', '2020-06-17 23:34:05'),
(90, 'টোটাল Total Lpg Gas', 'uploads/brands/T30cpBZISbgrLiU0IAmnJ2wvPVYa3nCXarzuHrwN.jpeg', 0, '-Total-Lpg-Gas-GRaP7', 'টোটাল Total Lpg Gas', NULL, '2020-06-17 23:34:47', '2020-06-17 23:34:47'),
(91, 'লাফ্জ Laugfs Lpg Gas', 'uploads/brands/fuKC59hwAKbE5q7vkuIlh4X1vTrk1yDSaOhONRbZ.jpeg', 0, '-Laugfs-Lpg-Gas-bBtz2', 'লাফ্জ Laugfs Lpg Gas', NULL, '2020-06-17 23:35:46', '2020-06-17 23:35:46'),
(92, 'নাভানা  Navana Lpg Gas', 'uploads/brands/WQHNmNdWoUhkjwZvJbdsHPAgncbnmfd5sTi6NFTh.jpeg', 0, '--Navana-Lpg-Gas-iwdGA', 'নাভানা  Navana Lpg Gas', NULL, '2020-06-17 23:36:41', '2020-06-17 23:36:41'),
(93, 'ওমেরা Omera Lpg Gas', 'uploads/brands/jheBfRQPIBpiEId16uCrY1wmSB4yyyRitW86MVl9.png', 0, '-Omera-Lpg-Gas-VwvdQ', 'ওমেরা Omera Lpg Gas', NULL, '2020-06-17 23:37:20', '2020-06-17 23:37:20'),
(94, 'সেনা Sena Lpg Gas', 'uploads/brands/eeyoOy7DHg6i0RcGq90KJb1YmJrqQnBPsISTNWqf.png', 0, '-Sena-Lpg-Gas-5EVPi', 'সেনা Sena Lpg Gas', NULL, '2020-06-17 23:38:04', '2020-06-17 23:38:04');

-- --------------------------------------------------------

--
-- Table structure for table `business_settings`
--

DROP TABLE IF EXISTS `business_settings`;
CREATE TABLE IF NOT EXISTS `business_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `business_settings`
--

INSERT INTO `business_settings` (`id`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'home_default_currency', '27', '2018-10-16 01:35:52', '2020-05-09 07:31:54'),
(2, 'system_default_currency', '27', '2018-10-16 01:36:58', '2020-01-26 04:22:13'),
(3, 'currency_format', '1', '2018-10-17 03:01:59', '2018-10-17 03:01:59'),
(4, 'symbol_format', '2', '2018-10-17 03:01:59', '2020-06-09 07:43:04'),
(5, 'no_of_decimals', '0', '2018-10-17 03:01:59', '2018-10-17 03:01:59'),
(6, 'product_activation', '1', '2018-10-28 01:38:37', '2019-02-04 01:11:41'),
(7, 'vendor_system_activation', '1', '2018-10-28 07:44:16', '2019-02-04 01:11:38'),
(8, 'show_vendors', '1', '2018-10-28 07:44:47', '2019-02-04 01:11:13'),
(9, 'paypal_payment', '0', '2018-10-28 07:45:16', '2020-07-10 07:42:39'),
(10, 'stripe_payment', '0', '2018-10-28 07:45:47', '2018-11-14 01:51:51'),
(11, 'cash_payment', '1', '2018-10-28 07:46:05', '2019-01-24 03:40:18'),
(12, 'payumoney_payment', '0', '2018-10-28 07:46:27', '2019-03-05 05:41:36'),
(13, 'best_selling', '1', '2018-12-24 08:13:44', '2019-02-14 05:29:13'),
(14, 'paypal_sandbox', '0', '2019-01-16 12:44:18', '2019-01-16 12:44:18'),
(15, 'sslcommerz_sandbox', '1', '2019-01-16 12:44:18', '2019-03-14 00:07:26'),
(16, 'sslcommerz_payment', '0', '2019-01-24 09:39:07', '2019-01-29 06:13:46'),
(17, 'vendor_commission', '6', '2019-01-31 06:18:04', '2020-06-01 22:55:46'),
(18, 'verification_form', '[{\"type\":\"text\",\"label\":\"Your name\"},{\"type\":\"text\",\"label\":\"Shop name\"},{\"type\":\"text\",\"label\":\"Email\"},{\"type\":\"text\",\"label\":\"License No\"},{\"type\":\"text\",\"label\":\"Full Address\"},{\"type\":\"text\",\"label\":\"Phone Number\"},{\"type\":\"file\",\"label\":\"Tax Papers\"}]', '2019-02-03 11:36:58', '2019-02-16 06:14:42'),
(19, 'google_analytics', '0', '2019-02-06 12:22:35', '2019-02-06 12:22:35'),
(20, 'facebook_login', '1', '2019-02-07 12:51:59', '2020-06-25 07:46:35'),
(21, 'google_login', '0', '2019-02-07 12:52:10', '2019-02-08 19:41:14'),
(22, 'twitter_login', '0', '2019-02-07 12:52:20', '2019-02-08 02:32:56'),
(23, 'payumoney_payment', '1', '2019-03-05 11:38:17', '2019-03-05 11:38:17'),
(24, 'payumoney_sandbox', '1', '2019-03-05 11:38:17', '2019-03-05 05:39:18'),
(36, 'facebook_chat', '1', '2019-04-15 11:45:04', '2020-06-19 23:22:31'),
(37, 'email_verification', '1', '2019-04-30 07:30:07', '2020-05-16 23:55:21'),
(38, 'wallet_system', '1', '2019-05-19 08:05:44', '2020-06-18 01:43:11'),
(39, 'coupon_system', '0', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(40, 'current_version', '1.9', '2019-06-11 09:46:18', '2019-06-11 09:46:18'),
(41, 'instamojo_payment', '0', '2019-07-06 09:58:03', '2019-07-06 09:58:03'),
(42, 'instamojo_sandbox', '1', '2019-07-06 09:58:43', '2019-07-06 09:58:43'),
(43, 'razorpay', '0', '2019-07-06 09:58:43', '2019-07-06 09:58:43'),
(44, 'paystack', '0', '2019-07-21 13:00:38', '2019-07-21 13:00:38'),
(45, 'pickup_point', '1', '2019-10-17 11:50:39', '2020-06-13 08:14:50'),
(46, 'maintenance_mode', '0', '2019-10-17 11:51:04', '2019-10-17 11:51:04'),
(47, 'voguepay', '0', '2019-10-17 11:51:24', '2019-10-17 11:51:24'),
(48, 'voguepay_sandbox', '1', '2019-10-17 11:51:38', '2020-10-01 16:06:42'),
(50, 'category_wise_commission', '1', '2020-01-21 07:22:47', '2021-06-06 17:22:12'),
(51, 'conversation_system', '1', '2020-01-21 07:23:21', '2020-01-21 07:23:21'),
(52, 'guest_checkout_active', '1', '2020-01-22 07:36:38', '2020-01-22 07:36:38'),
(53, 'facebook_pixel', '0', '2020-01-22 11:43:58', '2020-01-22 11:43:58'),
(54, 'bkash', '1', '2019-04-15 11:45:04', '2020-06-19 23:22:31'),
(55, 'rocket', '1', '2019-04-15 11:45:04', '2020-06-19 23:22:31'),
(56, 'cash', '1', '2019-04-15 11:45:04', '2020-06-19 23:22:31'),
(57, 'shurjopay', '1', '2018-10-28 07:45:16', '2020-07-10 07:42:39');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `banner` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commision_rate` int(11) DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured` int(1) NOT NULL DEFAULT '0',
  `top` int(1) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `banner`, `commision_rate`, `icon`, `featured`, `top`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(12, 'ঘরেরবাজার Ghorerbazar', 'uploads/categories/banner/NiQNJhp6yGaVUVh3bapf68qKE7H0iN6d0aYN09uB.png', NULL, 'uploads/categories/icon/fF511iarEXzjLw9sMxqr4SHXa4vQImkrukb89y8o.png', 1, 0, '-Ghorerbazar', NULL, NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(13, 'অফিস ও লাইব্রেরী Office & Livery', 'uploads/categories/banner/JNHuv7NQr6BIAzymniEcKmDvYsjeYDOInANz8Ssa.jpeg', NULL, 'uploads/categories/icon/o4wE9rqtO6xP3VUS1KouLlGj9PIz6Vx6XVb6ZeYI.jpeg', 1, 0, '---Office--Livery-oryZ7', 'অফিস-ও-লাইব্রেরী-Office-&-Livery', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(14, 'রান্নাঘরের সামগ্রী Kitchen utensils', 'uploads/categories/banner/qPMp1FjIBHsPnr5OowADkGLsTdjJ7NNU0AZjIbhb.jpeg', NULL, 'uploads/categories/icon/UaMysaQjsew8cwKq2M4pXc6EgtPVacqZ76DRrlPT.jpeg', 1, 0, '--Kitchen-utensils-Z3dR7', 'রান্নাঘরের-সামগ্রী-Kitchen-utensils', NULL, '2020-05-13 05:40:05', '2020-05-12 23:40:05'),
(15, 'গৃহস্থালি জিনিসপত্র Household Items', 'uploads/categories/banner/W2tcV5htVDVB2zQ33mpsxvWq8bYhEAxlugEO8SFQ.jpeg', NULL, 'uploads/categories/icon/DJpwqXvlHyhlIyiPE7jizp4C71EmhUS2fGD7lmLA.jpeg', 1, 0, '--Household-Items-iGxiv', 'গৃহস্থালি-জিনিসপত্র-Household-Items', NULL, '2020-05-13 05:42:19', '2020-05-12 23:42:19'),
(16, 'ডায়াবেটিক খাবার Diabetic Food', 'uploads/categories/banner/wyk9PtUv2yplE2159wjPUo9pb8nfFzdtQoWBwx6K.jpeg', NULL, 'uploads/categories/icon/mm2CXOpQpLn6UWLB8hUZ0gU0NFN5K0K6RmvP2CfW.jpeg', 1, 0, '--Diabetic-Food-GMnIZ', 'ডায়াবেটিক-খাবার-Diabetic-Food', NULL, '2020-05-13 05:42:52', '2020-05-12 23:42:52'),
(17, 'স্বাস্থ বিষয়ক খাবার Health Foods', 'uploads/categories/banner/crkAb5GyhcuBmsdwc1ouMpOoCCkaJLt2tQ3nQ9Xb.jpeg', NULL, 'uploads/categories/icon/1Qe6RDCneHeZDS7yhkoilAHN4qp9aIDBLKRVi6YL.jpeg', 1, 0, '---Health-Foods-nom7f', 'স্বাস্থ-বিষয়ক-খাবার-Health-Foods', NULL, '2020-05-14 04:06:24', '2020-05-13 22:06:24'),
(18, 'শিশু খাদ্য Baby Food', 'uploads/categories/banner/PMqCRzVzBz2j8cvcEHxDGQvKAJupRiKnMWHTx38E.jpeg', NULL, 'uploads/categories/icon/Lz5OOPBxoeH4XQuPMY2dy3EcOPtHH3dgu98g94gJ.jpeg', 1, 0, '--Baby-Food-d4ELa', 'শিশু-খাদ্য-Baby-Food', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(19, 'শিশুপণ্য Baby Products', 'uploads/categories/banner/7PfxyOUWddPRDhBQuwoSdXbVD9XRdsm9LRXfMcg5.jpeg', NULL, 'uploads/categories/icon/OKTBYYAtvOYLXw1yq2RZX1gq5gOp96IkqGldO6ID.jpeg', 1, 0, '-Baby-Products-lKuN5', 'শিশুপণ্য-Baby-Products', NULL, '2020-05-13 05:42:56', '2020-05-12 23:42:56'),
(20, 'পরিষ্কার ও পরিচ্ছন্নতা Clear & cleanliness', 'uploads/categories/banner/bEirmqDexEUZqkRtrdDPJDJMFBYkvimHXlV6a47U.jpeg', NULL, 'uploads/categories/icon/Rwpb6PXhVRLlvqsC0h94pfRpRsJW21E5eAEd3z0N.jpeg', 1, 0, '---Clear--cleanliness-r1KrR', 'পরিষ্কার-ও-পরিচ্ছন্নতা-Clear-&-cleanliness', NULL, '2020-05-13 05:42:57', '2020-05-12 23:42:57'),
(21, 'সৌন্দর্য ও প্রসাধনী Beauty & cosmetics', 'uploads/categories/banner/iEGisksloQTay92L5HtAw6NQMwwgpTIWlbGtogtN.jpeg', NULL, 'uploads/categories/icon/2i9IQwgef9LU76Hw4SWOtYV50piBsvHXleNWcj6j.jpeg', 1, 0, '---Beauty--cosmetics-bPXKN', 'সৌন্দর্য-ও-প্রসাধনী-Beauty-&-Cosmetics', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(22, 'জল খাবার Water Food', 'uploads/categories/banner/fLz5NGZakr8wZvj8IoQzxkcXYQmaVJmj4t0Y6Vgq.jpeg', NULL, 'uploads/categories/icon/5CcIkyDB5t3FFMzql8leS25XDCb1Z0uPfdsUghEf.jpeg', 1, 0, '--Water-Food-X9NOI', 'জল-খাবার-Water-Food', NULL, '2020-05-13 05:42:59', '2020-05-12 23:42:59'),
(23, 'নাস্তা ও পানীয় Breakfast and Drinks', 'uploads/categories/banner/Au9xpQRUwD3IXrgeHT9Nh5qWlqcZOcFP3oQIfzE1.jpeg', NULL, 'uploads/categories/icon/sjGxZhn3pGK70IkO4he2P2JFjYJGpfkeonuwaFGP.jpeg', 1, 1, '---Breakfast-and-Drinks-5CVzy', 'নাস্তা-ও-পানীয়-Breakfast-&-Drinks', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(24, 'দুধ, মধু, দুগ্ধজাত Milk, Honey, Dairy Products', 'uploads/categories/banner/ewFGQZ7O1vMwOBt0ANOn6LkSQzZ4PFj1RmHlbnYR.jpeg', NULL, 'uploads/categories/icon/Fpswr4cRgVb735SP81qiaIay0xbSGkQlXDbDJEWk.jpeg', 1, 1, '---Milk-Honey-Dairy-Products-7grzL', 'দুধ-মধু-দুগ্ধজাত-Milk-Honey-Dairy-Products', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(25, 'ডাল Pulses', 'uploads/categories/banner/fH9DJzTHehYQkoQcEvAbl6yaKzkgGCTuhVa4Ziuu.jpeg', NULL, 'uploads/categories/icon/TBnnrtVPBpwVPgEQo3Npp2kkzTF5vRi7NgROX2ML.jpeg', 1, 0, '-Pulses-FtGWV', 'ডাল-Pulses', NULL, '2020-06-08 08:14:07', '2020-06-08 02:14:07'),
(26, 'মসলা, রেডি মিক্স, আচার Spices, Ready Mix, Pickles', 'uploads/categories/banner/0KieTHwMlxEQWYjJdwhd6CvH8fRgraC89s37KTIi.jpeg', NULL, 'uploads/categories/icon/7myZLAB2zklXJxGcE12lp0rdVtjhnnAf4isnHuOa.jpeg', 1, 1, '----Spices-Ready-Mix-Pickles-lOntj', 'মসলা-রেডি-মিক্স-আচার-Spices-Ready-Mix-Pickles', NULL, '2020-05-14 05:31:25', '2020-05-13 23:31:25'),
(27, 'লবন, চিনি, সেমাই, সুজি Salt, Sugar, Semai, Semolin', 'uploads/categories/banner/71HPJ3dpWkslTjAm2A5xXMs9gZ7NP83sT3f4jK6n.jpeg', NULL, 'uploads/categories/icon/3OlHLS9y5gFzhkkt4g8sCSVbwft9g1Bev8cB0k8P.jpeg', 1, 1, '----Salt-Sugar-Semai-Semolina-MbBUx', 'লবন-চিনি-সেমাই-সুজি-Salt-Sugar-Semai-Semolina', NULL, '2020-05-14 05:31:25', '2020-05-13 23:31:25'),
(28, 'তেল, ঘি Oil, Ghee', 'uploads/categories/banner/iVMY4SqglLfQoWtBjDb5mT6WjiroH7apTWqkfiTF.jpeg', NULL, 'uploads/categories/icon/BHx35mIXjbyp47JKl7MsV984ynHW762tbWaj1faM.jpeg', 1, 1, '--Oil-Ghee-oe1iZ', 'তেল-ঘি-Oil-Ghee', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(29, 'চাল, আটা, ময়দা, বেসন Rice, Flour, Meal, Gram Flour', 'uploads/categories/banner/DvxLZWoqbseObdMETHG9Cp3oPDaFRjA1TsRcWbEg.jpeg', NULL, 'uploads/categories/icon/yPQbcgwzWee7NDVT02MkXKAQ1tflC02yZc6Qkm24.jpeg', 1, 1, '----Rice-Flour-Meal-Gram-Flour-7neoN', 'চাল-আটা-ময়দা-বেসন-Rice-Flour-Meal-Gram-Flour', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(30, 'মাছ, মাংস, ডিম, শুঁটকি Fish, Meat, Eggs, Dried Fis', 'uploads/categories/banner/mDAePwZ9zNPob7BuTLQsR3mPkD0qPM4h630JA2G1.jpeg', NULL, 'uploads/categories/icon/Brn7tU8NeOG5QfMRck0PCwcUucrDhWJrLdEwERAM.jpeg', 1, 1, '----Fish-Meat-Eggs-Dried-Fish-ldLU1', 'মাছ-মাংস-ডিম-শুঁটকি-Fish-Meat-Eggs-Dried-Fish', NULL, '2020-05-14 05:31:25', '2020-05-13 23:31:25'),
(31, 'ফলমূল Fruits', 'uploads/categories/banner/jxKB4HfXYR91Vj1Y7fO1mV68tq3lCPZMkYuVWVYf.jpeg', NULL, 'uploads/categories/icon/QCWwuwyHOTnWGKeiNfnlMcGoY1xIi9GFoFj0ebY4.jpeg', 1, 1, '-Fruits-5v9EH', 'ফলমূল-Fruits', NULL, '2020-06-02 07:40:46', '2020-06-02 01:40:46'),
(32, 'তাজা শাকসবজি Fresh Vegetables', 'uploads/categories/banner/6u3TLg7gKK7gfKeEGWQmnxE2lR7fiwS9pJWH3g9R.jpeg', NULL, 'uploads/categories/icon/RFqfCvdpOlG37UEwLbxd1ksONdw4UHmiNqMy900o.jpeg', 1, 1, '--Fresh-Vegetables-eokVL', 'তাজা-শাকসবজি-Fresh-Vegetables', NULL, '2020-05-14 05:31:25', '2020-05-13 23:31:25'),
(33, 'ঔষধ Drugs', 'uploads/categories/banner/vX4mAsPQGGTMyzMFtTkWF2DHHWGV5UC1iJOAunyi.jpeg', NULL, 'uploads/categories/icon/ukMpyMaBfHSkPkRbJ1bujJllQ1lJCO6fVaDsMomH.png', 0, 1, '-Drugs-ixYKH', 'ঔষধ Drugs', NULL, '2020-06-08 08:14:09', '2020-06-08 02:14:09'),
(34, 'এলপিজি গ্যাস LPG GAS', 'uploads/categories/banner/Mk65Cnk6RAjOGgjuDFujpyAb2mdOE1LhrLD0NI6B.jpeg', NULL, 'uploads/categories/icon/JnbCHxSI1XZrvlYqNrvDu7EhAYZeW1M2aqtUs9M9.jpeg', 1, 0, '--LPG-GAS-cZ05y', 'এলপিজি গ্যাস LPG GAS', NULL, '2021-06-06 23:27:07', '2021-06-06 17:27:07');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'IndianRed', '#CD5C5C', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(2, 'LightCoral', '#F08080', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(3, 'Salmon', '#FA8072', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(4, 'DarkSalmon', '#E9967A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(5, 'LightSalmon', '#FFA07A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(6, 'Crimson', '#DC143C', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(7, 'Red', '#FF0000', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(8, 'FireBrick', '#B22222', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(9, 'DarkRed', '#8B0000', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(10, 'Pink', '#FFC0CB', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(11, 'LightPink', '#FFB6C1', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(12, 'HotPink', '#FF69B4', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(13, 'DeepPink', '#FF1493', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(14, 'MediumVioletRed', '#C71585', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(15, 'PaleVioletRed', '#DB7093', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(16, 'LightSalmon', '#FFA07A', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(17, 'Coral', '#FF7F50', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(18, 'Tomato', '#FF6347', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(19, 'OrangeRed', '#FF4500', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(20, 'DarkOrange', '#FF8C00', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(21, 'Orange', '#FFA500', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(22, 'Gold', '#FFD700', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(23, 'Yellow', '#FFFF00', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(24, 'LightYellow', '#FFFFE0', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(25, 'LemonChiffon', '#FFFACD', '2018-11-05 02:12:26', '2018-11-05 02:12:26'),
(26, 'LightGoldenrodYellow', '#FAFAD2', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(27, 'PapayaWhip', '#FFEFD5', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(28, 'Moccasin', '#FFE4B5', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(29, 'PeachPuff', '#FFDAB9', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(30, 'PaleGoldenrod', '#EEE8AA', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(31, 'Khaki', '#F0E68C', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(32, 'DarkKhaki', '#BDB76B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(33, 'Lavender', '#E6E6FA', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(34, 'Thistle', '#D8BFD8', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(35, 'Plum', '#DDA0DD', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(36, 'Violet', '#EE82EE', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(37, 'Orchid', '#DA70D6', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(38, 'Fuchsia', '#FF00FF', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(39, 'Magenta', '#FF00FF', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(40, 'MediumOrchid', '#BA55D3', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(41, 'MediumPurple', '#9370DB', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(42, 'Amethyst', '#9966CC', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(43, 'BlueViolet', '#8A2BE2', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(44, 'DarkViolet', '#9400D3', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(45, 'DarkOrchid', '#9932CC', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(46, 'DarkMagenta', '#8B008B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(47, 'Purple', '#800080', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(48, 'Indigo', '#4B0082', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(49, 'SlateBlue', '#6A5ACD', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(50, 'DarkSlateBlue', '#483D8B', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(51, 'MediumSlateBlue', '#7B68EE', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(52, 'GreenYellow', '#ADFF2F', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(53, 'Chartreuse', '#7FFF00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(54, 'LawnGreen', '#7CFC00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(55, 'Lime', '#00FF00', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(56, 'LimeGreen', '#32CD32', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(57, 'PaleGreen', '#98FB98', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(58, 'LightGreen', '#90EE90', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(59, 'MediumSpringGreen', '#00FA9A', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(60, 'SpringGreen', '#00FF7F', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(61, 'MediumSeaGreen', '#3CB371', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(62, 'SeaGreen', '#2E8B57', '2018-11-05 02:12:27', '2018-11-05 02:12:27'),
(63, 'ForestGreen', '#228B22', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(64, 'Green', '#008000', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(65, 'DarkGreen', '#006400', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(66, 'YellowGreen', '#9ACD32', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(67, 'OliveDrab', '#6B8E23', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(68, 'Olive', '#808000', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(69, 'DarkOliveGreen', '#556B2F', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(70, 'MediumAquamarine', '#66CDAA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(71, 'DarkSeaGreen', '#8FBC8F', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(72, 'LightSeaGreen', '#20B2AA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(73, 'DarkCyan', '#008B8B', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(74, 'Teal', '#008080', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(75, 'Aqua', '#00FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(76, 'Cyan', '#00FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(77, 'LightCyan', '#E0FFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(78, 'PaleTurquoise', '#AFEEEE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(79, 'Aquamarine', '#7FFFD4', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(80, 'Turquoise', '#40E0D0', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(81, 'MediumTurquoise', '#48D1CC', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(82, 'DarkTurquoise', '#00CED1', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(83, 'CadetBlue', '#5F9EA0', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(84, 'SteelBlue', '#4682B4', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(85, 'LightSteelBlue', '#B0C4DE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(86, 'PowderBlue', '#B0E0E6', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(87, 'LightBlue', '#ADD8E6', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(88, 'SkyBlue', '#87CEEB', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(89, 'LightSkyBlue', '#87CEFA', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(90, 'DeepSkyBlue', '#00BFFF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(91, 'DodgerBlue', '#1E90FF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(92, 'CornflowerBlue', '#6495ED', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(93, 'MediumSlateBlue', '#7B68EE', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(94, 'RoyalBlue', '#4169E1', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(95, 'Blue', '#0000FF', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(96, 'MediumBlue', '#0000CD', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(97, 'DarkBlue', '#00008B', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(98, 'Navy', '#000080', '2018-11-05 02:12:28', '2018-11-05 02:12:28'),
(99, 'MidnightBlue', '#191970', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(100, 'Cornsilk', '#FFF8DC', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(101, 'BlanchedAlmond', '#FFEBCD', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(102, 'Bisque', '#FFE4C4', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(103, 'NavajoWhite', '#FFDEAD', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(104, 'Wheat', '#F5DEB3', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(105, 'BurlyWood', '#DEB887', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(106, 'Tan', '#D2B48C', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(107, 'RosyBrown', '#BC8F8F', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(108, 'SandyBrown', '#F4A460', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(109, 'Goldenrod', '#DAA520', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(110, 'DarkGoldenrod', '#B8860B', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(111, 'Peru', '#CD853F', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(112, 'Chocolate', '#D2691E', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(113, 'SaddleBrown', '#8B4513', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(114, 'Sienna', '#A0522D', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(115, 'Brown', '#A52A2A', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(116, 'Maroon', '#800000', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(117, 'White', '#FFFFFF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(118, 'Snow', '#FFFAFA', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(119, 'Honeydew', '#F0FFF0', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(120, 'MintCream', '#F5FFFA', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(121, 'Azure', '#F0FFFF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(122, 'AliceBlue', '#F0F8FF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(123, 'GhostWhite', '#F8F8FF', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(124, 'WhiteSmoke', '#F5F5F5', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(125, 'Seashell', '#FFF5EE', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(126, 'Beige', '#F5F5DC', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(127, 'OldLace', '#FDF5E6', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(128, 'FloralWhite', '#FFFAF0', '2018-11-05 02:12:29', '2018-11-05 02:12:29'),
(129, 'Ivory', '#FFFFF0', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(130, 'AntiqueWhite', '#FAEBD7', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(131, 'Linen', '#FAF0E6', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(132, 'LavenderBlush', '#FFF0F5', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(133, 'MistyRose', '#FFE4E1', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(134, 'Gainsboro', '#DCDCDC', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(135, 'LightGrey', '#D3D3D3', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(136, 'Silver', '#C0C0C0', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(137, 'DarkGray', '#A9A9A9', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(138, 'Gray', '#808080', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(139, 'DimGray', '#696969', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(140, 'LightSlateGray', '#778899', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(141, 'SlateGray', '#708090', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(142, 'DarkSlateGray', '#2F4F4F', '2018-11-05 02:12:30', '2018-11-05 02:12:30'),
(143, 'Black', '#000000', '2018-11-05 02:12:30', '2018-11-05 02:12:30');

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

DROP TABLE IF EXISTS `conversations`;
CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `title` varchar(1000) COLLATE utf32_unicode_ci DEFAULT NULL,
  `sender_viewed` int(1) NOT NULL DEFAULT '1',
  `receiver_viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=297 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People\'s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'SS', 'South Sudan'),
(203, 'ES', 'Spain'),
(204, 'LK', 'Sri Lanka'),
(205, 'SH', 'St. Helena'),
(206, 'PM', 'St. Pierre and Miquelon'),
(207, 'SD', 'Sudan'),
(208, 'SR', 'Suriname'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands'),
(210, 'SZ', 'Swaziland'),
(211, 'SE', 'Sweden'),
(212, 'CH', 'Switzerland'),
(213, 'SY', 'Syrian Arab Republic'),
(214, 'TW', 'Taiwan'),
(215, 'TJ', 'Tajikistan'),
(216, 'TZ', 'Tanzania, United Republic of'),
(217, 'TH', 'Thailand'),
(218, 'TG', 'Togo'),
(219, 'TK', 'Tokelau'),
(220, 'TO', 'Tonga'),
(221, 'TT', 'Trinidad and Tobago'),
(222, 'TN', 'Tunisia'),
(223, 'TR', 'Turkey'),
(224, 'TM', 'Turkmenistan'),
(225, 'TC', 'Turks and Caicos Islands'),
(226, 'TV', 'Tuvalu'),
(227, 'UG', 'Uganda'),
(228, 'UA', 'Ukraine'),
(229, 'AE', 'United Arab Emirates'),
(230, 'GB', 'United Kingdom'),
(231, 'US', 'United States'),
(232, 'UM', 'United States minor outlying islands'),
(233, 'UY', 'Uruguay'),
(234, 'UZ', 'Uzbekistan'),
(235, 'VU', 'Vanuatu'),
(236, 'VA', 'Vatican City State'),
(237, 'VE', 'Venezuela'),
(238, 'VN', 'Vietnam'),
(239, 'VG', 'Virgin Islands (British)'),
(240, 'VI', 'Virgin Islands (U.S.)'),
(241, 'WF', 'Wallis and Futuna Islands'),
(242, 'EH', 'Western Sahara'),
(243, 'YE', 'Yemen'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe'),
(247, 'AF', 'Afghanistan'),
(248, 'AL', 'Albania'),
(249, 'DZ', 'Algeria'),
(250, 'DS', 'American Samoa'),
(251, 'AD', 'Andorra'),
(252, 'AO', 'Angola'),
(253, 'AI', 'Anguilla'),
(254, 'AQ', 'Antarctica'),
(255, 'AG', 'Antigua and Barbuda'),
(256, 'AR', 'Argentina'),
(257, 'AM', 'Armenia'),
(258, 'AW', 'Aruba'),
(259, 'AU', 'Australia'),
(260, 'AT', 'Austria'),
(261, 'AZ', 'Azerbaijan'),
(262, 'BS', 'Bahamas'),
(263, 'BH', 'Bahrain'),
(264, 'BD', 'Bangladesh'),
(265, 'BB', 'Barbados'),
(266, 'BY', 'Belarus'),
(267, 'BE', 'Belgium'),
(268, 'BZ', 'Belize'),
(269, 'BJ', 'Benin'),
(270, 'BM', 'Bermuda'),
(271, 'BT', 'Bhutan'),
(272, 'BO', 'Bolivia'),
(273, 'BA', 'Bosnia and Herzegovina'),
(274, 'BW', 'Botswana'),
(275, 'BV', 'Bouvet Island'),
(276, 'BR', 'Brazil'),
(277, 'IO', 'British Indian Ocean Territory'),
(278, 'BN', 'Brunei Darussalam'),
(279, 'BG', 'Bulgaria'),
(280, 'BF', 'Burkina Faso'),
(281, 'BI', 'Burundi'),
(282, 'KH', 'Cambodia'),
(283, 'CM', 'Cameroon'),
(284, 'CA', 'Canada'),
(285, 'CV', 'Cape Verde'),
(286, 'KY', 'Cayman Islands'),
(287, 'CF', 'Central African Republic'),
(288, 'TD', 'Chad'),
(289, 'CL', 'Chile'),
(290, 'CN', 'China'),
(291, 'CX', 'Christmas Island'),
(292, 'CC', 'Cocos (Keeling) Islands'),
(293, 'CO', 'Colombia'),
(294, 'KM', 'Comoros'),
(295, 'CG', 'Congo'),
(296, 'CK', 'Cook Islands');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

DROP TABLE IF EXISTS `coupons`;
CREATE TABLE IF NOT EXISTS `coupons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `discount` double(8,2) NOT NULL,
  `discount_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(15) NOT NULL,
  `end_date` int(15) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `type`, `code`, `details`, `discount`, `discount_type`, `start_date`, `end_date`, `created_at`, `updated_at`) VALUES
(1, 'product_base', 'ItNEw', '[{\"category_id\":\"28\",\"subcategory_id\":\"68\",\"subsubcategory_id\":\"90\",\"product_id\":\"94\"},{\"category_id\":\"28\",\"subcategory_id\":\"68\",\"subsubcategory_id\":\"90\",\"product_id\":\"94\"}]', 3.00, 'percent', 1593561600, 1594771200, '2020-06-23 07:37:38', '2020-06-23 07:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `coupon_usages`
--

DROP TABLE IF EXISTS `coupon_usages`;
CREATE TABLE IF NOT EXISTS `coupon_usages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `coupon_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `symbol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `exchange_rate` double(10,5) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '0',
  `code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `currencies`
--

INSERT INTO `currencies` (`id`, `name`, `symbol`, `exchange_rate`, `status`, `code`, `created_at`, `updated_at`) VALUES
(1, 'U.S. Dollar', '$', 1.00000, 1, 'USD', '2018-10-09 11:35:08', '2018-10-17 05:50:52'),
(2, 'Australian Dollar', '$', 1.28000, 1, 'AUD', '2018-10-09 11:35:08', '2019-02-04 05:51:55'),
(5, 'Brazilian Real', 'R$', 3.25000, 1, 'BRL', '2018-10-09 11:35:08', '2018-10-17 05:51:00'),
(6, 'Canadian Dollar', '$', 1.27000, 1, 'CAD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(7, 'Czech Koruna', 'Kč', 20.65000, 1, 'CZK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(8, 'Danish Krone', 'kr', 6.05000, 1, 'DKK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(9, 'Euro', '€', 0.85000, 1, 'EUR', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(10, 'Hong Kong Dollar', '$', 7.83000, 1, 'HKD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(11, 'Hungarian Forint', 'Ft', 255.24000, 1, 'HUF', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(12, 'Israeli New Sheqel', '₪', 3.48000, 1, 'ILS', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(13, 'Japanese Yen', '¥', 107.12000, 1, 'JPY', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(14, 'Malaysian Ringgit', 'RM', 3.91000, 1, 'MYR', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(15, 'Mexican Peso', '$', 18.72000, 1, 'MXN', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(16, 'Norwegian Krone', 'kr', 7.83000, 1, 'NOK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(17, 'New Zealand Dollar', '$', 1.38000, 1, 'NZD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(18, 'Philippine Peso', '₱', 52.26000, 1, 'PHP', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(19, 'Polish Zloty', 'zł', 3.39000, 1, 'PLN', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(20, 'Pound Sterling', '£', 0.72000, 1, 'GBP', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(21, 'Russian Ruble', 'руб', 55.93000, 1, 'RUB', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(22, 'Singapore Dollar', '$', 1.32000, 1, 'SGD', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(23, 'Swedish Krona', 'kr', 8.19000, 1, 'SEK', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(24, 'Swiss Franc', 'CHF', 0.94000, 1, 'CHF', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(26, 'Thai Baht', '฿', 31.39000, 1, 'THB', '2018-10-09 11:35:08', '2018-10-09 11:35:08'),
(27, 'Taka', 'Tk ', 84.00000, 1, 'BDT', '2018-10-09 11:35:08', '2018-12-02 05:16:13'),
(28, 'Indian Rupee', 'Rs', 68.45000, 1, 'Rupee', '2019-07-07 10:33:46', '2019-07-07 10:33:46');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `user_id`, `created_at`, `updated_at`) VALUES
(23, 28, '2020-05-18 07:27:35', '2020-05-18 07:27:35'),
(25, 30, '2020-05-24 03:30:56', '2020-05-24 03:30:56'),
(26, 31, '2020-05-24 05:36:01', '2020-05-24 05:36:01'),
(27, 33, '2020-05-24 08:22:03', '2020-05-24 08:22:03'),
(28, 34, '2020-05-24 21:40:38', '2020-05-24 21:40:38'),
(29, 35, '2020-05-24 22:24:43', '2020-05-24 22:24:43'),
(30, 37, '2020-06-02 04:19:37', '2020-06-02 04:19:37'),
(31, 38, '2020-06-04 13:36:39', '2020-06-04 13:36:39'),
(33, 40, '2020-06-12 08:01:26', '2020-06-12 08:01:26'),
(34, 42, '2020-06-15 02:22:47', '2020-06-15 02:22:47'),
(35, 43, '2020-06-16 00:39:51', '2020-06-16 00:39:51'),
(36, 44, '2020-06-18 16:08:51', '2020-06-18 16:08:51'),
(37, 45, '2020-06-30 23:06:52', '2020-06-30 23:06:52'),
(38, 46, '2020-07-07 08:12:39', '2020-07-07 08:12:39'),
(39, 47, '2020-07-08 09:44:25', '2020-07-08 09:44:25'),
(40, 48, '2020-07-08 10:59:35', '2020-07-08 10:59:35'),
(41, 49, '2020-07-08 21:45:22', '2020-07-08 21:45:22'),
(43, 51, '2020-07-10 22:15:16', '2020-07-10 22:15:16'),
(44, 52, '2020-07-11 15:54:58', '2020-07-11 15:54:58'),
(45, 54, '2020-07-17 03:13:00', '2020-07-17 03:13:00'),
(46, 55, '2020-07-17 08:17:42', '2020-07-17 08:17:42'),
(47, 56, '2020-07-17 22:35:17', '2020-07-17 22:35:17'),
(48, 57, '2020-07-18 00:10:02', '2020-07-18 00:10:02'),
(49, 58, '2020-07-18 10:02:22', '2020-07-18 10:02:22'),
(50, 59, '2020-07-18 12:20:26', '2020-07-18 12:20:26'),
(51, 60, '2020-07-20 05:30:37', '2020-07-20 05:30:37'),
(52, 61, '2020-07-21 14:21:09', '2020-07-21 14:21:09'),
(53, 62, '2020-07-21 18:46:27', '2020-07-21 18:46:27'),
(54, 63, '2020-07-25 11:19:00', '2020-07-25 11:19:00'),
(55, 64, '2020-07-25 11:21:44', '2020-07-25 11:21:44'),
(56, 65, '2020-07-25 17:00:24', '2020-07-25 17:00:24'),
(57, 66, '2020-07-25 17:30:52', '2020-07-25 17:30:52'),
(58, 67, '2020-07-29 00:58:57', '2020-07-29 00:58:57'),
(59, 68, '2020-07-29 12:43:22', '2020-07-29 12:43:22'),
(60, 69, '2020-08-04 10:32:20', '2020-08-04 10:32:20'),
(61, 70, '2020-08-09 08:41:44', '2020-08-09 08:41:44'),
(62, 71, '2020-08-09 10:29:44', '2020-08-09 10:29:44'),
(63, 72, '2020-08-11 10:40:49', '2020-08-11 10:40:49'),
(64, 73, '2020-08-12 10:47:17', '2020-08-12 10:47:17'),
(65, 74, '2020-08-12 11:22:16', '2020-08-12 11:22:16'),
(66, 75, '2020-08-13 02:43:35', '2020-08-13 02:43:35'),
(67, 76, '2020-08-13 08:02:18', '2020-08-13 08:02:18'),
(68, 77, '2020-08-14 08:21:03', '2020-08-14 08:21:03'),
(69, 78, '2020-08-14 14:32:32', '2020-08-14 14:32:32'),
(70, 79, '2020-08-14 22:42:21', '2020-08-14 22:42:21'),
(71, 80, '2020-08-18 08:14:38', '2020-08-18 08:14:38'),
(72, 81, '2020-08-18 08:32:55', '2020-08-18 08:32:55'),
(73, 82, '2020-08-18 11:52:28', '2020-08-18 11:52:28'),
(74, 84, '2020-08-20 01:06:20', '2020-08-20 01:06:20'),
(75, 85, '2020-08-20 03:06:26', '2020-08-20 03:06:26'),
(76, 86, '2020-08-20 07:56:11', '2020-08-20 07:56:11'),
(77, 87, '2020-08-20 10:43:10', '2020-08-20 10:43:10'),
(78, 88, '2020-08-20 14:17:51', '2020-08-20 14:17:51'),
(79, 89, '2020-08-27 13:06:46', '2020-08-27 13:06:46');

-- --------------------------------------------------------

--
-- Table structure for table `customer_withdraw_requests`
--

DROP TABLE IF EXISTS `customer_withdraw_requests`;
CREATE TABLE IF NOT EXISTS `customer_withdraw_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `message` longtext,
  `status` int(1) DEFAULT NULL,
  `viewed` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_withdraw_requests`
--

INSERT INTO `customer_withdraw_requests` (`id`, `user_id`, `amount`, `message`, `status`, `viewed`, `created_at`, `updated_at`) VALUES
(1, 99, 9.00, 'bkash personal\r\n01797944174', 0, 0, '2020-10-09 17:17:14', '2020-10-09 19:24:34'),
(2, 99, 10.00, 'hjj', 0, 0, '2020-10-09 22:46:54', '2020-10-09 22:46:54');

-- --------------------------------------------------------

--
-- Table structure for table `flash_deals`
--

DROP TABLE IF EXISTS `flash_deals`;
CREATE TABLE IF NOT EXISTS `flash_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` int(20) DEFAULT NULL,
  `end_date` int(20) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  `featured` int(1) NOT NULL DEFAULT '0',
  `background_color` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text_color` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'dark',
  `banner` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `flash_deal_products`
--

DROP TABLE IF EXISTS `flash_deal_products`;
CREATE TABLE IF NOT EXISTS `flash_deal_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flash_deal_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `discount` double(8,2) DEFAULT '0.00',
  `discount_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_settings`
--

DROP TABLE IF EXISTS `general_settings`;
CREATE TABLE IF NOT EXISTS `general_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `frontend_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'default',
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_background` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_login_sidebar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `favicon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google_plus` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `general_settings`
--

INSERT INTO `general_settings` (`id`, `frontend_color`, `logo`, `admin_logo`, `admin_login_background`, `admin_login_sidebar`, `favicon`, `site_name`, `address`, `description`, `phone`, `email`, `facebook`, `instagram`, `twitter`, `youtube`, `google_plus`, `created_at`, `updated_at`) VALUES
(1, 'default', 'uploads/logo/7Nc3hjaeZBVt2MSBzm0AI2ci11AbyYcYQnSBolaJ.png', 'uploads/admin_logo/gsCexzb2cCBflz0Ka1CIL5g51MLsvQP2CEXQOBEJ.png', 'uploads/admin_login_background/pRkHXkXYDGWovpFPHyHTbycdrvIyd3xRPydZPyc8.png', 'uploads/admin_login_sidebar/lp1NBYXGYgXzoKdUMyNoJqreX0YrInlagxwYBDMi.png', 'uploads/favicon/hXnFZfMSdy4S5JABnE8JYQoobQWdw5ZhlNna0p5F.png', 'GhorerBazar', 'Chandpur, Bangladesh.', 'Best online market', '01842210999', 'mail@ghorerbazar.com', 'https://www.facebook.com', 'https://www.instagram.com', 'https://www.twitter.com', 'https://www.youtube.com', 'https://www.googleplus.com', '2020-09-03 16:53:03', '2020-09-03 10:53:03');

-- --------------------------------------------------------

--
-- Table structure for table `home_categories`
--

DROP TABLE IF EXISTS `home_categories`;
CREATE TABLE IF NOT EXISTS `home_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `subsubcategories` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `home_categories`
--

INSERT INTO `home_categories` (`id`, `category_id`, `subsubcategories`, `status`, `created_at`, `updated_at`) VALUES
(9, 12, '[\"74\"]', 0, '2020-05-13 01:43:42', '2020-06-08 02:12:51'),
(10, 13, '[\"75\"]', 0, '2020-05-13 23:22:51', '2020-06-02 01:35:27'),
(11, 32, '[\"94\"]', 1, '2020-05-13 23:23:06', '2020-05-13 23:23:06'),
(14, 29, '[\"91\"]', 1, '2020-05-13 23:23:53', '2020-05-13 23:23:53'),
(15, 30, '[\"92\"]', 1, '2020-05-13 23:24:12', '2020-05-13 23:24:12'),
(16, 26, '[\"88\"]', 1, '2020-05-13 23:24:31', '2020-05-13 23:24:31'),
(17, 27, '[\"89\"]', 1, '2020-05-13 23:24:51', '2020-05-13 23:24:51'),
(18, 28, '[\"90\"]', 1, '2020-05-13 23:25:08', '2020-05-13 23:25:08'),
(19, 18, '[\"80\"]', 1, '2020-05-13 23:25:29', '2020-05-13 23:25:29'),
(20, 22, '[\"84\"]', 1, '2020-05-13 23:26:00', '2020-05-13 23:26:00'),
(21, 14, '[\"76\"]', 0, '2020-05-13 23:27:26', '2020-06-02 01:37:17'),
(22, 15, '[\"77\"]', 1, '2020-05-13 23:27:37', '2020-05-13 23:27:37'),
(23, 16, '[\"78\"]', 1, '2020-05-13 23:27:51', '2020-05-13 23:27:51'),
(24, 17, '[\"79\"]', 1, '2020-05-13 23:28:00', '2020-05-13 23:28:00'),
(25, 19, '[\"81\"]', 1, '2020-05-13 23:28:09', '2020-05-13 23:28:09'),
(26, 20, '[\"82\"]', 1, '2020-05-13 23:28:21', '2020-05-13 23:28:21'),
(27, 21, '[\"83\"]', 1, '2020-05-13 23:28:30', '2020-05-13 23:28:30'),
(28, 23, '[\"85\"]', 1, '2020-05-13 23:28:37', '2020-05-13 23:28:37'),
(29, 24, '[\"86\"]', 1, '2020-05-13 23:28:45', '2020-05-13 23:28:45'),
(30, 25, '[\"87\"]', 1, '2020-05-13 23:28:52', '2020-05-13 23:28:52'),
(31, 31, '[\"93\"]', 1, '2020-05-13 23:28:59', '2020-05-13 23:28:59'),
(32, 33, '[\"95\"]', 1, '2020-06-08 02:13:07', '2020-06-08 02:13:07'),
(33, 34, '[\"96\"]', 1, '2020-06-18 00:15:47', '2020-06-18 00:15:47');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rtl` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `code`, `rtl`, `created_at`, `updated_at`) VALUES
(1, 'English', 'en', 0, '2019-01-20 12:13:20', '2020-05-10 03:11:58'),
(3, 'Bangla', 'bd', 0, '2019-02-17 06:35:37', '2020-05-10 03:11:58'),
(4, 'Arabic', 'sa', 1, '2019-04-28 18:34:12', '2020-05-09 22:29:32');

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

DROP TABLE IF EXISTS `links`;
CREATE TABLE IF NOT EXISTS `links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `name`, `url`, `created_at`, `updated_at`) VALUES
(1, 'ঘরের বাজার অ্যাপস', 'https://mega.nz/file/j1RAQaAK#-YXbd0NQdlCFHCCg8E5wS14a2VgDElt-S9PHbAaOwC0', '2020-09-11 05:54:04', '2020-09-10 23:54:04'),
(2, 'ঘরের বাজার ফেসবুক পেইজ', 'https://www.facebook.com/ghorerbazarbangladesh/', '2020-06-22 15:10:34', '2020-06-22 09:10:34'),
(3, 'মোবাইল রিচার্জ', 'http://boss.com.bd/rtc/bdlr.html', '2020-06-23 04:41:17', '2020-06-23 04:41:17');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conversation_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text COLLATE utf32_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `guest_id` int(11) DEFAULT NULL,
  `shipping_address` longtext COLLATE utf8_unicode_ci,
  `payment_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'unpaid',
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `grand_total` double(8,2) DEFAULT NULL,
  `coupon_discount` double(8,2) NOT NULL DEFAULT '0.00',
  `code` mediumtext COLLATE utf8_unicode_ci,
  `date` int(20) NOT NULL,
  `viewed` int(1) NOT NULL DEFAULT '0',
  `delivery_viewed` int(1) NOT NULL DEFAULT '0',
  `payment_status_viewed` int(1) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=264 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `guest_id`, `shipping_address`, `payment_type`, `payment_status`, `payment_details`, `grand_total`, `coupon_discount`, `code`, `date`, `viewed`, `delivery_viewed`, `payment_status_viewed`, `created_at`, `updated_at`) VALUES
(111, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'paid', NULL, 80.00, 0.00, '20200606-06333288', 1591425212, 1, 0, 0, '2020-06-06 00:33:32', '2020-06-06 00:47:38'),
(118, NULL, 742205, '{\"name\":\"alamin\",\"email\":\"ghorerbazar24hour@gmail.com\",\"address\":\"Mohamaya\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"guest\"}', 'cash_on_delivery', 'unpaid', NULL, 1125.00, 0.00, '20200618-06303348', 1592461833, 1, 0, 0, '2020-06-18 00:30:33', '2020-06-18 00:35:28'),
(123, 78, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'cash_on_delivery', 'unpaid', NULL, 50.00, 0.00, '20200814-20362583', 1597437385, 1, 0, 0, '2020-08-14 14:36:25', '2020-08-21 19:19:52'),
(125, NULL, 419533, '{\"name\":\"Raju\",\"email\":\"mdmanwar.raju@gmail.com\",\"address\":\"House #A 12(B3), block#A, Extension Pallabi, Mirpur 11.5, Dhaka\",\"country\":\"Bangladesh\",\"city\":\"Dhaka\",\"postal_code\":\"1216\",\"phone\":\"01747862015\",\"checkout_type\":\"guest\"}', 'cash_on_delivery', 'unpaid', NULL, 748.00, 0.00, '20200915-06335178', 1600151631, 1, 0, 0, '2020-09-15 00:33:51', '2020-09-20 00:00:54'),
(126, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20200922-13230416', 1600780984, 1, 0, 0, '2020-09-22 07:23:04', '2020-09-22 07:24:53'),
(127, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13231461', 1600780994, 0, 0, 0, '2020-09-22 07:23:14', '2020-09-22 07:23:14'),
(128, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13231574', 1600780995, 0, 0, 0, '2020-09-22 07:23:15', '2020-09-22 07:23:15'),
(129, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13231527', 1600780995, 0, 0, 0, '2020-09-22 07:23:15', '2020-09-22 07:23:15'),
(130, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13232890', 1600781008, 0, 0, 0, '2020-09-22 07:23:28', '2020-09-22 07:23:29'),
(131, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13232944', 1600781009, 0, 0, 0, '2020-09-22 07:23:29', '2020-09-22 07:23:29'),
(132, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233056', 1600781010, 0, 0, 0, '2020-09-22 07:23:30', '2020-09-22 07:23:30'),
(133, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233049', 1600781010, 0, 0, 0, '2020-09-22 07:23:30', '2020-09-22 07:23:30'),
(134, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233091', 1600781010, 0, 0, 0, '2020-09-22 07:23:30', '2020-09-22 07:23:30'),
(135, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233050', 1600781010, 0, 0, 0, '2020-09-22 07:23:30', '2020-09-22 07:23:30'),
(136, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233865', 1600781018, 0, 0, 0, '2020-09-22 07:23:38', '2020-09-22 07:23:38'),
(137, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233890', 1600781018, 0, 0, 0, '2020-09-22 07:23:38', '2020-09-22 07:23:38'),
(138, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13233978', 1600781019, 0, 0, 0, '2020-09-22 07:23:39', '2020-09-22 07:23:39'),
(139, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234090', 1600781020, 0, 0, 0, '2020-09-22 07:23:40', '2020-09-22 07:23:40'),
(140, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234050', 1600781020, 0, 0, 0, '2020-09-22 07:23:40', '2020-09-22 07:23:40'),
(141, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234199', 1600781021, 0, 0, 0, '2020-09-22 07:23:41', '2020-09-22 07:23:41'),
(142, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234291', 1600781022, 0, 0, 0, '2020-09-22 07:23:42', '2020-09-22 07:23:42'),
(143, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234361', 1600781023, 0, 0, 0, '2020-09-22 07:23:43', '2020-09-22 07:23:43'),
(144, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234353', 1600781023, 0, 0, 0, '2020-09-22 07:23:43', '2020-09-22 07:23:43'),
(145, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234379', 1600781023, 0, 0, 0, '2020-09-22 07:23:43', '2020-09-22 07:23:43'),
(146, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234321', 1600781023, 0, 0, 0, '2020-09-22 07:23:43', '2020-09-22 07:23:43'),
(147, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234477', 1600781024, 0, 0, 0, '2020-09-22 07:23:44', '2020-09-22 07:23:44'),
(148, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234425', 1600781024, 0, 0, 0, '2020-09-22 07:23:44', '2020-09-22 07:23:44'),
(149, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13234696', 1600781026, 0, 0, 0, '2020-09-22 07:23:46', '2020-09-22 07:23:46'),
(150, 28, NULL, '{\"name\":\"Sumon\",\"email\":\"ghorerbazar.info@gmail.com\",\"address\":\"Chandpur\",\"country\":\"Bangladesh\",\"city\":\"Chandpur\",\"postal_code\":\"3600\",\"phone\":\"01771162553\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 0.00, 0.00, '20200922-13235830', 1600781038, 0, 0, 0, '2020-09-22 07:23:58', '2020-09-22 07:23:58'),
(152, 98, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20201004-10283054', 1601807310, 0, 0, 0, '2020-10-04 15:28:30', '2020-10-04 15:28:30'),
(155, 98, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"karim@gmail.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01712345678\",\"checkout_type\":\"logged\"}', 'cash', 'paid', NULL, 132.00, 0.00, '20201004-13040856', 1601816648, 0, 0, 0, '2020-10-04 18:04:08', '2020-10-04 18:04:47'),
(156, 100, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 70.00, 0.00, '20201026-16572826', 1603731448, 0, 1, 1, '2020-10-26 21:57:28', '2020-10-26 21:58:35'),
(157, 100, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'wallet', 'unpaid', NULL, 160.00, 0.00, '20201026-19582993', 1603742309, 0, 0, 0, '2020-10-27 00:58:29', '2020-10-27 00:58:29'),
(158, 100, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'wallet', 'unpaid', NULL, 160.00, 0.00, '20201026-20005246', 1603742452, 0, 0, 0, '2020-10-27 01:00:52', '2020-10-27 01:00:52'),
(159, 100, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'wallet', 'paid', NULL, 160.00, 0.00, '20201026-20185876', 1603743538, 0, 0, 0, '2020-10-27 01:18:58', '2020-10-27 01:19:14'),
(160, 100, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'cash_on_delivery', 'unpaid', NULL, 342.00, 0.00, '20201026-20212611', 1603743686, 0, 0, 0, '2020-10-27 01:21:26', '2020-10-27 01:21:26'),
(161, 100, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 80.00, 0.00, '20201026-20511923', 1603745479, 0, 0, 0, '2020-10-27 01:51:19', '2020-10-27 01:51:43'),
(162, 100, NULL, 'null', 'wallet', 'paid', NULL, 25.00, 0.00, '20201026-20591722', 1603745957, 0, 1, 1, '2020-10-27 01:59:17', '2020-10-27 02:13:55'),
(163, 100, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'cash_on_delivery', 'unpaid', NULL, 150.00, 0.00, '20201026-21165495', 1603747014, 0, 1, 1, '2020-10-27 02:16:54', '2020-10-27 02:18:03'),
(164, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 90.00, 0.00, '20201026-21453350', 1603748733, 0, 1, 1, '2020-10-27 02:45:33', '2020-10-27 02:46:24'),
(165, 101, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'cash_on_delivery', 'unpaid', NULL, 20.00, 0.00, '20201026-21482021', 1603748900, 0, 0, 0, '2020-10-27 02:48:20', '2020-10-27 02:48:20'),
(166, 101, NULL, '{\"name\":null,\"email\":null,\"address\":null,\"country\":null,\"city\":null,\"postal_code\":null,\"phone\":null,\"checkout_type\":null}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20201026-21595782', 1603749597, 0, 0, 0, '2020-10-27 02:59:57', '2020-10-27 02:59:57'),
(167, 101, NULL, '\"{\\\"name\\\":\\\"Annalise aaaaaaa\\\",\\\"email\\\":\\\"abubased@outlook.com\\\",\\\"address\\\":\\\"ssssssss\\\",\\\"country\\\":\\\"Bangladesh\\\",\\\"city\\\":\\\"ssssssss\\\",\\\"postal_code\\\":\\\"5740\\\",\\\"phone\\\":\\\"01797944174\\\",\\\"checkout_type\\\":\\\"logged\\\"}\"', 'cash_on_delivery', 'unpaid', NULL, 30.00, 0.00, '20201026-22142860', 1603750468, 0, 0, 0, '2020-10-27 03:14:28', '2020-10-27 03:14:28'),
(168, 101, NULL, '\"{\\\"name\\\":\\\"Annalise aaaaaaa\\\",\\\"email\\\":\\\"abubased@outlook.com\\\",\\\"address\\\":\\\"ssssssss\\\",\\\"country\\\":\\\"Bangladesh\\\",\\\"city\\\":\\\"ssssssss\\\",\\\"postal_code\\\":\\\"5740\\\",\\\"phone\\\":\\\"01797944174\\\",\\\"checkout_type\\\":\\\"logged\\\"}\"', 'cash_on_delivery', 'unpaid', NULL, 30.00, 0.00, '20201026-22202552', 1603750825, 0, 1, 1, '2020-10-27 03:20:25', '2020-10-27 03:20:53'),
(169, 101, NULL, '{\"name\":\"Annalise DDDDD\",\"email\":\"sasfass@outlook.com\",\"address\":\"ssssssssasfdasdf\",\"country\":\"Bangladesh\",\"city\":\"ssssssssdsaf\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 180.00, 0.00, '20201026-22230343', 1603750983, 0, 1, 1, '2020-10-27 03:23:03', '2020-10-27 03:25:02'),
(170, 101, NULL, '{\"name\":\"Md abu Based\",\"email\":\"abubased@customer.com\",\"address\":\"dfdfsgada\",\"country\":\"Bangladesh\",\"city\":\"dhaka\",\"postal_code\":\"5740\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 1125.00, 0.00, '20210511-09014820', 1620723708, 0, 1, 1, '2021-05-11 03:01:48', '2021-05-11 03:02:55'),
(171, 98, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 280.00, 0.00, '20210511-12374397', 1620736663, 1, 0, 0, '2021-05-11 06:37:43', '2021-05-11 06:38:20'),
(172, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', NULL, 'unpaid', NULL, 600.00, 0.00, '20210513-00242514', 1620865465, 0, 0, 0, '2021-05-12 18:24:25', '2021-05-12 18:24:25'),
(173, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', NULL, 'unpaid', NULL, 600.00, 0.00, '20210513-00281282', 1620865692, 0, 0, 0, '2021-05-12 18:28:12', '2021-05-12 18:28:12'),
(174, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', NULL, 'unpaid', NULL, 600.00, 0.00, '20210513-00311348', 1620865873, 0, 0, 0, '2021-05-12 18:31:13', '2021-05-12 18:31:13'),
(175, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', NULL, 'unpaid', NULL, 600.00, 0.00, '20210513-00372314', 1620866243, 0, 0, 0, '2021-05-12 18:37:23', '2021-05-12 18:37:23'),
(176, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', NULL, 'unpaid', NULL, 600.00, 0.00, '20210513-00401978', 1620866419, 0, 0, 0, '2021-05-12 18:40:19', '2021-05-12 18:40:19'),
(177, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', NULL, 'unpaid', NULL, 90.00, 0.00, '20210518-17581183', 1621360691, 0, 0, 0, '2021-05-18 11:58:11', '2021-05-18 11:58:11'),
(178, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'bkash', 'paid', NULL, 170.00, 0.00, '20210518-18312881', 1621362688, 0, 0, 0, '2021-05-18 12:31:28', '2021-05-18 12:31:36'),
(179, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'rocket', 'paid', NULL, 80.00, 0.00, '20210518-18401158', 1621363211, 0, 0, 0, '2021-05-18 12:40:11', '2021-05-18 12:40:18'),
(180, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20210518-18443447', 1621363474, 0, 0, 0, '2021-05-18 12:44:34', '2021-05-18 12:44:34'),
(181, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"sss@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20210528-06364438', 1622183804, 0, 0, 0, '2021-05-28 00:36:44', '2021-05-28 00:36:44'),
(182, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"sss@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 240.00, 0.00, '20210531-22005439', 1622498454, 0, 0, 0, '2021-05-31 16:00:54', '2021-05-31 16:00:54'),
(183, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"sss@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 240.00, 0.00, '20210531-22031969', 1622498599, 0, 0, 0, '2021-05-31 16:03:19', '2021-05-31 16:03:20'),
(184, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"sss@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'paypal', 'unpaid', NULL, 240.00, 0.00, '20210531-22033267', 1622498612, 0, 0, 0, '2021-05-31 16:03:32', '2021-05-31 16:03:32'),
(185, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-07542488', 1622534064, 0, 0, 0, '2021-06-01 01:54:24', '2021-06-01 01:54:24'),
(186, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-08333147', 1622536411, 0, 0, 0, '2021-06-01 02:33:31', '2021-06-01 02:33:31'),
(187, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-08372090', 1622536640, 0, 0, 0, '2021-06-01 02:37:20', '2021-06-01 02:37:20'),
(188, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-18373225', 1622572652, 0, 0, 0, '2021-06-01 12:37:32', '2021-06-01 12:37:32'),
(189, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-18504321', 1622573443, 0, 0, 0, '2021-06-01 12:50:43', '2021-06-01 12:50:43'),
(190, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210601-18572517', 1622573845, 0, 0, 0, '2021-06-01 12:57:25', '2021-06-01 12:57:33'),
(191, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 45.00, 0.00, '20210601-21471647', 1622584036, 0, 0, 0, '2021-06-01 15:47:16', '2021-06-01 15:47:27'),
(192, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210601-21481050', 1622584090, 0, 0, 0, '2021-06-01 15:48:10', '2021-06-01 15:48:18'),
(193, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 240.00, 0.00, '20210601-21493871', 1622584178, 0, 0, 0, '2021-06-01 15:49:38', '2021-06-01 15:49:47'),
(194, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-21505434', 1622584254, 0, 0, 0, '2021-06-01 15:50:54', '2021-06-01 15:50:54'),
(195, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22073630', 1622585256, 0, 0, 0, '2021-06-01 16:07:36', '2021-06-01 16:07:36'),
(196, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22073770', 1622585257, 0, 0, 0, '2021-06-01 16:07:37', '2021-06-01 16:07:37'),
(197, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22074336', 1622585263, 0, 0, 0, '2021-06-01 16:07:43', '2021-06-01 16:07:43'),
(198, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22074476', 1622585264, 0, 0, 0, '2021-06-01 16:07:44', '2021-06-01 16:07:44'),
(199, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22074458', 1622585264, 0, 0, 0, '2021-06-01 16:07:44', '2021-06-01 16:07:44'),
(200, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22074452', 1622585264, 0, 0, 0, '2021-06-01 16:07:44', '2021-06-01 16:07:45'),
(201, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 80.00, 0.00, '20210601-22074547', 1622585265, 0, 0, 0, '2021-06-01 16:07:45', '2021-06-01 16:08:05'),
(202, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 80.00, 0.00, '20210601-22075850', 1622585278, 0, 0, 0, '2021-06-01 16:07:58', '2021-06-01 16:08:16'),
(203, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 80.00, 0.00, '20210601-22075982', 1622585279, 0, 0, 0, '2021-06-01 16:07:59', '2021-06-01 16:08:16'),
(204, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 80.00, 0.00, '20210601-22075936', 1622585279, 0, 0, 0, '2021-06-01 16:07:59', '2021-06-01 16:08:16'),
(205, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 80.00, 0.00, '20210601-22080059', 1622585280, 0, 0, 0, '2021-06-01 16:08:00', '2021-06-01 16:08:16'),
(206, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'unpaid', NULL, 80.00, 0.00, '20210601-22080147', 1622585281, 0, 0, 0, '2021-06-01 16:08:01', '2021-06-01 16:08:01'),
(207, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'unpaid', NULL, 80.00, 0.00, '20210601-22080226', 1622585282, 0, 0, 0, '2021-06-01 16:08:02', '2021-06-01 16:08:02'),
(208, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'wallet', 'unpaid', NULL, 80.00, 0.00, '20210601-22080246', 1622585282, 0, 0, 0, '2021-06-01 16:08:02', '2021-06-01 16:08:02'),
(209, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 0.00, 0.00, '20210601-22101853', 1622585418, 0, 0, 0, '2021-06-01 16:10:18', '2021-06-01 16:10:18'),
(210, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-22285569', 1622586535, 0, 0, 0, '2021-06-01 16:28:55', '2021-06-01 16:28:55'),
(211, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-23012650', 1622588486, 0, 0, 0, '2021-06-01 17:01:26', '2021-06-01 17:01:26'),
(212, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210601-23204178', 1622589641, 0, 0, 0, '2021-06-01 17:20:41', '2021-06-01 17:20:41'),
(213, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210602-05593587', 1622613575, 0, 0, 0, '2021-06-01 23:59:35', '2021-06-01 23:59:36'),
(214, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210602-20024054', 1622664160, 0, 0, 0, '2021-06-02 14:02:40', '2021-06-02 14:02:40'),
(215, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210602-20134680', 1622664826, 0, 0, 0, '2021-06-02 14:13:46', '2021-06-02 14:13:54'),
(216, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210602-20255745', 1622665557, 0, 0, 0, '2021-06-02 14:25:57', '2021-06-02 14:26:05'),
(217, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210602-20282937', 1622665709, 0, 0, 0, '2021-06-02 14:28:29', '2021-06-02 14:28:37'),
(218, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210602-20351799', 1622666117, 0, 0, 0, '2021-06-02 14:35:17', '2021-06-02 14:35:17'),
(219, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 500.00, 0.00, '20210603-12094991', 1622722189, 0, 0, 0, '2021-06-03 06:09:49', '2021-06-03 06:10:02'),
(220, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210603-12282124', 1622723301, 0, 0, 0, '2021-06-03 06:28:21', '2021-06-03 06:28:32'),
(221, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210603-12300421', 1622723404, 0, 0, 0, '2021-06-03 06:30:04', '2021-06-03 06:30:14'),
(222, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210603-12402825', 1622724028, 0, 0, 0, '2021-06-03 06:40:28', '2021-06-03 06:40:37'),
(223, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20210607-20202549', 1623097225, 1, 0, 0, '2021-06-07 14:20:25', '2021-06-07 14:22:14'),
(224, 101, NULL, '{\"name\":\"Md abu Based\",\"email\":\"abubased@customer.com\",\"address\":\"sadfa\",\"country\":\"Afghanistan\",\"city\":\"sdas\",\"postal_code\":\"5455\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'wallet', 'paid', NULL, 25.00, 0.00, '20210607-20262157', 1623097581, 1, 0, 0, '2021-06-07 14:26:21', '2021-06-07 14:27:12'),
(225, 101, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 114.00, 0.00, '20210608-22595194', 1623193191, 1, 0, 0, '2021-06-08 16:59:51', '2021-06-08 17:33:20'),
(226, 100, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubasedjibon@outlook.com\",\"address\":\"afa\",\"country\":\"Bangladesh\",\"city\":\"asdfa\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 1200.00, 0.00, '20210609-01225038', 1623201770, 0, 0, 0, '2021-06-08 19:22:50', '2021-06-08 19:22:57'),
(227, 100, NULL, '{\"name\":\"Annalise aaaaaaa\",\"email\":\"abubased@outlook.com\",\"address\":\"ssssssss\",\"country\":\"Bangladesh\",\"city\":\"ssssssss\",\"postal_code\":\"5740\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 110.00, 0.00, '20210609-01323134', 1623202351, 0, 0, 0, '2021-06-08 19:32:31', '2021-06-08 19:32:38'),
(228, 101, NULL, 'null', 'shurjopay', 'unpaid', NULL, NULL, 0.00, '20210609-02450532', 1623206705, 0, 0, 0, '2021-06-08 20:45:05', '2021-06-08 20:45:05'),
(229, 101, NULL, 'null', 'shurjopay', 'unpaid', NULL, NULL, 0.00, '20210609-02473815', 1623206858, 0, 0, 0, '2021-06-08 20:47:38', '2021-06-08 20:47:38'),
(230, 101, NULL, 'null', 'shurjopay', 'unpaid', NULL, NULL, 0.00, '20210609-11501170', 1623239411, 0, 0, 0, '2021-06-09 05:50:11', '2021-06-09 05:50:11'),
(231, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210610-13355926', 1623332159, 0, 0, 0, '2021-06-10 07:35:59', '2021-06-10 07:35:59'),
(232, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14463997', 1623336399, 0, 0, 0, '2021-06-10 08:46:39', '2021-06-10 08:46:47'),
(233, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14493258', 1623336572, 0, 0, 0, '2021-06-10 08:49:32', '2021-06-10 08:49:41'),
(234, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14494135', 1623336581, 0, 0, 0, '2021-06-10 08:49:41', '2021-06-10 08:49:52'),
(235, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14494653', 1623336586, 0, 0, 0, '2021-06-10 08:49:46', '2021-06-10 08:49:57'),
(236, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'unpaid', NULL, 80.00, 0.00, '20210610-14495627', 1623336596, 0, 0, 0, '2021-06-10 08:49:56', '2021-06-10 08:49:56'),
(237, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14523052', 1623336750, 0, 0, 0, '2021-06-10 08:52:30', '2021-06-10 08:52:37'),
(238, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14570961', 1623337029, 0, 0, 0, '2021-06-10 08:57:09', '2021-06-10 08:57:17'),
(239, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14573657', 1623337056, 0, 0, 0, '2021-06-10 08:57:36', '2021-06-10 08:57:44'),
(240, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-14591359', 1623337153, 0, 0, 0, '2021-06-10 08:59:13', '2021-06-10 08:59:21'),
(241, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15001831', 1623337218, 0, 0, 0, '2021-06-10 09:00:18', '2021-06-10 09:00:25'),
(242, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15030445', 1623337384, 0, 0, 0, '2021-06-10 09:03:04', '2021-06-10 09:03:11'),
(243, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15050863', 1623337508, 0, 0, 0, '2021-06-10 09:05:08', '2021-06-10 09:05:15'),
(244, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15063237', 1623337592, 0, 0, 0, '2021-06-10 09:06:32', '2021-06-10 09:06:41'),
(245, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15081634', 1623337696, 0, 0, 0, '2021-06-10 09:08:16', '2021-06-10 09:08:24'),
(246, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15150573', 1623338105, 0, 0, 0, '2021-06-10 09:15:05', '2021-06-10 09:15:13'),
(247, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15181113', 1623338291, 0, 0, 0, '2021-06-10 09:18:11', '2021-06-10 09:18:19'),
(248, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-15364352', 1623339403, 1, 0, 0, '2021-06-10 09:36:43', '2021-06-10 09:40:08'),
(249, 101, NULL, '{\"name\":\"Sadia Maisa\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Bogura\",\"country\":\"Bangladesh\",\"city\":\"Bogura\",\"postal_code\":\"5880\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 160.00, 0.00, '20210610-15414932', 1623339709, 0, 0, 0, '2021-06-10 09:41:49', '2021-06-10 09:41:58'),
(250, 101, NULL, '{\"name\":\"Md Heru prodhan\",\"email\":\"abubased@customer.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 160.00, 0.00, '20210610-15462283', 1623339982, 0, 0, 0, '2021-06-10 09:46:22', '2021-06-10 09:46:31'),
(251, 101, NULL, '{\"name\":\"Md Heru prodhan\",\"email\":\"abubased@customer.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 160.00, 0.00, '20210610-15473666', 1623340056, 0, 0, 0, '2021-06-10 09:47:36', '2021-06-10 09:47:45'),
(252, 101, NULL, '{\"name\":\"Md Heru prodhan\",\"email\":\"abubased@customer.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 160.00, 0.00, '20210610-15501662', 1623340216, 1, 0, 0, '2021-06-10 09:50:16', '2021-06-10 09:51:17'),
(253, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 160.00, 0.00, '20210610-15561957', 1623340579, 0, 0, 0, '2021-06-10 09:56:19', '2021-06-10 09:56:28'),
(254, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-16024918', 1623340969, 0, 0, 0, '2021-06-10 10:02:49', '2021-06-10 10:02:59'),
(255, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-16090428', 1623341344, 0, 0, 0, '2021-06-10 10:09:04', '2021-06-10 10:09:14'),
(256, 101, NULL, '{\"name\":\"Md Heru prodhan\",\"email\":\"abubased@customer.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-16295623', 1623342596, 0, 0, 0, '2021-06-10 10:29:56', '2021-06-10 10:30:04'),
(257, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-16385327', 1623343133, 0, 0, 0, '2021-06-10 10:38:53', '2021-06-10 10:39:04'),
(258, 101, NULL, '{\"name\":\"Md Abu Based\",\"email\":\"abubased@outlook.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"0179794474\",\"checkout_type\":\"logged\"}', 'cash_on_delivery', 'unpaid', NULL, 80.00, 0.00, '20210610-17211885', 1623345678, 0, 0, 0, '2021-06-10 11:21:18', '2021-06-10 11:21:18'),
(259, 101, NULL, '{\"name\":\"Abu Based Jibon\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01862996647\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-17231269', 1623345792, 0, 0, 0, '2021-06-10 11:23:12', '2021-06-10 11:23:21'),
(260, 101, NULL, '{\"name\":\"Md Heru prodhan\",\"email\":\"abubased@customer.com\",\"address\":\"Tajpur,Bishubari,Gobindaganj\",\"country\":\"Bangladesh\",\"city\":\"Gaibandha\",\"postal_code\":\"5740\",\"phone\":\"01797944114\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-17383358', 1623346713, 0, 0, 0, '2021-06-10 11:38:33', '2021-06-10 11:38:42');
INSERT INTO `orders` (`id`, `user_id`, `guest_id`, `shipping_address`, `payment_type`, `payment_status`, `payment_details`, `grand_total`, `coupon_discount`, `code`, `date`, `viewed`, `delivery_viewed`, `payment_status_viewed`, `created_at`, `updated_at`) VALUES
(261, 101, NULL, '{\"name\":\"Sadia Maisa\",\"email\":\"jibonerasa9@gmail.com\",\"address\":\"Bogura\",\"country\":\"Bangladesh\",\"city\":\"Bogura\",\"postal_code\":\"5880\",\"phone\":\"01797944174\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-17431247', 1623346992, 0, 0, 0, '2021-06-10 11:43:12', '2021-06-10 11:43:20'),
(262, 101, NULL, '{\"name\":\"Md Arman\",\"email\":\"info@weblancerbd.com\",\"address\":\"Chittagong\",\"country\":\"Bangladesh\",\"city\":\"Hathazari\",\"postal_code\":\"4337\",\"phone\":\"01828335781\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-17452721', 1623347127, 0, 0, 0, '2021-06-10 11:45:27', '2021-06-10 11:45:35'),
(263, 101, NULL, '{\"name\":\"Md Arman\",\"email\":\"info@weblancerbd.com\",\"address\":\"Chittagong\",\"country\":\"Bangladesh\",\"city\":\"Hathazari\",\"postal_code\":\"4337\",\"phone\":\"01828335781\",\"checkout_type\":\"logged\"}', 'shurjopay', 'paid', NULL, 80.00, 0.00, '20210610-17465852', 1623347218, 0, 0, 0, '2021-06-10 11:46:58', '2021-06-10 11:47:06');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
CREATE TABLE IF NOT EXISTS `order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `seller_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `variation` longtext COLLATE utf8_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `tax` double(8,2) NOT NULL DEFAULT '0.00',
  `shipping_cost` double(8,2) NOT NULL DEFAULT '0.00',
  `quantity` int(11) DEFAULT NULL,
  `payment_status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unpaid',
  `delivery_status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'pending',
  `shipping_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pickup_point_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `seller_id`, `product_id`, `variation`, `price`, `tax`, `shipping_cost`, `quantity`, `payment_status`, `delivery_status`, `shipping_type`, `pickup_point_id`, `created_at`, `updated_at`) VALUES
(76, 111, 36, 98, NULL, 80.00, 0.00, 0.00, 1, 'paid', 'delivered', 'home_delivery', NULL, '2020-06-06 00:33:32', '2020-06-06 00:49:52'),
(83, 118, 1, 140, '১২কেজি', 1125.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-06-18 00:30:33', '2020-06-18 00:30:33'),
(88, 123, 1, 153, '১কেজি', 50.00, 0.00, 0.00, 1, 'unpaid', 'on_review', 'home_delivery', NULL, '2020-08-14 14:36:25', '2020-09-20 00:03:48'),
(90, 125, 1, 48, '1kg', 96.00, 0.00, 0.00, 3, 'unpaid', 'on_review', 'home_delivery', NULL, '2020-09-15 00:33:51', '2020-09-20 00:04:18'),
(91, 125, 1, 71, '1kg', 72.00, 0.00, 0.00, 1, 'unpaid', 'on_review', 'home_delivery', NULL, '2020-09-15 00:33:51', '2020-09-20 00:04:18'),
(92, 125, 1, 126, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'on_review', 'home_delivery', NULL, '2020-09-15 00:33:51', '2020-09-20 00:04:18'),
(93, 125, 1, 47, '1kg', 500.00, 0.00, 0.00, 10, 'unpaid', 'on_review', 'home_delivery', NULL, '2020-09-15 00:33:51', '2020-09-20 00:04:18'),
(94, 126, 1, 126, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-09-22 07:23:04', '2020-09-22 07:23:04'),
(95, 151, 1, 97, '1kg', 590.00, 0.00, 10.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-02 00:50:07', '2020-10-02 00:50:50'),
(96, 152, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-04 15:28:30', '2020-10-04 15:28:30'),
(97, 153, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'pickup_point', 1, '2020-10-04 17:11:16', '2020-10-04 17:11:16'),
(98, 154, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'pickup_point', 1, '2020-10-04 17:12:18', '2020-10-04 17:12:52'),
(99, 155, 1, 91, '10g', 52.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-04 18:04:08', '2020-10-04 18:04:47'),
(100, 155, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-04 18:04:08', '2020-10-04 18:04:47'),
(101, 156, 1, 129, '৫০টিব্যাগ', 70.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-26 21:57:28', '2020-10-26 21:58:08'),
(102, 157, 1, 129, '৫০টিব্যাগ', 70.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 00:58:29', '2020-10-27 00:58:29'),
(103, 157, 1, 104, '500ml', 90.00, 0.00, 0.00, 3, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 00:58:29', '2020-10-27 00:58:29'),
(104, 158, 1, 129, '৫০টিব্যাগ', 70.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 01:00:52', '2020-10-27 01:00:52'),
(105, 158, 1, 104, '500ml', 90.00, 0.00, 0.00, 3, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 01:00:52', '2020-10-27 01:00:52'),
(106, 159, 1, 129, '৫০টিব্যাগ', 70.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-27 01:18:58', '2020-10-27 01:19:14'),
(107, 159, 1, 104, '500ml', 90.00, 0.00, 0.00, 3, 'paid', 'pending', 'home_delivery', NULL, '2020-10-27 01:18:58', '2020-10-27 01:19:14'),
(108, 160, 1, 95, '1Ltr', 342.00, 0.00, 0.00, 3, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 01:21:26', '2020-10-27 01:21:26'),
(109, 161, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-27 01:51:19', '2020-10-27 01:51:43'),
(110, 162, 1, 153, '৫০০গ্রাম', 25.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2020-10-27 01:59:17', '2020-10-27 01:59:39'),
(111, 163, 1, 143, '৫০০গ্রাম', 150.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 02:16:54', '2020-10-27 02:16:54'),
(112, 164, 1, 65, '2kg', 90.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 02:45:33', '2020-10-27 02:45:33'),
(113, 165, 36, 114, 'পাতাPata', 20.00, 0.00, 0.00, 2, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 02:48:20', '2020-10-27 02:48:20'),
(114, 166, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 02:59:57', '2020-10-27 02:59:57'),
(115, 167, 1, 68, '500g', 30.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 03:14:28', '2020-10-27 03:14:28'),
(116, 168, 1, 68, '500g', 30.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 03:20:25', '2020-10-27 03:20:25'),
(117, 169, 1, 68, '500g', 30.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 03:23:03', '2020-10-27 03:23:03'),
(118, 169, 1, 143, '৫০০গ্রাম', 150.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2020-10-27 03:23:03', '2020-10-27 03:23:03'),
(119, 170, 1, 140, '১২কেজি', 1125.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-11 03:01:48', '2021-05-11 03:01:48'),
(120, 171, 1, 129, '৫০টিব্যাগ', 280.00, 0.00, 0.00, 4, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-11 06:37:43', '2021-05-11 06:37:43'),
(121, 172, 1, 97, '1kg', 590.00, 0.00, 10.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-12 18:24:25', '2021-05-12 18:24:25'),
(122, 173, 1, 97, '1kg', 590.00, 0.00, 10.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-12 18:28:12', '2021-05-12 18:28:12'),
(123, 174, 1, 97, '1kg', 590.00, 0.00, 10.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-12 18:31:13', '2021-05-12 18:31:13'),
(124, 175, 1, 97, '1kg', 590.00, 0.00, 10.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-12 18:37:23', '2021-05-12 18:37:23'),
(125, 176, 1, 97, '1kg', 590.00, 0.00, 10.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-12 18:40:19', '2021-05-12 18:40:19'),
(126, 177, 1, 65, '2kg', 90.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-18 11:58:11', '2021-05-18 11:58:11'),
(127, 178, 1, 65, '2kg', 90.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-05-18 12:31:28', '2021-05-18 12:31:36'),
(128, 178, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-05-18 12:31:28', '2021-05-18 12:31:36'),
(129, 179, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-05-18 12:40:11', '2021-05-18 12:40:18'),
(130, 180, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-18 12:44:34', '2021-05-18 12:44:34'),
(131, 181, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-28 00:36:44', '2021-05-28 00:36:44'),
(132, 182, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:00:54', '2021-05-31 16:00:54'),
(133, 182, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:00:54', '2021-05-31 16:00:54'),
(134, 182, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:00:54', '2021-05-31 16:00:54'),
(135, 183, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:03:19', '2021-05-31 16:03:19'),
(136, 183, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:03:20', '2021-05-31 16:03:20'),
(137, 183, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:03:20', '2021-05-31 16:03:20'),
(138, 184, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:03:32', '2021-05-31 16:03:32'),
(139, 184, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:03:32', '2021-05-31 16:03:32'),
(140, 184, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-05-31 16:03:32', '2021-05-31 16:03:32'),
(141, 185, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 01:54:24', '2021-06-01 01:54:24'),
(142, 186, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 02:33:31', '2021-06-01 02:33:31'),
(143, 187, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 02:37:20', '2021-06-01 02:37:20'),
(144, 188, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 12:37:32', '2021-06-01 12:37:32'),
(145, 189, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 12:50:43', '2021-06-01 12:50:43'),
(146, 190, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 12:57:25', '2021-06-01 12:57:33'),
(147, 191, 1, 108, '৫০গ্রাম', 45.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 15:47:16', '2021-06-01 15:47:27'),
(148, 192, 1, 126, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 15:48:10', '2021-06-01 15:48:18'),
(149, 193, 1, 127, '১০০গ্রাম', 240.00, 0.00, 0.00, 3, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 15:49:38', '2021-06-01 15:49:47'),
(150, 194, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 15:50:54', '2021-06-01 15:50:54'),
(151, 195, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:36', '2021-06-01 16:07:36'),
(152, 196, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:37', '2021-06-01 16:07:37'),
(153, 197, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:43', '2021-06-01 16:07:43'),
(154, 198, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:44', '2021-06-01 16:07:44'),
(155, 199, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:44', '2021-06-01 16:07:44'),
(156, 200, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:45', '2021-06-01 16:07:45'),
(157, 201, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:45', '2021-06-01 16:08:05'),
(158, 202, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:58', '2021-06-01 16:08:16'),
(159, 203, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:59', '2021-06-01 16:08:16'),
(160, 204, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 16:07:59', '2021-06-01 16:08:16'),
(161, 205, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-01 16:08:00', '2021-06-01 16:08:16'),
(162, 206, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:08:01', '2021-06-01 16:08:01'),
(163, 207, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:08:02', '2021-06-01 16:08:02'),
(164, 208, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:08:02', '2021-06-01 16:08:02'),
(165, 210, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 16:28:55', '2021-06-01 16:28:55'),
(166, 211, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 17:01:26', '2021-06-01 17:01:26'),
(167, 212, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 17:20:41', '2021-06-01 17:20:41'),
(168, 213, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-01 23:59:36', '2021-06-01 23:59:36'),
(169, 214, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-02 14:02:40', '2021-06-02 14:02:40'),
(170, 215, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-02 14:13:46', '2021-06-02 14:13:54'),
(171, 216, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-02 14:25:57', '2021-06-02 14:26:05'),
(172, 217, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-02 14:28:29', '2021-06-02 14:28:37'),
(173, 218, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-02 14:35:17', '2021-06-02 14:35:17'),
(174, 219, 1, 142, '১কেজি', 500.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-03 06:09:49', '2021-06-03 06:10:02'),
(175, 220, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-03 06:28:21', '2021-06-03 06:28:32'),
(176, 221, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-03 06:30:04', '2021-06-03 06:30:14'),
(177, 222, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-03 06:40:28', '2021-06-03 06:40:37'),
(178, 223, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-07 14:20:25', '2021-06-07 14:20:25'),
(179, 224, 1, 153, '৫০০গ্রাম', 25.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-07 14:26:21', '2021-06-07 14:26:40'),
(180, 225, 1, 95, '1Ltr', 114.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-08 16:59:51', '2021-06-08 16:59:51'),
(181, 226, 1, 136, '১২কেজি', 1200.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-08 19:22:50', '2021-06-08 19:22:57'),
(182, 227, 1, 94, '1ltr', 110.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-08 19:32:31', '2021-06-08 19:32:38'),
(183, 231, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-10 07:35:59', '2021-06-10 07:35:59'),
(184, 232, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:46:39', '2021-06-10 08:46:47'),
(185, 233, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:49:32', '2021-06-10 08:49:41'),
(186, 234, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:49:41', '2021-06-10 08:49:52'),
(187, 235, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:49:46', '2021-06-10 08:49:57'),
(188, 236, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-10 08:49:56', '2021-06-10 08:49:56'),
(189, 237, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:52:30', '2021-06-10 08:52:37'),
(190, 238, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:57:09', '2021-06-10 08:57:17'),
(191, 239, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:57:36', '2021-06-10 08:57:44'),
(192, 240, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 08:59:13', '2021-06-10 08:59:21'),
(193, 241, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:00:18', '2021-06-10 09:00:25'),
(194, 242, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:03:04', '2021-06-10 09:03:11'),
(195, 243, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:05:08', '2021-06-10 09:05:15'),
(196, 244, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:06:32', '2021-06-10 09:06:41'),
(197, 245, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:08:16', '2021-06-10 09:08:24'),
(198, 246, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:15:05', '2021-06-10 09:15:13'),
(199, 247, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:18:11', '2021-06-10 09:18:19'),
(200, 248, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:36:43', '2021-06-10 09:36:52'),
(201, 249, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:41:49', '2021-06-10 09:41:58'),
(202, 249, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:41:49', '2021-06-10 09:41:58'),
(203, 250, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:46:22', '2021-06-10 09:46:31'),
(204, 250, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:46:22', '2021-06-10 09:46:31'),
(205, 251, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:47:36', '2021-06-10 09:47:45'),
(206, 251, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:47:36', '2021-06-10 09:47:45'),
(207, 252, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:50:16', '2021-06-10 09:50:25'),
(208, 252, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:50:16', '2021-06-10 09:50:25'),
(209, 253, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:56:19', '2021-06-10 09:56:28'),
(210, 253, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 09:56:19', '2021-06-10 09:56:28'),
(211, 254, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 10:02:49', '2021-06-10 10:02:59'),
(212, 255, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 10:09:04', '2021-06-10 10:09:14'),
(213, 256, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 10:29:57', '2021-06-10 10:30:04'),
(214, 257, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 10:38:53', '2021-06-10 10:39:04'),
(215, 258, 1, 127, '১০০গ্রাম', 80.00, 0.00, 0.00, 1, 'unpaid', 'pending', 'home_delivery', NULL, '2021-06-10 11:21:18', '2021-06-10 11:21:18'),
(216, 259, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 11:23:12', '2021-06-10 11:23:21'),
(217, 260, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 11:38:33', '2021-06-10 11:38:42'),
(218, 261, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 11:43:12', '2021-06-10 11:43:20'),
(219, 262, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 11:45:27', '2021-06-10 11:45:35'),
(220, 263, 1, 128, '৫০টিব্যাগ', 80.00, 0.00, 0.00, 1, 'paid', 'pending', 'home_delivery', NULL, '2021-06-10 11:46:58', '2021-06-10 11:47:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('softghor@gmail.com', '$2y$10$WF1eA3pn5LL6JRiBOngMyeadV8mWU0uSft/Vd9Ow.raQ4NszguxjG', '2020-05-24 03:32:06'),
('firoz_bir10@yahoo.com', '$2y$10$zBDhE7J0YnuC2PiO/eNC5OQLcrwvprkndFWfrHS1H8PMuJikfF79S', '2020-07-17 22:36:47'),
('jashimshanto@gmail.com', '$2y$10$vf44L5zrvFWV/XmAvKro1u/1K4t/xTsyp0IwCYDHGMHWJTaurv4kq', '2020-07-24 22:43:35'),
('shahalambadsha@yahoo.com', '$2y$10$eeKxAHLlO5ByNBxdQef.T.QOb2PKmKBYRMer3pLxqmTu76EFxbRfO', '2020-07-25 17:26:52'),
('mirabubakor@yahoo.com', '$2y$10$30qasiMzBDqtQ/OWOXc8OuZqe1lD8tnrS0F3bYWO40DAxuvCcbPMm', '2020-07-29 12:44:33');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
CREATE TABLE IF NOT EXISTS `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `seller_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL DEFAULT '0.00',
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `seller_id`, `amount`, `payment_details`, `payment_method`, `created_at`, `updated_at`) VALUES
(1, 2, 75.20, NULL, 'cash', '2020-06-06 00:52:03', '2020-06-06 00:52:03');

-- --------------------------------------------------------

--
-- Table structure for table `pickup_points`
--

DROP TABLE IF EXISTS `pickup_points`;
CREATE TABLE IF NOT EXISTS `pickup_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `pick_up_status` int(1) DEFAULT NULL,
  `cash_on_pickup_status` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pickup_points`
--

INSERT INTO `pickup_points` (`id`, `staff_id`, `name`, `address`, `phone`, `pick_up_status`, `cash_on_pickup_status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Md. Abdur Rohim', 'Chandpur', '01842210999', 1, NULL, '2020-07-15 22:00:16', '2020-07-15 22:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `policies`
--

DROP TABLE IF EXISTS `policies`;
CREATE TABLE IF NOT EXISTS `policies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `policies`
--

INSERT INTO `policies` (`id`, `name`, `content`, `created_at`, `updated_at`) VALUES
(1, 'support_policy', '<p><strong>ঘরের বাজারের একটি বড় অংশ জুড়ে আছে আমার বাজার। এটি কি, কেন এবং কিভাবে, এই সম্পর্কে বিস্তারিত……\r\n\r\nআমাদের দৈনিক জীবনে অনেক সময়ে বিভিন্ন সেবা গ্রহণের প্রয়োজন পরে সেগুলো হতে পারে সমস্যার দরুন কিংবা প্রয়োজনের দরুন অথবা সময় এবং অর্থ সাশ্রয়ে।\r\n\r\nতা</strong></p><p><strong>র মধ্যে উল্লেখযোগ্য কয়েকটি হলোঃ ইলেকট্রিশিয়ান, ইলেকট্রনিক্স যন্ত্র রিপেয়ারিং, গাড়ি রিপেয়ারিং, পরিবহণ সেবা, ক্লিনিং সেবা, প্লাম্বিং সেবা, লণ্ড্রী সেবা, গ্যাজেট সার্ভিসিং সেবা, পেস্ট কন্ট্রোল সেবা, প্রিন্টিং এন্ড পেইন্টিং সেবা, বাবুর্চি সেবা । এই সেবাগুলো অনায়াসেই সময়, অর্থ সাশ্রয় করার পাশাপাশি মানে সঠিক প্রাপ্যতা নিশ্চিত করবে।\r\n</strong></p><p><strong>\r\nএই সকল সেবার মধ্যে যা পাবেনঃ\r\n</strong></p><p><strong>ইলেকট্রিশিয়ানঃ&nbsp;বাসাবাড়ি কিংবা প্রতিষ্ঠানের সকল ইলেক্ট্রিক কাজের জন্য ইলেক্ট্রিশিয়ান সেবা।\r\n\r\nই</strong></p><p><strong>লেকট্রনিক্স যন্ত্র রিপেয়ারিংঃ&nbsp;ফ্রিজ, এসি, টিভি, ফ্যান, ইলেক্ট্রিক কুকার, গিজার, ওয়াশিং মেশিন ইত্যাদি যন্ত্রাংশ সার্ভিসিং বা রিপেয়ারিং সেবা।\r\n\r\nগ্যা</strong></p><p><strong>জেট সার্ভিসিং সেবাঃ&nbsp;মোবাইল, ট্যাব, কম্পিউটার, মিউজিক প্লেয়ার ইত্যাদির সফটওয়্যার কিংবা হার্ডওয়্যারগত যে কোন সমস্যা সমাধান।</strong></p><p><strong>গাড়ি রিপেয়ারিংঃ&nbsp;আপনার ব্যবহৃত গাড়ি, যেমনঃ প্রাইভেট কার, মটরসাইকেল ইত্যাদির সমস্যার প্রাথমিক কাজের সেবা।</strong></p><p><strong>পরিবহণ সেবাঃ&nbsp;বাসা, অফিস ইত্যাদি পরিবর্তন এর জন্য কিংবা যে কোন পরিবহণ এর জন্য গাড়ি যেমনঃ প্রাইভেট কার, মাইক্রো বাস, বাস, ট্রাক, পিক আপ ভ্যান ইত্যাদি ভাড়া নিতে পারবেন।</strong></p><p><strong>ক্লিনিং সেবাঃ&nbsp;বাসা বাড়ি অফিস কিংবা ট্যাং পরিষ্কারের মানুষ পাবেন এখানেই।\r\n\r\nপ্লাম্বিং সেবাঃ&nbsp;আপনাদের পানির লাইনের যে কোন সমস্যার সমাধান এখানেই।</strong></p><p><strong>লণ্ড্রী সেবাঃ&nbsp;অনেক কাপড় ধোয়া এবং স্ত্রী করা নিয়ে সকল সমস্যার সমাধান দিবে সেবা বাজার।\r\n</strong></p><p><strong>পেস্ট কন্ট্রোল সেবাঃ&nbsp;বাসায় মশা, মাছি, তেলাপোকা, ছাড়পোকা ইত্যাদির অত্যাচারে অতিষ্ট হওয়া বন্ধ করবে চাহিদা সেবা বাজার।\r\n\r\nপ্রি</strong></p><p><strong>ন্টিং এন্ড পেইন্টিং সেবাঃ&nbsp;বাসার ওয়াল পেইন্ট, লিখনি কিংবা ডাটা রাইটিং ও প্রিন্টিং এর জন্য নিতে পারেন আমাদের সাহায্য।</strong></p><p><strong>বাবুর্চি সেবাঃ&nbsp;বাসায় কিংবা অফিসে বড় কোন অনুষ্ঠান, সভা সেমিনার এর জন্য বাবুর্চি সেবাও পাবেন সেবা বাজার থেকেই।\r\n\r\nএ সকল সেবা পেতে হলে&nbsp;এখানে ক্লিক&nbsp;করে চলে যান ঘরের বাজারের রিকোয়েস্ট ফরমে এবং ফরম পূরণ করে পাঠালেই আপনার সাথে যোগাযোগ করবে চাহিদা হেল্প সেন্টার এবং সঠিক সেবাটি দানের লক্ষ্যে ঘরের বাজার নির্বাচিত এক্সপার্ট&nbsp;চলে যাবে।</strong></p><p><strong>আরো বিস্তারিত জানতে আমাদের কল সেন্টারে কল করুন।\r\n</strong></p>', '2020-09-03 16:32:01', '2020-09-03 10:32:01'),
(2, 'return_policy', '<p><strong>স্বাস্থ্যবিধি\r\nএক্সক্লুসিভ সংগ্রহ\r\nশিশুর যত্ন\r\nরান্নাঘর ও রান্না\r\nফলমূল ও শাকসবজি\r\nপানীয় এবং পানীয়\r\nহোম কেয়ার এবং পরিষ্কারের\r\nসৌন্দর্য এবং শারীরিক যত্ন\r\nস্বাস্থ্য এবং প্রাপ্তবয়স্কদের যত্ন\r\nদুধ ও দুগ্ধ\r\nস্ন্যাকস এবং স্প্রেড\r\nঅফিস এবং স্কুলিং\r\nবাড়ি এবং রান্নাঘর সরঞ্জাম\r\nপোষা প্রাণীর যত্ন\r\nমোটরগাড়ি প্রয়োজনীয়তা\r\nপণ্য অনুসন্ধান করুন\r\nফেরত / প্রতিস্থাপন, বাতিলকরণ এবং ফেরতের নীতি Policy\r\nএকজন গ্রাহক আমাদের বিতরণ আধিকারিকের উপস্থিতিতে, বা পরবর্তী 12 ঘন্টাের মধ্যে বা নিম্নলিখিত পরিস্থিতিতে 72 ঘন্টাের মধ্যে যে কোনও বিতরণকৃত পণ্য ফেরত দিতে পারেন; যদি আপনি চালানটি থেকে কোনও পণ্য না পেয়ে থাকেন তবে দয়া করে আপনার প্রাপ্ত পরিমাণের পরিমাণটি পরিশোধ করুন এবং প্রদত্ত পরিমাণটি আমাদের পরিষেবা চার্জের নীতি পূরণ করতে পারে না, আপনি নীতিটি সম্মান করবেন (উদাহরণস্বরূপ; আপনার অর্ডার পরিমাণটি ছিল tk was 999 বা ততোধিক পরিষেবা নিখরচায় এবং পণ্য ফেরত দেওয়ার জন্য এবং চালানের মূল্য হ্রাস করার জন্য, আপনি স্বয়ংক্রিয়ভাবে পরিষেবা চার্জের অর্থ প্রদানের অধীনে থাকবেন)।</strong></p><p><strong>তাৎক্ষনিক রিটার্ন নীতি:</strong></p><p><strong>★যদি আপনার প্রত্যাশা অনুযায়ী পণ্য (গুলি) না পাওয়া যায়।\r\n</strong></p><p><strong>★পরিবহনের সময় ক্ষতিগ্রস্থ হলে।\r\n</strong></p><p><strong>★প্যাকেজিং, মান এবং ভলিউম সম্পর্কে যদি বিভ্রান্তি থাকে।</strong></p><p><strong>12 ঘন্টা রিটার্ন নীতি:\r\nএটি কেবলমাত্র আমাদের শাকসব্জী, ফল এবং অন্যান্য যে কোনও ধ্বংসযোগ্য পণ্যের জন্য প্রযোজ্য, যা পুনঃ বিতরণ শর্তাদির অধীনে উপভোগযোগ্য শর্তের জন্য ফেরত যেতে পারে।</strong></p><p><strong>72 ঘন্টা রিটার্ন নীতি:\r\nএকজন গ্রাহক নিম্নলিখিত পরিস্থিতিতে সরবরাহের তারিখ থেকে hours২ ঘন্টার মধ্যে যে কোনও বিতরণ পণ্য ফেরত দিতে পারেন;\r\nসর্বাধিক 25% বা তারও কম ত্রুটিযুক্ত 5 দিনের মধ্যে আপনি আমাদের কোনও অসম্পূর্ণ তবে না খালি পণ্য (গুলি) ফিরিয়ে দিতে পারেন।\r\n</strong></p><p><strong>নিম্নলিখিত পরিস্থিতিতে পণ্যগুলি ফেরত বা প্রতিস্থাপন করা যাবে নাঃ</strong></p><p><strong>১। পণ্য (গুলি) ব্যবহার বা ব্যবহারের পরে বা ইনস্টলেশন করার পরে\r\nপণ্য (গুলি) ভাঙা বা অর্ডার বাইরে\r\nক্ষয় বা ত্রুটি যা গ্রহণযোগ্য নয়\r\nকোনও ক্ষতি / ত্রুটিগুলি প্রস্তুতকারক / ব্যবসায়ীদের ওয়্যারেন্টি বা গ্যারান্টি জুড়ে না\r\nমূল প্যাকেজিং ছাড়াই পাওয়া যায় এমন কোনও পণ্য (গুলি) প্রত্যাবর্তনযোগ্য\r\nযে কোনও পণ্য সক্রিয় / ক্রমিক ক্রমিক বা ইউএসসি নম্বরগুলিতে টেম্পারড বা অনুপস্থিত পাওয়া গেছে\r\n</strong></p><p><strong>প্রত্যর্পণ নীতিঃ</strong></p><p><strong> ঘরের বাজার (ঘরের বাজার) সর্বদা গ্রাহকদের সেরা পরিষেবা সরবরাহ করতে এখানে। যে কোনও অপ্রত্যাশিত পরিস্থিতির জন্য, আমরা যদি পণ্য (গুলি) সরবরাহ করতে সক্ষম না হই বা আমাদের পরিষেবা সরবরাহ করতে না পারি, আমরা আপনাকে 12 ঘন্টা ফোন বা এসএমএসের মাধ্যমে অবহিত করব। আমাদের যে কোনও অভাবের জন্য যদি কোনও ফেরতের প্রয়োজন হয়, আমরা নিম্নলিখিত হিসাবে পরবর্তী 7 দিনের মধ্যে এটি করব।</strong></p><p><strong>ফেরত দেওয়ার কলগুলি নিম্নলিখিত পরিস্থিতিতে প্রক্রিয়া করা হবে;\r\nকোনও পণ্য সরবরাহ করতে অক্ষম\r\nযদি কোনও পণ্য (গুলি) গ্রাহকরা প্রদত্ত চালানের বিপরীতে ফিরে আসে\r\nফেরতের সময় ছাড়\r\nকোনও গ্রাহক কোনও প্রদত্ত আদেশ বাতিল করে এবং ফেরতের জন্য জিজ্ঞাসা করার সময়, বা আমরা (ঘরের বাজার)&nbsp;</strong><strong>কোনও ব্যাংক বা বিকাশ / রকেটের ডেবিট / ক্রেডিট কার্ড প্রদানের শর্তে, আমরা (ঘরের বাজার) পুরো অর্থ ফেরত দেব আমাদের (ঘরের বাজার ) শেষ থেকে পরিমাণ। ক্রেডিট / ডেবিট কার্ড প্রদানের ক্ষেত্রে গ্রাহক তার কার্ড ইস্যুকারী ব্যাংক থেকে একই পরিমাণ ফেরত পাবেন। তবে বিকাশ / রকেটের কাছ থেকে অর্থ প্রদানের ক্ষেত্রে, এমনকি যদি ঘরের বাজার&nbsp;</strong><strong>গ্রাহক প্রদত্ত সম্পূর্ণ পরিমাণ ফেরত দেয় তবে তারা (বিকাশ / রকেট) গ্রাহককে ফেরত দেওয়ার আগে লেনদেনের চার্জ (তাদের কোম্পানির নীতি অনুসারে) কেটে নেবে।</strong></p><p><strong>\r\n\r\nসরবরাহ নীতি:\r\nআমরা সরবরাহিত সমস্ত আইটেমের গুণমান নিশ্চিত করতে প্রতিশ্রুতিবদ্ধ। ঘরের বাজার&nbsp; কোনও মান উত্পাদনকারী পণ্য বিক্রি করছে না, আমরা মানের দিকে নজর দিচ্ছি। আমরা নিজের দায়িত্বে কোনও পণ্যের জন্য মানের গ্যারান্টির জন্য চ্যালেঞ্জ করি না। আমাদের প্রস্তুতকারক / ব্যবসায়ী / বিক্রেতার প্রতি আস্থা আছে যে তারা সর্বদা তাদের পুরোউৎপাদন / সরবরাহকৃত পণ্যের উপর গুণমানকে আশ্বাস দেয়।\r\n\r\n</strong></p><p><strong>অর্ডার বাতিলকরণ এবং ব্ল্যাকলিস্ট নীতি:\r\nপ্রদত্ত আদেশের শর্তে, ঘোড়াবাজার যে সেল নম্বর থেকে অর্ডার দেওয়া হয়েছে তা যাচাই করবে। যাচাই না করে, অর্থ প্রদানের অর্ডার সরবরাহ করা হবে না। গ্রাহক হিসাবে আপনি যে কোনও সময় আমাদের গ্রাহক পরিষেবা কেন্দ্রে কল করে বা ডেলিভারি ব্যক্তি যখন আপনার দরজায় আসে তখন আপনার অর্ডার বাতিল করতে পারেন। এই জাতীয় ক্ষেত্রে আমরা অর্ডারটির জন্য ইতিমধ্যে আপনার দ্বারা প্রদত্ত যে কোনও অর্থ প্রদান ফেরত দেব। অন্যদিকে, আমরা যদি কোনও গ্রাহকের দ্বারা প্রতারণামূলক লেনদেন বা ওয়েবসাইট বা আমাদের পরিষেবা ব্যবহারের শর্তাবলী লঙ্ঘন করে এমন কোনও লেনদেন সন্দেহ করি বা আমরা যদি পাই যে কোনও গ্রাহক আমাদের অনলাইন সিস্টেমের কোনও প্রযুক্তিগত দোষে নগদ দেওয়ার চেষ্টা করেছেন / ওয়েবসাইট / পরিষেবা, আমাদের পরিষেবা তালিকার অস্তিত্ব নেই এমন আইটেমগুলি পুনঃসূচনা করুন, আমাদের পরিষেবা তালিকায় বিদ্যমান বিশেষ অফার আইটেমগুলি পুনরায় চালু করুন তবে দাম বাড়ানো হয়েছে, বিক্রি করে লাভ করার জন্য আইটেমগুলি অর্ডার করুন, আমরা আমাদের নিজস্ব বিবেচনার ভিত্তিতে এই জাতীয় আদেশ বাতিল করতে পারি । আমরা সমস্ত প্রতারণামূলক লেনদেন এবং গ্রাহকদের একটি নেতিবাচক তালিকা বজায় রাখব এবং তাদের আমাদের সিস্টেমে অ্যাক্সেস অস্বীকার করব বা তাদের দেওয়া কোনও আদেশ বাতিল করব।</strong></p>', '2020-09-04 02:29:27', '2020-09-03 20:29:27'),
(4, 'seller_policy', '<p><strong>বর্তমান সময়ে সবচেয়ে আলোচিত একটা বিষয় হচ্ছে ই-কমার্স বিজনেস, যাকে অন্য ভাবে বলে ইলেক্ট্রনিক কমার্স যা ইন্টারনেট এর মাধ্যমে সম্পন্ন হচ্ছে। বর্তমানে সবচেয়ে নির্ভরযোগ্য এবং দ্রুতসময়ে পণ্য প্রাপ্তির ক্ষেত্রে ই-কমার্স অনেক জোরালো ভুমিকা রেখেই চলেছে। তাই এই সম্ভাবনা কে কাজে লাগানোর একটা প্রয়াস হিসেবে এই উদ্দ্যোগটি নেয়া হচ্ছে যার মুল উদ্দেশ্য শুধুই ব্যবসা করা নয় বরং সারা বাংলাদেশ এ একটা সার্ভিস চালু করা এবং তাঁর সাথে একটা সুপরিচিত ব্র্যান্ড হিসেবে আত্মপ্রকাশ করা। আর সেই লক্ষেই&nbsp;ghorerbazar.com&nbsp;প্রতিটি জেলা বা পৌরসভাতে “ঘরের বাজার ” প্রদান করছে।\r\n\r\nব্যস্ততার ফাঁকে প্রত্যেকটি মানুষ তার নিত্য পয়োজনীয় জিনিস যেন ঘরে বসে অনায়াসেই কেনাকাটা করতে পারে এমন লক্ষ্য নিয়েই “ঘরের বাজার” এর যাত্রা শুরু করেছে। আমাদের উদ্দেশ্য একটি তা হলো, বর্তমান সময়ের তথ্য প্রযুক্তিকে পুরোপুরিভাবে কাজে লাগানো এবং সেটি সঠিক পথে। এই প্রযুক্তির যুগে যখন গোটা পৃথিবীর মানুষ ই-কমার্স অর্থাৎ অনলাইনে বাজার সদাই সহ সকল দরকারি কাজ সম্পূর্ণ করে নিজের জীবনকে গতিশীল করছে, সেখানে আমরা কেন পিছিয়ে থাকব ? সেজন্যই প্রযুক্তির এই দুনিয়ায়&nbsp;ghorerbazar.com&nbsp;থেকে “ঘরের বাজার ” নিয়ে আপনার এলাকার উন্নয়নে অবদান রাখুন এবং ব্যাক্তিগত জীবনে আর্থিক স্বচ্ছলতা নিয়ে আসুন।\r\n\r\nএজেন্ট হতে হলে যা যা থাকতে হবে তা হলোঃ\r\n\r\nনিজস্ব একটি বিজনেস রুম বা অফিস বা দোকান।অনলাইন, কম্পিউটার, ওয়েবপেইজ ও ইন্টারনেট সম্পর্কে সাধারণ জ্ঞান।নিজের প্রতিষ্ঠানে ইন্টারনেট সহ কম্পিউটার ও প্রিন্টার।শুরুতে জামানত হিসেবে কোন প্রকার ফি ঘরের বাজারকে প্রদান করতে না হলেও একটি ব্যবসা নিজ এলাকায় সফলভাবে শুরু করার আগে ব্র্যান্ডিং অতীব জরুরী (যেমনটা জরুরী একটি দোকান শুরুর আগে ডেকোরেশনের সহ আনুষঙ্গিক অনেক কিছুরই) আর তাই নিজ এলাকার প্রচারের জন্য খরচ করার মানসিকতা থাকতে হবে। যেখানে ঘরের বাজারের কোন প্রকার বাধ্যতামূলক নিতি থাকবে না।\r\n\r\nএজেন্টগনদের কমিশন ও পাওনাঃ\r\n\r\nঘরের বাজার এ পণ্য বিক্রয়ের ক্ষেত্রে কোন প্রকার অর্থ চাহিদাকে প্রদান করতে হবে না। লভ্যাংশ সহ পুরো টাকাটি এজেন্টগণ গ্রহণ করবেন।এজেন্টগণকে ঘরের বাজার পয়েন্ট সেবার চুক্তি অনুযায়ী কমিশন প্রদান করা হবে।\r\n\r\n&nbsp;\r\n\r\nকি করতে হবে আমাকে?\r\n\r\nএকটি অফিস বা দোকানে বসে ইন্টারনেট কানেকশন সহ একটি মোবাইল অথবা ল্যাপটপ মাঝে মাঝে চেক করতে হবে আপনার&nbsp;এলাকায় কি কি অর্ডার আসছে।\r\nসেই অর্ডার গুলো প্রিন্ট আউট করে ডেলিভারি ম্যান কে লিস্ট দিয়ে দিবেন, সে নির্দিষ্ট হোলসেল দোকান থেকে পণ্য সংগ্রহ করে যথা সময় ডেলিভারি করবে। আপনি তা মনিটরিং করবেন। এতটুকুই কাজ আপনার।\r\n\r\nঅর্ডার কিভাবে আসবে?\r\n\r\nঘরের বাজার এজেন্টশিপ এমন একটি ব্যবস্থা যেখানে আপনার এলাকার বা আপনার শপের সম্পুর্ণ বিজনেস আপনার নিজস্ব। আপনার সেল কিংবা লাভ থেকে কোন অংশই চাহিদা গ্রহণ করবে না। তাই আপনার ব্যবসার জন্য প্রচারণা থেকে শুরু করে অর্ডার আনয়ন সম্পুর্ণ আপনাকেই করতে হবে। এছাড়াও আমরা সারাদেশে সেন্ট্রাল বিজ্ঞাপনের ব্যবস্থা করছি, যেমন ফেসবুক বিজ্ঞাপন, অনলাইন বিজ্ঞাপন, টিভি অ্যাড ও লোকাল পত্রিকায় পর্যায়ক্রমে প্রচারনা চালাচ্ছি। যার ফলে অনলাইন কিংবা মোবাইল ফোনের মাধ্যমে সরাসরি আমাদের কাস্টোমার অর্ডার কালেক্টর বা কাস্টমার সার্ভিস সেন্টারে অর্ডার আসবে এবং মেইল কিংবা ড্যাশবোর্ড নোটিফিকেশন এর মাধ্যমে সরাসরি আপনার কাছেও যাবে।\r\n\r\nআমার লাভ কি হবে?\r\n\r\nধরুন, আপনি আপনার এলাকায় অল্প কিছুদিনে ১ম শ্রেণীর ১০০ কাস্টমার তৈরি করেছেন যাদের ফ্যামিলির সকল বাজার সরবরাহ করবার দায়িত্ব আপনি নিয়েছেন আর তা হচ্ছে বাজার মূল্যেই। এখন স্বাভাবিক একটি ফ্যামিলিতে প্রতি মাসে কমপক্ষে ৫-৬ হাজার টাকার বাজার তো লাগবেই। তাহলে ১০০ কাস্টমারের জন্য কমপক্ষে ৫-৬ লক্ষ টাকার বাজার সরবরাহ করতে হচ্ছে । আর এ সকল বাজার নিশ্চয়ই আপনিই পাইকারি দোকান থেকে সংগ্রহ করবেন যা থেকে আপনার লভ্যাংশ বের হবে। যার সম্পুর্ণই আপনার। অর্থাৎ কোম্পানি আপনার বিক্রয় ও আয় এর উপর কোন প্রকার কমিশন গ্রহণ করবে না। অতএব ক্রমবর্ধমানভাবে অর্ডারের পরিমাণ বৃদ্ধি পাবে সাথে সাথে আপনার আয়ও বৃদ্ধি পেতে থাকবে।\r\n\r\nইনভেস্ট কত করতে হবে?\r\n\r\nব্যবসা মানেই কিছু ইনভেস্ট তো থাকবেই তবে প্রযুক্তির সহযোগিতায় ভার্চুয়াল বিজনেস এর এজেন্টশিপ নিতে পারলে তেমন কোন ইনভেস্ট করতে হবে না। কিছু ইনভেস্ট করতে হবে যা আপনার নিজের এলাকার ঘরের বাজারকে ব্র্যান্ডিং ও প্রয়োজনীয় অফিস উপকরণ ক্রয়ে খরচ হবে। যেমন আপনাকে আপনার লোকাল কিছু ব্র্যান্ডিং এর জন্য নিজস্ব উদ্দ্যগে প্রচারণার খরচ বহন করতে হবে যাতে আপনার এলাকায় অর্ডার বৃদ্ধি পায় সেটা আপনার স্বদিচ্ছার উপর নির্ভর করছে। যে টাকা আপনি নিজে থেকেও খরচ করতে পারবেন অথবা আমাদের মাধ্যমেও করাতে পারবেন। তবে সঠিক ভাবে খরচ করানোর জন্য আমাদের মাধ্যমে করানোটাকেই&nbsp;রিকমেন্ড করি। একটা ট্রেডিশনাল মূদি দোকানের জন্য ১০-১৫ লক্ষ টাকা ইনভেস্ট করতে হয় আর সেখান থেকে ক্রয় করে শুধুমাত্র ওই পাড়া বা মহল্লার কাস্টমাররা। কিন্তু আপনার এই অনলাইন স্থানীয় বাজার ব্যবসাতে পুরো পৌরসভার সকলই আপনার কাস্টমার। তাই নিজের ব্যবসা বৃদ্ধির জন্য আপনি যতো প্রচারণা করবেন আপনার ততো প্রবৃদ্ধি হবে, আর সেজন্য খরচ করার মানসিকতা থাকতে হবে। এর মানে এই নয় যে ঘরের বাজারকে এই অর্থ দিতে হবে। বরং ঘরের বাজারের মাধ্যমে করলে ব্যবিসার সামগ্রিক উন্নয়নের জন্য সেখানে ঘরের বাজার কিছু ভুর্তুকি প্রদান করবে। এটি আপনিই খরচ করবেন আপনার কাজে দ্রুত ব্যবসা বৃদ্ধির জন্য। এ নিয়ে যে কোন ধরণের সঠিক গাইডলাইন এর জন্য ঘরের বাজার সর্বাত্বক সহযোগিতা করতে প্রস্তুত।</strong></p><p>“ব্যবসা শুরুর আগে অবশ্যই ১ম মাসের ফী দিয়ে শুরু করতে হবে।”\r\n</p><p>\r\nকিভাবে পরিচালনা করবো?\r\n\r\nপ্রথম অবস্থায় আপনি ৪-৫ জন পাইকারি দোকানদার যেমনঃ ফলের দোকান, সব সবজি পাওয়া যায় এমন সবজির দোকান, মাছের দোকান, মাংসের দোকান এবং মুদি দোকানের সাথে কথা বলে রাখবেন যে আমার ডেলিভারি ম্যান লিস্ট নিয়ে আপনার কাছে পণ্য নিতে আসবেন আপনি পাইকারি মূল্যে আমাদের সকল পণ্য দেবার চেষ্টা করবেন কেননা আমাদের দিনদিন ব্যপক অর্ডার বৃদ্ধি পাবে। সেই অর্ডারকৃত পণ্য নিয়ে ডেলিভারি ম্যান ডেলিভারি করবে। ডেলিভারি শেষে আপনাকে হিসাব বুঝিয়ে দিবে। প্রাথমিক অবস্থায় এইটুকু মনিটরিং করতে পারলেই হবে। যদি আপনার আগে থেকেই এই ধরণের ব্যবসা প্রতিষ্ঠান থাকে তাহলে তো আরো লাভ করতে পারবেন কিংবা নিজে থেকে যদি সকল পণ্য ষ্টক করতে পারেন তাহলে আরো অনেক ভালো ।\r\n\r\nডেলিভারি ম্যানের বেতন কে দিবে?\r\n\r\nব্যবসা যেহেতু সম্পুর্ণই আপনার তাই ডেলিভারি থেকে শুরু করে অন্যন্য সকল ব্যয় আপনাকেই বহন করতে হবে। আমরা এই ব্যপারে আপনাকে পরামর্শ হিসেবে বলতে পারি। ১ম ৩ মাস পরীক্ষামূলকভাবে ১-৩ জন ডেলিভারি ম্যান নিয়োগ দিন। পরীক্ষামূলকভাবে থাকাকালীন তাদেরকে প্রতি ডেলিভারিতে ২৫ টাকা হারে প্রদান করতে পারেন। যাতে দেখা যাচ্ছে দিনে ১০ টা ডেলিভারিও যদি করে তাহলে মাসে ৭৫০০ টা পাচ্ছে আর ১০ টা ডেলিভারি করতে সর্বোচ্চ ৪-৫ ঘন্টা লাগবে। এভাবে তারা তিনমাস কাজ করবে পরবর্তিতে সব সময়ের জন্য পার্মানেন্ট আপনার অফিসে নিযূক্ত করতে পারেন। ২০ টাকা ডেলিভারি চার্জ নিয়ে ২৫ টাকা করে দিবেন এখানে ৫ টাকা আপনার ভুর্তুকি হলেও আশাকরি ভালো ফলাফল হবে। তবে এটা সম্পুর্ণই আপনার উপর নির্ভর করবে আপনি কিভাবে নিবেন।\r\n</p><p>ঘরের&nbsp;হতে যা পাবেনঃ\r\n\r\nনিজ এলাকা/জোন ভিত্তিক শুধুমাত্র একজন ব্যাক্তিকে/প্রতিষ্ঠানকে ঘরের বাজারের মনোনীত এজেন্টশিপ প্রদান করা হবে।সতন্ত্র এজেন্ট প্যানেল, পণ্য প্যানেল ও বিক্রয় প্যানেল সহ অর্ডার/হিসাব মনিটরিং এর ব্যবস্থা।এলাকা/জোন ভিত্তিক নিজের অনলাইন গ্রোসারি শপ ব্যবস্থাপনা।সারা দেশে একটি বৃহৎ ব্র্যান্ডিং এর আওতায় ও কেন্দ্রীয় ব্যানারে ব্যবসার সুযোগ।ডেভেলপকৃত ওয়েবসাইট ও মোবাইল অ্যাপস পরিচালনা টিম।সকাল ৮ টা থেকে রাত ৮ টা পর্যন্ত ১২ ঘন্টা নিরবচ্ছিন্ন এজেন্ট ও আপনার কাস্টমার এর কল সেন্টার সাপোর্ট ব্যবস্থা।সারাবছর অব্যাহত কেন্দ্রীয় অনলাইন ও অফলাইন বিজ্ঞাপন ব্যবস্থা।ভুর্তুকিতে লোকাল বিজ্ঞাপণের জন্য প্রচারণা মালামাল ক্রয়ের ব্যবস্থা। কে যা প্রদান করতে হবেঃ\r\nউপরোক্ত সকল সেবা গ্রহণের মাধ্যমে নিজ এলাকায় একক ব্যবসা পরিচালনার জন্য মাসিক ভিত্তিতে শুধুমাত্র ২,৫০০ টাকা সার্ভিস চার্জ স্বরূপ প্রদান করতে হবে।</p><p>শুধুই ঢাকা নয় বরং সারা দেশে ই-কমার্স ব্যবসা পরিচালনার জন্য বিভাগ, জেলা , উপজেলা বা পৌরসভা ভিত্তিক ঘরের বাজার এজেন্ট প্রদান করা হচ্ছে।\r\n\r\nআরো বিস্তারিত জানতে সরাসরি কল করুনঃ\r\n০১৮৪২২১০৯৯৯ সকাল ১০টা থেকে  বিকাল৫ টা পর্যন্ত।\r\n\r\n</p>', '2020-09-03 15:35:27', '2020-09-03 09:35:27'),
(5, 'terms', NULL, '2019-10-29 12:54:51', '2019-10-28 18:00:00'),
(6, 'privacy_policy', '<strong>ঘরের বাজার শুধুমাত্র আপনার অনুমতি ছাড়া কোন প্রকার তথ্য সংগ্রহ বা সংরক্ষণ করে না। আপনি যখন আমাদের ওয়েবসাইটে ভিজিট করার পর আপনার একাউন্ট তৈরি করে সেখানে তথ্য প্রদান করেন ঠিক সেই সময়ের ততটুকু তথ্যই আমরা সংরক্ষণ করি।\r\n\r\nএই তথ্যগুলো আপনার প্রয়োজনে আপনার কষ্ট লাঘব করতে কিংবা বিরক্তি থেকে রক্ষা করতে এবং হিসেব ঠিক রাখতে মূলত ব্যবহৃত হয়। যে কোন নতুন প্রমোশন ছাড় কিংবা অফার থাকলে সেটি যে আপনি সরাসরি আগে পান সেটিও নিশ্চিত করবে এই তথ্যের মাধ্যমে।\r\n\r\nএই তথ্যগুলো (যেমন- মোবাইল নাম্বার, ইমেইল) এর মাধ্যেমে আপনি আপনার অর্ডার কিংবা কয়েরির অবস্থান মুলক নিশ্চিত তথ্য পান যা আপনাকে আপনার প্রয়োজনকে নিশ্চিত করে।\r\n\r\nতবে আপনার তথ্য নিয়ে শঙ্কিত হওয়ার কিছু নেই, আমরা আপনার তথ্য আমাদের অর্থাৎ ঘরের বাজার অভ্যন্তরীণ ব্যবহার ছাড়া অন্য কোথাও ব্যবহার করি না কিংবা বাহিরে কোথাও কারো কাছে প্রকাশ করি না। আপনার তথ্যগুলো সবসময় আমাদের কাছে খুবই গুরুত্বপূর্ণ তাই আপনার তথ্যগুলো সর্বোচ্চ নিরাপদে রাখে ঘরের বাজার।\r\n\r\nগ্রাহক সম্পর্কিত তথ্য আমাদের ব্যবসায়ের একটি গুরুত্বপুর্ন&nbsp; অংশ এবং আমরা এটি অন্যান্যদের কাছে বিক্রয়ের মাধ্যমে ব্যাবসা করি না।\r\n</strong>', '2020-09-03 15:11:09', '2020-09-03 09:11:09');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `added_by` varchar(6) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'admin',
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `subsubcategory_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `photos` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `thumbnail_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `featured_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flash_deal_img` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_provider` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tags` mediumtext COLLATE utf8_unicode_ci,
  `description` longtext COLLATE utf8_unicode_ci,
  `unit_price` double(8,2) NOT NULL,
  `purchase_price` double(8,2) NOT NULL,
  `choice_options` mediumtext COLLATE utf8_unicode_ci,
  `colors` mediumtext COLLATE utf8_unicode_ci,
  `variations` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `todays_deal` int(11) NOT NULL DEFAULT '0',
  `published` int(11) NOT NULL DEFAULT '1',
  `featured` int(11) NOT NULL DEFAULT '0',
  `current_stock` int(10) NOT NULL DEFAULT '0',
  `unit` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `discount_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax` double(8,2) DEFAULT NULL,
  `tax_type` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_type` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'flat_rate',
  `shipping_cost` double(8,2) DEFAULT '0.00',
  `num_of_sale` int(11) NOT NULL DEFAULT '0',
  `meta_title` mediumtext COLLATE utf8_unicode_ci,
  `meta_description` longtext COLLATE utf8_unicode_ci,
  `meta_img` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pdf` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `rating` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `rating`, `created_at`, `updated_at`) VALUES
(30, 'Papaya', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/RqHDuEo6HxPzHs6j9X6D234rN02dyJkX0sI5Dad8.jpeg\"]', 'uploads/products/thumbnail/crb0ZkTm63hu7v46zS3yzJGszlrzlZRJ5Jj3S4jf.jpeg', 'uploads/products/featured/ZXZ9uYcI1NpdwDy0iMsBV5GiDSfgwxkpdvfR3A7A.jpeg', 'uploads/products/flash_deal/EPEUguSFwsLsKFC1XmBv7fKGhZESSP46VPg4qDo6.jpeg', 'youtube', NULL, 'পেঁপে-Papaya', NULL, 25.00, 70.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09c7\\u0981\\u09aa\\u09c7\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"25.00\",\"sku\":\"P-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"50.00\",\"sku\":\"P-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"70.00\",\"sku\":\"P-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'পেঁপে', NULL, 'uploads/products/meta/ncGa5dwVMLsY4qOjic8bq17lVHsnWpRaXnoLjeA6.jpeg', NULL, 'Papaya-Rd6x2', 0.00, '2020-05-13 23:40:22', '2020-06-18 07:16:45'),
(34, 'Zucchini', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/ATZhbQKlkBYZrb6YT8dazUKRvdMX6QWj2hG67aNG.png\"]', 'uploads/products/thumbnail/qzK7dAyjQKrXR6yHhMm0QsmEdNL0yDThXf1WBCSa.png', 'uploads/products/featured/AhXEWPoy7pVaAlGmcofYYc1Yvrou6bddF040712H.png', 'uploads/products/flash_deal/WdQlR9oIHVVV7WgfbjxqfBYrAYnXzugwSaYT6H5e.png', 'youtube', NULL, 'দুন্দুল-Zucchini', NULL, 25.00, 70.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a6\\u09c1\\u09a8\\u09cd\\u09a6\\u09c1\\u09b2\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"25.00\",\"sku\":\"Z-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"50.00\",\"sku\":\"Z-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"70.00\",\"sku\":\"Z-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 1, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'দুন্দুল', NULL, 'uploads/products/meta/MOu0PZDPHVz3imxPewiu9Idl3xDF1mPvpSfvUhG0.png', NULL, 'Zucchini-cWeh0', 0.00, '2020-05-14 00:02:04', '2020-06-18 07:21:49'),
(35, 'Cucumber', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/m3eDzoLGocBli0X8RbtMeYsPz0x7AIKb2QfFL7Ig.png\"]', 'uploads/products/thumbnail/bo5pAxOUUk6UIamNwqa9mYLYC9mcxsGJeI0FXFmd.png', 'uploads/products/featured/s7YPqVQB4LHwgUaYcJiilel7kvpP7j8QMTNjzIUF.png', 'uploads/products/flash_deal/awYQQULnicGDJtl1dpJmlNfk9PzOZUmc9wL7FzJI.png', 'youtube', NULL, 'Cucumber', NULL, 20.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b6\\u09b8\\u09be\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"20.00\",\"sku\":\"C-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"40.00\",\"sku\":\"C-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"60.00\",\"sku\":\"C-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'শসা', NULL, 'uploads/products/meta/dfKeVLFN2STXRjBQVZ1rBurw1oCKFJvmneIlqEq9.png', NULL, 'Cucumber-oiKrJ', 0.00, '2020-05-14 00:15:29', '2020-06-18 07:38:28'),
(36, 'Long eggplant black', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/NRW4cJ51U3NmOr0iweoNxBZxbB1or9pN8Nv6fsT5.png\"]', 'uploads/products/thumbnail/VURm9ToBCIXVsMJbfYBFZixRCNbJYTjSCw1ocBPQ.png', 'uploads/products/featured/noJqSCAuInPo4dF9CovycrvLknqroaSsHt86PkMJ.png', 'uploads/products/flash_deal/DGbpLJ5XByeW27ol8iGckmK6SlijYcqlXuCH4eOi.png', 'youtube', NULL, 'Long eggplant black', NULL, 35.00, 100.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09ae\\u09cd\\u09ac\\u09be \\u0995\\u09be\\u09b2\\u09cb \\u09ac\\u09c7\\u0997\\u09c1\\u09a8\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"35.00\",\"sku\":\"Leb-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"70.00\",\"sku\":\"Leb-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"100.00\",\"sku\":\"Leb-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লম্বা কালো বেগুন', NULL, 'uploads/products/meta/S20rv2mZNvc0miJYJodys8w6PcqnaR2vRmzy4BFQ.png', NULL, 'Long-eggplant-black-RGMCF', 0.00, '2020-05-14 00:19:09', '2020-06-18 07:40:24'),
(37, 'Carrots', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/QEUmCSdBMdfkAYec6T1gtwuO8Goax1wGQ2I9D56s.jpeg\"]', 'uploads/products/thumbnail/8klyEwKDWA7fIn3V1fYRcr4Gq3VsfpfAmTplteHc.jpeg', 'uploads/products/featured/39GmzwyA4LFbOlrHaLAtEkPHRBh2AowjgfXyU9Fb.jpeg', 'uploads/products/flash_deal/71c7vUfabCpuisHwAbx95rzmdWLFa4DxlNwSXrHe.jpeg', 'youtube', NULL, 'Carrots', NULL, 20.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0997\\u09be\\u0981\\u099c\\u09b0\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"20.00\",\"sku\":\"C-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"40.00\",\"sku\":\"C-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"60.00\",\"sku\":\"C-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'গাঁজর', NULL, 'uploads/products/meta/aatirOuuCpSiSGLxL3WvdnOHIyfMJczJtffx05dG.jpeg', NULL, 'Carrots-4p2zj', 0.00, '2020-05-14 00:24:52', '2020-06-18 07:41:07'),
(38, 'The barb', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/Hf46idUQzAPwTN9FJyp1G23gcJFN42mmFci4frao.jpeg\"]', 'uploads/products/thumbnail/frKH1PRZng6VLKGrD5Thd4fIyoOMi5SRE3bQwKM7.jpeg', 'uploads/products/featured/wbPQzXTtz0T33nC6pv4HmDZDkOO1mULsGufhqJrq.jpeg', 'uploads/products/flash_deal/o2euaqmwbwNUzity9syqs0WQF0iFZ5GEActK9Sbr.jpeg', 'youtube', NULL, 'The barb', NULL, 30.00, 90.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09b0\\u09ac\\u099f\\u09bf\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"30.00\",\"sku\":\"Tb-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"60.00\",\"sku\":\"Tb-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"90.00\",\"sku\":\"Tb-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বরবটি', NULL, 'uploads/products/meta/6P0f0U3WE5ZRWcUaoPl7HgkkRCI8z77IPoIHbx1N.jpeg', NULL, 'The-barb-0mpTk', 0.00, '2020-05-14 00:27:59', '2020-06-18 07:41:41'),
(39, 'Lau', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/ayWtBbCOXklLoAoroZOXT3MjpekpHQwUfmgQWBbf.jpeg\",\"uploads\\/products\\/photos\\/yylFiXfp8afrVf9BwS4IaI9lMRtcTPbs77Qt5kzU.jpeg\"]', 'uploads/products/thumbnail/ZaD9Ho7F0JG0e7GkjBMma5pTOYfSaVb6KrVBQWwx.jpeg', 'uploads/products/featured/46LRzpHEMp03gsFtoE5X5GfvCpJHwXCLRGOhZvKB.jpeg', 'uploads/products/flash_deal/VT3kOqWV4Vw0iS55nmAOeAUD0FjLYJSGXl59jxNh.jpeg', 'youtube', NULL, 'Lau', NULL, 55.00, 160.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u0989\",\"options\":[\"\\u09e7\\u09aa\\u09bf\\u099b\",\"\\u09e8\\u09aa\\u09bf\\u099b\",\"\\u09e9\\u09aa\\u09bf\\u099b\"]}]', '[]', '{\"\\u09e7\\u09aa\\u09bf\\u099b\":{\"price\":\"55.00\",\"sku\":\"L-\\u09e7\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"},\"\\u09e8\\u09aa\\u09bf\\u099b\":{\"price\":\"110.00\",\"sku\":\"L-\\u09e8\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"},\"\\u09e9\\u09aa\\u09bf\\u099b\":{\"price\":\"160.00\",\"sku\":\"L-\\u09e9\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লাউ', NULL, 'uploads/products/meta/VX15v7MCGAeuOOQZpThtaKe0s4Kb5jByaSFLr6eX.jpeg', NULL, 'Lau-7EBye', 0.00, '2020-05-14 00:31:42', '2020-06-18 07:43:32'),
(40, 'Sweet pumpkin', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/Fa6e1N6W3eNo4YIJcef9yiKVecNj3pv65BpXukuh.jpeg\",\"uploads\\/products\\/photos\\/dJfCMYI9rNMyxqaAwP3RjfKAYOWfqBenMeA8dXD3.jpeg\"]', 'uploads/products/thumbnail/jE5x1VQ2UfWzD4LLN2dxhDYmAYW3tgqWGEJEBjQF.jpeg', 'uploads/products/featured/insdtYmIDa4D7Vt41S3YaaGOoS72bslnlQgJCkdX.jpeg', 'uploads/products/flash_deal/BNyLh7EdCodQ6LgUNq6K91if3uD0ad82peTjaJeC.jpeg', 'youtube', NULL, 'Sweet-pumpkin', NULL, 30.00, 120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ae\\u09bf\\u09b7\\u09cd\\u099f\\u09bf \\u0995\\u09c1\\u09ae\\u09a1\\u09bc\\u09cb\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"30.00\",\"sku\":\"Sp-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"600.00\",\"sku\":\"Sp-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"90.00\",\"sku\":\"Sp-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"120.00\",\"sku\":\"Sp-\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'মিষ্টি কুমড়ো', NULL, 'uploads/products/meta/utSzqDgSKwwF5zNITMiYn50vMpzC4DEZTaxhXnUW.jpeg', NULL, 'Sweet-pumpkin-TYyfD', 0.00, '2020-05-14 00:46:40', '2020-06-18 07:44:23'),
(41, 'Pumpkin', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/ufQNc85IL8Tb0DUgNxnM23baidaf4vZbhQTShske.jpeg\"]', 'uploads/products/thumbnail/N1KIY8Bli7rAlDxf3KIASWbIifofFBtlXK3lOLCE.jpeg', 'uploads/products/featured/eqdldvIQyWMXgsY3mJLFJD5uLA7zFaElsSsVFaom.jpeg', 'uploads/products/flash_deal/PDxMYVJCrq95uH0oeJxENcUWgcsD72PLzE9uHxVn.jpeg', 'youtube', NULL, 'Pumpkin', NULL, 48.00, 140.00, '[{\"name\":\"choice_1\",\"title\":\"\\u099a\\u09be\\u09b2\\u0995\\u09c1\\u09ae\\u09a1\\u09bc\\u09be\",\"options\":[\"\\u09e7\\u09aa\\u09bf\\u099b\",\"\\u09e8\\u09aa\\u09bf\\u099b\",\"\\u09e9\\u09aa\\u09bf\\u099b\"]}]', '[]', '{\"\\u09e7\\u09aa\\u09bf\\u099b\":{\"price\":\"50.00\",\"sku\":\"P-\\u09e7\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"},\"\\u09e8\\u09aa\\u09bf\\u099b\":{\"price\":\"100.00\",\"sku\":\"P-\\u09e8\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"},\"\\u09e9\\u09aa\\u09bf\\u099b\":{\"price\":\"150.00\",\"sku\":\"P-\\u09e9\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'চালকুমড়া', NULL, 'uploads/products/meta/n1hCsVS3xmD5G3i92dLapfaHqSEM3OerxfAislZs.jpeg', NULL, 'Pumpkin-tNx7c', 0.00, '2020-05-14 01:09:40', '2020-06-18 07:44:56'),
(42, 'Pebbles', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/vYCTOMfx9UOxNqtYNK9T6Ydz22t4PkdTHNwqhZxp.jpeg\"]', 'uploads/products/thumbnail/45xQnCpjWDTY3k7kQ8DbJceALwSkXfuYMVJevaMX.jpeg', 'uploads/products/featured/tLAjuNi43Jsrst34KN6W2aMSARU5eVGt3rf5wQRV.jpeg', 'uploads/products/flash_deal/4gUgQtUItxCItpAuHkSpQusqrC0jZUbIpVOLCUsA.jpeg', 'youtube', NULL, 'Pebbles', NULL, 20.00, 120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09be\\u0981\\u0995\\u09b0\\u09cb\\u09b2\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"40.00\",\"sku\":\"P-500g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"80\",\"sku\":\"P-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"1200.00\",\"sku\":\"P-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'কাঁকরোল', NULL, 'uploads/products/meta/lrBOrN211VLTerqTvQdLfBMXCPX6M5W7ArzDuXmv.jpeg', NULL, 'Pebbles-iMuNE', 0.00, '2020-05-14 01:14:24', '2020-06-18 07:45:34'),
(43, 'Chichinga', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/E8p6IyBS6qxFE4UvzS7jl9MtjvaQLYvXA7CRH9Nj.jpeg\"]', 'uploads/products/thumbnail/QLhgdGJ4bbuV3EQsonqCwtPWxqoEzFSdV8ob0te0.jpeg', 'uploads/products/featured/5Gv136NN27Y1rX7sQAyjBahw6mvKiYw270ki18K5.jpeg', 'uploads/products/flash_deal/CiqXlhbukm1RDlpAhJclRpbZL09m9DrTCUTOBCHp.jpeg', 'youtube', NULL, 'Chichinga', NULL, 33.00, 90.00, '[{\"name\":\"choice_0\",\"title\":\"\\u099a\\u09bf\\u099a\\u09bf\\u0999\\u09cd\\u0997\\u09be\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"33.00\",\"sku\":\"C-500g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"65\",\"sku\":\"C-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"90.00\",\"sku\":\"C-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'চিচিঙ্গা', NULL, 'uploads/products/meta/et3rkTW3DrCwYvj0JIbaGEsMNI77fFN4ySLM87OX.jpeg', NULL, 'Chichinga-ZyK62', 0.00, '2020-05-14 01:20:56', '2020-06-18 07:39:42'),
(44, 'Red tomatoes', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/nrYdtoXORmP2bCmgTS6K18PqdRolkdiZQMZELnvw.jpeg\"]', 'uploads/products/thumbnail/t6JbCcuzC3K9sgXd6vbg4MIpI0G5WlcTKdTEDNGR.jpeg', 'uploads/products/featured/0T5M71yjeB4CP8oVKVgaPpl2yW7UfDlsNgHl45wB.jpeg', 'uploads/products/flash_deal/AzhLIZbBvniHoD5aQMcPNepeeC1rGeYsNTUrX70m.jpeg', 'youtube', NULL, 'Red tomatoes', NULL, 20.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u09b2 \\u099f\\u09ae\\u09c7\\u099f\\u09cb\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"20\",\"sku\":\"Rt-500g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"40\",\"sku\":\"Rt-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"60\",\"sku\":\"Rt-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লাল টমেটো', NULL, 'uploads/products/meta/rXcwYKMjMvWgXPjEcAS4b2KU96OZn6WhKavJet23.jpeg', NULL, 'Red-tomatoes-DRIE6', 0.00, '2020-05-14 09:40:04', '2020-06-18 07:39:08'),
(45, 'Raw banana', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/2jrUADs4rprW8fhLjVCAWkwMCx9TXha2xQl86a7w.jpeg\"]', 'uploads/products/thumbnail/izp1AUZdVgXgKGLJpAfXUgnXFPvrkCboyuFK7o9p.jpeg', 'uploads/products/featured/m3I1fgrAyT95HFXRi5TXCpK4scqEFb2cY7KFIIp8.jpeg', 'uploads/products/flash_deal/eXmcCkpGQXlDSXrfSreS0Grve5MSwWI3LoARb6el.jpeg', 'youtube', NULL, 'Raw banana', NULL, 40.00, 120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09be\\u0981\\u099a\\u09be \\u0995\\u09b2\\u09be\",\"options\":[\"4pic\",\"8pic\",\"12pic\"]}]', '[]', '{\"4pic\":{\"price\":\"40\",\"sku\":\"Rb-4pic\",\"qty\":\"98\"},\"8pic\":{\"price\":\"80\",\"sku\":\"Rb-8pic\",\"qty\":\"100\"},\"12pic\":{\"price\":\"120\",\"sku\":\"Rb-12pic\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'কাঁচা কলা', NULL, 'uploads/products/meta/XQFuiNvjQHHRiOkHRwDkMYc1dYpOaxvv91DbR2Ed.jpeg', NULL, 'Raw-banana-odScw', 0.00, '2020-05-14 09:48:42', '2020-06-18 07:46:20'),
(46, 'Round black eggplant', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/AzREOWsPHbOyK7TjcVj6fqeuHSxV39DESnNMNjNX.png\"]', 'uploads/products/thumbnail/lRRb8z5oN9fblbCaCN40mpjdw2EUNkwzALH6RGAw.png', 'uploads/products/featured/6yyeMonQ7lnVHZz2FGNitgdsfBhTP04Xz92kBofI.png', 'uploads/products/flash_deal/brhbkpxIyj52h7lsM2fLAtm8rtfmTF8iVwFFvuu3.png', 'youtube', NULL, 'Round black eggplant', NULL, 42.00, 100.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0997\\u09cb\\u09b2 \\u0995\\u09be\\u09b2\\u09cb \\u09ac\\u09c7\\u0997\\u09c1\\u09a8\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"42\",\"sku\":\"Rbe-500g\",\"qty\":\"97\"},\"1kg\":{\"price\":\"80\",\"sku\":\"Rbe-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"120\",\"sku\":\"Rbe-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, 'গোল কালো বেগুন', NULL, 'uploads/products/meta/liHUgcbc5AwEN6bPaMhcoWdDZ1dJzSF7hmYF7wLt.png', NULL, 'Round-black-eggplant-T8sqJ', 0.00, '2020-05-14 09:54:26', '2020-06-18 07:46:57'),
(47, 'Native onion', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/Ad0yHyNzjeS06NEMnd8hVh95RCscI7HLDTis8slt.jpeg\"]', 'uploads/products/thumbnail/mkMhPgVG7GhHTZGJBGVRqdKe5H3jXf6gvAfc6Ge3.jpeg', 'uploads/products/featured/BPvDc3uweZB6c28KlmYN3welWXSS6cLdros9Wh46.jpeg', 'uploads/products/flash_deal/kMzUrMzBoURU4BUbsfmIoTwWPW5DUhxlVf1Mliap.jpeg', 'youtube', NULL, 'Native onion', NULL, 25.00, 75.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a6\\u09c7\\u09b6\\u09bf \\u09aa\\u09bf\\u0981\\u09af\\u09bc\\u09be\\u099c\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"25\",\"sku\":\"No-500g\",\"qty\":\"98\"},\"1kg\":{\"price\":\"50\",\"sku\":\"No-1kg\",\"qty\":90},\"1.5kg\":{\"price\":\"75\",\"sku\":\"No-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, 'দেশি পিঁয়াজ', NULL, 'uploads/products/meta/H9YPD3JT3NOgbewAf4jV7IhaJooiDEOuGP32gd7c.jpeg', NULL, 'Native-onion-g0iNI', 0.00, '2020-05-14 09:58:19', '2020-09-15 00:33:51'),
(48, 'Potatoes', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/rxEztBicMpDLTnJC4yFZ5QoALnOFsjYZAKnoysz4.jpeg\"]', 'uploads/products/thumbnail/SCMP5KHPxnBrsWEw2spW7NpjZ2M8qlexvDnRaPxw.jpeg', 'uploads/products/featured/dTbJbYiUMVWCkYGAVXdIuUy8vJf1ASfwrgFFTHPu.jpeg', 'uploads/products/flash_deal/Pl0Q5IPcMe1VhhgiO7JttNG7XDzPTBs1J8jMRV2d.jpeg', 'youtube', NULL, 'Potatoes', NULL, 16.00, 48.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0997\\u09cb\\u09b2 \\u0986\\u09b2\\u09c1\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"16\",\"sku\":\"P-500g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"32\",\"sku\":\"P-1kg\",\"qty\":97},\"1.5kg\":{\"price\":\"48\",\"sku\":\"P-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'গোল আলু', NULL, 'uploads/products/meta/modhKjLRwSM0f9R070fhao1dd6p1kzxkca6UeaQU.jpeg', NULL, 'Potatoes-tvgul', 0.00, '2020-05-14 10:02:50', '2020-09-15 00:33:51'),
(49, 'Ginger Indian', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/u0dIzP47e7uaGkQku62WpSZ1rxlW2UnaBQlZmiUV.jpeg\"]', 'uploads/products/thumbnail/dGNIU4JM68uhvoQ13pLtRA7bYUZ7fMKGJ3UzXofh.jpeg', 'uploads/products/featured/15Rovkv2J56iKqATBPQPsSIqNTQAey7BwYdWP5bc.jpeg', 'uploads/products/flash_deal/Omy12yoBPHPD2TcjmfbsWbXn7KSBVTAMH2L9Jm1Y.jpeg', 'youtube', NULL, 'Ginger-Indian', NULL, 50.00, 200.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u09a6\\u09be \\u0987\\u09a8\\u09cd\\u09a1\\u09bf\\u09df\\u09be\\u09a8\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"50\",\"sku\":\"GI-500g\",\"qty\":\"97\"},\"1kg\":{\"price\":\"200\",\"sku\":\"GI-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"250\",\"sku\":\"GI-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, 'আদা ইন্ডিয়ান', NULL, 'uploads/products/meta/g1m7nua6hCl9QdwHfA7o221qcHwYCQMCPlnWdNUq.jpeg', NULL, 'Ginger-Indian-tP7PU', 0.00, '2020-05-14 23:25:33', '2020-06-18 07:48:30'),
(50, 'Garlic Premium Large', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/y9hdXvJMrL4Ya4dxxasP7EMtePxyLZrU4CbQAw4g.jpeg\"]', 'uploads/products/thumbnail/AMHwjFczSrcIBZHZJBSDB86TDJcukrwDpqojrEVi.jpeg', 'uploads/products/featured/f8X1ovN5j0jCp6mvK2CXPlFWDvCznsXvYckRHHO0.jpeg', 'uploads/products/flash_deal/p3hF629zLwccAp8TelbIRrNNOlLNccSerpJLUNic.jpeg', 'youtube', NULL, 'Garlic Premium (Large)', NULL, 85.00, 210.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09b8\\u09c1\\u09a8 \\u09aa\\u09cd\\u09b0\\u09bf\\u09ae\\u09bf\\u09df\\u09be\\u09ae (\\u09ac\\u09dc)\",\"options\":[\"500g\",\"1kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"85\",\"sku\":\"GPL-500g\",\"qty\":\"98\"},\"1kg\":{\"price\":\"170\",\"sku\":\"GPL-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"210\",\"sku\":\"GPL-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'রসুন প্রিমিয়াম (বড়)', NULL, 'uploads/products/meta/DVZiOjVir2MqYblLHEQlC56XvLoZXdcshfJPdhOd.jpeg', NULL, 'Garlic-Premium-Large-9FLRN', 0.00, '2020-05-14 23:29:39', '2020-06-18 07:49:06'),
(51, 'Beson boot', 'admin', 1, 29, 67, 91, 48, '[\"uploads\\/products\\/photos\\/riHe8VlWZEMlK7TZie0MV9B6AUPiMF5ZvLPyKGPB.jpeg\"]', 'uploads/products/thumbnail/FhG5mZkWaTV4JhLH4kWhhANICK8sZntrkyhCWHIG.jpeg', 'uploads/products/featured/yw53PF5bSHUIeYQSPAkmZsQiMGir8K23of8Xv34V.jpeg', 'uploads/products/flash_deal/Mmq18hx9ZHBYlOe6VoclUbmx7fBGjzFLVd4pL6RJ.jpeg', 'youtube', NULL, 'বুটের-বেশন-Beson-boot', NULL, 69.00, 135.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09c1\\u099f\\u09c7\\u09b0 \\u09ac\\u09c7\\u09b6\\u09a8\",\"options\":[\"500g\",\"1kg\"]}]', '[]', '{\"500g\":{\"price\":\"69\",\"sku\":\"Bb-500g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"135\",\"sku\":\"Bb-1kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বুটের বেশন- ৫০০গ্রাম/১কেজি', NULL, 'uploads/products/meta/71DmpflLpJbhfc2g0rwYHM99VsWRJMDltNLtKjW7.jpeg', NULL, 'Beson-boot-3rmAO', 0.00, '2020-05-15 00:09:19', '2020-06-18 07:54:20'),
(53, 'Flour', 'admin', 1, 29, 67, 91, 49, '[\"uploads\\/products\\/photos\\/ydZRiNKgUa8M2VWOWPhp8Ts9hYXEVxbvRfiLfN3j.jpeg\"]', 'uploads/products/thumbnail/uJghKMqYD1iNAaH2FoHq9YHx7NxBrwnCb7RJROIB.jpeg', 'uploads/products/featured/hxgz2taaQxUOXWCwjnkgSAU8VvafCX1Xuw8j0bJ6.jpeg', 'uploads/products/flash_deal/sqHW6SvvVTUkZJiVyCponJxCAWZHTakaW90Ofcpj.jpeg', 'youtube', NULL, 'আটা-Flour', NULL, 75.00, 75.00, '[{\"name\":\"choice_1\",\"title\":\"\\u0986\\u099f\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"75.00\",\"sku\":\"F-2kg\",\"qty\":\"100\"}}', 0, 1, 1, 100, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ফ্রেশ আটা ২কেজি', NULL, 'uploads/products/meta/DjQojVwHdXYYjVeZ1QfV4MsDmrFd0HJivV6B2SI1.jpeg', NULL, 'Flour-3P5xV', 0.00, '2020-05-15 01:08:24', '2020-06-16 10:03:12'),
(54, 'Chinigura Premium Aromatic Rice', 'admin', 1, 29, 67, 91, 67, '[\"uploads\\/products\\/photos\\/UPrV7WbxbOjcMquyvizbjhSVCfvcuQE2E201zkj0.jpeg\"]', 'uploads/products/thumbnail/OfWaWR5kv43YovPIfnEj73MMPWOMf9qnQv0CI2J4.jpeg', 'uploads/products/featured/6zMVmsDJhKPsk4kpouphJ0dB103LNijdgKD8g3bE.jpeg', 'uploads/products/flash_deal/wA3UdIjhlHjnPy2BayQw0lpPuEsOQn0XBcSRO72e.jpeg', 'youtube', NULL, 'Chinigura-Premium-Aromatic-Rice', NULL, 135.00, 135.00, '[]', '[]', '[]', 0, 1, 1, 100, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রুপচাঁদা চিনিগুড়া চাল ১কেজি', NULL, 'uploads/products/meta/IMqtAgjO6OvQvbZSeRIOAt0DVZbteiFerivOraeV.jpeg', NULL, 'Chinigura-Premium-Aromatic-Rice-pvyAK', 0.00, '2020-05-15 01:59:38', '2020-06-16 10:01:50'),
(55, 'Arang Chinigura Rice', 'admin', 1, 29, 67, 91, 66, '[\"uploads\\/products\\/photos\\/Oq80IBG4Dtze88q36BbO3l3tpg8hC7PeLTNOL9XX.jpeg\"]', 'uploads/products/thumbnail/WI4J3yEYifTb4xvcjlQn8jtiY4jRhXWOBOpGQmQ6.jpeg', 'uploads/products/featured/3yHbeKVC2dbSZvbSlxOFeYMnreJeNoWa7tOGrifD.jpeg', 'uploads/products/flash_deal/ZrXTGAIPDeFxpOutoCyhbW57mT8bxHpHxuRBerSk.jpeg', 'youtube', NULL, 'Arang-Chinigura-Rice', NULL, 245.00, 245.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u09dc\\u0982 \\u099a\\u09bf\\u09a8\\u09bf\\u0997\\u09c1\\u0981\\u09dc\\u09be \\u09b0\\u09be\\u0987\\u09b8\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"245\",\"sku\":\"ACR-2kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'আড়ং চিনিগুঁড়া চাল ২কেজি', NULL, 'uploads/products/meta/HZztwIlZLedDbmrMNqrImuo4sjMYKZQaqV63seIy.jpeg', NULL, 'Arang-Chinigura-Rice-p2fnF', 0.00, '2020-05-15 02:03:08', '2020-06-16 10:00:28'),
(56, 'Teer Sugarcane Rice', 'admin', 1, 29, 67, 91, 65, '[\"uploads\\/products\\/photos\\/79dcaqZhiqWRDeqHj9LHzpLlbowgKoKW0CKvlp6F.jpeg\"]', 'uploads/products/thumbnail/loqN4KGA7mWxFPbJhcFkZrEhfE0Al7Fm8OQCY44u.jpeg', 'uploads/products/featured/CiS6puQ5gIFir8LzmH7FsJ0sYTaQnqTk1iKnSeZZ.jpeg', 'uploads/products/flash_deal/HAbpnlbp03i1oI00RXTWFUffhD7JyrLh7SiYwxrA.jpeg', 'youtube', NULL, 'Teer-Sugarcane-Rice', NULL, 125.00, 125.00, '[]', '[]', '[]', 0, 1, 1, 100, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'তীর চিনিগুড়া চাল ১কেজি', NULL, 'uploads/products/meta/MmRtL83yyAI2JCTTByIbgkqQNhmPmYZWbzRt8xx0.jpeg', NULL, 'Teer-Sugarcane-Rice-R5XtA', 0.00, '2020-05-15 02:08:03', '2020-06-16 09:59:34'),
(57, 'Flour', 'admin', 1, 29, 67, 91, 69, '[\"uploads\\/products\\/photos\\/OgSjuVV7XtlqTu7MRsmTgojtgbmOoW8nIsT0OinA.jpeg\"]', 'uploads/products/thumbnail/Z5A8XcsOjQybRvWfqDs73u3wF1s9nmaPoujrUZ1e.jpeg', 'uploads/products/featured/8smJKIual0RrWNsYmRRcRbkOnyjCyfocVCs6JQsx.jpeg', 'uploads/products/flash_deal/WZsiXn8QGxXXNkLPmUJUIvSRmVxyc95IszfjBnlP.jpeg', 'youtube', NULL, 'আটা-Flour', NULL, 70.00, 70.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09c1\\u09b7\\u09cd\\u099f\\u09bf \\u0986\\u099f\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"70\",\"sku\":\"F-2kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'পুষ্টি আটা', NULL, 'uploads/products/meta/7j3Agtwd5XXUZRmGOmpOXg3tmY25lIWDCnqIiUGb.jpeg', NULL, 'Flour-sBXUs', 0.00, '2020-05-15 04:26:27', '2020-06-16 09:58:35'),
(58, 'Sugarcane Aromatic Rice', 'admin', 1, 29, 67, 91, 64, '[\"uploads\\/products\\/photos\\/LvWkDwJUTJIUnSRg6vBRbr2YT812ejgfwnbSSaM1.jpeg\"]', 'uploads/products/thumbnail/y8AacoCBMmPyVS3LKQLFhs00jCGUROATUQY7qhIW.jpeg', 'uploads/products/featured/Scywp4H9FT2koaT9A6Lpgs7QTcfoLNxVtqOzZLRp.jpeg', 'uploads/products/flash_deal/iwWlVBjQg6UvKV0DrouuPiTSLekgLm18Y7gVc03j.jpeg', 'youtube', NULL, 'Sugarcane-Aromatic-Rice', NULL, 120.00, 120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u099a\\u09bf\\u09a8\\u09bf\\u0997\\u09c1\\u0981\\u09dc\\u09be \\u098f\\u09b0\\u09cb\\u09ae\\u09be\\u099f\\u09bf\\u0995 \\u09b0\\u09be\\u0987\\u09b8\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"120\",\"sku\":\"SAR-1kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'স্বাদ চিনিগুঁড়া এরোমাটিক চাল ১কেজি', NULL, 'uploads/products/meta/tCr0ZuagxsiuFub8mHu4l0BLCn2YO4YgC5Yb2w5M.jpeg', NULL, 'Sugarcane-Aromatic-Rice-mAHdK', 0.00, '2020-05-15 04:30:05', '2020-06-16 09:57:29'),
(59, 'Aromatic Sugarcane Rice', 'admin', 1, 29, 67, 91, 63, '[\"uploads\\/products\\/photos\\/Ht7mNydVBVbiZMyNzwaSf5jKyF1IABgzTlLnfT7z.jpeg\"]', 'uploads/products/thumbnail/9pL9MPrxtdw5dkL54W2taJGiD5hgs9NTQZGe2Hup.jpeg', 'uploads/products/featured/2duGEPmc2nBy72znL7gKRnvZo1Jc7MHt9Ac4O4iw.jpeg', 'uploads/products/flash_deal/ZsjjT1z6CzpgWQzV5JPasoFJ0Ps9fiaZuiG2yJ2l.jpeg', 'youtube', NULL, 'Aromatic-Sugarcane-Rice', NULL, 235.00, 235.00, '[{\"name\":\"choice_0\",\"title\":\"\\u098f\\u09b0\\u09cb\\u09ae\\u09be\\u099f\\u09bf\\u0995 \\u099a\\u09bf\\u09a8\\u09bf\\u0997\\u09c1\\u0981\\u09dc\\u09be \\u09b0\\u09be\\u0987\\u09b8\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"235\",\"sku\":\"ASR-2kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'চাষী এরোমাটিক চিনিগুঁড়া চাল ১ কেজি', NULL, 'uploads/products/meta/Ug9AY6dPqGjvzBjGdmiyY8UpnpgkcI8xmobcPOKW.jpeg', NULL, 'Aromatic-Sugarcane-Rice-OOawr', 0.00, '2020-05-15 04:32:19', '2020-06-16 09:57:52'),
(60, 'Premier Sugarcane Rice', 'admin', 1, 29, 67, 91, 62, '[\"uploads\\/products\\/photos\\/MA0mQGMMl31WVSIAF1T9b3CWKai8T1sWH1Y9TxeY.jpeg\"]', 'uploads/products/thumbnail/rkpo3OdOgrHSXY3txaaYhGbQy4wStcJez1Z65L2H.jpeg', 'uploads/products/featured/H4EbyEeN85H3hFPo5drMPQRPTc6PY2WvXI2KdlCE.jpeg', 'uploads/products/flash_deal/VFMhUAYcHUP4vZrJCz9sRZ5lfJjhIcOrklDNXfdv.jpeg', 'youtube', NULL, 'Premier-Sugarcane-Rice', NULL, 130.00, 130.00, '[{\"name\":\"choice_0\",\"title\":\"\\u099a\\u09bf\\u09a8\\u09bf\\u0997\\u09c1\\u0981\\u09dc\\u09be \\u098f\\u09b0\\u09cb\\u09ae\\u09be\\u099f\\u09bf\\u0995 \\u09b0\\u09be\\u0987\\u09b8\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"130\",\"sku\":\"PSR-1kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'খুশবো চিনিগুঁড়া এরোমাটিক চাল ১ কেজি', NULL, 'uploads/products/meta/8YZqNAtvELYgJ2qn8LUnX5MvqtN2ZOT1E9uDut1f.jpeg', NULL, 'Premier-Sugarcane-Rice-v8D9h', 0.00, '2020-05-15 04:34:55', '2020-06-16 09:55:34'),
(61, 'Arrival Sugarcane Rice', 'admin', 1, 29, 67, 91, 61, '[\"uploads\\/products\\/photos\\/cn3rheccRWiIsXfJnzVN78QDsEUdVA7T9sIrvmaX.jpeg\"]', 'uploads/products/thumbnail/88jIEP9NZDvoOj2bkJcUANlNKxdsmLdUhwsaM243.jpeg', 'uploads/products/featured/8dO1Qtw7m3FhZjwcdzTf2aPE2bldWubTnL4qvSMc.jpeg', 'uploads/products/flash_deal/LWP1PPYsF4cav3YEQslKpkxXnGIbZYHk1E8ZXSep.jpeg', 'youtube', NULL, 'Arrival Sugarcane Rice', NULL, 130.00, 130.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u0997\\u09ae\\u09a8\\u09c0 \\u099a\\u09bf\\u09a8\\u09bf\\u0997\\u09c1\\u0981\\u09dc\\u09be \\u09b0\\u09be\\u0987\\u09b8\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"130\",\"sku\":\"ASR-1kg\",\"qty\":\"99\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'আগমনী চিনিগুঁড়া চাল ১কেজি', NULL, 'uploads/products/meta/IZkGx6GDzzaB816LeHn4W3xdZtV7x4UPBB9BzAIr.jpeg', NULL, 'Arrival-Sugarcane-Rice-DuQgm', 0.00, '2020-05-15 04:38:42', '2020-06-16 09:54:22'),
(62, 'Ifad Maida', 'admin', 1, 29, 67, 91, 60, '[\"uploads\\/products\\/photos\\/NDKDHnHmwBxaj1jq5KmyC4hWR6TMppKtnlW2C2IJ.jpeg\"]', 'uploads/products/thumbnail/2Oc6lQe8nn0pFsEwGyVdxI5S2Xbr40eODq790OYB.jpeg', 'uploads/products/featured/BbHtOjmgyxQhYBv19K1cDmD7Ls6BkbMuRo3LEAEa.jpeg', 'uploads/products/flash_deal/jvOBVKabEAiglZawiynMAbsKF9nD0bAg6UmZqp9c.jpeg', 'youtube', NULL, 'Ifad-Maida', NULL, 90.00, 90.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0987\\u09ab\\u09be\\u09a6 \\u09ae\\u09df\\u09a6\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"90\",\"sku\":\"IM-2kg\",\"qty\":\"98\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'ইফাদ ময়দা ২কেজি', NULL, 'uploads/products/meta/3cC25bpbAAnCxGLk9tl0ynvqb1to9Aoo3Q7k0utz.jpeg', NULL, 'Ifad-Maida-NKtIb', 0.00, '2020-05-15 04:40:51', '2020-06-16 09:52:58'),
(63, 'Bashundhara Maida', 'admin', 1, 29, 67, 91, 50, '[\"uploads\\/products\\/photos\\/Ha0ZbHbr8os5jcf7ZC0LlsKzpiV3UotMy7bG1ZAg.jpeg\"]', 'uploads/products/thumbnail/JQqLERAzgS2n7Qk3IkR7j2gF0MsNngeQSAnaUvcS.jpeg', 'uploads/products/featured/51dDc0b604r1U8ftTZVkjXDcUZPsyl4gC4yxLJpc.jpeg', 'uploads/products/flash_deal/6pYpTQC5SXmB3R6KLhj50ASkDNfBkJsvre30AHDI.jpeg', 'youtube', NULL, 'Bashundhara-Maida', NULL, 90.00, 90.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09b8\\u09c1\\u09a8\\u09cd\\u09a7\\u09b0\\u09be \\u09ae\\u09df\\u09a6\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"90\",\"sku\":\"BM-2kg\",\"qty\":\"99\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'বসুন্ধরা ময়দা ২কেজি', NULL, 'uploads/products/meta/3Vjjsrp01k8n4mma1oKd4qAf7xbj224MpRalDS7M.jpeg', NULL, 'Bashundhara-Maida-EKxxA', 0.00, '2020-05-15 04:45:12', '2020-06-16 09:51:51'),
(64, 'Fresh Maida', 'admin', 1, 29, 67, 91, 49, '[\"uploads\\/products\\/photos\\/R4gH0T3PpzvKVhNyrdyt8QjPFUT86Gh8TsS8K4wA.jpeg\"]', 'uploads/products/thumbnail/ZCao9NlxlUjTmA2ycPpFPfgYzJwbvCGVRW1gDgM9.jpeg', 'uploads/products/featured/7XUmQ4jam7NGd60Nlrw9wPGJKfFOjsx3aAgUitoF.jpeg', 'uploads/products/flash_deal/bZQ6GRtQ4pzFTJv5i5kLwkvo6yKzf0OEwFYVPagu.jpeg', 'youtube', NULL, 'Fresh-Maida', NULL, 90.00, 90.00, '[{\"name\":\"choice_1\",\"title\":\"\\u09ab\\u09cd\\u09b0\\u09c7\\u09b6 \\u09ae\\u09df\\u09a6\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"90.00\",\"sku\":\"FM-2kg\",\"qty\":\"99\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'ফ্রেশ ময়দা ২কেজি', NULL, 'uploads/products/meta/aef6q2anQBPe4L0rJDgM8VeNfUEvO7fOGJ2fG6Un.jpeg', NULL, 'Fresh-Maida-fRAYN', 0.00, '2020-05-15 04:48:08', '2020-06-16 09:52:16'),
(65, 'Pusti Maida', 'admin', 1, 29, 67, 91, 69, '[\"uploads\\/products\\/photos\\/HefG8D1mpOjFJPGAtgIVPTikeoyLxheVexcx7I75.jpeg\"]', 'uploads/products/thumbnail/7LX4LzljcJ0xqGv01EfPRadqoqDeKvYRTGhuvwGv.jpeg', 'uploads/products/featured/buckFq3WPJhgeC894t9UKohJ6tN1LLcNs2yFwWQk.jpeg', 'uploads/products/flash_deal/ZKKet58nCtHvtyjx9SgiPTqFp76O56mPE31R8ubQ.jpeg', 'youtube', NULL, 'Pusti-Maida', NULL, 90.00, 90.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09c1\\u09b7\\u09cd\\u099f\\u09bf \\u09ae\\u09df\\u09a6\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"90\",\"sku\":\"PM-2kg\",\"qty\":97}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, 'পুষ্টি ময়দা ২কেজি', NULL, 'uploads/products/meta/0yE2zp0DAWelvlX99HNNOUauKwNdOPA8IBi2D7Da.jpeg', NULL, 'Pusti-Maida-koP0r', 0.00, '2020-05-15 04:51:29', '2021-05-18 12:31:28'),
(66, 'Teer Maida', 'admin', 1, 29, 67, 91, 65, '[\"uploads\\/products\\/photos\\/oso4kbwNfZvrnIBe7zNBGIjYE850GoDPQ0aL7Cs9.jpeg\"]', 'uploads/products/thumbnail/JvocRRywxLz7ZHu13l7rhX0piC8LCOd6EAmIkjFD.jpeg', 'uploads/products/featured/Hndo0cTQuAIkdDQ4LWLKWU6esL6tIN7svYTPiNTx.jpeg', 'uploads/products/flash_deal/swI1Hj7OuwLYDxQy1B8Ag7CuRhtJLLLqgSzgmS9w.jpeg', 'youtube', NULL, 'Teer-Maida', NULL, 90.00, 90.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a4\\u09c0\\u09b0 \\u09ae\\u09df\\u09a6\\u09be\",\"options\":[\"2kg\"]}]', '[]', '{\"2kg\":{\"price\":\"90\",\"sku\":\"TM-2kg\",\"qty\":\"98\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'তীর ময়দা ২কেজি', NULL, 'uploads/products/meta/NJBkOgHDFGmEeqTlFyjSEa5Y4MLwroeVzGsH4RnP.jpeg', NULL, 'Teer-Maida-HjnCx', 0.00, '2020-05-15 04:53:22', '2020-06-16 09:46:53'),
(67, 'Bashundhara Suji', 'admin', 1, 27, 69, 89, 50, '[\"uploads\\/products\\/photos\\/YSyjti3zkgiOhn9OgfvLK3I5FQYAbMAuAGg8QPll.jpeg\"]', 'uploads/products/thumbnail/05snlGYZhfgO61gFa81RILXv2GIQcMMcJ5UJak9t.jpeg', 'uploads/products/featured/KfHSUIucbBI0Tm9E6jI1afkgYzemvUPIiMfNtg3M.jpeg', 'uploads/products/flash_deal/JznMi3qrF08VOTGXmVI769gYwGEFp6pcWQzvPIEG.jpeg', 'youtube', NULL, 'Bashundhara-Suji', NULL, 30.00, 30.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09b8\\u09c1\\u09a8\\u09cd\\u09a7\\u09b0\\u09be \\u09b8\\u09c1\\u099c\\u09bf\",\"options\":[\"500g\"]}]', '[]', '{\"500g\":{\"price\":\"30\",\"sku\":\"BS-500g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বসুন্ধরা সুজি ৫০০গ্রাম', NULL, 'uploads/products/meta/Fy5t6iCKgGT9xi0zFnjPsMY6RLDXrhvPzvvumD6v.jpeg', NULL, 'Bashundhara-Suji-iQmC2', 0.00, '2020-05-15 05:10:25', '2020-06-16 09:45:04'),
(68, 'Fresh Suji', 'admin', 1, 27, 69, 89, 49, '[\"uploads\\/products\\/photos\\/HsA8X4plpQbFvU9cPGdY7xmXmuboZkfD3ahFuRtA.jpeg\"]', 'uploads/products/thumbnail/eBcTossZ7vj2cbJxdBgd2NL4IIzdjInfN1efXdy6.jpeg', 'uploads/products/featured/0PQSEymlemokfKhsyBxZp2KGZMW3rlUcTAjGyhPB.jpeg', 'uploads/products/flash_deal/rma1cyYeqEFs8I4TTNdByPyZrcWMeIfuqTIaBo8E.jpeg', 'youtube', NULL, 'Fresh-Suji', NULL, 30.00, 30.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ab\\u09cd\\u09b0\\u09c7\\u09b6 \\u09b8\\u09c1\\u099c\\u09bf\",\"options\":[\"500g\"]}]', '[]', '{\"500g\":{\"price\":\"30\",\"sku\":\"FS-500g\",\"qty\":97}}', 0, 1, 1, 0, '500g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, 'ফ্রেশ সুজি ৫০০গ্রাম', NULL, 'uploads/products/meta/KobqZOYswwYlgZ7LQiy5wSXW7FYBEYt0P2giemtD.jpeg', NULL, 'Fresh-Suji-PvrNW', 0.00, '2020-05-15 05:13:44', '2020-10-27 03:23:03'),
(69, 'Pran lachcha Semai', 'admin', 1, 27, 69, 89, 57, '[\"uploads\\/products\\/photos\\/Wwy0ny4HKf6gHYux2zFS2cztGkq0AHa2t6UpxRsI.jpeg\"]', 'uploads/products/thumbnail/fPnsMuHfS5lUapoHTLUBgS1zRudbwawWZjFb1FD4.jpeg', 'uploads/products/featured/dj8vvtCgXxoHlmiibTYdRNNcWuaOUuwP65Kiwdjs.jpeg', 'uploads/products/flash_deal/ekg9pLOzBJ0jW75ZdlYXO70ThpANSvtOhU2n4507.jpeg', 'youtube', NULL, 'Pran-lachcha-semai', NULL, 35.00, 35.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09cd\\u09b0\\u09be\\u09a8 \\u09b2\\u09be\\u099a\\u09cd\\u099b\\u09be \\u09b8\\u09c7\\u09ae\\u09be\\u0987\",\"options\":[\"200g\"]}]', '[]', '{\"200g\":{\"price\":\"35\",\"sku\":\"PlS-200g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'প্রান লাচ্ছা সেমাই', NULL, 'uploads/products/meta/XAHUITvu7njcGpFqg2VvsIb1F4gTYOd8HqpkmVQN.jpeg', NULL, 'Pran-lachcha-Semai-kfkhS', 0.00, '2020-05-15 07:07:44', '2020-06-16 09:43:10'),
(70, 'Pran Ready Semai Mix', 'admin', 1, 27, 69, 89, 57, '[\"uploads\\/products\\/photos\\/Us6YOnpcoBofpuGiUmZ7aMAxQo56dQtGsUhvK7fx.jpeg\"]', 'uploads/products/thumbnail/C4bcQZrAgddrcfNNklIX2afd7DdHHRIZN6KrlB2N.jpeg', 'uploads/products/featured/E7bBrkGOPa09LuwKkXKdvPqKms4uJ4VjUtt6K3oJ.jpeg', 'uploads/products/flash_deal/sq84oAS1SESY2DBTWig8X7KUUI3mvdKoK0Pkf57W.jpeg', 'youtube', NULL, 'Pran Ready Semai Mix', NULL, 75.00, 75.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09cd\\u09b0\\u09be\\u09a8 \\u09b0\\u09c7\\u09a1\\u09bf \\u09b8\\u09c7\\u09ae\\u09be\\u0987 \\u09ae\\u09bf\\u0995\\u09cd\\u09b8\",\"options\":[\"140g\"]}]', '[]', '{\"140g\":{\"price\":\"75\",\"sku\":\"PRSM-140g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'প্রান রেডি সেমাই মিক্স', NULL, 'uploads/products/meta/Gk6bcG5J9N0Pr6g6pzDCOAUPmSPVP1GgJEJfUoSx.jpeg', NULL, 'Pran-Ready-Semai-Mix-vjBsd', 0.00, '2020-05-15 07:14:52', '2020-06-16 09:42:27'),
(71, 'Fresh Refined Sugar', 'admin', 1, 27, 69, 89, 57, '[\"uploads\\/products\\/photos\\/9nCarYhnWLCyyhO6q4kswvIdagqO9IYH5njUYdy4.jpeg\"]', 'uploads/products/thumbnail/UlBC8pSv59mLsdvpQtPhmGO90eHyuYSS2Le5ufN8.jpeg', 'uploads/products/featured/lo3n0rFloTrVu4DHsZionoHG18hU43YwVGlUhz6E.jpeg', 'uploads/products/flash_deal/NbcRm2XjDqLwqt1ijgZ2aHdEzAcU8U1bnpXJ35Dp.jpeg', 'youtube', NULL, 'Fresh-Refined-Sugar', NULL, 72.00, 72.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ab\\u09cd\\u09b0\\u09c7\\u09b6 \\u09b0\\u09bf\\u09ab\\u09be\\u0987\\u09a8\\u09cd\\u09a1 \\u09b8\\u09c1\\u0997\\u09be\\u09b0\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"72\",\"sku\":\"FRS-1kg\",\"qty\":99}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'ফ্রেশ রিফাইন্ড সুগার', NULL, 'uploads/products/meta/yKkuI5UoC70iyEqgpFF1AfddVkqoBMx0HmQmmbld.jpeg', NULL, 'Fresh-Refined-Sugar-SJlVO', 0.00, '2020-05-15 07:18:22', '2020-09-15 00:33:51'),
(72, 'ACI Pure Salt', 'admin', 1, 27, 69, 89, 51, '[\"uploads\\/products\\/photos\\/aURfj3osdLvOmOKTEjeG8kY2qlPkn7K6rmbrCU19.jpeg\"]', 'uploads/products/thumbnail/pYCNtIbDssmiJhA3KS8UANqsm7ofqYjtERmbKVBT.jpeg', 'uploads/products/featured/STMwJEAIgsS9d41yj7ABVb2pRralQRxfLt7YiHsV.jpeg', 'uploads/products/flash_deal/8wAjPZXZrsYRmZ5cDGOsi8I3fYxw6ymnVHPuyc4Y.jpeg', 'youtube', NULL, 'ACI-Pure-Salt', NULL, 35.00, 35.00, '[{\"name\":\"choice_0\",\"title\":\"\\u098f-\\u09b8\\u09bf-\\u0986\\u0987 \\u09aa\\u09bf\\u0993\\u09b0 \\u09b2\\u09ac\\u09a8 \\u09e7 \\u0995\\u09c7\\u099c\\u09bf\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"35\",\"sku\":\"APS-1kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'এ-সি-আই পিওর লবন ১ কেজি', NULL, 'uploads/products/meta/cDoLuaFLV8WVZKHA3caVg2rvdUo5JTb3lgCXU7oh.jpeg', NULL, 'ACI-Pure-Salt-Pa48h', 0.00, '2020-05-15 07:20:51', '2020-06-16 09:41:03'),
(73, 'Hilsa Fish Regular Size', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/LZ41vB4h2X3mRbd3Aq9ndP9vHAho2pq8EwznFdPQ.jpeg\"]', 'uploads/products/thumbnail/AQ7d47PefUzGYMNjqcimXYZAPql0DRCcuJROEaXv.jpeg', 'uploads/products/featured/KTvRbHEZrTKbAR5Lv16eVDT2OAgRXvVHorHKyZJy.jpeg', 'uploads/products/flash_deal/zUpsp4k9qCl9BSengszEyXOxtKI576cTvl1wvgfL.jpeg', 'youtube', NULL, 'Hilsa-Fish-Regular-Size', NULL, 500.00, 1300.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0987\\u09b2\\u09bf\\u09b6 \\u09ae\\u09be\\u099b \\u09b0\\u09c7\\u0997\\u09c1\\u09b2\\u09be\\u09b0 \\u09b8\\u09be\\u0987\\u099c\",\"options\":[\"500g\",\"800g\",\"1kg\",\"1.25kg\"]}]', '[]', '{\"500g\":{\"price\":\"500\",\"sku\":\"HFRS-500g\",\"qty\":\"100\"},\"800g\":{\"price\":\"800\",\"sku\":\"HFRS-800g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"1000\",\"sku\":\"HFRS-1kg\",\"qty\":\"100\"},\"1.25kg\":{\"price\":\"1300\",\"sku\":\"HFRS-1.25kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ইলিশ মাছ রেগুলার সাইজ', NULL, 'uploads/products/meta/03k9z5pJlvXjbZ8x19cRz1o6BJFUWgh9a5vb2sCA.jpeg', NULL, 'Hilsa-Fish-Regular-Size-RUDUG', 0.00, '2020-05-15 22:40:03', '2020-09-10 07:04:27'),
(74, 'Pangas Fish', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/eDtgO2POjSzY2hlnqIYmGo2PaOoRTwwL5xzdyD0m.jpeg\"]', 'uploads/products/thumbnail/CHSejGfH2bfRZHk4QMKn5nyRqmrXNMARSN6SOTb6.jpeg', 'uploads/products/featured/Dxcm8MeexjKqo3C71dR6qfvgW0tqvFYMNzoIXqxS.jpeg', 'uploads/products/flash_deal/6auZVYHMHJIX43m62JVY74hAkUbgP1FbKMheX97p.jpeg', 'youtube', NULL, 'Pangas Fish', NULL, 150.00, 250.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09be\\u0982\\u0997\\u09be\\u09b8 \\u09ae\\u09be\\u099b\",\"options\":[\"800g\",\"1kg\",\"1.5kg\",\"2kg\"]}]', '[]', '{\"800g\":{\"price\":\"150\",\"sku\":\"PF-800g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"2000\",\"sku\":\"PF-1kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"2200\",\"sku\":\"PF-1.5kg\",\"qty\":\"100\"},\"2kg\":{\"price\":\"250\",\"sku\":\"PF-2kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'পাংগাস মাছ', NULL, 'uploads/products/meta/ysrLgZphgX3dAI1Z1ZuOxV9Km1yIsFS7sd8Mgqn6.jpeg', NULL, 'Pangas-Fish-dTV5j', 0.00, '2020-05-15 22:44:04', '2020-06-16 09:39:02'),
(75, 'Shrimp', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/vYVA7XLldHMXwJDRf2zRBc1wDR2PjBQSUo3pDTFi.jpeg\"]', 'uploads/products/thumbnail/vGyyBcQrnJaxCsUYahfx7G5ddioPw99a6y1RGrvT.jpeg', 'uploads/products/featured/8IPX8kV4ktP6UHrX1CWh3tnevTXcrMV9oZelb7l6.jpeg', 'uploads/products/flash_deal/Xgaqz7ITtTo1d1ZshzDNQ2Yr4cLmn9CyG19V52ql.jpeg', 'youtube', NULL, 'Shrimp', NULL, 400.00, 800.00, '[{\"name\":\"choice_0\",\"title\":\"\\u099a\\u09bf\\u0982\\u09dc\\u09bf \\u09ae\\u09be\\u099b\",\"options\":[\"400g\",\"600g\"]}]', '[]', '{\"400g\":{\"price\":\"4\",\"sku\":\"S-400g\",\"qty\":\"100\"},\"600g\":{\"price\":\"800\",\"sku\":\"S-600g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'চিংড়ি মাছ', NULL, 'uploads/products/meta/apRW3vGFSpDg31IVT1hih1MPgq5ihPbThtJzgnQ9.jpeg', NULL, 'Shrimp-UcuZz', 0.00, '2020-05-15 22:49:16', '2020-06-16 09:38:13'),
(76, 'Rui Fish Desi A Grea', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/NWLg42md3JVgfDZ3MKLNrZoFKsdQ5ih1q1ATDFV4.jpeg\"]', 'uploads/products/thumbnail/3m7iXnInkd8TOhj4fH61YbPMjvqu8G7nXxSe2ACQ.jpeg', 'uploads/products/featured/1mJ3iL0KUu70ZnMmL9rBpQzlgrC4RwPbaQfz4FDh.jpeg', 'uploads/products/flash_deal/oVsIJywj43CKrTuwf36IBMUC6xtIPfJXDUsyr0g7.jpeg', 'youtube', NULL, 'Rui Fish Desi', NULL, 250.00, 500.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09c1\\u0987 \\u09ae\\u09be\\u099b \\u09a6\\u09c7\\u09b6\\u09c0\",\"options\":[\"500g\",\"800g\",\"1kg\",\"1.25kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"250\",\"sku\":\"RFDAG-500g\",\"qty\":\"100\"},\"800g\":{\"price\":\"3000\",\"sku\":\"RFDAG-800g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"350\",\"sku\":\"RFDAG-1kg\",\"qty\":\"100\"},\"1.25kg\":{\"price\":\"450\",\"sku\":\"RFDAG-1.25kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"500\",\"sku\":\"RFDAG-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রুই মাছ দেশী (এ গ্রেট)', NULL, 'uploads/products/meta/tpwwiHFAVVQzRWEonkDB14ayI8cgmP7EsASdCAL2.jpeg', NULL, 'Rui-Fish-Desi-A-Grea-sZuDg', 0.00, '2020-05-15 22:54:04', '2020-06-20 01:06:40'),
(77, 'Catla Fish A Grea', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/3UZrnWXJ2TsbdU0X01zqdLU9wMx5uXdp2PEm1ttw.jpeg\"]', 'uploads/products/thumbnail/nZKDfvWG3H2m2tCu2S4J5MMD3uaociIBJ56YMG4C.jpeg', 'uploads/products/featured/6qz2bF76b7EBG9BorbW3Ax59yWF9LiA64WESKhs9.jpeg', 'uploads/products/flash_deal/Y8Z9IN44mSvFzi5YO5Ye6mXcxqyqBJqYAE2lvTRU.jpeg', 'youtube', NULL, 'Catla-Fish', NULL, 300.00, 500.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09be\\u09a4\\u09b2\\u09be \\u09ae\\u09be\\u099b (\\u098f \\u0997\\u09cd\\u09b0\\u09c7\\u099f)\",\"options\":[\"500g\",\"800g\",\"1kg\",\"1.25kg\",\"1.5kg\"]}]', '[]', '{\"500g\":{\"price\":\"300\",\"sku\":\"CF-500g\",\"qty\":\"100\"},\"800g\":{\"price\":\"350\",\"sku\":\"CF-800g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"400\",\"sku\":\"CF-1kg\",\"qty\":\"100\"},\"1.25kg\":{\"price\":\"450\",\"sku\":\"CF-1.25kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"500\",\"sku\":\"CF-1.5kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'কাতলা মাছ (এ গ্রেট)', NULL, 'uploads/products/meta/LoloYvylp7eY1ZvnfgicNhlAzCFkK4xT9Lz7oc1e.jpeg', NULL, 'Catla-Fish-A-Grea-G1mjg', 0.00, '2020-05-15 23:12:14', '2020-06-16 09:36:05'),
(78, 'Beef With Bones A Great', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/3MzI58Rc6af26T2Di23Ru3e28CCvNXqjNs2beZas.jpeg\",\"uploads\\/products\\/photos\\/MbGPyWk4x0XEXIeTiteCMtMvA4vFVP13moMMiWvh.jpeg\"]', 'uploads/products/thumbnail/j6GoUjawxtm3GRZNrPqQind6uCVTygUBkXGu0jaz.jpeg', 'uploads/products/featured/cRH8jK9dyJjSbyUEmMZPNOptahGqgigpagGjMs2f.jpeg', 'uploads/products/flash_deal/2rjGy7LdLxZWXUX50poe6DPF4jMoz1wqL4Lp1KZo.jpeg', 'youtube', NULL, 'Beef-With-Bones-A-Great', NULL, 620.00, 620.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b9\\u09be\\u09dc\\u09b8\\u09b9 \\u0997\\u09b0\\u09c1\\u09b0 \\u09ae\\u09be\\u0982\\u09b8 (\\u098f \\u0997\\u09cd\\u09b0\\u09c7\\u099f)\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"620\",\"sku\":\"BWBAG-1kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'হাড়সহ গরুর মাংস (এ গ্রেট)', NULL, 'uploads/products/meta/RCEkTgLNiZsbkohlLFa1cJCVFEAj6qiNVGB1EsrO.jpeg', NULL, 'Beef-With-Bones-A-Great-Gz1m0', 0.00, '2020-05-15 23:22:11', '2020-06-16 09:34:19'),
(79, 'Whole Broiler Chicken Without Skin', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/JjXYyUNM6W7MQ4kUWmR21gzykL3SmbXb7HrCqK4i.png\"]', 'uploads/products/thumbnail/zFaeECWjsONEs5j777mV3ozK3NabSdwe8f5jHxUQ.png', 'uploads/products/featured/CWGrFJ1BfqU77UfrbHOy7KWbgUwr21c7D014Z3XJ.png', 'uploads/products/flash_deal/guo1LX44nyW6t6wdHjkgCEJNPtFLPIm1xvMI3TXH.png', 'youtube', NULL, 'Whole-Broiler-Chicken-Without-Skin', NULL, 190.00, 390.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u09b8\\u09cd\\u09a4 \\u09ac\\u09cd\\u09b0\\u09af\\u09bc\\u09b2\\u09be\\u09b0 \\u09ae\\u09c1\\u09b0\\u0997\\u09c0 \\u099a\\u09be\\u09ae\\u09dc\\u09be \\u099b\\u09be\\u09dc\\u09be\",\"options\":[\"800g\",\"1kg\",\"1.25kg\",\"1.5kg\",\"1.8kg\"]}]', '[]', '{\"800g\":{\"price\":\"170\",\"sku\":\"WBCWS-800g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"220\",\"sku\":\"WBCWS-1kg\",\"qty\":\"100\"},\"1.25kg\":{\"price\":\"270\",\"sku\":\"WBCWS-1.25kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"330\",\"sku\":\"WBCWS-1.5kg\",\"qty\":\"100\"},\"1.8kg\":{\"price\":\"390\",\"sku\":\"WBCWS-1.8kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'আস্ত ব্রয়লার মুরগী চামড়া ছাড়া', NULL, 'uploads/products/meta/dUsx4s9HJitioL3AfSJKF7oZottTBTcNkSJ6ikdM.png', NULL, 'Whole-Broiler-Chicken-Without-Skin-osv2s', 0.00, '2020-05-15 23:31:00', '2020-06-18 07:59:57');
INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `rating`, `created_at`, `updated_at`) VALUES
(80, 'Including Whole Broiler Chicken Skin', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/JvCVENapNZXVQV6nMN5AU6jH6bgk89iWUmyjgZXY.jpeg\"]', 'uploads/products/thumbnail/DX8twvo8aeBFNVyf2xbtEwyafCEgSHJFKj69kk2T.jpeg', 'uploads/products/featured/sSkrk2lHApvBWuR5IUPq7MPQvaPkykiZEzgwfj81.jpeg', 'uploads/products/flash_deal/ChfUWQUpsp0ylNGOe6L6oI2X0fCO0u1VZvjdperq.jpeg', 'youtube', NULL, 'Including-Whole-Broiler-Chicken-Skin', NULL, 145.00, 320.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u09b8\\u09cd\\u09a4 \\u09ac\\u09cd\\u09b0\\u09af\\u09bc\\u09b2\\u09be\\u09b0 \\u09ae\\u09c1\\u09b0\\u0997\\u09c0 \\u099a\\u09be\\u09ae\\u09dc\\u09be \\u09b8\\u09b9\",\"options\":[\"800g\",\"1kg\",\"1.25kg\",\"1.5kg\",\"1.8kg\"]}]', '[]', '{\"800g\":{\"price\":\"145\",\"sku\":\"IWBCS-800g\",\"qty\":\"100\"},\"1kg\":{\"price\":\"180\",\"sku\":\"IWBCS-1kg\",\"qty\":\"100\"},\"1.25kg\":{\"price\":\"225\",\"sku\":\"IWBCS-1.25kg\",\"qty\":\"100\"},\"1.5kg\":{\"price\":\"270\",\"sku\":\"IWBCS-1.5kg\",\"qty\":\"100\"},\"1.8kg\":{\"price\":\"320\",\"sku\":\"IWBCS-1.8kg\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'আস্ত ব্রয়লার মুরগী চামড়া সহ', NULL, 'uploads/products/meta/fT6v1csSDw5WhuLjybH4ScmuWkQLLB6E2jMZUVou.jpeg', NULL, 'Including-Whole-Broiler-Chicken-Skin-yHn8f', 0.00, '2020-05-15 23:35:33', '2020-06-18 08:02:06'),
(81, 'Golden Cock Chicken Skin Free', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/Cy5o0J56REpKaz1jz6GwV73IZ2TiH8bie145ygUF.jpeg\"]', 'uploads/products/thumbnail/dx65k9KrS2SidNvuIB1nJJ32MkUetpIqo2pqPPaS.jpeg', 'uploads/products/featured/iTmRN96tuZzS1YCSBGba272vaMXo5cXbxV3AyHaX.jpeg', 'uploads/products/flash_deal/S4KvfcKBzJy4KNgnSV3rgkDdAGGOfzvy6xtT73cN.jpeg', 'youtube', NULL, 'Golden-Cock-Chicken-Skin-Free', NULL, 220.00, 330.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b8\\u09cb\\u09a8\\u09be\\u09b2\\u09c0 \\u0995\\u0995 \\u09ae\\u09c1\\u09b0\\u0997\\u09c0 \\u099a\\u09be\\u09ae\\u09dc\\u09be \\u09ae\\u09c1\\u0995\\u09cd\\u09a4\",\"options\":[\"400g\",\"500g\",\"600g\"]}]', '[]', '{\"400g\":{\"price\":\"220\",\"sku\":\"GCCSF-400g\",\"qty\":\"100\"},\"500g\":{\"price\":\"275.00\",\"sku\":\"GCCSF-500g\",\"qty\":\"100\"},\"600g\":{\"price\":\"330\",\"sku\":\"GCCSF-600g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'সোনালী কক মুরগী চামড়া ছাড়া', NULL, 'uploads/products/meta/0ot8qfPPtqVaz3MPadeg6ErmPflnkiZqfV1pxhIG.jpeg', NULL, 'Golden-Cock-Chicken-Skin-Free-5BAUW', 0.00, '2020-05-15 23:52:21', '2020-06-18 08:08:37'),
(82, 'Shrimp Dry For Mash', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/MHNGlwBSQSoeXpE5NNoif5PS0RIpGJgqUwq4PlfW.png\"]', 'uploads/products/thumbnail/kGq5nTiwtgnKCJO3pc0EKxAMGfFukfVvQJwzzKa8.png', 'uploads/products/featured/pmI00QvokQZTbGrbGXzLzLU2DFMgfwZjpMoTez1x.png', 'uploads/products/flash_deal/D3CxumgRAdyitlK8PiprARLfWc8tR420VcdmvYVI.png', 'youtube', NULL, 'Shrimp-Dry -For-Mash', NULL, 130.00, 0.00, '[{\"name\":\"choice_0\",\"title\":\"\\u099a\\u09bf\\u0982\\u09dc\\u09bf \\u09b6\\u09c1\\u099f\\u0995\\u09bf (\\u09ad\\u09b0\\u09cd\\u09a4\\u09be\\u09b0 \\u099c\\u09a8\\u09cd\\u09af)\",\"options\":[\"100g\",\"300g\",\"500g\"]}]', '[]', '{\"100g\":{\"price\":\"130\",\"sku\":\"SDFM-100g\",\"qty\":\"100\"},\"300g\":{\"price\":\"390\",\"sku\":\"SDFM-300g\",\"qty\":\"100\"},\"500g\":{\"price\":\"650\",\"sku\":\"SDFM-500g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'চিংড়ি শুটকি (ভর্তার জন্য)', NULL, 'uploads/products/meta/yUx8G3n9bd2Cog0UISlgwQkhpcDJ3OvYWDpz6g6E.png', NULL, 'Shrimp-Dry-For-Mash-KiTtT', 0.00, '2020-05-16 00:09:49', '2020-06-16 09:31:02'),
(83, 'Banoful Semai', 'admin', 1, 27, 69, 89, 70, '[\"uploads\\/products\\/photos\\/mKmK1Je9uNhh5S8QLENUmdmpcvaOs1MLnnaN3DhD.jpeg\"]', 'uploads/products/thumbnail/9OY5cbao0eYPOYjJViy3wZ8zl040hmOSQ4LiYWo6.jpeg', 'uploads/products/featured/NwP6Js0trKnxlvISiqaHyY72kdxJLQ3y3FRjqKev.jpeg', 'uploads/products/flash_deal/cJ4FYwvklMOF5fGSZnnxZrwhTB9pk9yfEygo57qv.jpeg', 'youtube', NULL, 'Banoful-Semai', NULL, 35.00, 35.00, '[]', '[]', '[]', 0, 1, 1, 100, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বনফুল লাচ্চি সেমাই', NULL, 'uploads/products/meta/639GBRW9bttkBSaRAn8fX1bZPa7VLmwtMWdX4Yvu.jpeg', NULL, 'Banoful-Semai-P6W0f', 0.00, '2020-05-16 00:16:36', '2020-06-16 09:30:06'),
(84, 'Radhuni Pepper Powder', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/JmnhdKKHTi9XCP8j98sJq7Dah2VsBXhUAT8yHOiW.jpeg\",\"uploads\\/products\\/photos\\/nN1pNG2ioJNG7PsVGVxEOecgMWGj3YOdgWGcSNQt.jpeg\"]', 'uploads/products/thumbnail/bIZ0dc7I7ntIhp8ChJRU8woHtCmoWUrevG35ACPu.jpeg', 'uploads/products/featured/t9FyNhvRw5qIKJAdBlyBGugYY5nLxySJx9Gh1Du9.jpeg', 'uploads/products/flash_deal/86D3BVNiMaR3GvP5IPpddTxNoYrT4SHAUOgDqC2W.jpeg', 'youtube', NULL, 'Radhuni-Pepper-Powder', NULL, 50.00, 95.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u09ae\\u09b0\\u09bf\\u099a\\u09c7\\u09b0 \\u0997\\u09c1\\u0981\\u09dc\\u09be\",\"options\":[\"100g\",\"200g\"]}]', '[]', '{\"100g\":{\"price\":\"50\",\"sku\":\"RPP-100g\",\"qty\":\"100\"},\"200g\":{\"price\":\"95\",\"sku\":\"RPP-200g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী মরিচের গুঁড়া', NULL, 'uploads/products/meta/UxCMwWkgNc7pYMGgLzhuwMmIXkd9Sgiw8AEh6F0h.jpeg', NULL, 'Radhuni-Pepper-Powder-0KlMF', 0.00, '2020-05-16 00:34:42', '2020-06-16 09:19:42'),
(85, 'Radhuni Turmeric Powder', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/zs19Af0ObYP8rWUnxM16iTi7fjC6T2y5YfRwaEdS.jpeg\"]', 'uploads/products/thumbnail/3r5n2ZYT15kOzG1pCUJQAfcjTajJhjkBUv2WepkH.jpeg', 'uploads/products/featured/f1Re3q4XbheLKL2ycF02kZNWeiJGG4B2UrO7UZqZ.jpeg', 'uploads/products/flash_deal/blms0MitmJ6fCcufkjg3ix2YSZwzZNsNUQOe4eWY.jpeg', 'youtube', NULL, 'Radhuni-Turmeric-Powder', NULL, 48.00, 48.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u09b9\\u09b2\\u09c1\\u09a6\\u09c7\\u09b0 \\u0997\\u09c1\\u0981\\u09dc\\u09be\",\"options\":[\"100g\"]}]', '[]', '{\"100g\":{\"price\":\"48\",\"sku\":\"RTP-100g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী হলুদের গুঁড়া', NULL, 'uploads/products/meta/XmkOob2BMrVQ9R9JfJM7snZJLNullRhk2dzpHvCi.jpeg', NULL, 'Radhuni-Turmeric-Powder-mh0jF', 0.00, '2020-05-16 00:42:28', '2020-06-16 09:26:47'),
(86, 'Radhuni Cumin Powder', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/RwBbUVFX6crFJ9zd0L4o0MwoDPYykkHvvMR7d6uP.jpeg\"]', 'uploads/products/thumbnail/PtT7nrQxeio0Bk01uDKn4WFGrH4GqQWwvCz6YT96.jpeg', 'uploads/products/featured/44R5haR1vsHZmQrnoru6E3haiCz7C58xjLNH91jd.jpeg', 'uploads/products/flash_deal/ukjYD2Fa4PeogvI6a8saGfoYorcIyeCwziIXml6k.jpeg', 'youtube', NULL, 'Radhuni Cumin Powder', NULL, 40.00, 40.00, '[{\"name\":\"choice_1\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u099c\\u09bf\\u09b0\\u09be\\u09b0 \\u0997\\u09c1\\u0981\\u09a1\\u09bc\\u09be\",\"options\":[\"100g\"]}]', '[]', '{\"100g\":{\"price\":\"40\",\"sku\":\"RCP-100g\",\"qty\":\"100\"}}', 0, 1, 1, 100, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী জিরার গুঁড়া', NULL, 'uploads/products/meta/15q14Wy3HyOkCLB5YkHLAWQtpjdatCD4vAqbCFwb.jpeg', NULL, 'Radhuni-Cumin-Powder-MoWJC', 0.00, '2020-05-16 00:45:20', '2020-06-16 09:26:19'),
(87, 'Radhuni Hot Rpices', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/aGk5c52w6zfMLhYuhFn822sZmXlljcLUZq6FO1xb.jpeg\"]', 'uploads/products/thumbnail/KOhHX1vd3b3dqqb1IgjRAwZfK1tZICRLpLG5FtiT.jpeg', 'uploads/products/featured/fyqaajaW3s3EdYRPez6ta7pfwpDVlRAP0egb1sQj.jpeg', 'uploads/products/flash_deal/rLiAyf6dwUs2Ok69C3ZhGyd9OpRnLYBjqVc3J6PN.jpeg', 'youtube', NULL, 'Radhuni-Hot-Rpices', NULL, 60.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u0997\\u09b0\\u09ae \\u09ae\\u09b6\\u09b2\\u09be\\u09b0 \\u0997\\u09c1\\u0981\\u09dc\\u09be\",\"options\":[\"40g\"]}]', '[]', '{\"40g\":{\"price\":\"60\",\"sku\":\"RHR-40g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী গরম মশলার গুঁড়া', NULL, 'uploads/products/meta/3EdXl4MCA3d34Jb6Z8enRPMLFmDSZdYrUQdqLtl5.jpeg', NULL, 'Radhuni-Hot-Rpices-IjSfO', 0.00, '2020-05-16 00:48:16', '2020-06-16 09:22:54'),
(88, 'Radhuni Roast Spices', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/RqGqkFc3OwxT2R4dAf8OA7E6O9OQ5R73LZJ7b6yS.jpeg\"]', 'uploads/products/thumbnail/7Xk9wIGyYbyLdxWePJhMFjWhPcT9Y2sIWT0oXa7W.jpeg', 'uploads/products/featured/oM7YHHjNz3Ql4KDWQisYnbldwpYupP3ZTbfgLvf5.jpeg', 'uploads/products/flash_deal/yp0nMDrdOGqFrGAr09XW5JTUHUWKSNekFiws3fcN.jpeg', 'youtube', NULL, 'Radhuni-Roast-Spices', NULL, 60.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u09b0\\u09cb\\u09b8\\u09cd\\u099f \\u09ae\\u09b6\\u09b2\\u09be\",\"options\":[\"35g\"]}]', '[]', '{\"35g\":{\"price\":\"60\",\"sku\":\"RRS-35g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী রোস্ট মশলা', NULL, 'uploads/products/meta/e0j0GiGqOBWLVigLEGfjFfTlBIPywtYqRRkQ1gCa.jpeg', NULL, 'Radhuni-Roast-Spices-bKePt', 0.00, '2020-05-16 00:52:17', '2020-06-16 09:25:26'),
(89, 'Radhuni Biryani Spices', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/pDFf7ii9RM8RxA5zOpWeK7l2Nh2jZ67pUkggH86j.jpeg\"]', 'uploads/products/thumbnail/afCqa3AN5bP2JIs5P5awdjKCOQkD9Pop7MJim56s.jpeg', 'uploads/products/featured/egFL5XzkZprm6ZEphIdoBYlCIMrBduv2mWFpM6v6.jpeg', 'uploads/products/flash_deal/DWwwiYJXWroX1YXuv41aivPlugKH8jaSSZi6WEdM.jpeg', 'youtube', NULL, 'Radhuni-Biryani-Spices', NULL, 55.00, 55.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u09ac\\u09bf\\u09b0\\u09bf\\u09af\\u09bc\\u09be\\u09a8\\u09bf \\u09ae\\u09b6\\u09b2\\u09be\",\"options\":[\"40g\"]}]', '[]', '{\"40g\":{\"price\":\"55\",\"sku\":\"RBS-40g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী বিরিয়ানি মশলা', NULL, 'uploads/products/meta/eMdArt9Ygzfoh1GrN78dXHjPyXe4iKgeiYlcle8n.jpeg', NULL, 'Radhuni-Biryani-Spices-ZqFgo', 0.00, '2020-05-16 00:54:06', '2020-06-16 09:24:31'),
(90, 'Radhuni Chicken Seasoning', 'admin', 1, 26, 75, 88, 71, '[\"uploads\\/products\\/photos\\/oUGxYCY4Of4R3qD7uXLNHHOis6Z5tdUqKrcmLS4L.jpeg\"]', 'uploads/products/thumbnail/gO1xCtQODrlZlR6n28lwUH9qeDLgG1c53zOJg9sc.jpeg', 'uploads/products/featured/fAxZj9a1gKffKGGxUfShTO3vXMErWUDDzl6K3Tvd.jpeg', 'uploads/products/flash_deal/Te45UQyhUKmyAK4P5U1l01n7MpnZEpHdf783ibPy.jpeg', 'youtube', NULL, 'Radhuni-Chicken-Seasoning', NULL, 18.00, 18.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09be\\u0981\\u09a7\\u09c1\\u09a8\\u09c0 \\u099a\\u09bf\\u0995\\u09c7\\u09a8 \\u09ae\\u09b6\\u09b2\\u09be\",\"options\":[\"20g\"]}]', '[]', '{\"20g\":{\"price\":\"18\",\"sku\":\"RCS-20g\",\"qty\":\"10\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রাঁধুনী চিকেন মশলা', NULL, 'uploads/products/meta/uHMiY0wEIQ2oWS9pPYRrHXVov7Ms9uAIqFC6zdeO.jpeg', NULL, 'Radhuni-Chicken-Seasoning-Ny3Bh', 0.00, '2020-05-16 00:56:01', '2020-06-16 09:23:33'),
(91, 'Cardamom', 'admin', 1, 26, 75, 88, 48, '[\"uploads\\/products\\/photos\\/A3QiJorsJU9E1M623jax0smyhtdEVCjsUu4R7njI.jpeg\"]', 'uploads/products/thumbnail/XFqmC2pB0LifE94wURONXhiDeZKr5I5JaUdBb1pO.jpeg', 'uploads/products/featured/i3AHbH4zyKKmdfUEVxkdkXx2eonvKfx6yRiqZ1Hb.jpeg', 'uploads/products/flash_deal/B8CbTUOlc3iWhEoxeJQWn7WLMotZUqkVYe5BfqYl.jpeg', 'youtube', NULL, 'Cardamom', NULL, 52.00, 520.00, '[{\"name\":\"choice_0\",\"title\":\"\\u098f\\u09b2\\u09be\\u099a\\u09bf\",\"options\":[\"10g\",\"30g\",\"50g\",\"70g\",\"100g\"]}]', '[]', '{\"10g\":{\"price\":\"52\",\"sku\":\"C-10g\",\"qty\":99},\"30g\":{\"price\":\"156\",\"sku\":\"C-30g\",\"qty\":\"100\"},\"50g\":{\"price\":\"260\",\"sku\":\"C-50g\",\"qty\":\"100\"},\"70g\":{\"price\":\"364\",\"sku\":\"C-70g\",\"qty\":\"100\"},\"100g\":{\"price\":\"52\",\"sku\":\"C-100g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'এলাচি', NULL, 'uploads/products/meta/OK147ZDyUp4BH5Wgwsq7nZWWbv1foXQdiJF0hsXw.jpeg', NULL, 'Cardamom-7U267', 0.00, '2020-05-16 01:00:31', '2020-10-04 18:04:08'),
(92, 'Clove', 'admin', 1, 26, 75, 88, 48, '[\"uploads\\/products\\/photos\\/3gUy39BDd4ueowfs0pH6RvXTCeoqr0s70Qj7vywq.jpeg\"]', 'uploads/products/thumbnail/efOvYg0YEB5y3oKWpUTb1JjOACl4o5JphPN1AVh8.jpeg', 'uploads/products/featured/t8CjKs6b1r4y1efTXwvm11XBNOBVbAU7GZYejIAV.jpeg', 'uploads/products/flash_deal/0zcBOvI2zxytWdafFlMpNUKSZWoHq1uOvNuw8BGu.jpeg', 'youtube', NULL, 'Clove', NULL, 26.00, 65.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09ac\\u0999\\u09cd\\u0997\",\"options\":[\"20g\",\"50g\"]}]', '[]', '{\"20g\":{\"price\":\"26\",\"sku\":\"C-20g\",\"qty\":\"100\"},\"50g\":{\"price\":\"65\",\"sku\":\"C-50g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লবঙ্গ', NULL, 'uploads/products/meta/j4QP8BFG1yVSsLRsTT9V0McDYIwuAQr7dvhlbkXk.jpeg', NULL, 'Clove-apqTC', 0.00, '2020-05-16 01:02:38', '2020-06-18 08:23:43'),
(93, 'Cinnamon', 'admin', 1, 26, 75, 88, 48, '[\"uploads\\/products\\/photos\\/C6szfP1bmLwhBfaaQL2yD71fVit8qS6UalSx4L2e.jpeg\"]', 'uploads/products/thumbnail/p8lstnOeOI77kwCBl8svoOFtm8jSG77477DUx3cu.jpeg', 'uploads/products/featured/AMkzt75lJ9erj9LDHW2b7h8XQoDInfX7mz6H95tG.jpeg', 'uploads/products/flash_deal/AD1g3e8prJ4itTkNhGqemOYc9tqmflHwgvH3imUh.jpeg', 'youtube', NULL, 'Cinnamon', NULL, 30.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a6\\u09be\\u09b0\\u09c1\\u099a\\u09bf\\u09a8\\u09bf\",\"options\":[\"50g\",\"100g\"]}]', '[]', '{\"50g\":{\"price\":\"30\",\"sku\":\"C-50g\",\"qty\":\"100\"},\"100g\":{\"price\":\"60\",\"sku\":\"C-100g\",\"qty\":\"100\"}}', 0, 1, 1, 0, 'g', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 5, 'দারুচিনি', NULL, 'uploads/products/meta/tMfa6wFtXfugdtRANX3C2sCbOtseelwJBGF977Y3.jpeg', NULL, 'Cinnamon-lSVwk', 0.00, '2020-05-16 01:04:24', '2020-06-18 08:25:42'),
(94, 'Pusti Soybean Oil', 'admin', 1, 28, 68, 90, 69, '[\"uploads\\/products\\/photos\\/L0aQrFlsSUI4aNFwsypne1yMPGEEd54yoJBmdCo5.jpeg\",\"uploads\\/products\\/photos\\/4NjuE9bp91MTTXOijRlRW12wyumC6Anpdy9o0DvV.jpeg\"]', 'uploads/products/thumbnail/KZGIxawUx88PnHKxAiQK71cjyhwbuDvhw1X4T33V.jpeg', 'uploads/products/featured/eZpp9CYNjfw15B6uH8JIZvgblYW4AfKFsSFI7l11.jpeg', 'uploads/products/flash_deal/lu9gtLcfJYyD7OCXiQR58bWKCND7V3jbHVwXzzVr.jpeg', 'youtube', NULL, 'Pusti-Soybean-Oil', NULL, 110.00, 0.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09c1\\u09b7\\u09cd\\u099f\\u09bf \\u09b8\\u09df\\u09be\\u09ac\\u09bf\\u09a8 \\u09a4\\u09c7\\u09b2\",\"options\":[\"1ltr\",\"2ltr\",\"5ltr\"]}]', '[]', '{\"1ltr\":{\"price\":\"110\",\"sku\":\"PSO-1ltr\",\"qty\":60},\"2ltr\":{\"price\":\"218\",\"sku\":\"PSO-2ltr\",\"qty\":\"100\"},\"5ltr\":{\"price\":\"530\",\"sku\":\"PSO-5ltr\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Lit', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 40, 'পুষ্টি সয়াবিন তেল- ১লিটার/২লিটার/৫লিটার', NULL, 'uploads/products/meta/HmYr63AE2ZazApwLLVzlOYfiJLmJENz6EsGdoP9X.jpeg', NULL, 'Pusti-Soybean-Oil-fSqGW', 0.00, '2020-05-16 01:12:53', '2021-06-08 19:32:31'),
(95, 'Rupchanda Soybean Oil', 'admin', 1, 28, 68, 90, 67, '[\"uploads\\/products\\/photos\\/UVNRqFXy3MOzphEWJS92hwwPqqWJFabvWDcVwaiM.jpeg\",\"uploads\\/products\\/photos\\/7oNYPLHZmDIANBUfFNtMLmTKs7iJJBTvAO6vFjSI.png\"]', 'uploads/products/thumbnail/lL7BrULcj6wk2JJ3vRBQ9WdDt3HzhVwDPVQUMao8.jpeg', 'uploads/products/featured/A5WQ7LQTvrR7PbTKGtlwE1lmMggemONnfdnIOQIC.jpeg', 'uploads/products/flash_deal/MoPsxA4A2pQ6pMxlBYFfkhgnCtBl1evC3xBgkBhd.jpeg', 'youtube', NULL, 'Rupchanda-Soybean-Oil', '<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAlgCWAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAHCAcIDASIAAhEBAxEB/8QAHQABAAEFAQEBAAAAAAAAAAAAAAQCAwUGBwEICf/EAE4QAAICAQIDBAUIBQYNAwUAAAABAgMEBRESITEGE0FRBxQiYXEyUoGRobHB0RUjNkJyCGKSorLhFhckJjM0NWNzdILS8CUnU0NVZJPC/8QAGgEBAAIDAQAAAAAAAAAAAAAAAAQFAQIDBv/EADURAQABAwIDBAkEAgIDAAAAAAABAgMRBCEFEjETQVFhFCIycYGRodHwBiMzscHhFSQ0QvH/2gAMAwEAAhEDEQA/APqkAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABnN+3/pFfZ3Itx8bHjO2D4d5y2Tl8fBHK7eos081c4hvbt1XJ5aXSAfP9npe1eWpQxoV48IOPFKW7fD7uhNn6UMymHe5Waq6mufBQpNPy2b5lfPGdNExG+/klRw+9MZw7mD51v8ATPqVdj7lqdak1vKCUmvB7dN/duZnT/S1qFsK5tY9kH/N2Zmri+np9rMfAjQXp6O4g1rsT2mj2kxLZutV21NKST5NM2UsLV2m7RFdE5iUSuiaKppq6gAOjUAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAPmv0u2952qyVHd8V+3Jrw23PpKyShXKT6Jbny/2yyZZHaa63ZNRlOcntvzb6FTxirFqE7h/wDJlp+PxT1a5bS9leMUl4ePUu34ebqmdHCwqu8mo77OSik347vyX3l/RZWTnk2qCXHPlJR23CwL83Ur4y9Ypakv10a5TSWy8Fz+o83Rvdx4Qua6sUI2foKxF6vdw+sRWzcbFLZ+XJ7HmgcbVkGttnxbeXh96MvqOkZOBjcFl9tlaW8ZzrlBPnv0lzMXoVl0sy+fDJS/eTXLq+nuNa6pqoq3zhmmYzEu7+hCXs5sfcjqxxz0NXWV6vbXbvFWVvbdddjsZ6vhVWdLSotbGL0gALFEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAY/tBf6touZauqre308j5z1XNfqeW3GmTnc4uUqlLlxdG/Be99DufpD1GjC0C6FstpT5peaTPnnWcDNycTHwq8WdlmRPvp7weyTe635pN8+j8uhA1VUZiJd7TZtIy514VMVXUpKK3ahHr9CPdUu1S3hu03NtoyILh4VJqM15Pbp7n4FOJiZVdcYvGmtlt0RmMHGjF8WZVPh6cEZqLZDprpicw6zlo+Xdr2bGUMyjIut4eFTlZDZe/i6/ZuZXQseemY8+8Vc8ix8VkuBbb+S9yNmz8aDs3xqJwh5SmpMx8sO+T9mmb+CNqqqZaxlP0nU7qcyicNouMlzS2O3VS46oT+dFM4RRi3VzjKddiS8dtztWg5UMvScayuXEuBRfxS2ZM0lWcw5XY708AE1yAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAa7227P4OvaPZXn0q2Na4kuJrpz6o4/q/YmOrYSdWbmVZWPLjxrKrEmvc9+W/vOu9vdcq0bQb95L1i6LhXHfnz6s4foHabLw89Ubd/C6aWz5uKKjiGot2q4iUqzbqqpmYbJoPZOeBpsK83NzMy9+1Kdtu7T8YprbdGSr0Onfkpv4zbMpVf3sE/El4Vsap8TimRYiivfDaZqhgJaDS3s1Z/Ta/Euw7PUPbldt/xH+Zmsu7vJ8SSXwIGZmzoqlKLbaXRGZi3TvMMRzS90zRcH9L4uNkRcoWNpxdj3eyb8zo2Bh0YOOqMWtV1Lmoo+c8LtPmUdo6c6+cvYs3UZeC8j6H0fUaNV06nLxpKVdkU+T6PyJvDr9u7ExT1aX7dVGMpoALNHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAi6lnU6dh2ZOTLhrgt/e/cSLZxrrlObSjFbtvwOK+lvtPbfHuKJ8FX7q/Ej6rURYomqXS3RNdWIaj2+7VXazqdlkpewvZjFdIryLfYjT3dl+tWRfLZx382abXxWT2l1Z1bsjjyxdOq4/lSW/wPH3a5v3c1LTHZ0YhtFK2jt4khESqRfjNk6mrCLMLjZFyoJwZIctyzc+RmqrJEYc47YYUsecbYbd1N8/DZma9FHbF6PqUNPzrH6lkyUU30rn4P4E7XcOOZiWVSXKS5M5jlY1lN0qZJxtre23mvMh2bs6a7mlJ5Yu0Yl9fp7pNA556I+009U0pabnyfruLH2ZPrZX4P4rodDPYWL1N6iK6e9VV0TRPLIADq1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADEdoNQ9VpVNb/XW/YvFmtdUURzSzEZYztNqLtTpqk1VF7Sa/efkc11/s7LW7oStsVdcfBR3b+JtdrlOW76eG54keb1l3t6t+ibajk6NXxOymFixiu7jOS5puKMrDGdaSijIzXMp2IdNumno3mqZ6o0YTRcjxeJd25nnPfob4aqOKT8Ciaky9yPdkxjIgWUuXJojrS6bL+O6qEn03a5mXcE/E84Wub5oxyxnMs5lDwNMjgZ8MzB3qvh0a8V5fA6bpOoV5+Opx2jYuU4b80zQoTXiTMTItw8hXVPZ/eveT9Hf7Ccdzlcp5/e38ETTM2GdjKyHJrlKPkyWegpqiqMwiTGAAGQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADRdVyPWMuy1vdSe0f4V0Nzzp93hXyXVQbX1Gg5D2nwp8tis4lcxTFPi7WaczlT8qWxccVF8yO3w8yLh+sRsseRNuO/s7v8A85FJMpKeoO2UnCO6XX3COPZL5Nbf0FyN1cKYqKfFvvLn1Liz7IraCSN4invkWvVLdvkSPPU7fmSLzz7ZPnJlLz79+UhPZ+bG608WxfuS+oodE09uCX1EuGqZC+a/iiv9Kzb9uuL96ewjs/FjMoUqLFFylBqPm+Raj4olZV/f7cO6Xk2WFFxTexrVjOzMNJ9Jenavn0aXDs/XkTzI5D/0L24Vt1fu+JuGmQz6NPoq1itQzYxXHw9H7yandGTpo491zlwdX/cWpTbn7bbaWz3ZvGIjzJzLL9mr3VqEYb+zYtn+BuCOY6TDLxu0GJJXTsxZ2LdTfyd2dORd8PqzbmPBGuxiQAE9yAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAELWpcOl5L/AJho9ntWN/abrr74dJv+C+80u3rLZlNxT2qUiz0lacWt39Jba322f0ouqO/J77e4OCT5dCqw7qEtkH08y8oFyFS23lJRj5sYMo6hJ9Ee8El4faSHCC+TKH07lO/86tfQzGIMrHBLy+0cEvIvbvora9/4WUzlYuacJL+a/wAxiDKzKMo9UyznZ3q9EJd3BRT5tvZIlOTa9rkQc/Gd8dtoTj+9Ca3T8n8TIy8p24ma7K3w8a4l4pp8yJ8qcm+rKYapFaBgVai6q82qLUmuW6W/RfUYdate4SlTjKTXPbmdK8TO0tY82yYXs5ND25caZv5zzGsksaE3H29lJo6FB8UIvzW5bcMn1anC91h6AC0cQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABiO1c1DQ7220uS3XhzNKWTS8WN0bV3WyXE31N17Ttfo+EXzUrYx2+s0/KxKLqZ0WVp1SfOPRFNxLeuEiz0W5Tj3bcbIRe3KUnyLkXF9JJ/BkHL0XDzMWOPdXLuorZJTa8NkX8PTq8ayuVUp7QioKLe62RWYdspqRidUmsnLWK1ZFwXHx8O8fh9hlq6XGpw4pNtv2m+a3MVZpWV3+W/0hbw2w4ILbbuny5o2xkY+GV39d9n6yHBHuuS68T+UuZ73teNRGM77ZvIScZc+W3v38WTf0JmPHphDUt5Rc3NuO3Hv8np5F+vRclYNFc8iud9cWnJrk9306GJgQK401rk5SsUdnLbn7S25/UV4sqbLPU65z72ptt/Ff3mQlpOT3seHIrjBR2b2fE/iUUYWRTnWWW2Vyr22jwrmun5GuGcsDl5UNHnThW5OXK2c0+JJy34ui678tjL6PbK/Gk52O3hm4qTi09iTqmJZlYVteNYqb5bcFu27i9yjTMS/Fxu7yb+/s3349tmxgyrtxabba7La4znXvwtrfbfqYjVoxqtnGzLsrjNpxjFdd+Wz28NzYVFFm7Cx77YzuqhOUeSchjdjK8vZw5JSa2h1XXob9gy48KiS57wi/sNIrittvA3TTHxafQ/5uxb8NnEzDheSQAWzgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMD2us4aMGHz8lL+rJmvzMx2xn/AJTpNfzrZy+qD/MxFi5spOITm5hJteytIrS9kp2Li6IgOqqK5pnlnyn8SuK3Fsfa3No6MLXNdCqNkl4sp8RszGB65SfVsp23fM92GwFMvZi38DyXQrcN9t+ie5TNbmGXkeg4VxJtbtdD3bYLqMC7UvaRtmiS30+C+a2vt/vNWpW7Nj0CW+PZHykWWgnFeHG70ZUAFwjgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXO1dTll6dZtvGHec/JtL+8xLhxcT3XJ9DaNegpYDly3hJSX3Gr2rh5p9Sm4hTivm8Ui1O2FuVUo82tl5iOx7vKUefE0iumKlLbYr4h1VRhPdNRe3nsXLec2tuRVKEopfLUTxRaW66m8QZWYUynJqK3a8+Rd9Su23aj9aPHx8Tbin8T3fp+ph9omGFuePOEHJ8Oy8mi0kvF8iTJNrlVBe9HkIRb3sXLzjsYwysbrfpuNmotuO/kXoRUZcXEt109x5OyPPrJ+98jHKI01tGLT33CRd4XN8MYl+jClJrj5R8RyyZW69omwdnk+5tb+cjDTjj02cDn7fhuzPaCl6jxxaalJ7NeO3IsNFT+45XZ2ZIAFwjgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADE9qJOOi3tScenNfE51LVMiLaUU0n0Z0Lte3HQMmUXzST+05Gs62yW8oQ+08v+ob1yiaYonCdpKYmJyzdesZWzSgkn4bkvF1bKVkXVjqc/JdTARyJvpCP1knHvthJSjtGXmpNHmqdZfid6kvs6J7mwz1bJtSTqt4vmuG/4FtatdDZSV9fntVL8jHLPzX0umv+tlcsvOmknlT/AKTJNPEKoneqfz4tJt+Sd+lpObjLIyHF9WqJv8Cn9LWJNRuyGl4Kiz8iIsrL2W10/pkyuORmNf6xL+kzp/yX5+S17OElalbPk5Zb3/3Ni/8A5Ld9+e4xeJjZc3vzcqnFbfSkR5TzJLb1qX1sjWU5Ep7yypM0q4jE7NotMnDIypfKoyovxTg/yLVuTnJvu8TKl8YMgWetcK3vk9unNoi2wyG9+8f1s0nXzPSWYtwrztV1mhSbxZ1JfvNMx8+0eqzr2V7XwPLsXJk/9Zko+K3Za9VdUOHdNnKrVXKulU/NvFNMdYYLI1HULMtSttUmn1lBN/WztPo11GeXoypulxTq5p+5nIcnTnZPi7zZ+SR0D0VOSzbYNvaNbj15PmuZb8GvVekxGerhqqaeTZ04AHtFYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMN2v8A2ey/gvvOM0/KOzdsP2ey/gvvOL0/KPJ/qP2qfcnaPpKfWS6vAiVLkTKjydScvwL0ehbitjHdoNfwNBxe+z7Upv5FUecpvySMUUVXJxTGZYz3MuuQd1dfOycY/wATSOT5Pa7Xda1/D0yiqzSqL25cTj7bglu3uzD5MNPze0mjTxtTvzsXIyHTfj5FrcotJ/ZyJ9OgxtXPdnbf/W+NnbsopnFc4+rtizsST2WVS3/xEXk4tcUWpL3Pc4frmhPSuzmq3348K7Fnb0TUt2qW+SGP6/ovYjF1WnOyas2y1Nw7xuKhLfhTi/dszPoVuqImivrOIbxZprinlq6zjp/t22fPwLMkc/7N9u8h6fiZGvUxjjZMpQryYeLXnHy95vtV1eRTG2icbK5reMovdMiXtNXZn1nGqiaOq3YnsQruTJ0+jIl0TnS0Q7eaZt/orW+flPyh+Rp93Q3T0ULfLzn5RS+3+4vOBx/2qXDU+xLpAAPeKsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYXth+z2X8F95xel+3yO0dsf2fyvgji9Xyl8Tyn6i9qn3J2k6SyNPQl0kKnoTKflI8nMJqB2s1+js7pE8y72rPk1V+M5eRyrT8jJu1rR+0etyry8TNsdW/VY029o8uiLna7U8zW+2LvwMWWZiaRNcVUeaaT9pv/wA8CVi4uPHMVGOnf2b7QJ7JL/Vrtt/o5/cXdqxTYtcs9Zjf3Y6e/G8eLbE00+c/nzhP1TULdIy9MyNfuplfXnWQrnBJP1eSa3e3lyMZfpGF2ay8bOzbqXZZqUbKLYyb4aNue/5kbLw8rtVkYuHXR/lmDGVGVlSfsSSeyfvbRseF2H0XTaYLNldmT25RlJpfRFHPmptctuZ9adpppjrHdjw6/F1rops24murGY3hrGRq+LnYmqaZm6g3XdqkJVysbaVDfNp+RP17IwtZ7PZ60VXd9bmUY/d2LbZqPDFxXzduZmFpGg2Z0aK8HHrnD29opvb4voR+0PYm+zLWraDlzhmJqarlJJcly4X+DJl2zRZpouV5p3+GduuOmcIdnV27tzFru8V3MqwcXS8TMqnCWDosbK+F/v2pKP0rfcp7FZWpaLpVOdqDbwMqfHOprnQpP2ZL3c+a8DWuzN71KWL2c1SDpdF9mRbCb2lkST3UPjvzZOy8nUcXTNdy9WhKqzUpLHxcV8+SW26Xw2InZ1W5m1VOcz398TPd9aplLt5xyeMuv8UZwU4tNSW6aI1xrfo6zMqWkfo/Uq5Qy8VJ7S6uDW8X9Rs1yKu/a7K5NLnVTy1TSx9xvHonXt6i/wCD8TSbl4G8eihbLUX74/iXPAv/ACqfj/SLqv43QQAe6VgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwnbL9n8j6DjNXX6Ts3bFb6FcvNo40ocL3XTc8x+oLdU4qjom6SY6J1HQ91HJ9T03KyW9u6qlNfQimjmiU6qr6J1XxU6pxcZKXRpnk6IzXGyZV0lxr0d23XaZk1adlQq1eeZC+6Nn79PLfbz8TJX5scW3Ih2YzlxZWXKm7Dthzrsbe84eK8/IwHa7Qbey2rVZ2kXtYzn+osT9qqXXgfmtidourV6n2khqiwVHKxsSdtsIf/VsjF80X+psYmq71id8bdfP/Hgl2rVNc9rG9O8/Lul0LTcarRMCnBxYqzJkuKT8W/GcizvK3V6sR2fLg5ynLrP3LyRJ0hTu0iGdXwZGVl1d9u3tGT8IJ+CXT6DzWXLGydMz5Q7uUWo2R8t/D7STotFGljnq3uT1n/EPP6u9Vennnotadp3d6rqspQ5KtKL29xO0a6MNMwKWudsZJP3psuZGb6vqqre3c3w5fxIwE8+VFNact5Y2RJfQ9/zJV23F2iaK94nKPFymxVmnu++WI9Kei7Y0Nbwt68zFa4518m1vyfxTf1Mt9l8unU8SvMWXDN1eiEJZE8tberQfOXCung+ZtGoZMNT0bMpnDbix5uSfwOMaXqNuDg6ji0wXHmquHeeKS33j9O6KS3pa6rM2KpzVTjE+U90/3h6LSUel0+p3f5b92O7Qz1H0g5DUv8nuqdda90Xy/FnTbebfuNI9HPZjH0hrJzmpalOPspdK0/Be83e1EXiNqKKqeWNojDa/ctV3Ji10jb5IOR8o3r0VL9XqD/nR/E0e1bzRvfotSVWobdeOP3E7gFEzqIq8MoOqmOTDewAe4VoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwnbLZaFc303RwDJ1aWJe+DacXLmmd+7aNLQbd+jaR8zazdj418lvK2xy5QjF8yBrKaaoxV0c665pmJpbVj67W4x3ralJclyKb9XsvXByrh9rOe157vu7xvaUXslvyW3kbBjZtWVCKtl3dvjLwZWW9JZtzzUwV6uqrNMys+kKtZHZWdkObx7oWP4dH95qPYbNowe02n2KyTVq7u3jjsoyfLZea6czffVY3U3UZFtbx74Oue8l0ficzWmTxsvLwrFJZtEvYnxcmt+TS9/Xc11NEVUVU1dJ/wAr/gt+mu3VZrnH+4w67dGzQMq3IxYTu0q6XHfjw5ypl4zrX3oua/mV5miudVkbqpbTrth0kt/sZi+xfaWvVcaOHmSVeoVLhlGb2dnvXvKMzvNC1HIx64ReFnfrauL5MLOk4/Do/rKvh+suW6p0mp6x0nxj/wCdETX6SaImO97qeoqVGDYnvODX17IxilPOzrq4dJSjv8eRY1KudUq45lNuE3Llxe1XL+Gf4PY2PQdNrwY/pDLtrhVw8UOfJfzpMm6rW27NrtYnOemO+VLb0t29e7OqMeKntVFaJpmblppKzF7nbzsb2X2P7DlGjY8crUNPpg7HfZkKMo8K4VHls0/PqbJ2v1yXaXUoY2LPhwKH8pv5TfLi/BF7sHpdNOZfqNlu9NKdVM5R2UrH1a9y8yPoKLlNv93eur8h7K1FGg09VdU4nrj6Q3q+9K2TjLZpl+nXpQ2rsirPenszDWY7e8lkV8L578RDvy6cSL7uXHb04vBFrcs0Vxy1RmHj6L9dE56NmnrCsscaYpSj8rd80dJ9EFneYOfLq+8W/wBRwDTc91Zys3Ut21Ldrn57rr9J3v0MOE9LzrK1KKlYns/gd9Fat268W4de3quz6zowALduAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANe7eWd32cvn4ppr4nydrOVG6tOc0r6rVF8U+HiW/J7+B9W+kJb9mMhPzR8f5bl+nYOGzl3v73Tx6kDWezPucLn8lMeazF2Rvtikn7b24XuufvNi0+UcOn1jOk41rpDxl7kjVLc/JxsrIprsUFGxraEOFLn4J80UK626fFZOUpecnucaKc0x7oQr1UU1zLbMPXY25jhlwjHGn04Vzh+ZL1vSP0nXTZXZGvMq/wBWyU/ZkvmSfl7/AANRqcunJfEn4edk4+6ovlBPrFdBXbiYw56fW12K4qj88kO6uUsvuM1WYmqxlyb5J+/fy8tvcZjH7Ta5p8Y05MKM+uMto8ceJ7+7xGTqtmZRGnUqacumPSNkOa+DXNEP1TCkoPFzM7GSe8YSatgvh0ZX3tDFe1URMfnR6zTfqLTXI5b8Y9+/y/2y+V261HLxpUS0miUJrhanFyi/oMHY9TytP7nKuVGDXKU4UuW23PnFfbtuSVi8lZPVb+7324q8Xbf6d9i4pYNElONNubdvup5k+JL4RWy+vc42tD2W1umI/PN2ucb0FuOeiI5o8IzK3pejrOcZ0KdOm1N8eVs4zt90V4+BtUnTh40J3xWPh1R4aaV1f5t+Zgru0GfbwxlkbbclwxS2+BjLrbLrHO2cpyfi+ZPs6fk3nq83xLjNWsxEez4efmy9HaCUsuSy4L1Sb24YrnD3rzKdUos5W48lbTLmpR5owdniRvWrKJt0WTg/5r2JE056Kui7napmdKo47a7bZd3LvJKcpSkkopb9Ntvt3Pof0D5CydJ1KyO/d99FRT8kj5pjkyuxqVObnbKblJ8L6c9lvvtv9B9H/wAnhf5tZr8719w0cevMz4ytaao7SmI8IdYABaJIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1r0hfs1d/EvxPjrUdnrKUpRjHvebk0kvjvyPsb0gbf4OW8XTiX3M+NtZdMdUm8iMp0qz2klza3+KIWq3zHkj3dq6ZY3JlFZ2RwyU4940pefv6v7y9Ts9tmQpOp22OlSjVxPhTe7S95Ip2XT7TnRHqwrb9Wa5lPjuSaMe+6EpVVzmo9XGLe31FjEvdN9Vq5uD32fR+42irtJZHidWF3UXtyrTS32+BrVVhwoopr9qfo15QsbcFGUpfN2Nku1LTIY9HcadRK2Ps2V2VJJ8vP3Moh2hsiqmsGEZw6yUduJ7r8C3qWsLLouqliqNlk+JWPk48+nvNebLpTEW4nln6J09ZwO74I40NuKT4e6io9Pt8vcRNVyab9OSo0+OMrGmv1S57OW/Pb4F6Ot2011q7Ch7cHGPCub6bS6dS5l6zk40bO+xqXGaik5c9mvB/kc+0pzEZ+rrVNU0zM+Hg1lRfE31KbHt5bmer1GcMm/IeK+K2UXCPC17tttunwKtX1vIsx76p4ldVdqa4ox5pv3/Q+RvFynOM7o02fVmrf5NVtbfVkO7qTZpbdSFd1O0ONFSdj2z9RorllwlXxtrHXWPXdvn+B9LfyeF/mtmP8A3/4HzVizrjg1QV9EpufFKuNaU4rZ7by6s+mf5PSX+COQ143/AIGmi9qffK7ome1pz4Q6kACzTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABrvbxb9n7Fy5zXX4M+NNXbWtySsdb73lOL2a5+DPsrt83Hs9Np81Nfcz4v1m51a9KyMoxlG3feTaS5+7n9RD1Od8eDhdj1qfex2bKUtRyOOzvJcXOT23b28duW/wAC7Xskt2RdWtc9Tvm51zc3GW8G+F7xXTcpqse/M5WvYp90f0rtTT68yy9Vdk65zrhOUILeUoxbUfiyTDUciWVTbYt1U4bRjNrdR3+/kQ1qmQtM/R8JxopcnKU4J8U9+W0ufQ9txuCFMl3q5SbcU5JbbcmvPxRHv1W5nlrhZ6LhV+5Ym/Yrju2/Onxwmx1TIeXddRCMONSjtLn1e/h/4zx599d1Vy4Iyoq7qLb2W3ny5IpWBfHR46g93V3jg21tvz5NeZY0+iObqNNdym6PlT4Vvv5fea1WrNujnx3IlunU3tVGmqnEzP8AS5HWX3l0v1UnbBwcpqTXPq9y53s7O7Trp9ifGpdW/JfA2/L0ueFpmo3Sx4xweUOK6r23u9kox59dzTdSwp6Rkyo4ozqlFWVNfNfg/g0yLor1q/VMU04x5p3FtLVpbfNRcz3TG353MjjPPz1ZTjUKdUp8T57vffxk/NkfU6snEsVWbjQoshwp7eO2/Nvx+UdN0urE0TSNPWT3NLt5wvcG14dX4N79fyKe0VWNr3Z7OlS68jJx63Li7rh4dn7Sb35vlvv+ZLptW6atoR6rFyq1PNVvjyx93KJzbT32IF8+fgXJT2W2xCvnu+pLwpLdDYFZJaFgR4Uo95KW/Pnvvt4befi2uR9Mfyev2Ksfne/uR8qq294eEniyrxouXBc4NKyT67S6fQfVf8nlf5h8XnfL8DTRUcsz75/tdUTzXYnyh1AAFklgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXO3/wCz1n8a+5nxVrM3DtFJwSbV3JSnwLr5+B9rdvf2fn/GvuZ8U6vZ3XatPve52yP9J83n1Iep7/c519aWG1exS1XJcVtvJNrjU+ey/e8Siqb2KtfnKWtZLlcrZvh3mtub2Xk2vqZHpl7Jztfx0+6EK/TmuWUwqo5uXRjyshWrZKLnZLhjFebZmLMmla/PTXnp0RsWPHKbXBw9OJteBrW/Lktz2DTfTZmty1Tc9pvo9bc0eYt9/wCfD3sxmynVbPGlkwvhTN7Ouzig35x8DY9Ary9OpozMO+q+qycW4QW8lPwTXXzXI0pS2e7fLx2M3StFrnXZDUs2M4vfljptNePUXKIrpmiekoUzVz9pmc/ne3LXdVz7ozwsfGsnNWKVs5wlFSm/k7rmo7cW7e/Pkafq2Dk6dKKz8im3Ik3vGNnE48/sR7TqGLw3Qt1LP2vSVklBPfbddPhsWM+OkerznhZebbd822vZfFs4afS29PtbjDtqNXd1FHJXO3Vt3Zztpj06fVg61TK6mrlCS322S5J7dH9f0dR2s7b42RprwNBpsx6LU++cuW7fVLmc7di8zxyk1ut9vMkRRGcnpd/s+yzsuzs3b8yFbZtLYrnPlyIlr9rqdIcKKMNkuhCrs/pdkbb5StnNyhKTcI7eXs8uvmz6q/k7r/28qfnfM+Tc3Lps0bSqIZtlttW/HS0uGvly28T60/k8L/21xX522fectDE9/jP9raIjtNvB0wAFk7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXe3sW+ztrX7sk39TPhztpH/1jLX+8f3n6AZcKrMa2ORBTqcXxRfij567V+jvsln5c7suORp87pbqddjcE/fumQ9Vcps+vV0Ym1NzaHzFHl0JdCe2/gdyyPQnol9beldo97NuUZuDW/2Eav0IZfccL1bG7zqtocX2pkOdfZ7p/Pg41aS74ONpsqTl7zsNfoM1KT56nTH3up/mX/8AETm//eaf/wBT/Mz6Xbnx+U/Zy9Du+DjacvIqjvt0R16XoP1CL/2rS/8Ao2/Ef4kc7bnqlH9Ff9xj0y15/Kfsx6Dd8PrH3ch3l5G16L2sxtOwFjWaRh2ey4ykor9Zy5OW6e7+w3T/ABJ5X72r0L4QX/cev0JXbc9ao+mC/wC40nWWp65+U/ZmNFeju+sNA1XtFh5tE66dFw8VT4Xx1wXFDZ7tLkuTLmndq69O9ZqxtNoljWXO2Ebdm6+m23Lry/I3p+hRpLftDjRfk61/3FP+JmC+V2jxGvdBfmPTLGN5/tiNFfz0c67Qa/Vq2L3a03Gxrnb3jurjFSa224W0l8TVrep3D/E3gcMe87T0Q+dvWvzK36Iuy1bTu7S5VzXhVStmZjX6emN6m9OivZ3hw+h/rI/E+0v5PK29GOC/O2z+0cy0j0cdjKcmqFWFq2pWt8u9s4K/i9uex37sfpeNo+g0YmFRXj0xbarr34Vu/fzf0knR6mjUVT2fckU6eq1vUzQALFsAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI+ocsHI2+Y/uOUarDMcdqMGrKq39qNktt/gdV1War03KnLlGNcm37kjSnFOK59Ss4jPqxHi72erQ7bYV2ON/ZjIil+9XPdNkvFxsPIjxLSdUp9yaNxVMH7ih08L34uRQTp6Jnok87Aw0/Dim/U9Ye3hsyuNWP0jpOry+Mf7zZKLZqHDFRW3i0mPWrU9lKP0QX5HWNPZ74+jXmlgI41LX+xtRf8SX5lXq1f7ug5b+O3/cZ5ZN3z/6qPVk3bf6R/YZ9GseH0g5qmCjiy39nQLP+qcF+JOo02uyvezT41S+a5p/cT+/t/8AkkU99b/8s/6Rn0a14fSPsc8sHPDyU2o6HU178mKFODdJz7zRaIbdP1ye5no2zb522b/xMXW277Oyf0Nj0a14fSPszzy123TrOJ7aPhJec7d/uR5HGzqovucLTE/Dk395mpuUus5P4sphBOXM07CjO0f0czDRo7QXSirM3Gxqd95Qqh1XkdA7LTtWFKm2xWOt8pbbdTArbwWyM/2c22t29xaaHNNcRlwu7wzQALpHAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGO7Rvbs/qb8sax/1WaZTYpYtM/nRT+w3LtL+z2qf8rb/YZz/TrOPR8GW/WmL/qoqeJzjDvZ72ThLdbnu/tc/EsUy3giqUua+JU5dl6yXDBRXLzKa4OfQpa4ppeBenaq4KMFszf3i7GmuPypnvDQv3vtIDk292+Y4jPNHgYT9qPnD/J/MgcR45Dn8jDJKNEvkPn8SxkcvgiK5eJcdnEuYmuPAwtylzCls9y3N7N7FLkznllLhPfY2Ts1zjd9BqdUm0bT2W+Rf8UWOhnNyHK57LPAAu0YAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAY7tJ+z2qf8AK2/2Gc10OW+g6f8A8CH9lHS+0P8AsHUv+Wt/sM5b2fnxaDpz8Hjwf9Up+KziKUiz3s1Q/wBWj2x77Fiia7tbsqnPfZop8u+EiUuFciiUm3z8imct+hRKXtdTbLGFziHEWnZFdWWpZEU9k92Y5ohlJ3G5FVz95UsiPiYiuJEjc9k9oFmNil0aPZT3TRtkeSluE+Zb4t09j2L5mM7i7W+ZtnZN71X/ABRqMXzNt7INOjIa+cvuLDQT+9EOV32WwgAv0UAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQdcW+jZ6//Hs/ss5VpMndomBZxbSdMXuvh5HWdVXFpmZHzpmv6rORdnJ8ejY0FFpVQjDn8Cl4t/6pFjvToVvpxNorjGUJpJ+y/BlF1rxouxwlOH7yjza955jZlGWnLHk5RXV7Nbe4o5lITLVwx2iyxKEuXteBdXMi5WYsbIhG6E1TNcrUt4xfk/I2mRX3SS3bbKEoRfRlz1jH2X+UUrfpvNBTplzjbS/hNGuGVUH4FTS225HicfnR+hnkrK4LedkEvfIYFuVe8vZ5F6cdq4qPVeJCuyJ3tVYC3cntK5r2YL3ebJyjwVxim2orbn1Eb9CVmuPDvzEd3L2uiK2uXIVx3ezNs7sCi1JOL5Pqbn2NjtiX++a+41Jrmltsuht/ZBbYt/8AEvuLPh0fvR8XG77LPgA9CigAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAC1lrixbk/GDX2HMceMVQuCPDHwS8DqU1xQlHzTRy2t7Upb9Cn4v7FMu9jrK4ttimUVFeykvgFJJ9UeORQTKU9g+ZW1uua5FtMvVS3jJea5GYlhYli48+c6a5fGKPPUsVdKK/6KJenVxycq2m+caOFbwb58ZCp1XSreOMdSx+8jLh4Hvu39RtiIp5pxj4HlCpYeMulEPqK1jULpTD6i9kW4uPwOzNx4xly3bfJ+RH0zMxdRy76cbKx7I1fKlGUuX9Xb7TFPJVOKZifjEszmOq6klySSXkUz6cup7bZX6w6aG7JR6yS9lfSxJpLbxMztswtt9D2r5fuKZNeJ7XLZmsTuJD+Ubd2Se+Ld/EvuNMc1ubd2PlvRf5boteHT+9Djd9lsQAPQooAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHMYQ272ua5wskvtOnGhdo8OeFqU5JfqrW5r3+ZWcUomq1Ex3O1icVMXONdacp8MYrq34FNfc2pSqnGSfzXuQO0/frR5PF3dm6+T1LGlY1/des2uv5Kjww38H138zzMzvhM7malGmEN7Jbbeciy8nCjFyd3JeUkzWLprW82ChZdCEJNKvlwz28TMLGhThvJy8iyvEj7MYuXtTfu8l8DSq5MTiIZimMZTcrPxKMaOROzu61FyhxNuU37l4Gg5eVLVJS1BY+Li4/Glsq1Kx+/bruZ/U5RyMOS7iOPQmpRnzlOS8m30PNAu0vSbFRKuNnF7cbJQUmiNN7tZ7OJx57u1NHL62GFu9TVuOsbM7yUntLjoSjFfHcyWk63Vi5t2Dld0o/u2465N+TRsmfqulOreUaZLboqf7jUc/Axcq6OotQpxo+wox5cT38TScaffny2x2m2MN1jn4zgpPZxfTZ839Bcrsx7UnB779EzDYslHDgppWR234Z+K9zPLcimmr1jHk+CGzsr6tLz9x2t6qKtnGq1hmLHBcntsVUKLfRbeZTGymdFV07IKElupb9SPmajgVS9XyLJQnNbLhTfXp0JtMx1cvJPt4YPdrdLyRtnZKHDi2y8HJJGp0VTsnVUlxSe0YpdZHQtNxliYddX7yW8n7y44Zbmbk190OF6dsJIAL1GAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHv4FLcvBFQAsylb4RRanPJXSKJYMYGMnbnL5MEY7PhnZVLqtx4zg/sNkBrNOYxLOXPZaRnQTXcOUfIohp2VGtwliTivLh3+46KNiBXwuzVOYzDpF6qHJ12bni4ThgY1sLI8Thvv1fX7ka3n9ne0GXqGPfkU2W1U7Pu99kvFpLyO+bHmyI1fA7NU83NPg6U6qqnbDiOZo+qZeDLHnhzre/Epdfr5mEn2V1ZV0qGPPir5bpc/vPonZeQ4V5L6jSP0/ZiMc0/T7N/TK/CHz/ldmtXupsjXTY5Te/tLoSH2Z1SWmYuHDGcVVLjlNpvi8eh3jhXkhwryRin9PaeMetOxOtrnucdjpuqqUXHDj7Pg4ya+OxDq7K5nHZO3Fna7E094vlv5czt3CvJHuy8kZj9PaWJzv8ANj025jDjS7L5c4QhLHyVGL32TS/EysdFzHOM/UZOcekpy3aOobDYlWuEaa3GIhzq1FdXVpGm4+p4j4oYy43+9LmzM1X6o9uKtGeBYUWoojFPRxmc9WMrtzuXFBEiM8l9Yolg3wwsxlb4xRcTl4oqBkFv4gAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP/2Q==\">', 114.00, 550.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09c2\\u09aa\\u099a\\u09be\\u0981\\u09a6\\u09be \\u09b8\\u09af\\u09bc\\u09be\\u09ac\\u09bf\\u09a8 \\u09a4\\u09c7\\u09b2\",\"options\":[\"1Ltr\",\"2Ltr\",\"5Ltr\"]}]', '[]', '{\"1Ltr\":{\"price\":\"114\",\"sku\":\"RSO-1Ltr\",\"qty\":72},\"2Ltr\":{\"price\":\"226\",\"sku\":\"RSO-2Ltr\",\"qty\":\"100\"},\"5Ltr\":{\"price\":\"550\",\"sku\":\"RSO-5Ltr\",\"qty\":\"99\"}}', 0, 1, 0, 0, 'Ltr', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 27, 'রূপচাঁদা সয়াবিন তেল', NULL, 'uploads/products/meta/gFLDPzpb3fM2FDALTfBoiXjWGZowawGJLZHfiFfd.jpeg', NULL, 'Rupchanda-Soybean-Oil-3P13Z', 0.00, '2020-05-16 01:19:50', '2021-06-08 16:59:51'),
(96, 'Diploma Full Cream Milk Powder', 'admin', 1, 24, 71, 86, 73, '[\"uploads\\/products\\/photos\\/rL0FfKjG1ekmpkfYmzwN1KL1wKGG4q03ODGDIQqP.jpeg\"]', 'uploads/products/thumbnail/kDp9WHCIZtd4OX5EamNXhNFeq7O4Qf8KYa0pIhFr.jpeg', 'uploads/products/featured/qRkuUbsYkiyWzL3M6GgdHe9BklziFYFwCQtCJ7ZN.jpeg', 'uploads/products/flash_deal/i3Xdu4e0m2drgvIZI2rI2FeqDff9fhqwrhf5ZKLD.jpeg', 'youtube', NULL, 'Diploma-Full-Cream-Milk-Powder', NULL, 630.00, 630.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a1\\u09bf\\u09aa\\u09cd\\u09b2\\u09cb\\u09ae\\u09be \\u09ab\\u09c1\\u09b2 \\u0995\\u09cd\\u09b0\\u09bf\\u09ae \\u09ae\\u09bf\\u09b2\\u09cd\\u0995 \\u09aa\\u09be\\u0989\\u09a1\\u09be\\u09b0\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"630\",\"sku\":\"DFCMP-1kg\",\"qty\":\"99\"}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'ডিপ্লোমা ফুল ক্রিম মিল্ক পাউডার', NULL, 'uploads/products/meta/5OQExyOMdeHAuYrxO7J8DMkWI57Kg2lAGEkqqbBm.jpeg', NULL, 'Diploma-Full-Cream-Milk-Powder-V59qr', 0.00, '2020-05-16 02:08:17', '2020-06-16 09:06:45'),
(97, 'Marks Milk Powder', 'admin', 1, 24, 71, 86, 72, '[\"uploads\\/products\\/photos\\/Je3JYYQZ3ILcoG6iEhEYcKkbzIm9iM1ChVwJOVTr.png\"]', 'uploads/products/thumbnail/OOyeFpGTgmsdiblXfA5mYIeKYwpCpMT7xEmusMDA.png', 'uploads/products/featured/Zs8E6TmpdotquaxnikAXnXrq6TdS4fm3Sje4y9Rz.png', 'uploads/products/flash_deal/R8cH4bkoLv9k5kA2fVdDPTfnhvy1uzFNu2EMhXoH.png', 'youtube', NULL, 'Marks-Milk-Powder-Poly', NULL, 590.00, 590.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ae\\u09be\\u09b0\\u09cd\\u0995\\u09b8 \\u09ae\\u09bf\\u09b2\\u09cd\\u0995 \\u09aa\\u09be\\u0989\\u09a1\\u09be\\u09b0\",\"options\":[\"1kg\"]}]', '[]', '{\"1kg\":{\"price\":\"590\",\"sku\":\"MMP-1kg\",\"qty\":1}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 10.00, 9, 'মার্কস মিল্ক পাউডার', NULL, 'uploads/products/meta/QoezILe0pwTQGhoUP58VldEY21NdbYlYgglPvTYq.png', NULL, 'Marks-Milk-Powder-QPPUc', 0.00, '2020-05-16 02:11:45', '2021-05-12 18:40:19'),
(99, 'Fresh Maker Jumb Baby Weight Wipes 120', 'admin', 1, 19, 63, 81, 49, '[\"uploads\\/products\\/photos\\/vlR15NRjGzxbAa0qrFIsC5T21wBwITSDRODRNKWX.jpeg\"]', 'uploads/products/thumbnail/LkaLS11SThFc3A67EmF4AdB8i40l9JHnkejkxhkM.jpeg', 'uploads/products/featured/2fnD1pI9rgZiQLGxdjuAF2pufVnZy2uvvhfSzHBX.jpeg', 'uploads/products/flash_deal/3r6eYUQGkw34PC7tbz0j7UfL9INjy6Mvz3fyH7It.jpeg', 'youtube', NULL, 'Fresh Maker Jumb Baby Weight Wipes 120', NULL, 205.00, 205.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ab\\u09cd\\u09b0\\u09c7\\u09b6 \\u09ae\\u09c7\\u0995\\u09be\\u09b0 \\u099c\\u09be\\u09ae\\u09cd\\u09ac \\u09ac\\u09c7\\u09ac\\u09bf \\u0993\\u09af\\u09bc\\u09c7\\u099f \\u0993\\u0987\\u09df\\u09be\\u0987\\u09aa\\u09cd\\u09b8 \\u09e7\\u09e8\\u09e6 \\u099f\\u09bf\",\"options\":[\"Pc\"]}]', '[]', '{\"Pc\":{\"price\":\"205\",\"sku\":\"FMJBWW1-Pc\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'pc', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ফ্রেশ মেকার জাম্ব বেবি ওয়েট ওইয়াইপ্স ১২০ টি', NULL, 'uploads/products/meta/8jxLGRlv9gjkADef6UZ1b8lsJqo9nAfAYt1whlum.jpeg', NULL, 'Fresh-Maker-Jumb-Baby-Weight-Wipes-120-uERgV', 0.00, '2020-06-02 00:20:40', '2020-06-16 09:06:00'),
(100, 'VIM Dishwashing Bar', 'admin', 1, 20, 64, 82, 76, '[\"uploads\\/products\\/photos\\/wmwvz1UxENX65TOgrJXBDvffsQ14qe6RCHhxt4S7.jpeg\"]', 'uploads/products/thumbnail/VNWCGj6RTDMAy1VXszpD1A9aBn1JvhbbMcNXPb6I.jpeg', 'uploads/products/featured/s1nnS5JmRTsnD90vlLQX2phgrzbZgjvCeLPbts9X.jpeg', 'uploads/products/flash_deal/kwVj8iXLMLMTWg973D9q8kbzvkKmaqu1Oyz46o3r.jpeg', 'youtube', NULL, 'Vim', NULL, 32.00, 32.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ad\\u09bf\\u09ae \\u09a1\\u09bf\\u09b6\\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09ac\\u09be\\u09b0\",\"options\":[\"300gm\"]}]', '[]', '{\"300gm\":{\"price\":\"32.00\",\"sku\":\"VDB-300gm\",\"qty\":\"10\"}}', 0, 1, 0, 0, '300gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ভিম ডিশওয়াশিং বার', NULL, 'uploads/products/meta/irItv6fs5Z7TbiYTGMboMd2bKhQWNwZBGCL0dWz7.jpeg', NULL, 'VIM-Dishwashing-Bar-rCPg5', 0.00, '2020-06-02 00:31:35', '2020-06-16 09:05:01'),
(101, 'Apple', 'admin', 1, 31, 65, 93, 48, '[\"uploads\\/products\\/photos\\/89wff7h0GPKvWd4rZZIb1gy5fwNlRk2JCRP4uFU6.jpeg\"]', 'uploads/products/thumbnail/r2AXJMHyqfwkeyVnF51xKRQhPbb4ZwwHh6RuqzT2.jpeg', 'uploads/products/featured/De3TAfKDxdcQEmPHZkoMBoJsnCAlwUIlA8RFUIWj.jpeg', 'uploads/products/flash_deal/Zg0bk4uAAydqF3k9CPtCBd25OPOYBV0zqHPvLtsR.jpeg', 'youtube', NULL, 'Apple', NULL, 150.00, 150.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u09aa\\u09c7\\u09b2\",\"options\":[\"kg\"]}]', '[]', '{\"kg\":{\"price\":\"150\",\"sku\":\"A-kg\",\"qty\":\"99\"}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'আপেল', NULL, 'uploads/products/meta/vPWdGkQvXsbM6phsAwr4VAkkwMdj9TWCDa15ev2r.jpeg', NULL, 'Apple-45S74', 0.00, '2020-06-02 00:34:24', '2020-06-16 09:04:37'),
(102, 'Red Lentils', 'admin', 1, 25, 70, 87, 48, '[\"uploads\\/products\\/photos\\/sxbK7HmmqCZBbuzR52WT4HsSs2XbkDk8NZpEqO2P.jpeg\"]', 'uploads/products/thumbnail/61zRJoS5kMThMc7fle3eOshHCjLrMy8hj1UmKyAb.jpeg', 'uploads/products/featured/A1hcxPxStQlpCIiOxvPo36KxosxN3pLG2olCRsvA.jpeg', 'uploads/products/flash_deal/nhPXzw2Us8GmIZ4yZNyiUFWmwgWdYW2QVGiJix6n.jpeg', 'youtube', NULL, 'Red Lentils', NULL, 120.00, 120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ae\\u09b8\\u09c1\\u09b0\\u09c7\\u09b0 \\u09a1\\u09be\\u09b2\",\"options\":[\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"120\",\"sku\":\"RL-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'মসুরের ডাল', NULL, 'uploads/products/meta/CVQHYpIIFv4QouwiIIcsRttg67YbhKrSM45tlU2g.jpeg', NULL, 'Red-Lentils-kuvdL', 0.00, '2020-06-02 00:38:02', '2020-06-18 08:28:37'),
(103, 'Arang Dairy liquid milk with full butter', 'admin', 1, 24, 71, 86, 66, '[\"uploads\\/products\\/photos\\/fLgWvkXLfBqgk9X3Km28OCLbHWX5vOHXLtGjyacv.jpeg\"]', 'uploads/products/thumbnail/BI8XyZGdoOp4wKNDUvuTudr5cWD5GRIDA4ejl5h9.jpeg', 'uploads/products/featured/Xb7NYXjtjjKYfLc71DKjwHC8hMKTMKfYzOD6D8Rb.jpeg', 'uploads/products/flash_deal/wap3psH9vTEUXsz7RN0SHC05GQysAJSR0lrhn1tk.jpeg', 'youtube', NULL, 'Arang Dairy liquid milk with full butter', NULL, 70.00, 70.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0986\\u09dc\\u0982 \\u09a1\\u09c7\\u0987\\u09b0\\u09bf \\u09aa\\u09c2\\u09b0\\u09cd\\u09a3 \\u09a8\\u09a8\\u09c0\\u09af\\u09c1\\u0995\\u09cd\\u09a4 \\u09a4\\u09b0\\u09b2 \\u09a6\\u09c1\\u09a7\",\"options\":[\"Kg\"]}]', '[]', '{\"Kg\":{\"price\":\"70\",\"sku\":\"ADlmwfb-Kg\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'আড়ং ডেইরি পূর্ণ ননীযুক্ত তরল দুধ', NULL, 'uploads/products/meta/Vz2ZJaYohHRr4cz1BTKBr3LxDzGVTc0EZVIG6wlL.jpeg', NULL, 'Arang-Dairy-liquid-milk-with-full-butter-OjkkK', 0.00, '2020-06-02 00:41:58', '2020-06-16 09:03:20'),
(104, '7 Up', 'admin', 1, 23, 72, 85, 48, '[\"uploads\\/products\\/photos\\/MukxDPueFjX1OsNGzMM6LkLvjU45uzpAx38ql57X.jpeg\",\"uploads\\/products\\/photos\\/0OrSoIkdEAHWFfsWtxmbljaWLjTwUnriAfQD4GvH.jpeg\"]', 'uploads/products/thumbnail/i8VY2fouczs01L5h8RAqLRnfJWEddk5Vwgmr3XmI.jpeg', 'uploads/products/featured/uEehNa3fH98kOnGwgSNfs8Qt6lzPMNN6IHHYjzDf.jpeg', 'uploads/products/flash_deal/pjRoqEKhGdCDAbDu8D6IbxQgUIvX4p8aLxUAmojY.jpeg', 'youtube', NULL, '7 Up', NULL, 30.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"7 Up\",\"options\":[\"500ml\",\"1ltr.\",\"2ltr.\"]}]', '[]', '{\"500ml\":{\"price\":\"30\",\"sku\":\"7U-500ml\",\"qty\":91},\"1ltr.\":{\"price\":\"60\",\"sku\":\"7U-1ltr.\",\"qty\":\"100\"},\"2ltr.\":{\"price\":\"120\",\"sku\":\"7U-2ltr.\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Ltr.', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, '7Up-1Ltr/2Ltr', NULL, 'uploads/products/meta/c13phmbObmTQTQdBOHu1njV6N1QP2v50RLvrl3JD.jpeg', NULL, '7-Up-MBf0F', 0.00, '2020-06-02 00:48:00', '2020-10-27 01:18:58'),
(105, 'Head  Shoulder smooth  silky Shampoo 330 ml', 'admin', 1, 21, 73, 83, 48, '[\"uploads\\/products\\/photos\\/nAYoUDGo2lnoRmMiECPEkLy76QQPHTeiTaLjQYW1.jpeg\"]', 'uploads/products/thumbnail/AifEDQ7bNcCVxgL3vlBSY2mxi04s7qGkwmfAdX3q.jpeg', 'uploads/products/featured/3IkFduPoqLkfKWpaEI1UGULkOIRzEMPpVWhTN8lV.jpeg', 'uploads/products/flash_deal/yW8OMxgt5qn9qmUyxFQUJm9eGjynFldwvb8r6v2S.jpeg', 'youtube', NULL, 'Head & Shoulder smooth & silky Shampoo 330 ml', NULL, 360.00, 360.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b9\\u09c7\\u09a1 \\u098f\\u09a8\\u09cd\\u09a1 \\u09b8\\u09cb\\u09b2\\u09a1\\u09be\\u09b0 \\u09b8\\u09bf\\u09b2\\u09cd\\u0995\\u09bf \\u09b6\\u09cd\\u09af\\u09be\\u09ae\\u09cd\\u09aa\\u09c1\",\"options\":[\"330ml\"]}]', '[]', '{\"330ml\":{\"price\":\"360\",\"sku\":\"HSssS3m-330ml\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'হেড এন্ড সোলডার সিল্কি শ্যাম্পু', NULL, 'uploads/products/meta/a8fl7iJQGdJJrZEDvbKC4fAgKMohRNxDeo5q6ft2.jpeg', NULL, 'Head--Shoulder-smooth--silky-Shampoo-330-ml-WuZuk', 0.00, '2020-06-02 00:57:07', '2020-06-16 09:01:02'),
(106, 'LACTOGEN 2', 'admin', 1, 18, 58, 80, 75, '[\"uploads\\/products\\/photos\\/Vv2Zgv7mOSW4jiPc64vU7o9bBsJuU5vV4hIByYjU.jpeg\"]', 'uploads/products/thumbnail/4on8lXh3h1hEpi9gCl7YB3N0M5xCqJL0dDdAown9.jpeg', 'uploads/products/featured/OLJn4s0AW6IJqaN55ytg2RK5huCuVre28pKcVaI7.jpeg', 'uploads/products/flash_deal/tyaFa2CnW1msusTY1k4VDWska5veTmGmkoT3wDbf.jpeg', 'youtube', NULL, 'LACTOGEN 2', NULL, 230.00, 230.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09cd\\u09af\\u09be\\u0995\\u099f\\u09cb\\u099c\\u09c7\\u09a8 2 (6 \\u09a5\\u09c7\\u0995\\u09c7 12 \\u09ae\\u09be\\u09b8)\",\"options\":[\"180gm\"]}]', '[]', '{\"180gm\":{\"price\":\"230\",\"sku\":\"L2-180gm\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ল্যাকটোজেন 2 (6 থেকে 12 মাস)', NULL, 'uploads/products/meta/lmxyyIDFtYheVAxgBTq3phOLvi8dBPcC0Foo4X11.jpeg', NULL, 'LACTOGEN-2-tnvhw', 0.00, '2020-06-02 01:05:26', '2020-06-16 09:00:33'),
(107, 'MAGGI 2-Minute Noodles Masala', 'admin', 1, 22, 74, 84, 48, '[\"uploads\\/products\\/photos\\/PHRDnE05PU8eJ4NtgOAllFnsAiWyFND4xfLGWVW9.png\"]', 'uploads/products/thumbnail/p6IVa35SFnAL5QvnUefUcitqa7gJzJDw4kMWypYY.png', 'uploads/products/featured/S167FXjpGLUi4oL9d9RLInaTVoH5fZRTs0ytMR99.png', 'uploads/products/flash_deal/iWJ0qDG4xFLsSWdzIZFTkxoq7Tjrav4hwoVO5PiY.png', 'youtube', NULL, 'MAGGI 2-Minute Noodles Masala', NULL, 135.00, 135.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ae\\u09cd\\u09af\\u09be\\u0997\\u09bf 2-\\u09ae\\u09bf\\u09a8\\u09bf\\u099f \\u09a8\\u09c1\\u09a1\\u09b2\\u09b8 \\u09ae\\u09be\\u09b8\\u09be\\u09b2\\u09be\",\"options\":[\"496g\"]}]', '[]', '{\"496g\":{\"price\":\"135\",\"sku\":\"M2NM-496g\",\"qty\":\"99\"}}', 0, 1, 0, 0, 'pac', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'ম্যাগি 2-মিনিট নুডলস মাসালা', NULL, 'uploads/products/meta/B06N9erYy4qkmI6njyEqJygRvAhV5531XdMRCDN9.png', NULL, 'MAGGI-2-Minute-Noodles-Masala-aYB3M', 0.00, '2020-06-02 01:11:20', '2020-06-16 08:59:59'),
(108, 'Odnil Natural Air Freshener Brain Rose', 'admin', 1, 15, 60, 77, 49, '[\"uploads\\/products\\/photos\\/0TvxkFonykzATZLXSToFKk6Dy6E0O7rng7nmjbzA.jpeg\"]', 'uploads/products/thumbnail/AoRS3FpKQWLbMEXYbHdElXgdcpFjHy4iuTvl68pB.jpeg', 'uploads/products/featured/zNYzWKzAJ81H57QdQWswrXWGIvnvZ4QwtU11KEQN.jpeg', 'uploads/products/flash_deal/B4UhMz8wRsYBGuwazdqlOvKSpMMmdg4On4F7qO36.jpeg', 'youtube', NULL, 'Odnil Natural Air Freshener Brain Rose 50g', NULL, 45.00, 45.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0993\\u09a1\\u09cb\\u09a8\\u09bf\\u09b2 \\u09a8\\u09cd\\u09af\\u09be\\u099a\\u09be\\u09b0\\u09be\\u09b2 \\u098f\\u09af\\u09bc\\u09be\\u09b0 \\u09ab\\u09cd\\u09b0\\u09c7\\u09b6\\u09a8\\u09be\\u09b0 \\u09ac\\u09cd\\u09b0\\u09be\\u09af\\u09bc\\u09be\\u09a8 \\u09b0\\u09cb\\u099c\",\"options\":[\"\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\"]}]', '[]', '{\"\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"45.00\",\"sku\":\"ONAFBR-\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":99}}', 0, 1, 0, 0, 'Pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'ওডোনিল ন্যাচারাল এয়ার ফ্রেশনার ব্রায়ান রোজ', NULL, 'uploads/products/meta/WCC4KAXYebYeAMwvvIL2be1q7vxeGjovW5dzy6xt.jpeg', NULL, 'Odnil-Natural-Air-Freshener-Brain-Rose-wUCmm', 0.00, '2020-06-02 01:15:04', '2021-06-01 15:47:16'),
(111, 'VIM Dishwashing Bar', 'admin', 1, 20, 64, 82, 76, '[\"uploads\\/products\\/photos\\/uDmkBFDaBUq7iH7ZlxdxzvAy6JwBc9UzEJ0MDcTI.jpeg\"]', 'uploads/products/thumbnail/TdPvkMviUlUgre3xQOBq8xjSt4ujWZtGkMzsxckt.jpeg', 'uploads/products/featured/aOhdxKRvt0tkgd3uEYFkww3Etk3vGRvwCvHArO7B.jpeg', 'uploads/products/flash_deal/7W4BRDCo2L454EHvAOUjggvQk0I6RnoO8LdeXo7p.jpeg', 'youtube', NULL, 'Vim', NULL, 12.00, 12.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ad\\u09bf\\u09ae \\u09a1\\u09bf\\u09b8\\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09ac\\u09be\\u09b0\",\"options\":[\"100gm\"]}]', '[]', '{\"100gm\":{\"price\":\"12\",\"sku\":\"V-100gm\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, NULL, NULL, 'uploads/products/meta/R4F32LOSF2cAD2u6DwdXEBzMgAlrBYe4Y8c4rxD3.jpeg', NULL, 'VIM-Dishwashing-Bar-wIbHL', 0.00, '2020-06-14 03:40:19', '2020-06-14 03:43:40'),
(112, 'Vim Dishwashing Liquid', 'admin', 1, 20, 64, 82, 76, '[\"uploads\\/products\\/photos\\/INvnWpo3VXGs3Ho3ogkxsqFVkzxZwwY8iF215Mki.jpeg\",\"uploads\\/products\\/photos\\/LlxcjIhJv070xj801QfNmsYFxJTj2tAm6mm9H8SJ.jpeg\"]', 'uploads/products/thumbnail/ZLdAjUtF69uezYR6LZnoyfpk9s3uD1RezIod3U6B.jpeg', 'uploads/products/featured/h91MS3SIiU4PWsYmua3UJXzHTkSFuPPDu7hm3wHN.jpeg', 'uploads/products/flash_deal/ojqe0zyogOmFifE5hivFcJLYwcdW6fSIgT8HNYv1.jpeg', 'youtube', NULL, 'Vim Dishwashing Liquid', NULL, 45.00, 45.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ad\\u09bf\\u09ae \\u09a1\\u09bf\\u09b8\\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09b2\\u09bf\\u0995\\u09c1\\u0987\\u09a1\",\"options\":[\"250ml\"]}]', '[]', '{\"250ml\":{\"price\":\"45\",\"sku\":\"VDL-250ml\",\"qty\":\"10\"}}', 0, 1, 0, 0, 'ML', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, NULL, NULL, 'uploads/products/meta/YQ5IBKipfxzQn6GOpwyYGcjA59cW21Sd9yslnYMG.jpeg', NULL, 'Vim-Dishwashing-Liquid-YmiJ5', 0.00, '2020-06-14 03:59:50', '2020-06-14 04:19:00'),
(113, 'Vim Dishwashing Liquid', 'admin', 1, 20, 64, 82, 76, '[\"uploads\\/products\\/photos\\/pnJDYqqTJ8Vws8KR9Nydds8LPNwvuVaBZFTqDscV.jpeg\",\"uploads\\/products\\/photos\\/zWs0KsRLu1WP9mAsds5Fi9IQdUoZA7VUziydSlCd.jpeg\"]', 'uploads/products/thumbnail/eWg5NHcp56EF1WOU8nz0ZbhR6ed6aJnk0NM0x8p9.jpeg', 'uploads/products/featured/bF4ynq3vF7LgVlYwYUdrD6xxChyrwKpCxew3l1rE.jpeg', 'uploads/products/flash_deal/CYJba1XyKYzzYP3jIHcXsLvEvBM0PZcD3niQVi74.jpeg', 'youtube', NULL, 'Vim Dishwashing Liquid', NULL, 100.00, 100.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ad\\u09bf\\u09ae \\u09a1\\u09bf\\u09b8\\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09b2\\u09bf\\u0995\\u09c1\\u0987\\u09a1\",\"options\":[\"500ml\"]}]', '[]', '{\"500ml\":{\"price\":\"100\",\"sku\":\"VDL-500ml\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'ML', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ভিম ডিসওয়াশিং লিকুইড', NULL, 'uploads/products/meta/H9T1o04Eyr8cXN1LL1sAFnh5MOve8sRff9SoGonA.jpeg', NULL, 'Vim-Dishwashing-Liquid-b3fj7', 0.00, '2020-06-14 04:22:48', '2020-06-14 04:22:48');
INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `rating`, `created_at`, `updated_at`) VALUES
(114, 'Napa Tablets', 'seller', 36, 33, 76, 95, 82, '[\"uploads\\/products\\/photos\\/JOF0NfqPvlme8CagaYvMVNJGaNSAjIEAHsNbqiL7.jpeg\"]', 'uploads/products/thumbnail/aGEqUfuNa1mkWh1FX4MSZWZvnY543ljxsgY9uRXt.jpeg', 'uploads/products/featured/qirR6JdnciUen4NBMHbe76Ct57lmFnWfsJjhDNYu.jpeg', 'uploads/products/flash_deal/L1IAhvYYSGAxVQqaYw0cdzH3WidnZbpBElXy0e6I.jpeg', 'youtube', NULL, 'Napa', NULL, 10.00, 110.00, '[{\"name\":\"choice_1\",\"title\":\"\\u09a8\\u09be\\u09aa\\u09be Napa\",\"options\":[\"\\u09aa\\u09be\\u09a4\\u09be Pata\",\"\\u09ac\\u0995\\u09cd\\u09b8 Box\"]}]', '[]', '{\"\\u09aa\\u09be\\u09a4\\u09bePata\":{\"price\":\"10.00\",\"sku\":\"N-\\u09aa\\u09be\\u09a4\\u09bePata\",\"qty\":98},\"\\u09ac\\u0995\\u09cd\\u09b8Box\":{\"price\":\"110.00\",\"sku\":\"N-\\u09ac\\u0995\\u09cd\\u09b8Box\",\"qty\":\"100\"}}', 0, 1, 0, 100, 'পাতা Pata/ বক্স Box', 0.00, 'amount', 0.00, 'amount', 'free', 0.00, 1, 'নাপা Napa', NULL, 'uploads/products/meta/R5wSRznSS1QWL11LQONiM7prxMtNKuikOsFgd4U0.jpeg', NULL, 'Napa-Tablets-3NOCi', 0.00, '2020-06-14 08:31:24', '2020-10-27 02:48:20'),
(115, 'LUX SOAP BAR SOFT TOUCH', 'admin', 1, 21, 73, 83, 56, '[\"uploads\\/products\\/photos\\/sy6AkuUAHilhptiLRVLxSsIXWGRXbdt8wM9ruypx.jpeg\",\"uploads\\/products\\/photos\\/tL0j4sBFq8RoNZNOPxQ3w7LEfmjtvJDnj3Olj1ac.jpeg\",\"uploads\\/products\\/photos\\/3NUtRK1BV7eYzzIl6LCV3OmlCPXThz36QE60QRXo.jpeg\"]', 'uploads/products/thumbnail/bHGxhny5YUvxNb5kg9i1XX8PL3OpiCgKNYyTk7Zw.jpeg', 'uploads/products/featured/efhO0vHizCeOu9XC00PmdYFQNiTxpcjK9IUNTgOn.jpeg', 'uploads/products/flash_deal/XwYhJhN7l0aQ9iNgf17r9CkTpM10T9Jyl1GL6XMd.jpeg', 'youtube', NULL, 'LUX SOAP BAR SOFT TOUCH', NULL, 50.00, 50.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u0995\\u09cd\\u09b8 \\u09b8\\u09cb\\u09aa \\u09ac\\u09be\\u09b0 \\u09b8\\u09ab\\u099f \\u099f\\u09be\\u099a\",\"options\":[\"\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\",\"\\u09b8\\u09be\\u09a6\\u09be\",\"\\u09a8\\u09c0\\u09b2\"]}]', '[]', '{\"\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\":{\"price\":\"50\",\"sku\":\"LSBST-\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\",\"qty\":\"100\"},\"\\u09b8\\u09be\\u09a6\\u09be\":{\"price\":\"50\",\"sku\":\"LSBST-\\u09b8\\u09be\\u09a6\\u09be\",\"qty\":\"100\"},\"\\u09a8\\u09c0\\u09b2\":{\"price\":\"50\",\"sku\":\"LSBST-\\u09a8\\u09c0\\u09b2\",\"qty\":\"100\"}}', 0, 1, 1, 0, '150gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লাক্স সোপ বার সফট টাচ', NULL, 'uploads/products/meta/Gpf8Rg2V4L23hDI7MSJUgdzZtbnm4xkOuy8GThCW.jpeg', NULL, 'LUX-SOAP-BAR-SOFT-TOUCH-4SLoF', 0.00, '2020-06-16 22:24:13', '2020-06-16 22:30:14'),
(116, 'LUX SOAP BAR SOFT TOUCH', 'admin', 1, 21, 73, 83, 56, '[\"uploads\\/products\\/photos\\/YwVb02cyIIVndriI2YpTEnIwwuXgt1lifBe6tbxe.jpeg\",\"uploads\\/products\\/photos\\/WwoTypXQZR3PrKH2W7kjFAiP2yqjWYqZQRn6mSRx.jpeg\",\"uploads\\/products\\/photos\\/UOKjWK5bvzMW4lB3pbWHxakVCFz2M2RUMxM35DTC.jpeg\"]', 'uploads/products/thumbnail/3qkMc5JOK8xWxNQCYmpRfCeUOQNpf4a5Qhms7hZB.jpeg', 'uploads/products/featured/9NxC5JRAOGu0dvlDg3gnfT0d1kQRkvmR6ZAL1iV2.jpeg', 'uploads/products/flash_deal/2Jm8HQrNpVtU5i5CnBMoUyyrAyvDY8UgVKKgYFWE.jpeg', 'youtube', NULL, 'LUX SOAP BAR SOFT TOUCH', NULL, 35.00, 35.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u0995\\u09cd\\u09b8 \\u09b8\\u09cb\\u09aa \\u09ac\\u09be\\u09b0 \\u09b8\\u09ab\\u099f \\u099f\\u09be\\u099a\",\"options\":[\"\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf \",\"\\u09b8\\u09be\\u09a6\\u09be\",\"\\u09a8\\u09c0\\u09b2\"]}]', '[]', '{\"\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\":{\"price\":\"35\",\"sku\":\"LSBST-\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\",\"qty\":\"100\"},\"\\u09b8\\u09be\\u09a6\\u09be\":{\"price\":\"35\",\"sku\":\"LSBST-\\u09b8\\u09be\\u09a6\\u09be\",\"qty\":99},\"\\u09a8\\u09c0\\u09b2\":{\"price\":\"35\",\"sku\":\"LSBST-\\u09a8\\u09c0\\u09b2\",\"qty\":\"100\"}}', 0, 1, 0, 0, '100gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'লাক্স সোপ বার সফট টাচ', NULL, 'uploads/products/meta/jl1jyJ3sa6ko6hD3PpMK55F6GbnyyAD0ITVySZtx.jpeg', NULL, 'LUX-SOAP-BAR-SOFT-TOUCH-HrTFB', 0.00, '2020-06-16 22:28:49', '2020-06-17 00:47:14'),
(117, 'LUX SOAP BAR SOFT TOUCH', 'admin', 1, 21, 73, 83, 56, '[\"uploads\\/products\\/photos\\/hHHa7COCTK7CDFXyhpRuG5IQzh8eorApTuZv1jzX.jpeg\",\"uploads\\/products\\/photos\\/OhcztnBJRtUfXXovxM3elaQPk1ZXfeMaD7w2LMLB.jpeg\"]', 'uploads/products/thumbnail/EByz0dPtAb5AxpWpFmpuUpeIZW0sdDU9c6KobUpd.jpeg', 'uploads/products/featured/l9u9TUAZAHLkuYs4P7FO3SgKKezlEOMLNynLLGJi.jpeg', 'uploads/products/flash_deal/N3pYIAqmdsyDxR3GwaMrBWpq2RUjuKJfuvH0mHlC.jpeg', 'youtube', NULL, 'LUX SOAP BAR SOFT TOUCH', NULL, 24.00, 24.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u0995\\u09cd\\u09b8 \\u09b8\\u09cb\\u09aa \\u09ac\\u09be\\u09b0 \\u09b8\\u09ab\\u099f \\u099f\\u09be\\u099a\",\"options\":[\"\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf  \",\"\\u09b8\\u09be\\u09a6\\u09be\",\"\\u09a8\\u09c0\\u09b2\"]}]', '[]', '{\"\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\":{\"price\":\"24\",\"sku\":\"LSBST-\\u0997\\u09cb\\u09b2\\u09be\\u09aa\\u09bf\",\"qty\":\"100\"},\"\\u09b8\\u09be\\u09a6\\u09be\":{\"price\":\"24\",\"sku\":\"LSBST-\\u09b8\\u09be\\u09a6\\u09be\",\"qty\":\"100\"},\"\\u09a8\\u09c0\\u09b2\":{\"price\":\"24\",\"sku\":\"LSBST-\\u09a8\\u09c0\\u09b2\",\"qty\":\"100\"}}', 0, 1, 0, 0, '75gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, NULL, NULL, NULL, NULL, 'LUX-SOAP-BAR-SOFT-TOUCH-sX7kL', 0.00, '2020-06-16 22:38:26', '2020-06-17 00:42:57'),
(118, 'ACI SAVLON MILD SOAP', 'admin', 1, 21, 73, 83, 51, '[\"uploads\\/products\\/photos\\/bIsnwE3kDwcEg2o36vI2LhFZGpdJqEj1omR37dQF.jpeg\",\"uploads\\/products\\/photos\\/FlxgGxRatatSURppMBGOo5sbf9YNrNzmvoRBfTEu.jpeg\"]', 'uploads/products/thumbnail/6KogfE16N7TtgLVr2xa5hdRZYt9nPBT7xWTaz3JP.jpeg', 'uploads/products/featured/ztIDzJB5ijjJZftGy0WWd2LBgIHWeG1NheUw7OZP.jpeg', 'uploads/products/flash_deal/psbC2VNwNtaTUcVbPLg7OE7LY4ZLCuo0JNIGmqGt.jpeg', 'youtube', NULL, 'ACI SAVLON MILD SOAP', NULL, 50.00, 50.00, '[]', '[]', '[]', 0, 1, 1, 100, '125gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'এসিআই সেভলন মিল্ড সোপ', NULL, 'uploads/products/meta/PV08ReUrjcKrBH0602TfXs8chszEmkSaGOyBdqEl.jpeg', NULL, 'ACI-SAVLON-MILD-SOAP-9qgvh', 0.00, '2020-06-16 22:43:15', '2020-06-17 20:00:43'),
(119, 'DETTOL BATHING BAR SOAP SKIN CARE', 'admin', 1, 21, 73, 83, 51, '[\"uploads\\/products\\/photos\\/DpG2EGpoWogVPHttPELnPYjyAEmZXLUbrAp84EKa.jpeg\",\"uploads\\/products\\/photos\\/ciRKYRyLaOhImgGbYhNcKAHUeSq0LcvAsuME0qS2.jpeg\"]', 'uploads/products/thumbnail/HByom7eK9uAflO2Sck4Ov4isSCJu4PjiIRxD0Jiy.jpeg', 'uploads/products/featured/TMi3ZODaqxJI6fAJaKUnAqcyhNk8Zc9xtGMXKZrU.jpeg', 'uploads/products/flash_deal/QdznD7My0gv2COoqmz2qLYPFSNg1V2w2l6eXD2PC.jpeg', 'youtube', NULL, 'DETTOL BATHING BAR SOAP SKIN CARE', NULL, 58.00, 58.00, '[]', '[]', '[]', 0, 1, 1, 100, '125gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ডেটল বাথিং বার সোপ স্কিন কেয়ার', NULL, NULL, NULL, 'DETTOL-BATHING-BAR-SOAP-SKIN-CARE-qpQLV', 0.00, '2020-06-16 22:54:47', '2020-06-17 20:00:40'),
(120, 'HARPIC LIQUID TOILET CLEANER', 'admin', 1, 20, 64, 82, 83, '[\"uploads\\/products\\/photos\\/FSXOu4IV06feXx9yGzXAYgCZ7VECXvrQcoxz4nMd.jpeg\",\"uploads\\/products\\/photos\\/oPaY0gLsHdZSBQCiATZFiTFrofJDGDqY0pcaIq0G.jpeg\",\"uploads\\/products\\/photos\\/AiJusmh0OLnI8TH5Lwh1qZjCIa5siQdlBglEHfpU.jpeg\"]', 'uploads/products/thumbnail/T9EguQ1i6X0MntgiAU51EyV87MOSRZ2DbgKgYf5a.jpeg', 'uploads/products/featured/J408xoIqURbaHuXzqdju8MwtopzFthKMHSvUysPa.jpeg', 'uploads/products/flash_deal/LX5PkI1FNo2TkPmIYP0zlDq4pq70XSz4i438jTbB.jpeg', 'youtube', NULL, 'HARPIC LIQUID TOILET CLEANER', NULL, 40.00, 125.00, '[{\"name\":\"choice_1\",\"title\":\"\\u09b9\\u09be\\u09b0\\u09cd\\u09aa\\u09bf\\u0995 \\u09b2\\u09bf\\u0995\\u09c1\\u0987\\u09a1 \\u099f\\u09af\\u09bc\\u09b2\\u09c7\\u099f \\u0995\\u09cd\\u09b2\\u09bf\\u09a8\\u09be\\u09b0\",\"options\":[\"200ml\",\"500ml\",\"750ml\"]}]', '[]', '{\"200ml\":{\"price\":\"40.00\",\"sku\":\"HLTC-200ml\",\"qty\":\"100\"},\"500ml\":{\"price\":\"100.00\",\"sku\":\"HLTC-500ml\",\"qty\":\"100\"},\"750ml\":{\"price\":\"125.00\",\"sku\":\"HLTC-750ml\",\"qty\":\"100\"}}', 0, 1, 1, 100, '200ml/500ml/750ml', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'হার্পিক লিকুইড টয়লেট ক্লিনার', NULL, 'uploads/products/meta/4WIHP4N7MuYiyVgfEP8ddfsEau8RGlElEZy1iv1h.jpeg', NULL, 'HARPIC-LIQUID-TOILET-CLEANER-voe5o', 0.00, '2020-06-16 23:07:05', '2020-06-17 20:00:37'),
(122, 'WHEEL WASHING POWDER 2IN1 CLEAN  FRESH', 'admin', 1, 20, 64, 82, 56, '[\"uploads\\/products\\/photos\\/8dleyiN8SjTVG9GBQSiF52xGnA6p0nV7ISUJUyLF.jpeg\",\"uploads\\/products\\/photos\\/WiTY5jsp8slfJnAbL8SROuyb3c5YTJEBGCDZEXr2.jpeg\"]', 'uploads/products/thumbnail/OJTX3mo9YVur06FT4YK2ipBVSjXx1wzLHB6C02dO.jpeg', 'uploads/products/featured/Blv8lue39Pspdq7eg4SktcGxDmJlYCAZ29tDMnvr.jpeg', 'uploads/products/flash_deal/7YXZjbcrJmVq745sgLh8Umw2Q4azX61bdmLLfHFd.jpeg', 'youtube', NULL, 'WHEEL WASHING POWDER 2IN1 CLEAN & FRESH', NULL, 42.00, 85.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b9\\u09c1\\u0987\\u09b2\\u09bf\\u0982 \\u09aa\\u09be\\u0989\\u09a1\\u09be\\u09b0 \\u09e8\\u0987\\u09a8\\u09e7 \\u0995\\u09cd\\u09b2\\u09bf\\u09a8 \\u098f\\u09a8\\u09cd\\u09a1 \\u09ab\\u09cd\\u09b0\\u09c7\\u09b6\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"42.00\",\"sku\":\"WWP2CF-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"85.00\",\"sku\":\"WWP2CF-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, '500gm/1kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'হুইলিং পাউডার ২ইন১ ক্লিন এন্ড ফ্রেশ', NULL, 'uploads/products/meta/jRjAmunCYkTKVBkjPB8Md3vKrBFX2xhPy3Ek5BSy.jpeg', NULL, 'WHEEL-WASHING-POWDER-2IN1-CLEAN--FRESH-rFWeD', 0.00, '2020-06-16 23:29:09', '2020-06-17 20:00:31'),
(123, 'RIN WASHING POWDER POWER BRIGHT', 'admin', 1, 20, 64, 82, 56, '[\"uploads\\/products\\/photos\\/ksT9HrRsR0XXQrzIBFDZ7lzXcs0yGVmCiPDLSt5Y.jpeg\",\"uploads\\/products\\/photos\\/5sfMUVxfG8MDF5a0hc6IhXeqYKg7Mu7dhSX362lG.jpeg\",\"uploads\\/products\\/photos\\/7awvQJrWcpQJlyDluwvxpmzrxNdvBOGwKXymC0MI.jpeg\",\"uploads\\/products\\/photos\\/EHKUWEHsW79te7GBUu2KIOGGbkx6RCr8brQkq7JQ.jpeg\"]', 'uploads/products/thumbnail/dFZbT4jqL1dh9pKLgQs0KSrgt1YINpXU4nkkElkm.jpeg', 'uploads/products/featured/EHpq2QKymsovDiwRcP2mcnYrHckPiTaTj0QWf3Zc.jpeg', 'uploads/products/flash_deal/L6Bv3qlZJlt4X7KPWH3SZfE3NN78KpWfYaAf3J0X.jpeg', 'youtube', NULL, 'RIN WASHING POWDER POWER BRIGHT', NULL, 60.00, 120.00, '[{\"name\":\"choice_1\",\"title\":\"\\u09b0\\u09bf\\u09a8 \\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09aa\\u09be\\u0989\\u09a1\\u09be\\u09b0 \\u09aa\\u09be\\u0993\\u09af\\u09bc\\u09be\\u09b0 \\u09ac\\u09cd\\u09b0\\u09be\\u0987\\u099f\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"60\",\"sku\":\"RWPPB-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1200\",\"sku\":\"RWPPB-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 1, 0, '500gm/1kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রিন ওয়াশিং পাউডার পাওয়ার ব্রাইট', NULL, 'uploads/products/meta/TSxBKkeCEunrk3Idg5mkfb0dWcWiCr0vGLNmSSqA.jpeg', NULL, 'RIN-WASHING-POWDER-POWER-BRIGHT-ouAaf', 0.00, '2020-06-16 23:36:13', '2020-06-17 20:00:33'),
(124, 'SURF EXCEL WASHING POWDER', 'admin', 1, 20, 64, 82, 56, '[\"uploads\\/products\\/photos\\/OJG9WJkDXk5DGRgsFql84UDJvk2KftTm3UqBW0d7.jpeg\",\"uploads\\/products\\/photos\\/A7SenPc72gjAaMap5WfufLsLb2iq3Fl8d34lHdbY.jpeg\",\"uploads\\/products\\/photos\\/v0H3KpUT7dWq4qsAa8GIl8DabxhXKjLxyfpD11O1.jpeg\",\"uploads\\/products\\/photos\\/2ugAymrTd18jftppdg58WM1KUKiemIWLMkAfB76e.jpeg\"]', 'uploads/products/thumbnail/zHEMHbd5yGDQoqrZrPE2KDQ06nNbtUSg8TwGbPNP.jpeg', 'uploads/products/featured/mcIatdw9ViZUU0Qm8ExF24WKfmebR1fpCGBjhchc.jpeg', 'uploads/products/flash_deal/oU82rGHVMzETfauvMtuh0Dv4YaG1aMb65FfDgl50.jpeg', 'youtube', NULL, 'SURF EXCEL WASHING POWDER', NULL, 99.00, 198.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b8\\u09be\\u09b0\\u09cd\\u09ab \\u098f\\u0995\\u09cd\\u09b8\\u09c7\\u09b2 \\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09aa\\u09be\\u0993\\u09af\\u09bc\\u09be\\u09b0\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"99\",\"sku\":\"SEWP-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"198\",\"sku\":\"SEWP-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '500gm/1kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'সার্ফ এক্সেল ওয়াশিং পাওয়ার', NULL, 'uploads/products/meta/KJJlwcvpZux5tPaGU5r4UOZJRY7civHaaZt7dD90.jpeg', NULL, 'SURF-EXCEL-WASHING-POWDER-iCqGW', 0.00, '2020-06-16 23:43:04', '2020-06-16 23:43:04'),
(125, 'RIN WASHING POWDER ANTIBAC', 'admin', 1, 20, 64, 82, 56, '[\"uploads\\/products\\/photos\\/Wo7vCj5ikEv6Pk2nYUKkYVt2qzUcLYqDAtZzxQN0.jpeg\",\"uploads\\/products\\/photos\\/38jOBAm8NgH0xnAfEXUFUJc4d6BukQlcoWeprABb.jpeg\",\"uploads\\/products\\/photos\\/DhamLSTKVVb4QwMQffjhoq03IALYhc9J3McS8k9y.jpeg\",\"uploads\\/products\\/photos\\/oio9M18YMDo8YNOLDmPP8q660gRHL2tLeDt9ut3Z.jpeg\"]', 'uploads/products/thumbnail/Ss7W4QMLUo7MTuEtyERsfKOi8iIsMYPNh9U6zPqi.jpeg', 'uploads/products/featured/34sNjTUIYVPwo7fNfSFdpNNsbk18WEMC3mWKDiFC.jpeg', 'uploads/products/flash_deal/HacNSMuedVDmiGbmuDC5I1z0to578Yllgs87aP1f.jpeg', 'youtube', NULL, 'RIN WASHING POWDER ANTIBAC', NULL, 60.00, 120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b0\\u09bf\\u09a8 \\u0993\\u09af\\u09bc\\u09be\\u09b6\\u09bf\\u0982 \\u09aa\\u09be\\u0993\\u09af\\u09bc\\u09be\\u09b0 \\u098f\\u09a8\\u09cd\\u099f\\u09bf\\u09ac\\u09cd\\u09af\\u09be\\u0995\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"60\",\"sku\":\"RWPA-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"120\",\"sku\":\"RWPA-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '500gm/1kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'রিন ওয়াশিং পাওয়ার এন্টিব্যাক', NULL, 'uploads/products/meta/2uLJDGnMLjub0NO4b6A0gyoszpJeJRSoa9tgSby0.jpeg', NULL, 'RIN-WASHING-POWDER-ANTIBAC-3FSom', 0.00, '2020-06-16 23:48:01', '2020-06-16 23:48:01'),
(126, 'CLOSEUP TOOTHPASTE RED HOT', 'admin', 1, 21, 73, 83, 56, '[\"uploads\\/products\\/photos\\/gIKLZaZa9z5rEJDT26ANxI5vji1Tn52PfIFpi8Hk.jpeg\",\"uploads\\/products\\/photos\\/1NvHoMUTaxEeQDRJwNzp7jou6yBRCaeXhGpziikr.jpeg\",\"uploads\\/products\\/photos\\/HeoRUeloEfqvMPKIzcJFGkLByQ27DIGbSGmdPsxp.jpeg\",\"uploads\\/products\\/photos\\/jrga5VSZ9pcrWFMF1yIuoiTMzCt3xF94IfgI5Exk.jpeg\"]', 'uploads/products/thumbnail/cEnsuLTrrx9FrBt4DEbC04mWE7lg4ZnfDZr2Gzst.jpeg', 'uploads/products/featured/LyKJKEUVydzdhDx8G2B5aNSkE5mUY1hPF6KOskhV.jpeg', 'uploads/products/flash_deal/nCpB4LHyvrpvrNe8NrciDxZqr9BM8pgTlHNBV0Ko.jpeg', 'youtube', NULL, 'CLOSEUP TOOTHPASTE RED HOT', NULL, 80.00, 110.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09cd\\u09b2\\u09cb\\u099c\\u0986\\u09aa \\u099f\\u09c1\\u09a5\\u09aa\\u09c7\\u09b8\\u09cd\\u099f \\u09b0\\u09c7\\u09a1 \\u09b9\\u099f\",\"options\":[\"\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u09ea\\u09eb\\u0997\\u09cd\\u09b0\\u09be\\u09ae\"]}]', '[]', '{\"\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"80\",\"sku\":\"CTRH-\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":997},\"\\u09e7\\u09ea\\u09eb\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"110\",\"sku\":\"CTRH-\\u09e7\\u09ea\\u09eb\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"}}', 0, 1, 1, 0, '100gm/145gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 3, 'ক্লোজআপ টুথপেস্ট রেড হট', NULL, 'uploads/products/meta/VwoWB1lN7yRVzCEfPGqAqmLlKf3IAuY6MizNrvja.jpeg', NULL, 'CLOSEUP-TOOTHPASTE-RED-HOT-LJ6Nq', 0.00, '2020-06-16 23:57:53', '2021-06-01 15:48:10'),
(127, 'CLOSEUP TOOTHPASTE MENTHOL FRESH', 'admin', 1, 21, 73, 83, 56, '[\"uploads\\/products\\/photos\\/8v1oxaWppGIlwNpLCaOyzyI88h7B64CDXkiGNTJr.jpeg\",\"uploads\\/products\\/photos\\/Bs011Pj1PG4ituUmkuwxQnM2KqfqiSg9QGOZ3E4g.jpeg\",\"uploads\\/products\\/photos\\/k4f4dk3jLDnr5KEsFMBRVMCSdpwBaVq1JyNiDpzu.jpeg\",\"uploads\\/products\\/photos\\/CvvekbU0zY5HI0xO4VELbsbjG21l0Pe3tgoucMJJ.jpeg\"]', 'uploads/products/thumbnail/7S5zUh5jzjMXheTk97d7mUozs6764Mx19h5QU1FQ.jpeg', 'uploads/products/featured/TdFhuhFiSC4rvLKnWfDCX7IwRG5Krgvud5Dwerif.jpeg', 'uploads/products/flash_deal/BMjA7jl1t6LvktSV8VIMTMa7gi1q3XHBzAwbkbEC.jpeg', 'youtube', NULL, 'CLOSEUP TOOTHPASTE MENTHOL FRESH', NULL, 80.00, 110.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09cd\\u09b2\\u09cb\\u099c\\u0986\\u09aa \\u099f\\u09c1\\u09a5\\u09aa\\u09c7\\u09b8\\u09cd\\u099f \\u09ae\\u09c7\\u09a8\\u09a5\\u09b2 \\u09ab\\u09cd\\u09b0\\u09c7\\u09b6\",\"options\":[\"\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u09ea\\u09eb\\u0997\\u09cd\\u09b0\\u09be\\u09ae\"]}]', '[]', '{\"\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"80\",\"sku\":\"CTMF-\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":64},\"\\u09e7\\u09ea\\u09eb\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"110\",\"sku\":\"CTMF-\\u09e7\\u09ea\\u09eb\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"}}', 0, 1, 1, 0, '100gm/145gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 34, 'ক্লোজআপ টুথপেস্ট মেনথল ফ্রেশ', NULL, 'uploads/products/meta/05fpKga9lvI0FTz2YE38fXqqmKvCxq6WkoEwp4tQ.jpeg', NULL, 'CLOSEUP-TOOTHPASTE-MENTHOL-FRESH-nMaJ5', 0.00, '2020-06-17 00:06:42', '2021-06-10 11:21:18'),
(128, 'ISPAHANI MIRZAPORE TEA BAG', 'admin', 1, 24, 71, 86, 61, '[\"uploads\\/products\\/photos\\/fZh19IAmVBpCznMGV2mjYQlE4ZUTTRPsNN4oWDYq.jpeg\"]', 'uploads/products/thumbnail/V6DyizKYUJlor6FqGJGrjEygPfQhzCax0JFXLvCT.jpeg', 'uploads/products/featured/46OFgUOzMsb3FqMMqHINyoI5zIP9sZUXW1aTFYVM.jpeg', 'uploads/products/flash_deal/4GYGjkIu8DPioVCb7ufqT2XoH21aEemiJd1yFeL4.jpeg', 'youtube', NULL, 'ISPAHANI MIRZAPORE TEA BAG', NULL, 80.00, 80.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0987\\u09b8\\u09cd\\u09aa\\u09be\\u09b9\\u09be\\u09a8\\u09c0 \\u09ae\\u09bf\\u09b0\\u099c\\u09be\\u09aa\\u09c1\\u09b0 \\u099f\\u09bf \\u09ac\\u09cd\\u09af\\u09be\\u0997\",\"options\":[\"\\u09eb\\u09e6 \\u099f\\u09bf \\u09ac\\u09cd\\u09af\\u09be\\u0997\"]}]', '[]', '{\"\\u09eb\\u09e6\\u099f\\u09bf\\u09ac\\u09cd\\u09af\\u09be\\u0997\":{\"price\":\"80\",\"sku\":\"IMTB-\\u09eb\\u09e6\\u099f\\u09bf\\u09ac\\u09cd\\u09af\\u09be\\u0997\",\"qty\":41}}', 0, 1, 1, 0, '50 Tea Bags', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 59, 'ইস্পাহানী মিরজাপুর টি ব্যাগ', NULL, 'uploads/products/meta/3Fd10NnfoM9PvWJvPAodV5oFpjJsCJxUWhUooBQO.jpeg', NULL, 'ISPAHANI-MIRZAPORE-TEA-BAG-Tt9y3', 0.00, '2020-06-17 00:20:37', '2021-06-10 11:46:58'),
(129, 'SEYLON GOLD BLEND TEA BAGS', 'admin', 1, 24, 71, 86, 48, '[\"uploads\\/products\\/photos\\/eersARDjvJTeMUMpvSaCvCszCHry3GIXlo1PIfbc.jpeg\"]', 'uploads/products/thumbnail/UPcWYFhyu6J0ACDtqPhwCm93H25Fu5XTu0yFJEcH.jpeg', 'uploads/products/featured/lBU6k0RICmpkZUzyfUTubxaoDBGq56vScpHgnqI2.jpeg', 'uploads/products/flash_deal/sxoFUdG7b6DWuKh0lQz4LRMZpOqK243Vf6Nb5dih.jpeg', 'youtube', NULL, 'SEYLON GOLD BLEND TEA BAGS', NULL, 70.00, 70.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b8\\u09bf\\u09b2\\u09a8 \\u0997\\u09cb\\u09b2\\u09cd\\u09a1 \\u09ac\\u09cd\\u09b2\\u09c7\\u09a8\\u09cd\\u09a1 \\u099f\\u09bf \\u09ac\\u09cd\\u09af\\u09be\\u0997\",\"options\":[\"\\u09eb\\u09e6 \\u099f\\u09bf \\u09ac\\u09cd\\u09af\\u09be\\u0997\"]}]', '[]', '{\"\\u09eb\\u09e6\\u099f\\u09bf\\u09ac\\u09cd\\u09af\\u09be\\u0997\":{\"price\":\"70\",\"sku\":\"SGBTB-\\u09eb\\u09e6\\u099f\\u09bf\\u09ac\\u09cd\\u09af\\u09be\\u0997\",\"qty\":0}}', 0, 1, 1, 0, '50 Tea Bags', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 7, 'সিলন গোল্ড ব্লেন্ড টি ব্যাগ', NULL, 'uploads/products/meta/N2epJNt8fSMnKIPtVx8z5f59dL45M3vpEYCAyqAu.jpeg', NULL, 'SEYLON-GOLD-BLEND-TEA-BAGS-8AgTf', 0.00, '2020-06-17 00:26:15', '2021-05-11 06:37:43'),
(130, 'PARACHUTE COCONUT OIL', 'admin', 1, 21, 73, 83, 56, '[\"uploads\\/products\\/photos\\/HqzaziX98mVkHB6RPRMv7NC6W9luIvQECUJExhbp.jpeg\"]', 'uploads/products/thumbnail/J1q18qDXnGmo6XNA8aVTe56fOXH0WrdQ645HsCWa.jpeg', 'uploads/products/featured/FTaoRiuqUdczy8oAN4PPjvw1J3wj5OOGFRtcZ9mT.jpeg', 'uploads/products/flash_deal/m7MNlXDGKZzHESFImoJi7J2ugrtR7NHN0ciYjVOE.jpeg', 'youtube', NULL, 'PARACHUTE COCONUT OIL', NULL, 190.00, 290.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09cd\\u09af\\u09be\\u09b0\\u09be\\u09b8\\u09c1\\u099f \\u0995\\u09cb\\u0995\\u09cb\\u09a8\\u09be\\u099f \\u09a4\\u09c7\\u09b2\",\"options\":[\"\\u09e8\\u09eb\\u09e6\\u098f\\u09ae\\u098f\\u09b2\",\"\\u09eb\\u09e6\\u09e6\\u098f\\u09ae\\u098f\\u09b2\"]}]', '[]', '{\"\\u09e8\\u09eb\\u09e6\\u098f\\u09ae\\u098f\\u09b2\":{\"price\":\"190\",\"sku\":\"PCO-\\u09e8\\u09eb\\u09e6\\u098f\\u09ae\\u098f\\u09b2\",\"qty\":\"100\"},\"\\u09eb\\u09e6\\u09e6\\u098f\\u09ae\\u098f\\u09b2\":{\"price\":\"290\",\"sku\":\"PCO-\\u09eb\\u09e6\\u09e6\\u098f\\u09ae\\u098f\\u09b2\",\"qty\":\"100\"}}', 0, 1, 0, 0, '250ml/500ml', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'প্যারাসুট কোকোনাট তেল', NULL, 'uploads/products/meta/44pIqGWXyAWJBrIWd9O5ICfLrtRBnKxOIRIsBnIW.jpeg', NULL, 'PARACHUTE-COCONUT-OIL-tZ25v', 0.00, '2020-06-17 00:36:59', '2020-06-17 00:36:59'),
(131, 'Bashundhara Lpg gas', 'admin', 1, 34, 77, 96, 85, '[\"uploads\\/products\\/photos\\/KwpgDoVwSgSrHPtkSqxLu2nYGh3Cz6xHSTRsngxN.jpeg\"]', 'uploads/products/thumbnail/LszQZx7KL6C606w5vZupiyUZHM7vtyxUp8TwgyL5.jpeg', 'uploads/products/featured/A9gLHPWdH9KmCeTW1wiiNiY3Dtfu1PTVnBeDwVvE.jpeg', 'uploads/products/flash_deal/hvQj7mbICOOLI9LR59Xh5REgIzly9IZM5IfRL4Qh.jpeg', 'youtube', NULL, 'Bashundhara Lpg gas', NULL, 1120.00, 1120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09b8\\u09c1\\u09a8\\u09cd\\u09a7\\u09b0\\u09be \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1120\",\"sku\":\"BLg-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বসুন্ধরা এলপিজি গ্যাস Bashundhara Lpg gas', NULL, 'uploads/products/meta/yczadFVDV8oi2VC7OS95cbn87pjOUQNc9875gZaa.jpeg', NULL, 'Bashundhara-Lpg-gas-g1Xtr', 0.00, '2020-06-17 23:42:46', '2020-06-17 23:42:46'),
(132, 'Fresh Lpg Gas', 'admin', 1, 34, 77, 96, 86, '[\"uploads\\/products\\/photos\\/Nf8JmVD6sU8mz31rrftzVZob9ZWk3OaHzg9Jgd74.jpeg\"]', 'uploads/products/thumbnail/iIu0y01olPgM23wCqAxrOrIcv81epU3c7mBun0s5.jpeg', 'uploads/products/featured/MlXFmm2Na9bOtRIrxDNUKCcbIr798mvm1vrM6eB5.jpeg', 'uploads/products/flash_deal/P6op2572ku7UrKxTnIRhnvLeurXWFMhEGvBaUHPA.jpeg', 'youtube', NULL, 'Fresh Lpg Gas', NULL, 1120.00, 1120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ab\\u09cd\\u09b0\\u09c7\\u09b6 \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1120\",\"sku\":\"FLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ফ্রেশ এলপিজি গ্যাস  Fresh Lpg Gas', NULL, 'uploads/products/meta/hxODovSaDk2gZCZQVL9sns6NNegzsMD8gNBhsyzO.jpeg', NULL, 'Fresh-Lpg-Gas-W8VEX', 0.00, '2020-06-17 23:45:44', '2020-06-17 23:45:44'),
(133, 'Jamuna Lpg Gas', 'admin', 1, 34, 77, 96, 87, '[\"uploads\\/products\\/photos\\/HgryVZYyyJQk70cNVCzW0nDDZhgC4Wk3AOURnJHD.jpeg\"]', 'uploads/products/thumbnail/VftfQisSNRfcMavIdJJKSfMvrZlyTzeDTLq1s17S.jpeg', 'uploads/products/featured/sdKqgAzPFDq09ktDMy0nEQwyBhZocTz7yxBfsdtt.jpeg', 'uploads/products/flash_deal/MYSfrbf0nEqKfeuxgiR9HArQlPSiJO53z7WXi4U2.jpeg', 'youtube', NULL, 'Jamuna Lpg Gas', NULL, 1125.00, 1125.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09af\\u09ae\\u09c1\\u09a8\\u09be \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1125\",\"sku\":\"JLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Jamuna Lpg Gas', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'যমুনা এলপিজি গ্যাস Jamuna Lpg Gas', NULL, 'uploads/products/meta/iaeURNaQuwglfRb9CRtQy4k1ybn2VLezlJ5z6z7I.jpeg', NULL, 'Jamuna-Lpg-Gas-4qb9P', 0.00, '2020-06-17 23:48:17', '2020-06-17 23:48:17'),
(134, 'Bm Lpg Gas', 'admin', 1, 34, 77, 96, 88, '[\"uploads\\/products\\/photos\\/rC1O7WXaUB6FCoHfZmKoFxEBEx265uZFJKs251n4.jpeg\"]', 'uploads/products/thumbnail/ofk4941LiZegSa4afRAo9LZPa8xXDU6v3I7PAnVs.jpeg', 'uploads/products/featured/Uz2LRlCLUHjiIhzJjwrmc2z6Y54AV4rbJuDVs4uD.jpeg', 'uploads/products/flash_deal/wIpsKwHLPCKUanl4reJiF2GXuXpVCnKw5s0hySq1.jpeg', 'youtube', NULL, 'Bm Lpg Gas', NULL, 1120.00, 1120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09bf\\u098f\\u09ae \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1120\",\"sku\":\"BLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বিএম এলপিজি গ্যাস Bm Lpg Gas', NULL, 'uploads/products/meta/ENaNJIm3z9ZIfzDNDERuNpIdmEDlDo0mToHZK0e0.jpeg', NULL, 'Bm-Lpg-Gas-Cd9Eb', 0.00, '2020-06-17 23:50:59', '2020-06-17 23:50:59'),
(135, 'Beximco Lpg Gas', 'admin', 1, 34, 77, 96, 89, '[\"uploads\\/products\\/photos\\/qW5LYLhYLjoGqgXPwi9ARYIvbDASMGXvu8a092VS.png\"]', 'uploads/products/thumbnail/t7nbrvFaQuQY9yK6QUh622gXMzq8PEVJwlkAId5H.png', 'uploads/products/featured/3609tR2LGNVPgVeqd5DUdlxdFnsJ7D9vadahYvl7.png', 'uploads/products/flash_deal/5KAFTPaLVaJAINiaVwkwlsdAAts8VzmQv6mAznf0.png', 'youtube', NULL, 'Beximco Lpg Gas', NULL, 1125.00, 1125.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09c7\\u0995\\u09cd\\u09b8\\u09bf\\u09ae\\u0995\\u09cb \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1125\",\"sku\":\"BLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বেক্সিমকো এলপিজি গ্যাস Beximco Lpg Gas', 'Order Now  ?01842210999', 'uploads/products/meta/isAdNdMmE9t7uvQXJkIyJJwYvK5LeZYLjR7dkGsx.png', NULL, 'Beximco-Lpg-Gas-iGia4', 0.00, '2020-06-17 23:55:34', '2020-06-20 03:48:08'),
(136, 'Total Lpg Gas', 'admin', 1, 34, 77, 96, 90, '[\"uploads\\/products\\/photos\\/PtmhX4x6Z9bbFPhfElSvVni6ftnWKRLJTO0DGFij.png\"]', 'uploads/products/thumbnail/JjIVQKenLiOiTMOhxqsLa4IGpdjyBdwcTgfU4bG5.png', 'uploads/products/featured/RVeGBpuMLtFPehiSCB8NBEBKp0L9GFjF1q71Kfsf.png', 'uploads/products/flash_deal/obpBErdGZ2rpEy4tjszrNObRzAPiNnw0kMWPKQKt.png', 'youtube', NULL, 'Total Lpg Gas', NULL, 1200.00, 1200.00, '[{\"name\":\"choice_0\",\"title\":\"\\u099f\\u09cb\\u099f\\u09be\\u09b2 \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1200\",\"sku\":\"TLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":99}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'টোটাল এলপিজি গ্যাস Total Lpg Gas', NULL, 'uploads/products/meta/JCYu64y4WsmaNQ6Aeyj5pm26LGrbNO6SOrozOaL0.png', NULL, 'Total-Lpg-Gas-iHXFl', 0.00, '2020-06-18 00:03:15', '2021-06-08 19:22:50'),
(137, 'Laugfs Lpg Gas', 'admin', 1, 34, 77, 96, 91, '[\"uploads\\/products\\/photos\\/yw7zKKv1wgjeXUv3B5ZAgkFIWgSDWMN0k31oKHDZ.png\"]', 'uploads/products/thumbnail/AldTm1K8PAMMQ0YOZnxOrbtrYyWsQ5Q0RdzxgaCz.png', 'uploads/products/featured/EkJdmt4UZG7w8VHuOwf95V9xGHVz9U4N9DyhIZkZ.png', 'uploads/products/flash_deal/jkM8wtF2828kkFc9dp7gCMW1hyoD3jocSvAX66s5.png', 'youtube', NULL, 'Laugfs Lpg Gas', NULL, 1120.00, 1120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u09ab\\u09cd\\u099c \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1120\",\"sku\":\"LLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লাফ্জ এলপিজি গ্যাস Laugfs Lpg Gas', NULL, 'uploads/products/meta/SU0nXb6CA3FvJlnaaKgiRUIiVNUIdoK6lWjeaDQe.png', NULL, 'Laugfs-Lpg-Gas-6MQfK', 0.00, '2020-06-18 00:06:04', '2020-06-18 00:06:04'),
(138, 'Navana Lpg Gas', 'admin', 1, 34, 77, 96, 92, '[\"uploads\\/products\\/photos\\/xO6qba6Hb3vZ4zXByq7YY2j6Fzw0TtPpWHXru7KT.png\"]', 'uploads/products/thumbnail/cnJXbkrlqePQwnEYmJmF4INBoT7XItFoVpTPr1pe.png', 'uploads/products/featured/GPSyxOSIzFk9M0UGLQDhfspDtbbLSDadHj6Ia33C.png', 'uploads/products/flash_deal/n8RQyaagmLBVyHj6W2FBx1Cziim8V7lb1AmA44At.png', 'youtube', NULL, 'Navana Lpg Gas', NULL, 1120.00, 1120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a8\\u09be\\u09ad\\u09be\\u09a8\\u09be \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1120\",\"sku\":\"NLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'নাভানা এলপিজি গ্যাস Navana Lpg Gas', NULL, 'uploads/products/meta/08MKMqNN8DuEP7qebjrjAFAmw4a90ndTVZQ4eX0x.png', NULL, 'Navana-Lpg-Gas-NqQrH', 0.00, '2020-06-18 00:08:37', '2020-06-18 00:08:37'),
(139, 'Sena Lpg Gas', 'admin', 1, 34, 77, 96, 94, '[\"uploads\\/products\\/photos\\/a6oRvy3ilzR7kgqcGC8uPbwREITWh2HgObIhFfYv.png\"]', 'uploads/products/thumbnail/371f315XXcEoeN0FgfjpAhK9AxDaaGbPcw1EtFQP.png', 'uploads/products/featured/3TBH4K0XzSK897t7iB13MlQOjxXk227htGRf1ZGF.png', 'uploads/products/flash_deal/aKQZIfHoO8HJVQ8uznr1iHIDbVayb0jXhypppbWu.png', 'youtube', NULL, 'Sena Lpg Gas', NULL, 1120.00, 1120.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b8\\u09c7\\u09a8\\u09be \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1120\",\"sku\":\"SLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'সেনা এলপিজি গ্যাস Sena Lpg Gas', NULL, 'uploads/products/meta/5rQdorSRq2BzTwuPjw0l5HIimJMzTwBdLoM19bM3.png', 'uploads/products/pdf/OtsM5tyGOYs6dW5ZqKlMiDXrsDuqIUiMTG3ROCrD.png', 'Sena-Lpg-Gas-R31F5', 0.00, '2020-06-18 00:10:59', '2020-06-18 08:34:32'),
(140, 'Omera Lpg Gas', 'admin', 1, 34, 77, 96, 93, '[\"uploads\\/products\\/photos\\/rCbmDunrARufFO0jCjDpZBKzaO4rpxQvBbYFjJ9l.jpeg\"]', 'uploads/products/thumbnail/bw8ebAJG5cc7m91HXCFsXD8wwNQDjfBaJUw0Mkvn.jpeg', 'uploads/products/featured/9cmjr6NBEjVnkylTBtxFGKbxvSrjuJpDCUT1oo7i.jpeg', 'uploads/products/flash_deal/KB8sPDosH1kC1J5BSKBbDEnbYJT4VaDCr6EhMulG.jpeg', 'youtube', NULL, 'Omera Lpg Gas', NULL, 1125.00, 1125.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0993\\u09ae\\u09c7\\u09b0\\u09be \\u098f\\u09b2\\u09aa\\u09bf\\u099c\\u09bf \\u0997\\u09cd\\u09af\\u09be\\u09b8 Omera Lpg Gas\",\"options\":[\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1125\",\"sku\":\"OLG-\\u09e7\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":98}}', 0, 1, 0, 0, '12kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'ওমেরা এলপিজি গ্যাস Omera Lpg Gas', 'Omera Lpg Gas', 'uploads/products/meta/suuYWzvC48ULLV7cdxPUvyAYTkGP5GmxvFQRAP0B.jpeg', NULL, 'Omera-Lpg-Gas-oPhrD', 0.00, '2020-06-18 00:14:58', '2021-05-11 03:01:48'),
(141, 'Pangas', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/XLiAm1dUYZ17vRzolRMs2cVCbqqUFufnf9BzmsL6.jpeg\"]', 'uploads/products/thumbnail/gnWRXMD9UmlAtTh7RTumKAGksTP4txWWfz9CvJsI.jpeg', 'uploads/products/featured/POAlYhe4j6aleJywKVGGQkQpSyCBrEBCrnmO7lb9.jpeg', 'uploads/products/flash_deal/DXHKCfc9l2JGLRMntldNqlT8wBq0fMe9IjXqn5iR.jpeg', 'youtube', NULL, 'Pangas', NULL, 150.00, 375.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u09be\\u0982\\u0997\\u09be\\u09b8 \\u09ae\\u09be\\u099b\",\"options\":[\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e8.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"150\",\"sku\":\"P-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"225\",\"sku\":\"P-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"300\",\"sku\":\"P-\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e8.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"375\",\"sku\":\"P-\\u09e8.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 10.00, 0, 'পাংগাস মাছ Pangas Fish', NULL, 'uploads/products/meta/zcRDJzwHoHMn8U6D4myL7m8rSx15z4rsJt7MTYqt.jpeg', NULL, 'Pangas-27Ydj', 0.00, '2020-06-20 00:53:01', '2020-06-20 00:53:01'),
(142, 'Meni Fish', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/DHWZFlgI8adRNuKkLZ60hzUFuYIjXVLHYGJAuSPw.jpeg\"]', 'uploads/products/thumbnail/F0knDxDVo0SfXKdlLUeIRoCyE5z1N8V8YdANoI0y.jpeg', 'uploads/products/featured/VD0K81RfkWkuH6n8jasKiPDYVdXEYgNmYpl8bxWE.jpeg', 'uploads/products/flash_deal/9v4KA2afQ7ndSIl49epLP9d4nOUQ9MNMe958AZEI.jpeg', 'youtube', NULL, 'Meni Fish', NULL, 500.00, 1000.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ae\\u09c7\\u09a8\\u09bf \\u09ae\\u09be\\u099b\",\"options\":[\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"500\",\"sku\":\"MF-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":9},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"750\",\"sku\":\"MF-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"10\"},\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"1000\",\"sku\":\"MF-\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"10\"}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 1, 'মেনি মাছ Meni Fish', NULL, 'uploads/products/meta/zaqfFgdtvxF5NOTVOnHguWQpJXQDV2igqdDSJOMB.jpeg', NULL, 'Meni-Fish-PrkyN', 0.00, '2020-06-20 00:57:48', '2021-06-03 06:09:49'),
(143, 'Curfew fish', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/gYl5711GrOYyFUsJDDNCbuMuXCbAsYnEnBclyGBB.jpeg\"]', 'uploads/products/thumbnail/19lURlgdx7RVDIMGhUKMfod7ApSiq6NTYBMR5gU5.jpeg', 'uploads/products/featured/6Tj9tFBfEhHQkkCW4J5bYcuMyBPxxBQtf0Yu8TfJ.jpeg', 'uploads/products/flash_deal/iqIf1YPEyxXMGGr4mjYf0adKJidhlSHrHXZdeNuo.jpeg', 'youtube', NULL, 'Curfew fish', NULL, 150.00, 450.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09be\\u09b0\\u09cd\\u09ab\\u09c1 \\u09ae\\u09be\\u099b\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"150\",\"sku\":\"Cf-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":98},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"300\",\"sku\":\"Cf-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"450\",\"sku\":\"Cf-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'Kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'কার্ফু মাছ Curfew fish', NULL, 'uploads/products/meta/vErnyZDXAtuBYHN447WfvXhF9lgqT1Tl3U4wIjqE.jpeg', NULL, 'Curfew-fish-KE4NJ', 0.00, '2020-06-20 01:01:10', '2020-10-27 03:23:03'),
(144, 'Tilapia fish', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/LMDhjFYd2EbsmN7Hiktj5golo7lpQSo7rZnkU4Go.jpeg\"]', 'uploads/products/thumbnail/LToYxoUWhIq81sswvp8A33eaWXgvNJLs3xUXqlbH.jpeg', 'uploads/products/featured/howvVQBhs9GxJei9f21BrQPYbrDB1arC76bYNUSd.jpeg', 'uploads/products/flash_deal/lGsr25w9jrvjoYTNaslWEZlT2IKK4QB302frqcsq.jpeg', 'youtube', NULL, 'Tilapia fish', NULL, 150.00, 300.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a4\\u09c7\\u09b2\\u09be\\u09aa\\u09bf\\u09df\\u09be \\u09ae\\u09be\\u099b\",\"options\":[\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"150\",\"sku\":\"Tf-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"225\",\"sku\":\"Tf-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"300\",\"sku\":\"Tf-\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'তেলাপিয়া মাছ  Tilapia fish', NULL, 'uploads/products/meta/6FDdy7gsX0iQAe5eqHsiEnvxmFAIPiIF5l602Kkn.jpeg', NULL, 'Tilapia-fish-mXTkd', 0.00, '2020-06-20 01:05:07', '2020-06-20 01:05:07'),
(145, 'Green Pepper', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/bafuDwieidKDwiABnlvVt1en8CFYlWzlnrqcsFF4.jpeg\"]', 'uploads/products/thumbnail/M2JdvEStyceYgCwsU2xYO2HfwR5tQzkxtugpXwud.jpeg', 'uploads/products/featured/68AZnD6FTA3heRztiGNuwaBm7NtrySqMTJcJhde4.jpeg', 'uploads/products/flash_deal/sJzEEXQHOIYi6QHIqQFauMXsUespuDrT6QFmIJ3r.jpeg', 'youtube', NULL, 'Green Pepper', NULL, 16.00, 80.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09be\\u0981\\u099a\\u09be \\u09ae\\u09b0\\u09bf\\u099a\",\"options\":[\"\\u09e8\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e8\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"16\",\"sku\":\"GP-\\u09e8\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"40\",\"sku\":\"GP-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"80\",\"sku\":\"GP-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"}}', 0, 1, 0, 0, 'gm/kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'কাঁচা মরিচ Green Pepper', NULL, 'uploads/products/meta/r8qCW9nU8LSxbn8uLV9BpmemhS0yyGlzPZCcWjKd.jpeg', NULL, 'Green-Pepper-UZVkd', 0.00, '2020-06-20 01:13:53', '2020-07-31 09:21:29'),
(146, 'Coriander', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/o4LwvIazq1Tg9FDdB6OK6WlXKRGp8vOGZd5bJv0y.jpeg\"]', 'uploads/products/thumbnail/7qZLTuU9P9REUvHPvOf7kndVRGLTNs7KpQP5J1Sj.jpeg', 'uploads/products/featured/P1OG3sSA9nmRWFqRw7ysjavsxzZTusDqNICVCPLc.jpeg', 'uploads/products/flash_deal/inolwEMG8Xf2zzZubXstpmyDv67KMEYoRc1FMiFx.jpeg', 'youtube', NULL, 'Coriander', NULL, 0.00, 0.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09a7\\u09a8\\u09c7\\u09aa\\u09be\\u09a4\\u09be\",\"options\":[\"\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e8\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\"]}]', '[]', '{\"\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"C-\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"C-\\u09e7\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e7\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"C-\\u09e7\\u09eb\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e8\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"C-\\u09e8\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"}}', 0, 1, 0, 0, 'gm/kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'ধনেপাতা Coriander', NULL, 'uploads/products/meta/SM7uRiaK3kzFpHm7O53flaYWCYBQFqf9W06QyW6r.jpeg', NULL, 'Coriander-kV3GS', 0.00, '2020-06-20 01:19:17', '2020-07-31 09:20:59'),
(147, 'Red Spinach', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/S3iICi4HBP3qaK7BqWpvBqbkAdTa2o6KDR4PJF48.jpeg\"]', 'uploads/products/thumbnail/MnoZP5DPoFKPumcbOG6CpJecXczs4MBIoRc8zwO1.jpeg', 'uploads/products/featured/MhoH9HRyS9qMl6PIwiHwbRchisdVsJZ8ulnv4AKr.jpeg', 'uploads/products/flash_deal/ebvuThzIJUtfIBh3eVEvxEieTJWzBzjOR6Afg4ZO.jpeg', 'youtube', NULL, 'Red Spinach', NULL, 0.00, 0.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09be\\u09b2 \\u09b6\\u09be\\u0995\",\"options\":[\"\\u09e9\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09ed\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09e9\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"RS-\\u09e9\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"RS-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09ed\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"RS-\\u09ed\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"0\",\"sku\":\"RS-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"}}', 0, 1, 0, 0, 'gm', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'লাল শাক Red Spinach', NULL, 'uploads/products/meta/YuiqQ3hJxoh11FrkRRBbGzHg84fQNjzM4WcM9tNB.jpeg', NULL, 'Red-Spinach-akpeq', 0.00, '2020-06-20 01:22:57', '2020-07-31 09:20:34'),
(148, 'Lemon', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/oouudbawg4i4aPBWvTMceroyvh7r2MM1nEadBAuR.jpeg\"]', 'uploads/products/thumbnail/UFSoFGVM6hQPNKnWlZ1O7ILULBVli1aJoAaC4Zej.jpeg', 'uploads/products/featured/MLXVA2cYiowHhKUEuP64m1CKPqyuxFi10JD1O4L3.jpeg', 'uploads/products/flash_deal/TaXQIGlwaiF2hYnTtZ7Kh1lwWj6izShjB3XBq5XH.jpeg', 'youtube', NULL, 'Lemon', NULL, 10.00, 80.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b2\\u09c7\\u09ac\\u09c1\",\"options\":[\"\\u09e7\\u09aa\\u09bf\\u099b\",\"\\u09e8\\u09aa\\u09bf\\u099b\",\"\\u09e7\\u09b9\\u09be\\u09b2\\u09bf\",\"\\u09e8\\u09b9\\u09be\\u09b2\\u09bf\"]}]', '[]', '{\"\\u09e7\\u09aa\\u09bf\\u099b\":{\"price\":\"10\",\"sku\":\"L-\\u09e7\\u09aa\\u09bf\\u099b\",\"qty\":99},\"\\u09e8\\u09aa\\u09bf\\u099b\":{\"price\":\"20\",\"sku\":\"L-\\u09e8\\u09aa\\u09bf\\u099b\",\"qty\":\"100\"},\"\\u09e7\\u09b9\\u09be\\u09b2\\u09bf\":{\"price\":\"40\",\"sku\":\"L-\\u09e7\\u09b9\\u09be\\u09b2\\u09bf\",\"qty\":\"100\"},\"\\u09e8\\u09b9\\u09be\\u09b2\\u09bf\":{\"price\":\"80\",\"sku\":\"L-\\u09e8\\u09b9\\u09be\\u09b2\\u09bf\",\"qty\":99}}', 0, 1, 0, 0, 'pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 2, 'লেবু Lemon', NULL, 'uploads/products/meta/yjZq8yy1GFrjInVnrdaOmDj8qj5dYPnvaoVBm9fm.jpeg', NULL, 'Lemon-I1mv2', 0.00, '2020-06-20 01:27:01', '2020-07-01 22:57:00'),
(149, 'Cabbage', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/34mm8eX7gVnl1VG5oKVqBvRABFxp5rtTLF2G7UNu.jpeg\"]', 'uploads/products/thumbnail/uZ1TogvpCVRORr7liR8rJBPQdcKl6FIuxSUsfF4z.jpeg', 'uploads/products/featured/FCrBPjRvsWgJHL2Wub4tollIE93nhuT8d0TwTPs8.jpeg', 'uploads/products/flash_deal/ygG2uhgyv35GWnC3QhV151sER3dWo8XkRXWaLBLK.jpeg', 'youtube', NULL, 'Cabbage', NULL, 0.00, 0.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09ac\\u09be\\u0981\\u09a7\\u09be\\u0995\\u09aa\\u09bf\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"C-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"0\",\"sku\":\"C-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"0\",\"sku\":\"C-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"},\"\\u09e8\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"0\",\"sku\":\"C-\\u09e8\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'বাঁধাকপি  Cabbage', NULL, 'uploads/products/meta/QKB1mEZxhrs3917zdJiuUxUjcBPjIlqLKuqnpjQF.jpeg', NULL, 'Cabbage-fyHfD', 0.00, '2020-06-20 01:30:34', '2020-07-31 09:20:11'),
(150, 'Beans', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/DFrbWMTfEF89vry8nXzkUFg1ayztWuU2htJXKxvh.jpeg\"]', 'uploads/products/thumbnail/mxQ8rFmuthSMo5slZPU0wsvXvgcmQlcX8RbTmgks.jpeg', 'uploads/products/featured/x4sJR3cP7OVrRIrMnGioRpEKimINun4fkrPW47FA.jpeg', 'uploads/products/flash_deal/JlPoUHD8owFtkdLZTfCJcP66tam0o6e5s8v8jdIJ.jpeg', 'youtube', NULL, 'Beans', NULL, 0.00, 0.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09b6\\u09bf\\u09ae\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"0\",\"sku\":\"B-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"0\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"0\",\"sku\":\"B-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"0\",\"sku\":\"B-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'শিম Beans', NULL, 'uploads/products/meta/028n4gl4v5mKrxTZx39PerBGcFZz3T5bttWKWo1u.jpeg', NULL, 'Beans-vzY7F', 0.00, '2020-06-20 01:35:16', '2020-07-31 09:19:20'),
(151, 'Kachu face', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/5p5XU76B7yCHkA65myrYKyQgl9MSEs2xRtqWwmxG.jpeg\"]', 'uploads/products/thumbnail/T33N2dfXIhJ9dTxGMCTrAVToOyoA13Q2yBcTqNvr.jpeg', 'uploads/products/featured/Jb6VF7FNM1JYyYsGXSGO8i4HEbQ2pmQrGnhZNAcD.jpeg', 'uploads/products/flash_deal/f1DVnNL2J5m0qSlqkLWi1XrgTXdyGN0WW3GPTdU5.jpeg', 'youtube', NULL, 'Kachu face', NULL, 30.00, 60.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u099a\\u09c1\\u09b0 \\u09ae\\u09c1\\u0996\\u09c0\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"30\",\"sku\":\"Kf-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"60\",\"sku\":\"Kf-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'কচুর মুখী', NULL, 'uploads/products/meta/lxarlsx4C7Vb4XRvGcL5NTO1n4NKDt7oNv6mGhUJ.jpeg', NULL, 'Kachu-face-tEFVg', 0.00, '2020-06-20 01:43:07', '2020-07-31 09:18:59');
INSERT INTO `products` (`id`, `name`, `added_by`, `user_id`, `category_id`, `subcategory_id`, `subsubcategory_id`, `brand_id`, `photos`, `thumbnail_img`, `featured_img`, `flash_deal_img`, `video_provider`, `video_link`, `tags`, `description`, `unit_price`, `purchase_price`, `choice_options`, `colors`, `variations`, `todays_deal`, `published`, `featured`, `current_stock`, `unit`, `discount`, `discount_type`, `tax`, `tax_type`, `shipping_type`, `shipping_cost`, `num_of_sale`, `meta_title`, `meta_description`, `meta_img`, `pdf`, `slug`, `rating`, `created_at`, `updated_at`) VALUES
(152, 'Karala', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/6PQn9X2ANrvSRDWZCr4bELJc84dGT19zGReHGVol.jpeg\"]', 'uploads/products/thumbnail/yfr206oNzCYIcx6JRpBbuXxhVce9fD9XWFjFrquA.jpeg', 'uploads/products/featured/7CxzInlB8S1vta7agIiVkyFl6IagDqHwA1U4Db5j.jpeg', 'uploads/products/flash_deal/eDaKcd0YmM2Fs4Q4O0ukOo3UT2HIAC0pAPHqGrDO.jpeg', 'youtube', NULL, 'Karala', NULL, 35.00, 105.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09b0\\u09b2\\u09be\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"35\",\"sku\":\"K-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":\"100\"},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"70\",\"sku\":\"K-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"105\",\"sku\":\"K-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"100\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'করলা', NULL, 'uploads/products/meta/FdxuKNG03rEhDcZ26baxmpPucUgCT3kuft9IdmhF.jpeg', NULL, 'Karala-9jRyk', 0.00, '2020-06-20 01:46:50', '2020-06-20 01:46:50'),
(153, 'Patal', 'admin', 1, 32, 56, 94, 48, '[\"uploads\\/products\\/photos\\/ULTasrYS6CKkIYqpZyfOfeCnYMIEVNBIKvYQd8aW.jpeg\"]', 'uploads/products/thumbnail/JCVHyhqK4EPfkb03dG4JkjvkVwaOiAYcoeN2eoEc.jpeg', 'uploads/products/featured/XyJQpvuUaKNbUAGF1KLrlDMtinuXS41J6pjcCBle.jpeg', 'uploads/products/flash_deal/s1oPlMyCWVBAihEvOAZ1peW0INAiRh5jTXyhroup.jpeg', 'youtube', NULL, 'Patal', NULL, 25.00, 75.00, '[{\"name\":\"choice_0\",\"title\":\"\\u09aa\\u099f\\u09b2\",\"options\":[\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\":{\"price\":\"25\",\"sku\":\"P-\\u09eb\\u09e6\\u09e6\\u0997\\u09cd\\u09b0\\u09be\\u09ae\",\"qty\":98},\"\\u09e7\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"50\",\"sku\":\"P-\\u09e7\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":99},\"\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\":{\"price\":\"75\",\"sku\":\"P-\\u09e7.\\u09eb\\u0995\\u09c7\\u099c\\u09bf\",\"qty\":\"0\"}}', 0, 1, 0, 0, 'kg', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 4, 'পটল', NULL, 'uploads/products/meta/AUtwBIk4Ez3Y8ZbSW0rkLdLXG4o7pfLtuNpFYRhz.jpeg', NULL, 'Patal-tgwYN', 0.00, '2020-06-20 01:48:45', '2021-06-07 14:26:21'),
(154, 'Quail eggs', 'admin', 1, 30, 66, 92, 48, '[\"uploads\\/products\\/photos\\/LLAR8xL2DBXYevF9ZMh1p0GyJqcrOTftaQKhxBy3.jpeg\"]', 'uploads/products/thumbnail/DyCByQdTZ7tj4lHMKXx66HvrMMmXrd76bGnYSveQ.jpeg', 'uploads/products/featured/lBnUxuxOScuirawdhxsHaZXGsabJ7PHAi8MgGt5o.jpeg', 'uploads/products/flash_deal/kEva7mVztHK0zlT60dBCm1VARYqUMvDFg5hFa98s.jpeg', 'youtube', NULL, 'Quail eggs', NULL, 125.00, 240.00, '[{\"name\":\"choice_0\",\"title\":\"\\u0995\\u09cb\\u09df\\u09c7\\u09b2\\u09c7\\u09b0 \\u09a1\\u09bf\\u09ae\",\"options\":[\"\\u09eb\\u09e6\\u09aa\\u09bf\\u099b\",\"\\u09e7\\u09e6\\u09e6\\u09aa\\u09bf\\u099b\"]}]', '[]', '{\"\\u09eb\\u09e6\\u09aa\\u09bf\\u099b\":{\"price\":\"125\",\"sku\":\"Qe-\\u09eb\\u09e6\\u09aa\\u09bf\\u099b\",\"qty\":\"1000\"},\"\\u09e7\\u09e6\\u09e6\\u09aa\\u09bf\\u099b\":{\"price\":\"240\",\"sku\":\"Qe-\\u09e7\\u09e6\\u09e6\\u09aa\\u09bf\\u099b\",\"qty\":\"1000\"}}', 0, 1, 0, 0, '50pic/100pic', 0.00, 'amount', 0.00, 'amount', 'flat_rate', 0.00, 0, 'কোয়েলের ডিম Quail eggs', NULL, 'uploads/products/meta/iU2e4R4xq34oJvGSAIZ1vzvWSptAiYjK2TK1CV8a.jpeg', NULL, 'Quail-eggs-mg8PU', 0.00, '2020-06-20 03:08:32', '2020-06-20 03:08:32');

-- --------------------------------------------------------

--
-- Table structure for table `product_stocks`
--

DROP TABLE IF EXISTS `product_stocks`;
CREATE TABLE IF NOT EXISTS `product_stocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `stocks` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `comment` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Manager', '[\"1\",\"2\",\"4\"]', '2018-10-10 04:39:47', '2018-10-10 04:51:37'),
(2, 'Accountant', '[\"2\",\"3\"]', '2018-10-10 04:52:09', '2018-10-10 04:52:09'),
(3, 'Pick up point 001', '[\"14\"]', '2020-07-15 21:58:16', '2020-07-15 21:58:16');

-- --------------------------------------------------------

--
-- Table structure for table `searches`
--

DROP TABLE IF EXISTS `searches`;
CREATE TABLE IF NOT EXISTS `searches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `query` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `searches`
--

INSERT INTO `searches` (`id`, `query`, `count`, `created_at`, `updated_at`) VALUES
(1, 'Watermelon', 76, '2020-05-11 05:27:44', '2020-09-25 03:42:49'),
(2, 'hair Trimmer', 88, '2020-05-11 08:11:18', '2020-09-24 03:54:49'),
(3, 'Trimmer', 97, '2020-05-11 08:11:26', '2020-09-26 04:09:15'),
(4, 'Coffee', 109, '2020-05-11 11:17:16', '2020-09-26 16:14:38'),
(5, 'beef', 119, '2020-05-11 21:34:54', '2020-09-24 07:22:07'),
(6, 'Half hand gloves', 1, '2020-05-14 03:46:50', '2020-05-14 03:46:50'),
(7, 'Gloves', 3, '2020-05-14 03:47:02', '2020-06-18 08:06:37'),
(8, 'mutton', 1, '2020-05-14 17:44:31', '2020-05-14 17:44:31'),
(9, 'akher chini', 1, '2020-05-15 16:02:09', '2020-05-15 16:02:09'),
(10, 'Fish', 6, '2020-05-15 16:14:29', '2020-08-04 01:13:40'),
(11, 'Cheese', 3, '2020-05-16 06:16:16', '2020-08-14 05:36:07'),
(12, 'oil', 6, '2020-05-16 11:19:39', '2020-08-13 06:22:10'),
(13, 'Book', 1, '2020-05-16 17:01:49', '2020-05-16 17:01:49'),
(14, 'Batter', 1, '2020-05-18 10:22:19', '2020-05-18 10:22:19'),
(15, 'Diaper', 1, '2020-05-19 09:02:55', '2020-05-19 09:02:55'),
(16, 'Water', 1, '2020-05-20 12:52:12', '2020-05-20 12:52:12'),
(17, 'Mixed herb', 1, '2020-05-20 14:28:59', '2020-05-20 14:28:59'),
(18, 'Mango', 3, '2020-05-26 08:13:05', '2020-06-12 11:42:40'),
(19, 'garbage', 1, '2020-05-27 04:57:10', '2020-05-27 04:57:10'),
(20, 'trash', 2, '2020-05-27 09:46:32', '2020-05-27 09:46:43'),
(21, 'egg', 25, '2020-05-28 09:26:37', '2020-09-15 06:11:49'),
(22, 'eggs', 2, '2020-05-28 09:27:50', '2020-08-11 09:36:01'),
(23, 'Mop', 3, '2020-05-29 11:23:53', '2020-06-07 07:36:38'),
(24, 'Cleaning', 1, '2020-05-29 11:24:05', '2020-05-29 11:24:05'),
(25, 'savlon an', 1, '2020-06-01 02:45:41', '2020-06-01 02:45:41'),
(26, 'savlon', 23, '2020-06-01 02:47:56', '2020-07-18 10:06:38'),
(27, 'Tresemme', 1, '2020-06-01 09:46:22', '2020-06-01 09:46:22'),
(28, 'Keratin', 1, '2020-06-01 09:46:37', '2020-06-01 09:46:37'),
(29, 'soyabean', 1, '2020-06-01 23:04:33', '2020-06-01 23:04:33'),
(30, 'Mamypoko', 1, '2020-06-02 07:44:15', '2020-06-02 07:44:15'),
(31, 'anticipated', 1, '2020-06-03 09:08:41', '2020-06-03 09:08:41'),
(32, 'Hand Sanitizer', 7, '2020-06-03 09:10:00', '2020-08-11 02:37:39'),
(33, 'hand senitizer', 2, '2020-06-03 09:12:03', '2020-06-03 09:12:16'),
(34, 'Finis', 1, '2020-06-04 12:29:11', '2020-06-04 12:29:11'),
(35, 'Dim', 3, '2020-06-04 22:49:08', '2020-08-12 00:37:06'),
(36, 'Paper', 1, '2020-06-05 00:51:32', '2020-06-05 00:51:32'),
(37, 'whip', 1, '2020-06-05 04:48:25', '2020-06-05 04:48:25'),
(38, 'Tea', 11, '2020-06-05 07:09:23', '2020-09-13 00:32:16'),
(39, 'Basmoti', 4, '2020-06-05 09:32:57', '2020-06-05 09:33:38'),
(40, 'thai soup', 1, '2020-06-06 00:08:16', '2020-06-06 00:08:16'),
(41, 'soup', 1, '2020-06-06 00:08:21', '2020-06-06 00:08:21'),
(42, 'Lobn', 1, '2020-06-07 20:25:40', '2020-06-07 20:25:40'),
(43, 'Fol', 1, '2020-06-07 20:26:06', '2020-06-07 20:26:06'),
(44, 'Chicken egg', 1, '2020-06-08 22:04:35', '2020-06-08 22:04:35'),
(45, 'গুড়', 2, '2020-06-09 08:49:18', '2020-06-09 08:49:29'),
(46, 'Dettol antiseptic', 4, '2020-06-11 10:48:59', '2020-06-11 10:53:41'),
(47, 'Antiseptic', 2, '2020-06-11 10:50:01', '2020-06-11 10:50:05'),
(48, 'Savlon antiseptic', 1, '2020-06-11 10:54:55', '2020-06-11 10:54:55'),
(49, 'Parsly', 1, '2020-06-12 06:04:58', '2020-06-12 06:04:58'),
(50, 'ডিম', 21, '2020-06-12 08:04:42', '2020-08-20 15:09:17'),
(51, 'Alattrol', 1, '2020-06-12 11:13:04', '2020-06-12 11:13:04'),
(52, 'handwash', 1, '2020-06-14 09:33:27', '2020-06-14 09:33:27'),
(53, 'hand wash', 1, '2020-06-14 09:33:57', '2020-06-14 09:33:57'),
(54, 'Magic masala', 2, '2020-06-14 20:05:50', '2020-06-14 20:06:03'),
(55, 'রং পেন্সিল', 5, '2020-06-15 03:14:46', '2020-06-15 03:18:15'),
(56, 'sliced chesse', 2, '2020-06-15 23:31:43', '2020-06-15 23:31:56'),
(57, 'chesse', 4, '2020-06-15 23:32:01', '2020-08-02 12:20:34'),
(58, 'coffe', 1, '2020-06-15 23:33:50', '2020-06-15 23:33:50'),
(59, 'আনারস', 2, '2020-06-16 01:05:37', '2020-06-16 01:05:38'),
(60, 'ডেটল', 1, '2020-06-16 13:13:21', '2020-06-16 13:13:21'),
(61, 'Malta', 1, '2020-06-18 02:31:22', '2020-06-18 02:31:22'),
(62, 'রান্নার সামগ্রী', 1, '2020-06-18 04:04:57', '2020-06-18 04:04:57'),
(63, 'Coolfiewala', 5, '2020-06-19 12:23:41', '2020-06-19 12:27:29'),
(64, 'তেল', 1, '2020-06-19 23:04:07', '2020-06-19 23:04:07'),
(65, 'Buscuit', 4, '2020-06-21 03:16:51', '2020-06-21 03:17:31'),
(66, 'Biscuits', 2, '2020-06-21 03:23:19', '2020-07-17 04:44:23'),
(67, 'Covid 19 protection', 1, '2020-06-21 03:28:49', '2020-06-21 03:28:49'),
(68, 'Hand Rub', 2, '2020-06-22 01:19:17', '2020-06-22 01:19:20'),
(69, 'Date', 2, '2020-06-22 09:28:53', '2020-08-11 11:24:15'),
(70, 'DIPLOMA', 2, '2020-06-22 23:42:28', '2020-08-04 09:48:20'),
(71, 'Mus', 3, '2020-06-22 23:42:55', '2020-06-22 23:42:55'),
(72, 'Moshur', 2, '2020-06-22 23:43:07', '2020-06-22 23:43:15'),
(73, 'Kn 95 mask', 2, '2020-06-23 07:47:50', '2020-06-23 07:48:35'),
(74, 'Sriracha', 2, '2020-06-23 23:31:44', '2020-06-23 23:33:32'),
(75, 'casew badam', 1, '2020-06-24 13:40:16', '2020-06-24 13:40:16'),
(76, 'casew nut', 1, '2020-06-24 13:40:50', '2020-06-24 13:40:50'),
(77, 'almond', 2, '2020-06-24 13:40:59', '2020-08-20 07:54:01'),
(78, 'Whipped cream spray', 1, '2020-06-26 04:11:46', '2020-06-26 04:11:46'),
(79, 'winco food', 1, '2020-06-26 08:33:37', '2020-06-26 08:33:37'),
(80, 'breaking', 1, '2020-06-26 08:35:23', '2020-06-26 08:35:23'),
(81, 'Chocolate', 1, '2020-06-27 00:43:52', '2020-06-27 00:43:52'),
(82, 'Cildip5', 1, '2020-06-27 02:29:15', '2020-06-27 02:29:15'),
(83, 'Napa extened', 3, '2020-06-27 02:29:48', '2020-06-27 02:29:58'),
(84, 'Dal', 2, '2020-06-27 04:30:47', '2020-07-18 06:06:36'),
(85, 'Roll on', 1, '2020-06-27 07:39:55', '2020-06-27 07:39:55'),
(86, 'Ice_cream', 3, '2020-06-28 08:21:23', '2020-06-28 08:21:33'),
(87, 'pusti-soybean-oil', 1, '2020-06-29 05:14:02', '2020-06-29 05:14:02'),
(88, 'Kodomo', 3, '2020-06-30 03:15:33', '2020-07-12 08:43:59'),
(89, 'Savlon anantiseptic liquid', 2, '2020-06-30 23:14:01', '2020-06-30 23:14:03'),
(90, 'Savlon antiseptic liquid', 2, '2020-06-30 23:15:24', '2020-07-04 03:04:13'),
(91, 'Savlon liquid', 1, '2020-06-30 23:25:08', '2020-06-30 23:25:08'),
(92, 'dettol antiseptic liquid', 1, '2020-07-01 02:18:09', '2020-07-01 02:18:09'),
(93, 'noodles', 4, '2020-07-01 10:36:22', '2020-07-20 11:38:39'),
(94, 'Sepnil', 1, '2020-07-01 10:56:08', '2020-07-01 10:56:08'),
(95, 'Sepnil hand Wash', 1, '2020-07-01 10:56:55', '2020-07-01 10:56:55'),
(96, 'Dettol', 5, '2020-07-01 11:37:35', '2020-07-18 10:07:17'),
(97, 'Noddles', 1, '2020-07-03 01:15:06', '2020-07-03 01:15:06'),
(98, 'Ramen', 3, '2020-07-03 01:16:04', '2020-08-02 12:19:34'),
(99, 'Tengra', 1, '2020-07-03 03:08:59', '2020-07-03 03:08:59'),
(100, 'Chanachur', 3, '2020-07-03 16:55:49', '2020-07-03 17:00:06'),
(101, 'Supari', 1, '2020-07-03 16:56:39', '2020-07-03 16:56:39'),
(102, 'Royal crispy supari', 1, '2020-07-03 16:57:10', '2020-07-03 16:57:10'),
(103, 'Dalmoth', 1, '2020-07-03 16:57:23', '2020-07-03 16:57:23'),
(104, 'everyday', 1, '2020-07-07 08:09:32', '2020-07-07 08:09:32'),
(105, 'Lactogen', 2, '2020-07-08 08:14:09', '2020-08-14 07:18:59'),
(106, 'seylon gold blend tea bags', 1, '2020-07-09 06:32:22', '2020-07-09 06:32:22'),
(107, 'coconut milk', 1, '2020-07-09 07:20:13', '2020-07-09 07:20:13'),
(108, 'Oats krunch', 1, '2020-07-09 09:16:02', '2020-07-09 09:16:02'),
(109, 'Anti hair fall shampoo', 1, '2020-07-09 22:18:06', '2020-07-09 22:18:06'),
(110, 'Head & shoulder', 2, '2020-07-09 22:18:39', '2020-07-09 22:18:41'),
(111, 'Whipped cream', 2, '2020-07-10 08:07:50', '2020-07-10 08:08:16'),
(112, 'Sugar', 1, '2020-07-10 11:42:31', '2020-07-10 11:42:31'),
(113, 'Johnsons', 1, '2020-07-12 08:44:44', '2020-07-12 08:44:44'),
(114, 'Johnsons top to toe', 1, '2020-07-12 08:46:39', '2020-07-12 08:46:39'),
(115, 'Wet wipes', 1, '2020-07-13 05:35:31', '2020-07-13 05:35:31'),
(116, 'Hand sanisitizer', 1, '2020-07-13 06:59:25', '2020-07-13 06:59:25'),
(117, 'Savlon anticeptic', 1, '2020-07-16 08:45:29', '2020-07-16 08:45:29'),
(118, 'Savlon liquid anticeptic', 1, '2020-07-16 08:46:22', '2020-07-16 08:46:22'),
(119, 'face cream', 2, '2020-07-16 10:44:41', '2020-07-16 10:44:42'),
(120, 'To spicy  noodls', 1, '2020-07-16 22:24:14', '2020-07-16 22:24:14'),
(121, 'Toast', 1, '2020-07-17 04:44:06', '2020-07-17 04:44:06'),
(122, 'চিনিগুড়া', 1, '2020-07-17 11:03:59', '2020-07-17 11:03:59'),
(123, 'battery', 1, '2020-07-17 18:13:20', '2020-07-17 18:13:20'),
(124, 'onion', 33, '2020-07-17 18:14:44', '2020-09-18 17:41:08'),
(125, 'Oats', 1, '2020-07-17 22:02:21', '2020-07-17 22:02:21'),
(126, 'Ruchi banana chips', 1, '2020-07-18 02:55:03', '2020-07-18 02:55:03'),
(127, 'Ruchi chips', 2, '2020-07-18 02:55:32', '2020-07-18 02:55:33'),
(128, 'Baby bath', 1, '2020-07-18 04:47:01', '2020-07-18 04:47:01'),
(129, 'Consukun', 1, '2020-07-18 06:35:26', '2020-07-18 06:35:26'),
(130, 'Rin', 1, '2020-07-18 09:22:27', '2020-07-18 09:22:27'),
(131, 'Shutki', 1, '2020-07-18 21:19:01', '2020-07-18 21:19:01'),
(132, 'Monin blue cyrum', 2, '2020-07-19 05:22:20', '2020-07-19 05:22:47'),
(133, 'Olay', 1, '2020-07-19 09:23:59', '2020-07-19 09:23:59'),
(134, 'Mushroom', 1, '2020-07-19 10:40:32', '2020-07-19 10:40:32'),
(135, 'Noodles aci', 1, '2020-07-20 11:37:22', '2020-07-20 11:37:22'),
(136, 'ACI Pure Thai Noodles Hot & Spicy', 1, '2020-07-20 11:45:07', '2020-07-20 11:45:07'),
(137, 'Imported onion', 2, '2020-07-21 07:23:04', '2020-07-26 02:51:39'),
(138, 'ইম্পোর্টেড পিয়াজ', 1, '2020-07-21 07:24:31', '2020-07-21 07:24:31'),
(139, 'Studio x', 2, '2020-07-21 12:25:46', '2020-07-21 12:26:32'),
(140, 'Plastic', 1, '2020-07-23 04:12:55', '2020-07-23 04:12:55'),
(141, 'Plastic drum', 1, '2020-07-23 04:13:51', '2020-07-23 04:13:51'),
(142, 'পিঁয়াজ', 4, '2020-07-25 04:07:23', '2020-08-12 14:20:47'),
(143, 'Pyaj', 1, '2020-07-25 11:14:00', '2020-07-25 11:14:00'),
(144, 'Tissue', 2, '2020-07-26 01:29:54', '2020-07-26 01:30:15'),
(145, 'Diapant', 1, '2020-07-26 23:33:27', '2020-07-26 23:33:27'),
(146, 'Pran tomato', 1, '2020-07-26 23:38:44', '2020-07-26 23:38:44'),
(147, 'Nestle LACTOGEN 3 Follow Up Formula (12 Month+) TIN', 1, '2020-07-27 00:37:57', '2020-07-27 00:37:57'),
(148, 'Nestle LACTOGEN 3 Follow Up', 1, '2020-07-27 00:38:23', '2020-07-27 00:38:23'),
(149, 'Chips', 2, '2020-07-27 12:09:06', '2020-08-20 06:23:03'),
(150, 'Krispy', 1, '2020-07-27 12:09:44', '2020-07-27 12:09:44'),
(151, 'gas', 1, '2020-07-29 23:49:04', '2020-07-29 23:49:04'),
(152, 'Rfl', 2, '2020-07-30 03:29:24', '2020-07-30 03:30:36'),
(153, 'Nestea', 1, '2020-07-30 07:45:18', '2020-07-30 07:45:18'),
(154, 'Ice tea', 1, '2020-07-30 07:45:34', '2020-07-30 07:45:34'),
(155, 'Rice', 6, '2020-07-31 07:49:40', '2020-09-02 11:25:21'),
(156, 'Orange juice', 2, '2020-08-03 12:55:31', '2020-08-03 12:55:33'),
(157, 'Google gift', 2, '2020-08-04 00:02:50', '2020-08-04 00:03:07'),
(158, 'Losectil', 1, '2020-08-04 01:21:27', '2020-08-04 01:21:27'),
(159, 'Speaker', 1, '2020-08-04 09:47:30', '2020-08-04 09:47:30'),
(160, 'Headphone', 1, '2020-08-04 09:47:50', '2020-08-04 09:47:50'),
(161, 'Nido', 1, '2020-08-04 09:48:01', '2020-08-04 09:48:01'),
(162, 'Mutton with bone in', 1, '2020-08-05 13:47:52', '2020-08-05 13:47:52'),
(163, 'Twining', 1, '2020-08-06 13:08:03', '2020-08-06 13:08:03'),
(164, 'Foster', 1, '2020-08-06 17:06:01', '2020-08-06 17:06:01'),
(165, 'Mashroom', 2, '2020-08-06 19:32:47', '2020-08-06 19:33:18'),
(166, 'Burger patty', 2, '2020-08-08 14:32:25', '2020-08-08 14:32:28'),
(167, 'Impoted Onion', 1, '2020-08-09 10:26:20', '2020-08-09 10:26:20'),
(168, 'adult diaper', 1, '2020-08-10 00:30:29', '2020-08-10 00:30:29'),
(169, 'প', 1, '2020-08-10 14:50:28', '2020-08-10 14:50:28'),
(170, 'wallet', 1, '2020-08-11 02:35:30', '2020-08-11 02:35:30'),
(171, 'hexisol', 1, '2020-08-11 02:38:00', '2020-08-11 02:38:00'),
(172, 'ডাব', 1, '2020-08-11 03:24:13', '2020-08-11 03:24:13'),
(173, 'Piyaj', 2, '2020-08-11 07:33:08', '2020-08-11 07:34:31'),
(174, 'Peyaj', 1, '2020-08-11 07:33:26', '2020-08-11 07:33:26'),
(175, 'Choco chip', 1, '2020-08-11 10:54:43', '2020-08-11 10:54:43'),
(176, 'পেঁয়াজ', 8, '2020-08-11 11:06:12', '2020-08-14 14:30:43'),
(177, 'Khehur', 1, '2020-08-11 11:21:57', '2020-08-11 11:21:57'),
(178, 'Khejur', 2, '2020-08-11 11:22:11', '2020-08-11 11:24:00'),
(179, 'পলিথিনব্যাগ', 4, '2020-08-11 12:39:04', '2020-08-11 12:40:47'),
(180, 'পলিথনের বেগ', 2, '2020-08-11 12:44:34', '2020-08-11 12:44:58'),
(181, 'পপলিথিনেরব্যাগ', 1, '2020-08-11 12:55:41', '2020-08-11 12:55:41'),
(182, 'Semai', 1, '2020-08-11 13:57:18', '2020-08-11 13:57:18'),
(183, 'Alu', 1, '2020-08-12 10:38:45', '2020-08-12 10:38:45'),
(184, 'Peyaz', 1, '2020-08-12 18:56:33', '2020-08-12 18:56:33'),
(185, 'Chinthol soap', 1, '2020-08-12 23:40:53', '2020-08-12 23:40:53'),
(186, 'native onion', 2, '2020-08-13 01:09:53', '2020-09-13 04:26:31'),
(187, 'Wallpaper', 1, '2020-08-13 21:08:09', '2020-08-13 21:08:09'),
(188, 'Piuaz', 1, '2020-08-13 23:18:05', '2020-08-13 23:18:05'),
(189, 'Piyaz', 1, '2020-08-13 23:18:33', '2020-08-13 23:18:33'),
(190, 'Mask', 1, '2020-08-13 23:19:50', '2020-08-13 23:19:50'),
(191, 'Women products', 1, '2020-08-14 04:08:51', '2020-08-14 04:08:51'),
(192, 'Chees', 1, '2020-08-14 05:35:55', '2020-08-14 05:35:55'),
(193, 'Onion imported', 2, '2020-08-14 22:47:10', '2020-08-14 22:47:11'),
(194, 'Onions', 3, '2020-08-14 22:48:11', '2020-09-15 06:06:50'),
(195, 'মসলা', 10, '2020-08-15 01:50:14', '2020-08-15 03:05:06'),
(196, 'মলসা', 1, '2020-08-15 01:54:09', '2020-08-15 01:54:09'),
(197, 'খোলা মসলা', 3, '2020-08-15 02:41:41', '2020-08-15 02:42:17'),
(198, 'জিরা', 2, '2020-08-15 02:54:27', '2020-08-15 02:54:45'),
(199, 'Shampoo', 1, '2020-08-15 12:48:09', '2020-08-15 12:48:09'),
(200, 'Kidz daiper', 1, '2020-08-15 22:13:02', '2020-08-15 22:13:02'),
(201, 'Radhuni kabab moshla', 1, '2020-08-15 23:28:13', '2020-08-15 23:28:13'),
(202, 'Coffee mate', 1, '2020-08-15 23:30:08', '2020-08-15 23:30:08'),
(203, 'Chingri', 1, '2020-08-16 22:55:49', '2020-08-16 22:55:49'),
(204, 'potatoes', 4, '2020-08-18 08:16:02', '2020-09-15 00:18:29'),
(205, 'Vegetables', 1, '2020-08-19 22:51:03', '2020-08-19 22:51:03'),
(206, 'Nerf', 1, '2020-08-20 10:32:22', '2020-08-20 10:32:22'),
(207, 'Perfumes', 1, '2020-08-21 12:41:58', '2020-08-21 12:41:58'),
(208, 'Mixed nuts', 2, '2020-08-22 02:20:09', '2020-08-22 02:20:39'),
(209, 'Chocolate bar', 1, '2020-08-22 14:56:27', '2020-08-22 14:56:27'),
(210, 'tetley', 2, '2020-08-22 23:49:58', '2020-08-31 00:30:51'),
(211, 'tata tea', 1, '2020-08-22 23:50:19', '2020-08-22 23:50:19'),
(212, 'offers', 1, '2020-08-23 02:49:34', '2020-08-23 02:49:34'),
(213, 'chal', 3, '2020-08-23 23:37:27', '2020-09-16 13:56:03'),
(214, 'meet', 1, '2020-08-24 08:57:04', '2020-08-24 08:57:04'),
(215, 'meat', 1, '2020-08-24 08:57:08', '2020-08-24 08:57:08'),
(216, 'School bag', 5, '2020-08-26 05:30:17', '2020-08-26 05:31:07'),
(217, 'chiken', 2, '2020-08-26 11:23:56', '2020-08-26 11:24:11'),
(218, 'নারিকেল', 2, '2020-08-26 11:29:50', '2020-08-26 11:30:04'),
(219, 'Capsicum', 1, '2020-08-29 17:48:18', '2020-08-29 17:48:18'),
(220, 'Vegetable', 1, '2020-08-29 17:48:31', '2020-08-29 17:48:31'),
(221, 'Pasta sauce double cheddar', 6, '2020-08-30 06:56:51', '2020-09-01 21:03:50'),
(222, 'amul', 1, '2020-09-01 05:36:37', '2020-09-01 05:36:37'),
(223, 'text', 2, '2020-09-02 04:26:40', '2020-09-02 04:28:39'),
(224, 'hilsa-fish-regular-size', 1, '2020-09-10 06:02:11', '2020-09-10 06:02:11'),
(225, 'meril', 2, '2020-09-10 22:23:53', '2020-09-10 22:24:05'),
(226, 'onlion', 1, '2020-09-13 04:26:18', '2020-09-13 04:26:18'),
(227, 'garlic premium (large)', 1, '2020-09-15 00:20:57', '2020-09-15 00:20:57'),
(228, 'teer-sugarcane-rice', 1, '2020-09-15 00:21:57', '2020-09-15 00:21:57'),
(229, 'quail eggs', 1, '2020-09-15 00:24:38', '2020-09-15 00:24:38'),
(230, 'Milk', 1, '2020-09-15 00:25:33', '2020-09-15 00:25:33'),
(231, 'চাল', 3, '2020-09-16 13:56:17', '2020-09-16 13:56:19'),
(232, 'condom', 1, '2020-09-17 07:18:46', '2020-09-17 07:18:46'),
(233, 'Rupchanda Oil', 2, '2020-09-17 10:15:53', '2020-09-17 10:16:12'),
(234, 'Rupchanda', 1, '2020-09-17 10:16:41', '2020-09-17 10:16:41'),
(235, 'Papaya', 1, '2021-05-11 03:24:00', '2021-05-11 03:24:00');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

DROP TABLE IF EXISTS `sellers`;
CREATE TABLE IF NOT EXISTS `sellers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `verification_status` int(1) NOT NULL DEFAULT '0',
  `verification_info` longtext COLLATE utf8_unicode_ci,
  `cash_on_delivery_status` int(1) NOT NULL DEFAULT '0',
  `sslcommerz_status` int(1) NOT NULL DEFAULT '0',
  `stripe_status` int(1) DEFAULT '0',
  `paypal_status` int(1) NOT NULL DEFAULT '0',
  `paypal_client_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paypal_client_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssl_store_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ssl_password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instamojo_status` int(1) NOT NULL DEFAULT '0',
  `instamojo_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instamojo_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razorpay_status` int(1) NOT NULL DEFAULT '0',
  `razorpay_api_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `razorpay_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paystack_status` int(1) NOT NULL DEFAULT '0',
  `paystack_public_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paystack_secret_key` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voguepay_status` int(1) NOT NULL DEFAULT '0',
  `voguepay_merchand_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `admin_to_pay` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `user_id`, `verification_status`, `verification_info`, `cash_on_delivery_status`, `sslcommerz_status`, `stripe_status`, `paypal_status`, `paypal_client_id`, `paypal_client_secret`, `ssl_store_id`, `ssl_password`, `stripe_key`, `stripe_secret`, `instamojo_status`, `instamojo_api_key`, `instamojo_token`, `razorpay_status`, `razorpay_api_key`, `razorpay_secret`, `paystack_status`, `paystack_public_key`, `paystack_secret_key`, `voguepay_status`, `voguepay_merchand_id`, `admin_to_pay`, `created_at`, `updated_at`) VALUES
(1, 3, 1, '[{\"type\":\"text\",\"label\":\"Name\",\"value\":\"Mr. Seller\"},{\"type\":\"select\",\"label\":\"Marital Status\",\"value\":\"Married\"},{\"type\":\"multi_select\",\"label\":\"Company\",\"value\":\"[\\\"Company\\\"]\"},{\"type\":\"select\",\"label\":\"Gender\",\"value\":\"Male\"},{\"type\":\"file\",\"label\":\"Image\",\"value\":\"uploads\\/verification_form\\/CRWqFifcbKqibNzllBhEyUSkV6m1viknGXMEhtiW.png\"}]', 1, 1, 1, 0, NULL, NULL, 'activ5c3c5dac9254d', 'activ5c3c5dac9254d@ssl', 'pk_test_CqAfBW85ZifDyuEOhGaD4ZbE', 'sk_test_mRRMmV4GnBJ4UT7qeLlDe5F8', 0, NULL, NULL, 0, NULL, NULL, 1, 'pk_test_855c5f39d8f662a5d63fabe25ead64fe21018f15', 'sk_test_1175e92519f88e9c665d0b980f53ff1cfffbbc38', 0, NULL, 78.40, '2018-10-07 04:42:57', '2020-01-26 04:21:11'),
(2, 36, 1, '[{\"type\":\"text\",\"label\":\"Your name\",\"value\":\"IMTIZ RAHMAN\"},{\"type\":\"text\",\"label\":\"Shop name\",\"value\":\"KHAN MEDICAL\"},{\"type\":\"text\",\"label\":\"Email\",\"value\":\"ghorerbazar24hour@gmail.com\"},{\"type\":\"text\",\"label\":\"License No\",\"value\":\"12434\"},{\"type\":\"text\",\"label\":\"Full Address\",\"value\":\"Xhandra bazar,faridgonj,chandpur\"},{\"type\":\"text\",\"label\":\"Phone Number\",\"value\":\"01814137813\"},{\"type\":\"file\",\"label\":\"Tax Papers\",\"value\":\"uploads\\/verification_form\\/QgFdedic6vJzG4sSMr9MRW7C10G17IeUPyVL61Pu.jpeg\"}]', 1, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 20.00, '2020-06-01 23:01:23', '2020-10-27 02:48:25'),
(3, 41, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2020-06-15 00:38:31', '2020-10-01 17:18:10'),
(4, 83, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2020-08-19 08:37:46', '2020-08-19 08:37:46'),
(5, 95, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2020-09-09 17:41:57', '2020-09-09 17:41:57'),
(6, 97, 0, NULL, 0, 0, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, NULL, 0, NULL, 0.00, '2020-09-24 13:50:32', '2020-09-24 13:50:32'),
(7, 100, 1, '[{\"type\":\"text\",\"label\":\"Name\",\"value\":\"Mr. Seller\"},{\"type\":\"select\",\"label\":\"Marital Status\",\"value\":\"Married\"},{\"type\":\"multi_select\",\"label\":\"Company\",\"value\":\"[\\\"Company\\\"]\"},{\"type\":\"select\",\"label\":\"Gender\",\"value\":\"Male\"},{\"type\":\"file\",\"label\":\"Image\",\"value\":\"uploads\\/verification_form\\/CRWqFifcbKqibNzllBhEyUSkV6m1viknGXMEhtiW.png\"}]', 1, 1, 1, 0, NULL, NULL, 'activ5c3c5dac9254d', 'activ5c3c5dac9254d@ssl', 'pk_test_CqAfBW85ZifDyuEOhGaD4ZbE', 'sk_test_mRRMmV4GnBJ4UT7qeLlDe5F8', 0, NULL, NULL, 0, NULL, NULL, 1, 'pk_test_855c5f39d8f662a5d63fabe25ead64fe21018f15', 'sk_test_1175e92519f88e9c665d0b980f53ff1cfffbbc38', 0, NULL, 78.40, '2018-10-07 04:42:57', '2020-01-26 04:21:11');

-- --------------------------------------------------------

--
-- Table structure for table `seller_withdraw_requests`
--

DROP TABLE IF EXISTS `seller_withdraw_requests`;
CREATE TABLE IF NOT EXISTS `seller_withdraw_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `amount` double(8,2) DEFAULT NULL,
  `message` longtext,
  `status` int(1) DEFAULT NULL,
  `viewed` int(1) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seller_withdraw_requests`
--

INSERT INTO `seller_withdraw_requests` (`id`, `user_id`, `amount`, `message`, `status`, `viewed`, `created_at`, `updated_at`) VALUES
(1, 100, 100.00, '01797944174', 0, 0, '2020-10-09 13:05:48', '2020-10-09 13:05:48'),
(2, 7, 10.00, 'fhgfj', 0, 0, '2020-10-12 20:32:49', '2020-10-12 20:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `seo_settings`
--

DROP TABLE IF EXISTS `seo_settings`;
CREATE TABLE IF NOT EXISTS `seo_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `author` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `revisit` int(11) NOT NULL,
  `sitemap_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo_settings`
--

INSERT INTO `seo_settings` (`id`, `keyword`, `author`, `revisit`, `sitemap_link`, `description`, `created_at`, `updated_at`) VALUES
(1, 'online,ghorerbazar ,ghorer,Chandpur ,Bangladesh ,Shopping ,Market,Fish,Vegetables ,Rice,Oil', 'webdevelopmentsbd', 11, 'https://ghorerbazar.com/', 'online shop', '2020-08-28 00:55:20', '2020-08-27 18:55:20');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('ThsIMXrFVy91o9eGBEqZ1itLLKGa3eN3z66AYs1S', NULL, '::1', 'Mozilla/5.0 (Windows NT 6.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36', 'YTo1OntzOjY6Il90b2tlbiI7czo0MDoiY0syOG8zWVczbUxzbGx2YjlWUTRQd1lybUJEUzNMT2pWdHl6c3g2NyI7czo2OiJsb2NhbGUiO3M6MjoiYmQiO3M6OToiX3ByZXZpb3VzIjthOjE6e3M6MzoidXJsIjtzOjM0OiJodHRwOi8vbG9jYWxob3N0L2dob3JlcmJhemFyL2xvZ2luIjt9czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czozOiJ1cmwiO2E6MTp7czo4OiJpbnRlbmRlZCI7czoyMzk6Imh0dHA6Ly9sb2NhbGhvc3QvZ2hvcmVyYmF6YXIvYWRtaW4vb3JkZXJzL2V5SnBkaUk2SWpodlVqZGFZVzFCTTBORFRIbzNjWFJzVUVsY0wyNVJQVDBpTENKMllXeDFaU0k2SWtKQ2MzRmpWVFU1SzA5c05rcEpkWEF3T0ZwS1RuYzlQU0lzSW0xaFl5STZJbU00Wm1RNU1XRTNOREEwTm1ZeE16VXpOR1JpTVRjM016a3dOakV6WkRZelpUVTBNVFZqTm1Vek1XUTFOMlJqTmpkbU1HVXpZVGN5WlRKbU5UUmxORE1pZlE9PS9zaG93Ijt9fQ==', 1623642068);

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

DROP TABLE IF EXISTS `shops`;
CREATE TABLE IF NOT EXISTS `shops` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sliders` longtext COLLATE utf8_unicode_ci,
  `address` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `pick_up_point_id` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`id`, `user_id`, `name`, `logo`, `sliders`, `address`, `facebook`, `google`, `twitter`, `youtube`, `slug`, `meta_title`, `meta_description`, `pick_up_point_id`, `created_at`, `updated_at`) VALUES
(1, 3, 'Demo Seller Shop', 'shop/logo/Gt1xw7vjTpMnwpADkGSilc35qrAfcw02kuZ36Jdn.png', '[\"uploads\\/shop\\/sliders\\/lToeKDeUyWcxy1HRs2yH37oBLyIwEwyPkqdyXBRO.jpeg\",\"uploads\\/shop\\/sliders\\/asDBJ3Bro1ijNaNnx3Hpnp6uq3n66ndyLczOJ0F6.jpeg\",\"uploads\\/shop\\/sliders\\/ltwUfHND4QP1K7bPFbuOC4i8v6zL9KHJKzex4zaX.jpeg\"]', 'House : Demo, Road : Demo, Section : Demo', 'www.facebook.com', 'www.google.com', 'www.twitter.com', 'www.youtube.com', 'Demo-Seller-Shop-1', 'Demo Seller Shop Title', 'Demo description', NULL, '2018-11-27 10:23:13', '2019-08-06 06:43:16'),
(2, 36, 'KHAN MEDICAL', 'uploads/shop/logo/MsumzKIAh0MLbBKj72LOO95CqY2Sgul6P7lV6rnm.jpeg', '[\"uploads\\/shop\\/sliders\\/vsaDhZ8kBijFFCV2iCGx5fdUtp15x4MSoyRGSzd9.jpeg\"]', 'Chandra bazar,faridgonj,chandpur', NULL, NULL, NULL, NULL, 'KHAN-MEDICAL-2', 'KHAN MEDICAL', 'Drug', '[]', '2020-06-01 23:01:23', '2020-06-14 08:18:53'),
(3, 41, 'Online grocery shop', NULL, '[]', 'Uttar,Dhaka,Bangladesh', NULL, NULL, NULL, NULL, 'Online-grocery-shop-3', 'Online grocery shopping and delivery in dhaka.', 'Online grocery shopping and delivery in dhaka। buy fresh food item or personal care and more..', '[]', '2020-06-15 00:38:31', '2020-06-15 00:47:51'),
(4, 83, 'OldStar', NULL, NULL, '100/2/1 Middlepirerbag Mirpur Dhaka Bangladesh', NULL, NULL, NULL, NULL, 'OldStar-', NULL, NULL, NULL, '2020-08-19 08:37:46', '2020-08-19 08:37:46'),
(5, 95, 'Ghjgff', NULL, NULL, 'Hffhjhv', NULL, NULL, NULL, NULL, 'Ghjgff-5', 'Jhvvgggvcc', 'Jvccvv', '[]', '2020-09-09 17:41:57', '2020-09-09 17:42:57'),
(6, 97, 'Rahim bekary', NULL, NULL, 'chanidupur', NULL, NULL, NULL, NULL, 'Rahim-bekary-', NULL, NULL, NULL, '2020-09-24 13:50:32', '2020-09-24 13:50:32');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `published` int(1) NOT NULL DEFAULT '1',
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `photo`, `published`, `link`, `created_at`, `updated_at`) VALUES
(15, 'uploads/sliders/A9RrHujIIxxzsRoxDJX42xf4LXxaKlLiTfNllrZZ.jpeg', 1, '#', '2020-05-16 01:41:04', '2020-05-16 01:41:04'),
(25, 'uploads/sliders/G32A6jpFe5z9yQUnu1BvnovIab9d96HwiUc9DFgm.jpeg', 0, 'https://ghorerbazar.com/product/Lemon-I1mv2', '2020-06-20 02:57:03', '2020-09-03 00:37:43'),
(26, 'uploads/sliders/1LJk0mVj8bZAbyC3ZLEYTZzIQPP9oLLtl2CnC7bY.jpeg', 0, 'https://ghorerbazar.com/product/Kachu-face-tEFVg', '2020-06-20 02:57:44', '2020-09-03 00:37:41'),
(27, 'uploads/sliders/mBPt2oVbdmUZP8F5Ie1XyjOGuhFp9iT1SEVNJ2S3.jpeg', 0, 'https://ghorerbazar.com/product/Karala-9jRyk', '2020-06-20 02:59:07', '2020-09-03 00:37:40'),
(28, 'uploads/sliders/dguBwSiAoU0tokUNexRHpWrWDERWttXYRBEUl3kE.jpeg', 0, 'https://ghorerbazar.com/product/Patal-tgwYN', '2020-06-20 03:00:04', '2020-09-03 00:37:37'),
(29, 'uploads/sliders/HWL9fa7pxOAYt9SzsoCpEZWOhQ1zz7udkPqqV5JB.jpeg', 0, 'https://ghorerbazar.com/product/Beans-vzY7F', '2020-06-20 03:02:35', '2020-09-03 00:37:36'),
(30, 'uploads/sliders/pkwxl2r78Nb9MTLrMqhB1eVUkDFaS0f8e4dbld6d.png', 1, 'https://bhorerkonthosor.ghorerbazar.com/', '2020-08-30 05:24:42', '2020-08-30 05:24:42'),
(35, 'uploads/sliders/TDofFI6iQ72q5v9TxMhacfEcrLEe86sjIzYu2WqM.png', 1, 'https://ghorerbazar.com', '2021-06-03 06:52:33', '2021-06-03 06:52:33');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `user_id`, `role_id`, `created_at`, `updated_at`) VALUES
(1, 53, 3, '2020-07-15 21:57:16', '2020-07-15 21:58:45');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
CREATE TABLE IF NOT EXISTS `subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'mohammedjahangiralamtitu@gmail.com', '2020-06-02 04:22:09', '2020-06-02 04:22:09'),
(2, 'jahidul.krishi@gmail.com', '2020-06-03 09:13:04', '2020-06-03 09:13:04'),
(3, 'ghorerbazar.info@gmail.com', '2020-06-06 08:31:40', '2020-06-06 08:31:40'),
(4, 'mohammadjahangiralamtitu@gmail.com', '2020-06-12 08:03:38', '2020-06-12 08:03:38'),
(5, 'mdjahidh16@gmail.com', '2020-06-15 01:01:46', '2020-06-15 01:01:46'),
(6, 'abmfazlulhaque@yahoo.com', '2020-06-23 07:48:34', '2020-06-23 07:48:34'),
(7, 'taniaishtiaq1988@gmail.com', '2020-07-17 08:10:18', '2020-07-17 08:10:18'),
(8, 'kamrulhassann099@gmail.com', '2020-08-11 11:03:46', '2020-08-11 11:03:46'),
(9, 'rafiaaysha47@gmail.com', '2020-08-13 02:45:33', '2020-08-13 02:45:33'),
(10, 'oldstarbd@gmail.com', '2020-08-19 08:35:01', '2020-08-19 08:35:01'),
(11, 'nasimakumkum@gmail.ccom', '2020-08-19 22:55:23', '2020-08-19 22:55:23'),
(12, 'ij5387007@gmail.com', '2020-08-20 05:13:19', '2020-08-20 05:13:19');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(11) NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_category_id` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `name`, `category_id`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(55, 'ঘরেরবাজার ghorerbazar', 12, '-ghorerbazar', NULL, NULL, '2020-05-12 07:44:25', '2020-05-12 16:03:45'),
(56, 'তাজা শাকসবজি Fresh vegetables', 32, '--Fresh-vegetables-iwnsU', 'তাজা-শাকসবজি-Fresh-vegetables', NULL, '2020-05-13 21:58:29', '2020-05-13 21:58:29'),
(57, 'অফিস ও লাইব্রেরি Office & Livery', 13, '---Office--Livery-326kh', 'অফিস-ও-লাইব্রেরি-Office-Livery', NULL, '2020-05-13 22:00:11', '2020-05-13 22:00:11'),
(58, 'শিশু খাদ্য Baby Food', 18, '--Baby-Food-ytwYc', 'শিশু-খাদ্য-Baby-Food', NULL, '2020-05-13 22:01:31', '2020-05-13 22:01:31'),
(59, 'রান্নাঘরের সামগ্রী Kitchen utensils', 14, '--Kitchen-utensils-RWEG0', 'রান্নাঘরের-সামগ্রী-Kitchen-utensils', NULL, '2020-05-13 22:03:00', '2020-05-13 22:03:00'),
(60, 'গৃহস্থালি জিনিসপত্র Household Items', 15, '--Household-Items-lv9US', 'গৃহস্থালি-জিনিসপত্র-Household-Items', NULL, '2020-05-13 22:04:27', '2020-05-13 22:04:27'),
(61, 'ডায়াবেটিক খাবার Daibetic Food', 16, '--Daibetic-Food-Sxet0', 'ডায়াবেটিক-খাবার-Daibetic-Food', NULL, '2020-05-13 22:05:27', '2020-05-13 22:05:27'),
(62, 'স্বাস্থ বিষয়ক খাবার Health Food', 17, '---Health-Food-fhZTQ', 'স্বাস্থ-বিষয়ক-খাবার-Health-Food', NULL, '2020-05-13 22:07:41', '2020-05-13 22:07:41'),
(63, 'শিশুপণ্য Baby Products', 19, '-Baby-Products-ZgRbe', 'শিশুপণ্য-Baby-Products', NULL, '2020-05-13 22:08:45', '2020-05-13 22:08:45'),
(64, 'পরিষ্কার ও পরিচ্ছন্নতা Clear & Cleanliness', 20, '---Clear--Cleanliness-ftFng', 'পরিষ্কার-ও-পরিচ্ছন্নতা-Clear-&-Cleanliness', NULL, '2020-05-13 22:10:22', '2020-05-13 22:10:22'),
(65, 'ফলমূল Fruits', 31, '-Fruits-q6Qg1', 'ফলমূল-Fruits', NULL, '2020-05-13 22:11:10', '2020-05-13 22:11:10'),
(66, 'মাছ,মাংশ,ডিম,শুঁটকি Fish,Meat,Egg,Dried Fish', 30, '-FishMeatEggDried-Fish-4s5EF', 'মাছ-মাংশ-ডিম-শুঁটকি-Fish-Meat-Egg-Dried-Fish', NULL, '2020-05-13 22:12:54', '2020-05-13 22:12:54'),
(67, 'চাল, আটা, ময়দা, বেসন Rice, Flour, Meal, Gram Flour', 29, '-RiceFlourMealGram-Flour-noRmE', 'চাল-আটা-ময়দা-বেসন-Rice-Flour-Meal-Gram-Flour', NULL, '2020-05-13 22:14:40', '2020-05-13 23:14:28'),
(68, 'তেল, ঘি Oil, Ghee', 28, '--Oil-Ghee-g2HBl', 'তেল-ঘি-Oil-Ghee', NULL, '2020-05-13 22:15:32', '2020-05-13 22:15:32'),
(69, 'লবন, চিনি,সেমাই, সুজি Salt, Sugar, Semai, Semolin', 27, '---Salt-Sugar-Semai-Semolin-dg9Nf', 'লবন-চিনি-সেমাই-সুজি-Salt-Sugar-Semai-Semolin', NULL, '2020-05-13 22:17:52', '2020-05-13 22:17:52'),
(70, 'ডাল Pulses', 25, '-Pulses-n3UMX', 'ডাল-Pulses', NULL, '2020-05-13 22:18:38', '2020-05-13 23:00:52'),
(71, 'দুধ, মধু, দুগ্ধজাত Milk, Honey, Dairy Product', 24, '---Milk-Honey-Dairy-Product-D3koe', 'দুধ-মধু-দুগ্ধজাত-Milk-Honey-Dairy-Product', NULL, '2020-05-13 22:20:26', '2020-05-13 22:20:26'),
(72, 'নাস্তা ও পানীয় Breakfast and Drinks', 23, '---Breakfast-and-Drinks-9cFZW', 'নাস্তা-ও-পানীয়-Breakfast-and-Drinks', NULL, '2020-05-13 22:22:12', '2020-05-13 22:22:12'),
(73, 'সৌন্দর্য ও প্রসাধনী Beauty & cosmetics', 21, '---Beauty--cosmetics-7pfId', 'সৌন্দর্য-ও-প্রসাধনী-Beauty-&-cosmetics', NULL, '2020-05-13 22:25:11', '2020-05-13 22:25:11'),
(74, 'জল খাবার Water Food', 22, '--Water-Food-wCyAD', 'জল-খাবার-Water-Food', NULL, '2020-05-13 22:33:52', '2020-05-13 22:33:52'),
(75, 'মসলা, রেডি মিক্স, আচার Spices, Ready Mix, Pickles', 26, '----Spices-Ready-Mix-Pickles-lqFNA', 'মসলা-রেডি-মিক্স-আচার-Spices-Ready-Mix-Pickles', NULL, '2020-05-13 22:35:36', '2020-05-13 23:03:56'),
(76, 'ঔষধ Drugs', 33, '-Drugs-ocrE5', 'ঔষধ Drugs', NULL, '2020-06-08 02:10:42', '2020-06-08 02:10:42'),
(77, 'এলপিজি গ্যাস LPG GAS', 34, '--LPG-GAS-Ms4Jj', 'এলপিজি গ্যাস LPG GAS', NULL, '2020-06-17 23:00:36', '2020-06-17 23:00:36');

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_categories`
--

DROP TABLE IF EXISTS `sub_sub_categories`;
CREATE TABLE IF NOT EXISTS `sub_sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category_id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `brands` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_sub_category_id` (`sub_category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sub_sub_categories`
--

INSERT INTO `sub_sub_categories` (`id`, `sub_category_id`, `name`, `brands`, `slug`, `meta_title`, `meta_description`, `created_at`, `updated_at`) VALUES
(74, 55, 'ঘরেরবাজার ghorerbazar', '[\"48\"]', '-ghorerbazar', NULL, NULL, '2020-05-12 07:44:43', '2020-05-12 16:05:12'),
(75, 57, 'অফিস ও লাইব্রেরি Office & Livery', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '---Office--Livery-p9I74', 'অফিস-ও-লাইব্রেরি-Office-&-Livery', NULL, '2020-05-13 22:39:10', '2020-05-13 22:39:10'),
(76, 59, 'রান্নাঘরের সামগ্রী Kitchen utensils', '[\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '--Kitchen-utensils-MkvlY', 'রান্নাঘরের-সামগ্রী-Kitchen-utensils', NULL, '2020-05-13 22:41:27', '2020-05-13 22:41:27'),
(77, 60, 'গৃহস্থালি জিনিসপত্র Household Items', '[\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '--Household-Items-eHsvr', 'গৃহস্থালি-জিনিসপত্র-Household-Items', NULL, '2020-05-13 22:43:06', '2020-05-13 22:43:06'),
(78, 61, 'ডায়াবেটিক খাবার Diabatic Food', '[\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '--Diabatic-Food-l44vX', 'ডায়াবেটিক-খাবার-Diabatic-Food', NULL, '2020-05-13 22:44:39', '2020-05-13 22:44:39'),
(79, 62, 'স্বাস্থ বিষয়ক খাবার Health Food', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '---Health-Food-bcA2t', 'স্বাস্থ-বিষয়ক-খাবার-Health-Food', NULL, '2020-05-13 22:46:23', '2020-05-13 22:46:23'),
(80, 58, 'শিশু খাদ্য Baby Food', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"75\"]', '--Baby-Food-x3rhA', 'শিশু-খাদ্য-Baby-Food', NULL, '2020-05-13 22:47:51', '2020-06-02 01:06:17'),
(81, 63, 'শিশুপন্য Baby Products', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\"]', '-Baby-Products-C3jbB', 'শিশুপন্য-Baby-Products', NULL, '2020-05-13 22:49:48', '2020-05-15 00:02:06'),
(82, 64, 'পরিষ্কার ও পরিচ্ছন্নতা Clear & cleanliness', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"76\",\"83\"]', '---Clear--cleanliness-zby7q', 'পরিষ্কার-ও-পরিচ্ছন্নতা-Clear-&-cleanliness', NULL, '2020-05-13 22:51:39', '2020-06-16 23:05:13'),
(83, 73, 'সৌন্দর্য ও প্রসাধনী Beault & cosmetics', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '---Beault--cosmetics-L9vbw', 'সৌন্দর্য-ও-প্রসাধনী-Beault-&-cosmetics', NULL, '2020-05-13 22:53:34', '2020-05-13 22:53:34'),
(84, 74, 'জল খাবার Water Food', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '--Water-Food-edoVM', 'জল-খাবার-Water-Food', NULL, '2020-05-13 22:54:50', '2020-05-13 22:54:50'),
(85, 72, 'নাস্তা ও পানীয় Breakfast and Drinks', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '---Breakfast-and-Drinks-iaIPe', 'নাস্তা-ও-পানীয়-Breakfast-and-Drinks', NULL, '2020-05-13 22:56:38', '2020-05-13 22:56:38'),
(86, 71, 'দুধ, মধু, দুগ্ধজাত Milk, Honey, Dairy Products', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"61\",\"66\",\"72\",\"73\"]', '---Milk-Honey-Dairy-Products-mP226', 'দুধ-মধু-দুগ্ধজাত-Milk-Honey-Dairy-Products', NULL, '2020-05-13 22:58:53', '2020-06-17 00:17:24'),
(87, 70, 'ডাল Pulses', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\"]', '-Pulses-xqe7F', 'ডাল-Pulses', NULL, '2020-05-13 23:02:04', '2020-05-14 23:59:16'),
(88, 75, 'মসলা, রেডি মিক্স, আচার Spice, Ready Mix, Pickles', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"61\",\"67\",\"69\",\"70\",\"71\"]', '----Spice-Ready-Mix-Pickles-112gS', 'মসলা-রেডি-মিক্স-আচার-Spice-Ready-Mix-Pickles', NULL, '2020-05-13 23:06:38', '2020-05-16 00:30:05'),
(89, 69, 'লবন, চিনি, সেমাই, সুজি Salt, Sugar, Semai, Semolin', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"69\",\"70\"]', '----Salt-Sugar-Semai-Semolin-Sg1LC', 'লবন-চিনি-সেমাই-সুজি-Salt-Sugar-Semai-Semolin', NULL, '2020-05-13 23:09:17', '2020-05-16 00:14:09'),
(90, 68, 'তেল, ঘি Oil, Ghee', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"69\",\"72\",\"73\",\"75\"]', '--Oil-Ghee-DPGsj', 'তেল-ঘি-Oil-Ghee', NULL, '2020-05-13 23:10:46', '2020-05-16 02:05:18'),
(91, 67, 'চাল, আটা, ময়দা, বেসন Rice, Flour, Meal, Gram Flour', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"69\"]', '----Rice-Flour-Meal-Gram-Flour-ysgad', 'চাল-আটা-ময়দা-বেসন-Rice-Flour-Meal-Gram-Flour', NULL, '2020-05-13 23:16:44', '2020-05-15 04:15:20'),
(92, 66, 'মাছ, মাংশ, ডিম, শুঁটকি Fish, Meat, Egg, Dried Fish', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '----Fish-Meat-Egg-Dried-Fish-hBr21', 'মাছ-মাংশ-ডিম-শুঁটকি-Fish-Meat-Egg-Dried-Fish', NULL, '2020-05-13 23:19:07', '2020-05-13 23:19:07'),
(93, 65, 'ফলমুল Fruits', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\"]', '-Fruits-p0acB', 'ফলমুল-Fruits', NULL, '2020-05-13 23:20:33', '2020-05-13 23:20:33'),
(94, 56, 'তাজা শাকসবজি Fresh Vegetables', '[\"48\",\"49\",\"50\",\"51\",\"52\",\"53\",\"55\",\"56\",\"57\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\"]', '--Fresh-Vegetables-Gv8Tj', 'তাজা-শাকসবজি-Fresh-Vegetables', NULL, '2020-05-13 23:21:54', '2020-05-14 23:56:19'),
(95, 76, 'ঔষধ Drugs', '[\"51\",\"52\",\"53\",\"77\",\"78\",\"79\",\"80\",\"81\",\"82\"]', '-Drugs-untgR', 'ঔষধ Drugs', NULL, '2020-06-08 02:12:28', '2020-06-08 02:12:28'),
(96, 77, 'এলপিজি গ্যাস LPG GAS', '[\"84\",\"85\",\"86\",\"87\",\"88\",\"89\",\"90\",\"91\",\"92\",\"93\",\"94\"]', '--LPG-GAS-sJDwE', 'এলপিজি গ্যাস LPG GAS', NULL, '2020-06-17 23:03:33', '2020-06-17 23:39:28');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

DROP TABLE IF EXISTS `tickets`;
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` int(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `viewed` int(1) NOT NULL DEFAULT '0',
  `client_viewed` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `code`, `user_id`, `subject`, `details`, `files`, `status`, `viewed`, `client_viewed`, `created_at`, `updated_at`) VALUES
(1, 100000, 28, 'yygghhjj', 'bggffggg', NULL, 'solved', 1, 1, '2020-09-22 13:35:42', '2020-09-22 07:35:42');

-- --------------------------------------------------------

--
-- Table structure for table `ticket_replies`
--

DROP TABLE IF EXISTS `ticket_replies`;
CREATE TABLE IF NOT EXISTS `ticket_replies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reply` longtext COLLATE utf8_unicode_ci NOT NULL,
  `files` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `ticket_replies`
--

INSERT INTO `ticket_replies` (`id`, `ticket_id`, `user_id`, `reply`, `files`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'vvhhhhm', NULL, '2020-09-22 07:34:57', '2020-09-22 07:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `topups`
--

DROP TABLE IF EXISTS `topups`;
CREATE TABLE IF NOT EXISTS `topups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `number` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `provider_id` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'customer',
  `name` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avatar_original` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `balance` double(8,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `provider_id`, `user_type`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `avatar`, `avatar_original`, `address`, `country`, `city`, `postal_code`, `phone`, `balance`, `created_at`, `updated_at`) VALUES
(1, NULL, 'admin', 'Suzon Khan', 'suzonkhan.sk@gmail.com', '2020-05-03 13:05:48', '$2y$10$JZ47ZE86Ln2Vmgg5.M0B4.6wVgvIv6LyadTACr0Dg2V9Ps246iWl2', 'Rt54NLmGhLZP8Odg3WWeWpizwgAExR3FVy8tWjANh6zwEmDWzHwdZGOWpht2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-03 13:19:48', '2020-09-09 22:50:20'),
(9, NULL, 'admin', 'Admin', 'admin@ghorerbazar.com', '2020-05-03 13:05:48', '$2y$10$DkZDy5TV8Q84sAUeMX6Jt.9lJiU7gUinktnaHvGwmYJefq1asDByW', 'ReWrn4nQ4Z4IsTIhQn2j73DicK5KBWq1HGQhDHCsEj18RQtHf9gIzy5Sae9Z', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-03 13:19:48', '2020-06-17 03:27:50'),
(28, NULL, 'customer', 'Sumon', 'ghorerbazar.info@gmail.com', '2020-05-24 04:53:11', '$2y$10$3iuW2gRj/Pu8Oytvbf6MNOlx7MlNXhWJTUip6Wrr/IzFLWKHNoQWW', 'wBTxsE2Fjjy1yPUMhRDOa4rfoPCAiA6yUh1Qo1dxWQnSgCNJN0FBPm2HQ0ca', NULL, 'uploads/users/JeT2ozE9ADm2xsNSpV9jpNJcPHq6SCba3pyjaisJ.png', 'Chandpur', 'BD', 'Chandpur', '3600', '01771162553', 0.00, '2020-05-18 07:27:35', '2020-06-05 05:48:19'),
(30, NULL, 'customer', 'softghor@gmail.com', 'softghor@gmail.com', '2020-05-24 03:34:29', '$2y$10$ool0IcA5uq.LsELDOEEfe.4ZPZMpLtiFM6FB9QNr1lI.4.0F2CSw.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-24 03:30:56', '2020-05-24 03:34:29'),
(31, NULL, 'customer', 'Tania', 'taniaishtiaq1988@gmail.com', '2020-05-24 05:36:21', '$2y$10$A2YMVZMicsKr20AlvJJOs.n19E.4Pqm/XAk57HereKpsw5QUghtxO', 'AU0SJEZBUqEyLRaXxAoPsJ5UFO2GAoRjOCrVch0pmnN7q4jFpY1AGRrXBFEW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-24 05:36:01', '2020-06-23 07:58:59'),
(33, NULL, 'customer', 'Reza', 'rezaahmed2946@gmail.com', '2020-05-24 08:22:21', '$2y$10$F501wVcSlBUjOfSwakmuqeK5A.1xtI6oyxioq283TUFycoQ1.tqvW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-24 08:22:03', '2020-05-24 08:22:21'),
(34, NULL, 'customer', 'Sumon', 'mi.sumon93@gmail.com', '2020-05-24 21:40:57', '$2y$10$8dqreEQ2sI0xkTg6M..RUeSWcejNBE5DTtckVb24qKg3AXPnhhM9y', 'exoGCgcWAlEY6PO9Ex4XsgyC3LZ1Iy3S9uzETgf8M3DEFn0b1IbyiOO4xi3l', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-24 21:40:38', '2020-05-24 21:40:57'),
(35, NULL, 'customer', 'Nazmus shakib', 'nazmusshakib905@gmail.com', '2020-05-24 22:25:56', '$2y$10$YDfYKphy35DSpJWYUTNga.4oyZsiwtW5g4ZGs4Lf6AWhCv.5nFRQG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-05-24 22:24:43', '2020-05-24 22:25:56'),
(36, NULL, 'seller', 'KHAN MEDICAL', 'ghorerbazar24hour@gmail.com', '2020-06-01 23:03:46', '$2y$10$l2uGPFsJLqjLdxJfQDYyGOdnr3GORajCJMw8d3vKFthl85B4GkaNu', NULL, NULL, 'uploads/NUwfpIofBNlSnHmn5RsW0XbcLeJxQwTgiyxcbjn9.jpeg', 'Chandra bazar', 'BD', 'Chandpur', '3600', '01814137813', 0.00, '2020-06-01 23:01:23', '2020-06-14 08:07:51'),
(37, NULL, 'customer', 'Jahangir sir.', 'mohammedjahangiralamtitu@gmail.com', NULL, '$2y$10$YcFaQ2v.EEod41wdOW5HmeRzc7yyGFUdlYbKZFMcuX3sSYuvdobg.', 'cjd09v92nPgJ0MPkaz2hcEysPDq4vTvdLXF5wYr0z6AXwSFIdblz9KYMH0pC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-02 04:19:37', '2020-06-02 04:19:37'),
(38, NULL, 'customer', 'Shahin Akther', 'shahustc@gmail.com', NULL, '$2y$10$71PEwt5Mr9z7SkioIHXFXOVmu6Yo3ioP6c2ZR/cSNwF7Eu62.Zv7S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-04 13:36:39', '2020-06-04 13:36:39'),
(40, NULL, 'customer', 'Md. Rezwan ul Karim', 'rezwanulkarim@yahoo.com', NULL, '$2y$10$GOXFk2cdou/qA86974gfX.WDiHHo/abVc1HESeQUxqItchrb/5cNy', 'COMJ3RjJNNF1rnA43Maf1Tc9C7rAcLFBWQiQfVSUEA3nFMIdjzIOo7LNM2YK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-12 08:01:26', '2020-06-12 08:01:26'),
(41, NULL, 'seller', 'Online grocery shop', 'mdjahidh16@gmail.com', '2020-06-15 01:00:36', '$2y$10$9noeExU28zhy0zDV71ZsZuY4wFB3lpOAxHltRJ5Z9T4HtRIjciFxO', 'UTLDGIJqa8gpI5vOQNo518uptux5XD3DuNx1PaIGyQyGxYrPQ5CEQ6uJRU0Y', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-15 00:38:31', '2020-06-15 01:00:36'),
(42, NULL, 'customer', 'Afsana', 'adritparijat@gmail.com', NULL, '$2y$10$gA6M1gt1B1lXmF4/k0f8.O2lw4W6ZJPUgyfM.B2zifGBImYNxef5a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-15 02:22:47', '2020-06-15 02:22:47'),
(43, NULL, 'customer', 'Polokkhan', 'mindfreakpolok16@gmail.com', NULL, '$2y$10$pgjTOpl/oO3v/gHRbBO2ceLyVFg5v267DNngXcnuePMr/YJhPkFOy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-16 00:39:51', '2020-06-16 00:39:51'),
(44, NULL, 'customer', 'Mashraka', 'mashraka009@gmail.com', NULL, '$2y$10$06Jia5V8msYg2IDt8NRQJ.TWOEI5ByKxhpt74Da0hqIVvXFoWoAPm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-18 16:08:51', '2020-06-18 16:08:51'),
(45, NULL, 'customer', 'Farhana Akhter', 'farhanabiddut2002@gmail.com', '2020-08-20 06:28:28', '$2y$10$bBoPFxtDw8krHdgttsYgy.kaQPf4ZgMe1AQGMZxf7MEJUDcc2dy3S', 'ug7FUEv5HxQ2D6f87gtX1l0Vp9VulzAS8hEXLhKvNDvz9AMG3MVdxCl3XhbV', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-06-30 23:06:52', '2020-08-20 06:28:28'),
(46, NULL, 'customer', 'Faroque', 'faroquehossain565@gmail.com', NULL, '$2y$10$t.O8TD/lTJDzWxaQ19Yj..f.CgOXaTWrhf2YBRbaTsN2hsNhq7BtS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-07 08:12:39', '2020-07-07 08:12:39'),
(47, NULL, 'customer', 'Kazi Hafizur Rahman', 'hafiz.truetzschler@gmail.com', NULL, '$2y$10$y9to42ll290YiX2GKuNEU.FujF7NPlSzLa7vzv4CjLN6GWPWHWuNy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-08 09:44:25', '2020-07-08 09:44:25'),
(48, NULL, 'customer', 'Dr. Ashis Saha', 'ashissaha26@yahoo.com', NULL, '$2y$10$eDvM890CIiY8ZAdeakw0COdA4iHS4SIfkKciRAmHw/.L24MgyUglS', 'qXcvVDi8sf5mnShMtTHUQ2Rx1E3CJyw8c1E1NQZ3qqYVpioUvNByNp9vAdl4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-08 10:59:35', '2020-07-08 10:59:35'),
(49, NULL, 'customer', 'N M Abdullah', 'adbullahujjal5@gmail.com', NULL, '$2y$10$Aa2Ozd9dHRHyWyPFd5vrKuBs35bs.AU4bAWVcKMviTdurK1euzYE2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-08 21:45:22', '2020-07-08 21:45:22'),
(51, NULL, 'customer', 'আহমেদ উল্লাহ তপু', 'tahmedullah@gmail.com', NULL, '$2y$10$sCKTXN9a.VsXH4ynBRYZNOA6ZRDkg9pT/g4jkR5Nh0LCFGsM3Zi.i', 'Flg6YuFXckJEwsjLSwnJP2M0ZzaQilaoqfk7ctdQCsXFOKlPuZLSgqYo2L2p', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-10 22:15:16', '2020-07-10 22:15:16'),
(52, NULL, 'customer', 'Junanjina ahmed', 'Jnjtrina@gmail.com', NULL, '$2y$10$tEYLtpP0EA3eb1HSvAsEl.JZ5hsn.p7s9p/GcNJvxY0BEiz9d5Xf6', 'X6p6rT5bxFuYxJk67Yo0hJjjWpm6jrUd0y3Bs5WQzaC1WYVtyaxkj2zSYZfv', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-11 15:54:58', '2020-07-11 15:54:58'),
(53, NULL, 'staff', 'মোঃ আঃ রহিম', 'ghorerbazarmdabdurrohim@gmail.com', NULL, '$2y$10$E.iyJLNsNjWaCAefl7uQgOlYSUFxqMCSTEiSblT3EF7uoIN3lWUya', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01842210999', 0.00, '2020-07-15 21:57:16', '2020-07-15 21:58:45'),
(54, NULL, 'customer', 'Md Nazmus Sakib', 'nsakib800720@gmail.com', NULL, '$2y$10$4sxgonMi8S18tMj8Cao9be2HxDBb7bZ1acuxQGJTrMis/jjWaJhyy', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-17 03:13:00', '2020-07-17 03:13:00'),
(55, NULL, 'customer', 'Justice AFM Abdur Rahman', 'justice.rahman@yahoo.com', NULL, '$2y$10$CwEtMUibr4AiiNLqjb41cuIhJoRyq5FIh9RdRMp66NqHE0io.8GaC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-17 08:17:42', '2020-07-17 08:17:42'),
(56, NULL, 'customer', 'Firoz', 'firoz_bir10@yahoo.com', NULL, '$2y$10$nEIlRPRyjmAgzqg9LzPRUeCsfdqb9Bzev/aZRtsEpxwTHG.w2kR0a', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-17 22:35:17', '2020-07-17 22:35:17'),
(57, NULL, 'customer', 'Zahid', 'zahidhasan04848@gmail.com', NULL, '$2y$10$zlC00Dv7UTqqRiWwYsW6/eF1xh9LBx8gFUenWW5LBUsNyW2Bza37O', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-18 00:10:02', '2020-07-18 00:10:02'),
(58, NULL, 'customer', 'Laxman kumar', 'laxmankumar1952@gmail.com', NULL, '$2y$10$WATL3UqYfmF7MNOrCFDuG.s8sP4p2fQOv954tr0eU2P5g381EEEL6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-18 10:02:21', '2020-07-18 10:02:21'),
(59, NULL, 'customer', 'Shoriful islam', 'Sagorshorif.chaldal@gmail.com', NULL, '$2y$10$2BPm7XyZDIKQJuFSe3eY0.d7beM1aLAVP64zZPg2pWaXYdYtqmsNW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-18 12:20:26', '2020-07-18 12:20:26'),
(60, NULL, 'customer', 'Jashimshanto', 'jashimshanto@gmail.com', NULL, '$2y$10$rPTDErslEIbIitFkwTfIMuM2L6rhm4p0jH60mswJJfqwVonTMECEa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-20 05:30:37', '2020-07-20 05:30:37'),
(61, NULL, 'customer', 'Iqbal Zoni', 'iqbalzoni24@gmail.com', NULL, '$2y$10$dDYCezLB9WLa5Y.o2q7sleIRqQnYHMDMvypxau05k.4xPXHp93SAK', 'FGFzv8iImdCQi43jMX2YiF6W6VS4o8e21Gt353CYl3edeA6giDImu5QiP2ly', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-21 14:21:09', '2020-07-21 14:21:09'),
(62, NULL, 'customer', 'Dulal Ahmed', 'dahmed664@gmail.com', NULL, '$2y$10$eyseSYRwj168LBGEF33e5.7.B6VeEpt04ej6hfCFn0nFijzqg5OJO', 'f3dnrXaFOJPyPC6vj3vfcHXdC64hWPz3QNqGdhQe97Mxf0tUgrkTmtt6SfpU', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-21 18:46:27', '2020-07-21 18:46:27'),
(63, NULL, 'customer', 'Wahid Miah', 'wahidmiah786@gmail.com', NULL, '$2y$10$qZmnLGhkfZYv2NxLgCTYXeIYsKSev9/jmaVTc.pcxOhSs/LBhyeuS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-25 11:19:00', '2020-07-25 11:19:00'),
(64, NULL, 'customer', 'Wahid Miah', 'badshatelecom01@gmail.com', NULL, '$2y$10$GyPhK9E/k.NmEVriRoyane5eC/uGBSq3/P3GfpKYtY.EujpHlhPjK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-25 11:21:44', '2020-07-25 11:21:44'),
(65, NULL, 'customer', 'Shah Alam Badsha', 'shahalambadsha@yahoo.com', NULL, '$2y$10$VgsHdHfcAld.GLaH9xL1nuhW9a1W6DyJxsgJucgydtPmQUzVu7v/.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-25 17:00:24', '2020-07-25 17:00:24'),
(66, NULL, 'customer', 'Shah Alam Badsha', 'shahalambadsha@gmail.com', NULL, '$2y$10$2B060nX29hgB76XNDSenU.N/f/6xXnR/l5ZDBZ1mM4XT4afTbcjWm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-25 17:30:52', '2020-07-25 17:30:52'),
(67, NULL, 'customer', 'Shohana', 's_zaman06@yahoo.com', NULL, '$2y$10$Iu9kRkxJNaxIQG.aY5gIPuppYFE9MZSZbg.Ouzlxgrr96dybaXb5O', 'Sw1pRnSu14FSFrkr6myBk4kvi8SCd8WCSAH7LkvCd5quZ3Yz7xvSXmtWdeQB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-29 00:58:57', '2020-07-29 00:58:57'),
(68, NULL, 'customer', 'MIR ABUBAKOR SIDDIQUE', 'mirabubakor@yahoo.com', NULL, '$2y$10$U1y/0fup8xyIouJcLoaAv.Vxbqm1o14TT14.wUMEd.zlSCo.MeBl2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-07-29 12:43:22', '2020-07-29 12:43:22'),
(69, NULL, 'customer', 'Md Moynul Islam', 'mimora07@gmail.com', NULL, '$2y$10$Ro2KfXvqwI6BXpJ4Bajik.XjGcGKhSMRMChWIX17QtO//BNAB9Pwm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-04 10:32:20', '2020-08-04 10:32:20'),
(70, NULL, 'customer', 'Engr Nurul Islam Miah', 'csmuna@gmail.com', NULL, '$2y$10$cPoWmCnNep5E0oIqY2YIMOPTEqCoDdn27yleCFa1MJ3NPljchTzNS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-09 08:41:44', '2020-08-09 08:41:44'),
(71, NULL, 'customer', 'E HOSSEN', 'saymeonline@gmail.com', NULL, '$2y$10$3bQ1O4mrvnqxgxqMwx2evusNL3nXO5cjF29f8wKQsuNVsz6JM1lcO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-09 10:29:44', '2020-08-09 10:29:44'),
(72, NULL, 'customer', 'Kamrul Hassan', 'kamrulhassann099@gmail.com', '2020-08-11 10:42:21', '$2y$10$wzBOiIl3UHox8EsGPrFYRO4z/Rmd1.YkjIJdY2yW73qgB6MeQf5qC', 'rrJ7fCXRdj007uCG7PRTsW9tqcRxpcOE3aHkEahDzyqorbA3PC9mC9xKE5Go', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-11 10:40:49', '2020-08-11 10:42:21'),
(73, NULL, 'customer', 'Md Ibrahim', '017712690@gmail.com', NULL, '$2y$10$lhs6/1lKdgpiQ71FxiH9TeG68TiK8yTyKu2q9G8hmvLG0gq0ijLTe', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-12 10:47:17', '2020-08-12 10:47:17'),
(74, NULL, 'customer', 'Md Ibrahim', 'ibra017712690@gmail.com', '2020-08-12 11:23:38', '$2y$10$EPM3AbDdhjobiTCw.LntCOeSJqAxEuyI8OnapJlryAox9Fv.M12FC', NULL, NULL, NULL, 'Demra Sarulia bazar wasaroad shotorupa teilars', 'BD', 'Dhaka', NULL, '01771269022', 0.00, '2020-08-12 11:22:16', '2020-08-12 11:27:24'),
(75, NULL, 'customer', 'Sumi', 'rafiaaysha47@gmail.com', NULL, '$2y$10$AZJaSj9XT4EvKQ5wkRSAgOp.sLOcuUk2wRoFySzVTquqY3.ywvVHW', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-13 02:43:35', '2020-08-13 02:43:35'),
(76, NULL, 'customer', 'Mahadi Hasan bappa', 'a.mahadihasanbappa2013@gmail.com', NULL, '$2y$10$Se.cek8FnLq9vX2/sXQi.u2WI3flE2F3/0g0DFNMRUvq7SC07AXgG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-13 08:02:18', '2020-08-13 08:02:18'),
(77, NULL, 'customer', 'Md tariful islam', 'adortarif.itt@gmail.com', NULL, '$2y$10$fdVnncpg7gNUC4ZLeTlO0.XT4jrNow8q6aUmuH3oXHqt3OFtlGuk2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-14 08:21:03', '2020-08-14 08:21:03'),
(78, NULL, 'customer', 'শরিফ খান', 'sarifkhan7285@gmail.com', NULL, '$2y$10$uWUhKTRjt3ANx12hUtdRpeEfLVFWFZHhHZdi8PTlQYe2ym94J/oBG', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-14 14:32:32', '2020-08-14 14:32:32'),
(79, NULL, 'customer', 'Tanvir Mahmud', 'tanvirinfo483@gmail.com', '2020-08-14 22:44:57', '$2y$10$bMb5rNTsOJQ05.Hh5Uku/eWfQAYZGPVYb7b.fAOyuHLROempBCwZ2', '6dzYZjAGKD0abuoSUc5cvdjVAB7UehywDkPSN8SJTTQBcftJo2GUDcmMp3SM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-14 22:42:21', '2020-08-14 22:44:57'),
(80, NULL, 'customer', 'Mashiur Rahman', 'mashiur1201@gmail.com', NULL, '$2y$10$CKe3DSRgL3IyvhpC/vFzSeOnDM4iRcBfdbbZu8v7NqCbHUDLGaiW.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-18 08:14:38', '2020-08-18 08:14:38'),
(81, NULL, 'customer', 'Russell', 'home.raf2000@gmail.com', NULL, '$2y$10$v0GJgf7cOQqDWUnJ9MYrBOBiBGZHR2ynzo.EnQoEZQKSnetE1AnPO', 'tb5VvJG7G0VlMiRQrh6hmchbhrMBIzYV98pCmopmyScXVcgnff3eB0uuNLH5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-18 08:32:55', '2020-08-18 08:36:53'),
(82, NULL, 'customer', 'Raihan Mostafiz', 'mr.raihan.uap@gmail.com', '2020-08-18 11:53:15', '$2y$10$RJJr2Y3NbnzArthvMYAp6ekuYw6W7T6LlBykZTrVYX8IKHMYXrZYm', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-18 11:52:28', '2020-08-18 11:53:15'),
(83, NULL, 'seller', 'OldStar', 'oldstarbd@gmail.com', NULL, '$2y$10$nh2xEbwkEVidjqOKmV0hyea68Du1yOLj04uazA12pga.AsS.32ef6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-19 08:37:46', '2020-08-19 08:37:46'),
(84, NULL, 'customer', 'Ekilis Habib', 'healerhabib26@gmail.com', NULL, '$2y$10$F3527rc2d5sYoAiDbrPs2e/d7yVwCwZdR2IoFGXVsr3Ykd7gz1JNu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-20 01:06:20', '2020-08-20 01:06:20'),
(85, NULL, 'customer', 'Jahid Hasan', 'jahid.hasan.babu@gmail.com', NULL, '$2y$10$Fc81tOHVu/iI34emyv.yF.r9j7KiTvMvQNx23V66Z6uM8gVa2QH5m', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-20 03:06:26', '2020-08-20 03:06:26'),
(86, NULL, 'customer', 'Mariam meem', 'mariamrahmanmeem@gmail.com', NULL, '$2y$10$/nLxtn./DXFFSPzPZRnMN.ZMMBS7jNnlcAyekcFAP2rp.Jyuo9Ag2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-20 07:56:11', '2020-08-20 07:56:11'),
(87, NULL, 'customer', 'Shoyeb Ahammed', 'shoyeb2700@gmail.com', NULL, '$2y$10$bO4NgeQTfrOmX2/qoJJdquFxRlXdFvgL75EBhrAF19bucWoAC.2hK', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-20 10:43:10', '2020-08-20 10:43:10'),
(88, NULL, 'customer', 'Alamin', 'owalifoysal7890@gmail.com', NULL, '$2y$10$SmoVgXiXc4GPA1MXk1AT5uoFQMXRLbC9ObSCAyisOnwT4qgUm9Sde', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-20 14:17:50', '2020-08-20 14:17:50'),
(89, NULL, 'customer', 'Eugenewromi', 'sor0kinmixail@yandex.com', NULL, '$2y$10$K/.RQvJ1JpHfcIo7RygN.eGFIa6VM3ZNayDV7XZJWHJ/xFsvSaIou', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-08-27 13:06:46', '2020-08-27 13:06:46'),
(95, NULL, 'seller', 'salim', 'mail@ghorerbazar.com', '2020-09-09 17:27:53', '$2y$10$owJOFj6HHJfEJ5qZN/3QmuMWTfOnb8B3d1N0PWzJ1UN.IfzmFFDx6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01818503905', 0.00, '2020-09-09 17:24:55', '2020-09-09 17:41:57'),
(97, NULL, 'seller', 'Rahim bekary', 'info@gmail.com', NULL, '$2y$10$FGKjluHd9MZbkQkMAfikbOZiH2PQHn.drNqP4j4zkBxozgHXPl5XO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.00, '2020-09-24 13:50:32', '2020-09-24 13:50:32'),
(98, NULL, 'admin', 'Md abu Based', 'abubased@outlook.com', '2020-08-06 05:00:00', '$2y$10$RCHsbnNcWk5gmOMxqL1SAuqDrkULhNThCLGe8EVavizzh8MZAB4Va', 's9s927QrycdACWuh44rFuLcDmoEDGfT9ftsPuQUTQTgJhGzLG6sm3tyUdiOo', NULL, NULL, NULL, NULL, NULL, NULL, '01797944174', 132.00, '2020-09-26 16:24:32', '2021-05-18 12:35:53'),
(100, NULL, 'seller', 'Md abu Based', 'abubased@seller.com', '2020-10-01 05:00:00', '$2y$10$RCHsbnNcWk5gmOMxqL1SAuqDrkULhNThCLGe8EVavizzh8MZAB4Va', 'ZI48kZcIVSbkFsV3WQzrSeLSzC7NNfDB7TnAlIUDqS1hGQXp6PPOL64TzDaP', NULL, NULL, NULL, NULL, NULL, NULL, '01797944114', 565.00, '2020-10-07 15:17:07', '2020-10-27 01:59:39'),
(101, NULL, 'customer', 'Md abu Based', 'abubased@customer.com', '2020-10-01 05:00:00', '$2y$10$RCHsbnNcWk5gmOMxqL1SAuqDrkULhNThCLGe8EVavizzh8MZAB4Va', '0qQ2Bb0AtTDeZWpcDn9FkdbhaQZmdK97epQiiXdHK1MVoXeDduQn7tysxX2O', NULL, NULL, NULL, NULL, NULL, NULL, '01797944114', 210.00, '2020-10-07 15:17:07', '2021-06-07 14:26:40');

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

DROP TABLE IF EXISTS `wallets`;
CREATE TABLE IF NOT EXISTS `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_details` longtext COLLATE utf8_unicode_ci,
  `number` varchar(12) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TnxId` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wallets`
--

INSERT INTO `wallets` (`id`, `user_id`, `amount`, `payment_method`, `payment_details`, `number`, `TnxId`, `status`, `created_at`, `updated_at`) VALUES
(11, 98, 132.00, 'cash', NULL, '0123456789', 0, 0, '2020-10-04 18:04:47', '2021-05-18 12:35:53'),
(12, 101, 160.00, 'bkash', NULL, '0123456789', 0, 0, '2021-05-18 12:31:36', '2021-05-18 12:34:21'),
(13, 101, 80.00, 'rocket', NULL, '0123456789', 0, 0, '2021-05-18 12:40:18', '2021-05-18 12:40:18');

-- --------------------------------------------------------

--
-- Table structure for table `wishlists`
--

DROP TABLE IF EXISTS `wishlists`;
CREATE TABLE IF NOT EXISTS `wishlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
